from django.shortcuts import render
from django.contrib.auth import authenticate, login, logout, get_user_model
from django.utils import timezone
from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver
from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from rest_framework.authentication import TokenAuthentication
from rest_framework.status import (HTTP_201_CREATED,
                                   HTTP_200_OK,
                                   HTTP_400_BAD_REQUEST, HTTP_401_UNAUTHORIZED,
                                   HTTP_404_NOT_FOUND, HTTP_409_CONFLICT)
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework import viewsets
from rest_framework import permissions, status, generics, authentication
from rest_framework.decorators import detail_route, \
                    list_route, permission_classes, api_view
from rest_framework.decorators import action
from rest_framework.authtoken.models import Token
from user_account.models import User, UserManager
from paylock.permissions import IsSuperUserPermission
from paylock.paginations import StandardResultsSetPagination
from transaction_type.models import TransactionType
from payment_method.models import PaymentMethod
from user_account.models import User, UserManager
from transaction.models import Transaction
from .serializers import  GeneralNotificationSerializer
from dashboard_notification.models import Notification
from request_funds_transfer.models import RequestFundsTransfer


class NotificationsViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated, )
    serializer_class = GeneralNotificationSerializer
    queryset = Notification.objects.all().order_by('-id')

    @action(methods=('GET',), detail=False, url_path='dashboard', permission_classes=(IsAuthenticated,))
    def transaction_invitation_notifications(self, request, *args, **kwargs):
        """ Notify collaborator or contractor on dashboard that he/she has been added to a transaction.
        
        """
        user = request.user
        notification = Notification.objects.filter(receiver=user,
            mark_as_read=False,
            )
        notification = self.serializer_class(notification, many=True)
        return Response({'status_code': HTTP_200_OK,
            'message': 'Notifications',
            'result': {
                'notifications': notification.data,
                } 
            })
    
    @action(methods=('POST',), detail=False, url_path='mark-as-read', 
        permission_classes=(IsAuthenticated,))
    def mark_as_read(self, request, *args, **kwargs):
        notification_id = request.data.get('notification_id')
        try:
            notification = Notification.objects.get(pk=notification_id)
            notification.mark_as_read = True
            notification.save()
            return Response({
                'status_code': status.HTTP_200_OK,
                'message': 'Notification has been marked as read',
                }, status = status.HTTP_200_OK)
        except Notification.DoesNotExist:
             return Response({
                'status_code': status.HTTP_404_NOT_FOUND,
                'message': 'Notification object not found.',
                }, status = status.HTTP_404_NOT_FOUND)
        
    
    @action(methods=('GET',), detail=False, url_path='list', 
        permission_classes=(IsAuthenticated,))
    def notification_list(self, request, pk=None, *args, **kwargs):
        user = request.user
        paginator= StandardResultsSetPagination()
        notifications = Notification.objects.filter(
            receiver=user
            ).order_by('-id')
        result_page = paginator.paginate_queryset(notifications, request)
        serialized_notification = self.serializer_class(result_page, many=True)
        paginated_notifications = paginator.get_paginated_response(serialized_notification.data)
        return Response({
            'status_code': status.HTTP_200_OK,
            'message': 'List of notifications',
            'result': {
                'data': paginated_notifications.data
            }
            }, status = status.HTTP_200_OK)
        