from django.test import TestCase, RequestFactory, Client
from rest_framework.test import APIRequestFactory
from rest_framework.test import APIClient
from rest_framework.test import force_authenticate
from django.contrib.auth import get_user_model
from django.utils import timezone
from rest_framework.authtoken.models import Token
import json
from payment_method.models import PaymentMethod
from transaction_type.models import TransactionType
from transaction.models import Transaction
from user_profile.models import UserProfile
from dashboard_notification.models import TransactionNotification
from request_funds_transfer.models import RequestFundsTransfer


User = get_user_model()

class TransactionInvitationTest(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.credentials = {
            'email': 'kenmartey89@gmail.com',
            'phone_number': '00233246424340',
            'password': 'ceremony',
            }
        self.payment_method_input = {
            'payment_type':'Mobile Money',
            'created_date': timezone.now()
        }
        self.transaction_type_input = {
            'transaction_type': 'Product',
            'created_date': timezone.now()
        }
        self.user = User.objects.create_superuser(**self.credentials)
        self.payment_method = PaymentMethod.objects.create(**self.payment_method_input, 
        user=self.user)
        self.transaction_type = TransactionType.objects.create(**self.transaction_type_input, 
        user=self.user)
        self.user_profile = {
            'first_name': 'Kennedy',
            'last_name': 'Anyinatoe',
            'date_of_birth': '1989-07-09',
            'current_address': 'East Legon',
            'profile_image': ''

        }
        self.user_profile = UserProfile.objects.create(**self.user_profile, 
            user=self.user)
        self.transaction_info = {
        'name': 'Nike shoes',
        'transaction_type': self.transaction_type,
        'description': 'A pair of Nike shoes from Jumia',
        'payment_method': self.payment_method,
        'amount': '900',
        'markup': '0.00',
        'contractor': self.user,
        'contractor_agreement': False,
        'project_owner_agreement': True,
        'created_date': timezone.now()
        }
        self.transaction = Transaction.objects.create(**self.transaction_info, user=self.user)
        self.approve_transfer = {
            'transaction_id': self.transaction.id,
            'transaction': self.transaction,
            'published_date': timezone.now()
        }
        self.transfer_request = RequestFundsTransfer.objects.create(**self.approve_transfer)
        

    def test_send_transaction_invitation_notification_on_dashboard(self):
        client = APIClient()
        headers = {
            'HTTP_AUTHORIZATION': 'Token {}'.format(
                self.user.auth_token)
        }
        transaction = Transaction.objects.create(**self.transaction_info, user=self.user)
        create_transaction_notification= TransactionNotification.objects.create(transaction=transaction, 
        notification_type='Transaction invitation', published_date=timezone.now()
        )
        print ('===================CREATE TRANSACTION NOTIFICATION TEST=========================')
        print ('>>> >>> >>> /CREATE TRANSACTION NOTIFICATION TEST/ content', create_transaction_notification)
        # self.assertEqual(create_transaction_notification.status_code, 200)
        print ('===================END OF TRANSACTION NOTIFICATION TEST========================')
        print ('      ')
    
    def test_send_funds_request_notification_on_dashboard(self):
        client = APIClient()
        headers = {
            'HTTP_AUTHORIZATION': 'Token {}'.format(
                self.user.auth_token)
        }
        funds_request_notification= TransactionNotification.objects.create(funds_request=self.transfer_request, 
        notification_type='Funds request', published_date=timezone.now()
        )
        print ('===================FUNDS REQUEST NOTIFICATION TEST=========================')
        print ('>>> >>> >>> /FUNDS REQUEST NOTIFICATION TEST/ content', funds_request_notification)
        print ('===================FUNDS REQUEST NOTIFICATION TEST========================')
        print ('      ')

        