from django.apps import AppConfig


class DashboardNotificationConfig(AppConfig):
    name = 'dashboard_notification'
