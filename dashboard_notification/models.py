from django.db import models
from django.conf import settings
from django.utils import timezone
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType

# Create your models here.
AUTH_USER_MODEL = getattr(settings, 'AUTH_USER_MODEL', 'auth.User')

class Notification(models.Model):
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')
    event_type = models.CharField(max_length=250, default="created")
    sender = models.ForeignKey(
        AUTH_USER_MODEL, related_name='notifications',
        on_delete=models.CASCADE
        )
    receiver = models.ForeignKey(
        AUTH_USER_MODEL, related_name='receiver_notifications',
        on_delete=models.CASCADE
        )
    notification_type = models.CharField(max_length=100, blank=True)
    mark_as_read = models.BooleanField(default=False)
    published_date = models.DateTimeField(blank=True, null=True)
    created_date = models.DateTimeField(default=timezone.now)
    modified = models.DateTimeField(auto_now=True)

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __unicode__(self):
        return '%s' % (self.notification_type)

    def __str__(self):
        return '%s' % (self.notification_type)
