from rest_framework import serializers
from transaction.serializers import TransactionSerializer
from transaction_invitation.serializers import TransactionInvitationSerializer
from request_funds_transfer.serializers import RequestFundsTransferSerializer
from wallet.serializers import UserWalletSerializer
from reverse_transaction.serializers import ReverseTransactionSerializer
from dispute.serializers import DisputeSerializer
from .models import Notification
from transaction.models import Transaction
from transaction_invitation.models import TransactionInvitation
from request_funds_transfer.models import RequestFundsTransfer
from wallet.models import UserWallet
from reverse_transaction.models import ReverseTransaction
from dispute.models import Dispute
from generic_relations.relations import GenericRelatedField

class GeneralNotificationSerializer(serializers.ModelSerializer):
    """
        A `Notification` serializer with a `GenericRelatedField` mapping all possible
        models to their respective serializers.

    """
    content_object = GenericRelatedField({
        Transaction: TransactionSerializer(),
        TransactionInvitation: TransactionInvitationSerializer(),
        RequestFundsTransfer: RequestFundsTransferSerializer(),
        ReverseTransaction: ReverseTransactionSerializer(),
        Dispute: DisputeSerializer()

    })

    class Meta:
        model = Notification
        fields = ('id','notification_type', 'mark_as_read', 'content_object',
        'content_type', 'object_id','published_date', 'created_date', 'modified')

