from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver
from django.utils import timezone
from transaction.models import Transaction
from dashboard_notification.models import Notification
from request_funds_transfer.models import RequestFundsTransfer
from transaction_invitation.models import TransactionInvitation
from reverse_transaction.models import ReverseTransaction

from dispute.models import Dispute

# Create your signals here.
@receiver(post_save, sender=Transaction)
def create_transaction_notification(sender, instance, created=False, **kwargs):
    """
    
    """
    if created:
        transaction = sender.objects.get(pk=instance.pk)
        notification = Notification.objects.create(
            content_object = transaction,
            notification_type = 'Transaction invitation',
            published_date= timezone.now(),
            sender=transaction.user,
            receiver=transaction.contractor
        )

@receiver(post_save, sender=TransactionInvitation)
def accept_transaction_invitation_notification(sender, instance, created=True, **kwargs):
    """
        Notification is sent to transaction owners when a transaction is either accepted 
        or rejected by collaborators.

    """
    invitation = sender.objects.get(pk=instance.pk)
    if invitation.status == 'approved':
        invitation_notification = Notification.objects.create(
            content_object = invitation,
            notification_type = 'Accepted Transaction invitation',
            published_date= timezone.now(),
            sender=invitation.acceptor,
            receiver=invitation.transaction.user
        )
    elif invitation.status == 'decline':
        invitation_notification = Notification.objects.create(
            content_object = invitation,
            notification_type = 'Declined Transaction invitation',
            published_date= timezone.now(),
            sender=invitation.acceptor,
            receiver=invitation.transaction.user
        )

@receiver(post_save, sender=RequestFundsTransfer)
def create_funds_request_notitification(sender, instance, created=False, **kwargs):
    """
        Notification is created and sent to transaction owner when a collaborator 
        requests for funds at any point of the transaction.
    """
    if created and instance.transaction is not None:
        funds_request = sender.objects.get(pk=instance.pk)
        funds_request_notification = Notification.objects.create(
            content_object = funds_request,
            notification_type = 'Funds request invitation',
            published_date= timezone.now(),
            sender=funds_request.transaction.contractor, # the contractor
            receiver=funds_request.transaction.user # the transaction owner. 
        )

@receiver(post_save, sender=RequestFundsTransfer)
def funds_request_action_notification(sender, instance, created=True, **kwargs):
    """
        An action taken by a transaction owner when funds are requested by contractor. 
        The request is either approved or rejected.
        Notification is sent to collaborator based on the action.
    """
    funds_request = sender.objects.get(pk=instance.pk)
    if funds_request.status == 'approved':
        funds_request_notification = Notification.objects.create(
            content_object = funds_request,
            notification_type = 'Funds request Approved',
            published_date= timezone.now(),
            sender=funds_request.transaction.user, # the contractor
            receiver=funds_request.transaction.contractor # the transaction owner. 
        )
    elif funds_request.status == 'decline':
        funds_request_notification = Notification.objects.create(
            content_object = funds_request,
            notification_type = 'Funds request Rejected',
            published_date= timezone.now(),
            sender=funds_request.transaction.user, # the contractor
            receiver=funds_request.transaction.contractor # the transaction owner. 
        )

@receiver(post_save, sender=ReverseTransaction)
def reverse_transaction_with_approval(sender, instance, created=False, *args, **kwargs):
    """
        A notification is sent to collaborator when a transaction owner decides to terminate 
        the transaction by reversing the transaction.
    """
    if created:
        reverse_obj = sender.objects.get(pk=instance.pk)
        reverse_notification =  Notification.objects.create(
            content_object = reverse_obj,
            notification_type = 'Reverse transaction',
            published_date= timezone.now(),
            sender=reverse_obj.transaction.user, # the contractor
            receiver=reverse_obj.transaction.contractor # the transaction owner. 
        )

@receiver(post_save, sender=ReverseTransaction)
def approve_and_reject_reverse_transaction(sender, instance, created=True, *args, **kwargs):
    """
        Reverse transaction notification sent to user When user approves or rejects the 
        reverse created by the transaction owner.
    """
    reverse_obj = sender.objects.get(pk=instance.pk)
    if reverse_obj.status == 'approved':
        reverse_notification =  Notification.objects.create(
            content_object = reverse_obj,
            notification_type = 'Reverse transaction approved',
            published_date= timezone.now(),
            sender=reverse_obj.transaction.contractor, # the contractor
            receiver=reverse_obj.transaction.user # the transaction owner. 
        )
    elif reverse_obj.status == 'decline':
        reverse_notification =  Notification.objects.create(
            content_object = reverse_obj,
            notification_type = 'Reverse transaction declined',
            published_date= timezone.now(),
            sender=reverse_obj.transaction.contractor, # the contractor
            receiver=reverse_obj.transaction.user # the transaction owner. 
        )


# this should go under the task
@receiver(post_save, sender=Dispute)
def create_dispute_notification_by_user(sender, instance, created=False, *args, **kwargs):
    """ The dispute notification is created and sent to collaborator when disptute is raised
        on a transaction.

    """
    if created:
        dispute = sender.objects.get(pk=instance.pk)
        if dispute.user == dispute.transaction.user:
            dispute_notification = Notification.objects.create(
                content_object = dispute,
                notification_type = 'Transaction dispute raised',
                published_date= timezone.now(),
                sender=dispute.transaction.user, # the user
                receiver=dispute.transaction.contractor # the contractor 
            )
        else:
            dispute_notification = Notification.objects.create(
                content_object = dispute,
                notification_type = 'Transaction dispute raised',
                published_date= timezone.now(),
                sender=dispute.transaction.contractor, # the contractor
                receiver=dispute.transaction.user # the user 
            )
    
# put it in the background job.
@receiver(post_save, sender=Dispute)
def track_dispute_stage(sender, instance, created=True, *args, **kwargs):
    """ Dispute notification is sent to user or collaborator when dispute
        stage changes.

    """
    dispute = sender.objects.get(pk=instance.pk)
    if dispute.stage == 'in_progress':
        if dispute.user==dispute.transaction.user:
            dispute_notification =  Notification.objects.create(
                content_object = dispute,
                notification_type = 'Dispute in process',
                published_date= timezone.now(),
                sender=dispute.transaction.user, # the user or transaction owner
                receiver=dispute.transaction.contractor # the contractor. 
            )
        else:
            dispute_notification =  Notification.objects.create(
                content_object = dispute,
                notification_type = 'Dispute in process',
                published_date= timezone.now(),
                sender=dispute.transaction.contractor, # the user or transaction owner
                receiver=dispute.transaction.user # the contractor. 
            )
    elif dispute.stage == 'resolved':
        if dispute.user == dispute.transaction.user:
            dispute_notification =  Notification.objects.create(
                content_object = dispute,
                notification_type = 'Dispute resolved',
                published_date= timezone.now(),
                sender=dispute.transaction.user, # the contractor
                receiver=dispute.transaction.contractor # the transaction owner. 
            )
        else:
            dispute_notification =  Notification.objects.create(
                content_object = dispute,
                notification_type = 'Dispute resolved',
                published_date= timezone.now(),
                sender=dispute.transaction.contractor, # the user or transaction owner
                receiver=dispute.transaction.user # the contractor. 
            )
