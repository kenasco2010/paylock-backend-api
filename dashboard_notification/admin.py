from django.contrib import admin
from .models import Notification
# Register your models here.
class NotificationAdmin(admin.ModelAdmin):
    list_display = ('notification_type', 'created_date',
        'modified')

admin.site.register(Notification, NotificationAdmin)