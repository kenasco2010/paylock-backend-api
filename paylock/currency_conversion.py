from django.conf import settings
import requests

def convert_currency(currency, amount):
	response = requests.get(settings.CURRENCY_LAYER_API, '&from=' + str(currency) +'&to=USD&amount=' + str(amount))
	rate = response.json()
	try:
		if rate['result']:
			usd_amount = rate['result']
	except KeyError:
		usd_amount = 0
	return usd_amount