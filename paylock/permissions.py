from rest_framework import permissions

class IsSuperUserPermission(permissions.BasePermission):
    """This gives priority to super user class"""
    message = 'You are not authorized to perform this operation.'
    
    def has_object_permission(self, request, view, obj):
        if request.user.is_superuser:
            return obj.user
        
    def has_permission(self, request, view):
        return request.user.is_superuser
            
        

class IsOwner(permissions.BasePermission):
    """This checks if the object belongs to the loggedin user"""
    def has_object_permission(self, request, view, obj):
        return obj.user == request.user