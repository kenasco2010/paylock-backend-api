"""paylock URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf.urls import include
from django.urls import path
from django.conf.urls.static import static
from django.conf import settings
urlpatterns = [
    path('admin/', admin.site.urls),
    path(r'api/', include('user_account.urls')),
    path(r'api/', include('user_profile.urls')),
    path(r'api/', include('payment_method.urls')),
    path(r'api/', include('payment.urls')),
    path(r'api/', include('transaction_type.urls')),
    path(r'api/', include('transaction.urls')),
    path(r'api/', include('transaction_invitation.urls')),
    path(r'api/', include('request_funds_transfer.urls')),
    path(r'api/', include('identity_verification.urls')),
    path(r'api/', include('dashboard_notification.urls')),
    path(r'api/', include('reverse_transaction.urls')),
    path(r'api/', include('wallet.urls')),
    path(r'api/', include('withdraw.urls')),
    path(r'api/', include('user_account_setting.urls')),
    path(r'api/', include('dispute.urls')),
    path(r'api/', include('legal_firm.urls')),
    path(r'api/', include('paylock_tips.urls')),
    path(r'api/', include('milestone.urls')),
    path(r'api/', include('import_export.urls')),
    path(r'api/', include('wallet_direct.urls')),
    path(r'api/', include('api_integration.urls')),


] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
