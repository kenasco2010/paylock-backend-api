import urllib
import requests
from django.conf import settings

import boto3

# Create an SNS client
client = boto3.client(
    "sns",
    aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
    aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY,
    region_name="us-west-2"
)

def send_text_message(message, url, params):
    bundleUlr = url + urllib.parse.urlencode(params)
    requests.get(bundleUlr)

def send_text_message_about_transaction(instance):
    """Send text message to Collaborator when a user 
        creates a transaction and adds him/her.
    
    """
    message = 'You have been added to this transaction: {}. ' \
    'The amount paid in your name is ${} by  {} {}'.format(
        instance.name, instance.amount, instance.user.user_profile.first_name,
        instance.user.user_profile.last_name)
    text_message = client.publish(
        PhoneNumber=instance.contractor.phone_number,
        Message=message,
        MessageAttributes = {
                   'AWS.SNS.SMS.SMSType': {
                       'DataType': 'String',
                       'StringValue': 'Transactional'  # or 'Promotional'
                   }
               }
    )
    

# def welcome_user_text_message(instance):
#     """
#         Welcome text message sent to user 
#         when user joins PayLock
#     """
#     message = "Welcome to PayLock! "\
#             "PayLock helps secure your funds during a transaction. " \
#             "Kindly complete your profile and enjoy our services. You can " \
#             "send us an email on support@paylock.io for any assistance. " \
#             "Thank you."   
#     text_message = client.publish(
#         PhoneNumber=instance.phone_number,
#         Message=message
#     )

def update_phone_number_text_message(user):
    """
        Text message a user receives when 
        he/she updates his contact on PayLock
    """
    message = "Hello, your phone number has been updated successfully on PayLock. " \
            "If it's not you, kindly change your password to prevent fraud in your account. " \
            "Thank you."   
    text_message = client.publish(
        PhoneNumber=user.phone_number,
        Message=message
    )

def update_user_on_phone_number_change_attempt(user):
    """
        Notify a user when someone attempts to change 
        his/her phone number.
    """
    message = "Hello, someone made an attempt to change your phone number on PayLock. " \
            "Kindly login to your account " \
            "and change your password to prevent fraud in your account." \
            "Thank you."   
    text_message = client.publish(
        PhoneNumber=user.phone_number,
        Message=message,
        MessageAttributes = {
                   'AWS.SNS.SMS.SMSType': {
                       'DataType': 'String',
                       'StringValue': 'Transactional'  # or 'Promotional'
                   }
               }
    )

def verify_phonenumber_on_paylock_registration(user, randomtoken ):
    # if created:
    """
        Send code verification token to user to enter before being able to login
    """
    text_message = client.publish(
        PhoneNumber=user.phone_number,
        Message="Thanks for joining PayLock! " \
        "Input your verification token: {} to verify your account. "
        "You can only access PayLock Dashboard when you verify with this token.".format(randomtoken),
        MessageAttributes = {
                   'AWS.SNS.SMS.SMSType': {
                       'DataType': 'String',
                       'StringValue': 'Transactional'  # or 'Promotional'
                   }
               }
    )

def phone_number_verification_after_invitation(user, randomtoken ):
    # if created:
    """
        Send code verification token to collaborator when not found 
        on PayLock and it's invited to join a transaction.
    """
    text_message = client.publish(
        PhoneNumber=user.phone_number,
        Message="Thanks for joining PayLock! " \
        "Input your verification token: {} to verify your account. "
        "You can only access PayLock Dashboard when you verify with this token.".format(randomtoken),
        MessageAttributes = {
                   'AWS.SNS.SMS.SMSType': {
                       'DataType': 'String',
                       'StringValue': 'Transactional'  # or 'Promotional'
                   }
               }
    )

def resend_phone_num_verification_token(user, token):
    """
        Resend phone verification token to user
    """
    text_message = client.publish(
        PhoneNumber=user.phone_number,
        Message="Your verification token: {}".format(token),
        MessageAttributes = {
                   'AWS.SNS.SMS.SMSType': {
                       'DataType': 'String',
                       'StringValue': 'Transactional'  # or 'Promotional'
                   }
               }
    )

# API TRANSACTIONS TEXT MESSAGES
# Send link to sellers phone number to state if Item is delievered and needs his money.
def send_funds_request_text_message_link_to_seller(buyer_full_name, user_phone_number, transaction_id ):
    """
        This function send a text message to the seller of the product/Item to 
        respond if the item is delivered to the buyer so that funds can be released
    """
    text_message = client.publish(
        PhoneNumber=user_phone_number,
        Message="A buyer, {} has paid for an item/service on your behalf into an escrow account on PayLock. "
        "Click on the link below if you have delivered the item to complete the transaction. "
        "{}webview/delivered/{}".format(buyer_full_name, settings.DOMAIN_URL, transaction_id),
        MessageAttributes = {
                   'AWS.SNS.SMS.SMSType': {
                       'DataType': 'String',
                       'StringValue': 'Transactional'  # or 'Promotional'
                   }
               }
    )

# Send link to buyer phone number to respond if item has been delivered
def send_release_funds_text_message_link_to_buyer(transaction_name, user_phone_number, transaction_id):
    # if created:
    """
        This function send a text message to the buyer of the product/Item to 
        respond if the item has been delivered.
    """
    print('Text message function passed in there.', user_phone_number)
    text_message = client.publish(
        PhoneNumber=user_phone_number,
        Message="The seller requests funds from the escrow account for your "
        "purchase of {}. Click on this link to complete the transaction "
        "{}webview/release-funds/{}".format(
            transaction_name, settings.DOMAIN_URL, transaction_id),
        MessageAttributes = {
                   'AWS.SNS.SMS.SMSType': {
                       'DataType': 'String',
                       'StringValue': 'Transactional'  # or 'Promotional'
                   }
               }
    )