INVITATION_STATUS = (
    ("pending", "Pending"),
    ("approved", "Approved"),
    ("decline", "Decline")
)

APPROVAL_STATUS = (
    ("pending", "Pending"),
    ("approved", "Approved"),
    ("declined", "Declined")
)
TRANSACTION_STAGE = (
    ("initialized", "Initialized"),
    ("ongoing", "Ongoing"),
    ("completed", "Completed"),
    ("reversed", "Reversed"),
    ("reversing", "Reversing"),
    ("canceled", "Canceled"),
    ("in_dispute", "In dispute"),
    ("dispute_resolved", "Dispute resolved"),
    ("dispute", "Dispute")
)
FREELANCE_PROJECT_TYPE = (
    ('one_time', 'One time project'),
    ('milestone', 'Milestone project')
)
TRADE_TYPE = (
    ('import', 'Import'),
    ('export', 'Export')
)
TRANSACTION_ROLE = (
    ('owner', 'Owner'),
    ('collaborator', 'Collaborator')
)
ESCROW_FEE_PAYMENT = (
    ('owner', 'Owner'),
    ('collaborator', 'Collaborator'),
    ('both', 'Both')
)
DISPUTE_STAGE = (
    ('pending', 'pending'),
    ('in_progress', 'In progress'),
    ('resolved', 'Resolved')
)
ID_TYPE = (
    ("passport", "Passport"),
    ("driver", "Driver"),
    ("voter", "Voter"),
    ("social_security", "Ssnit"),
    ("NationalID", "National ID"),
)
COUNTRY = (
    ("GH", "Ghana"),  
)
WITHDRAW_STATUS  =(
    ("pending", "Pending"),
    ("approved", "Approved")
)

REVERSE_TRANSACTION_STATUS =(
    ("pending", "Pending"),
    ("approved", "Approved"),
    ("decline", "Decline")
)
LEGAL_REP_STATUS = (
    ("available", "Available"),
    ("busy", "Busy")
)

CURRENCY_CHOICES = (
        ('GHS', 'Ghana Cedis'),
        ('USD', 'US Dollar')
    ) 
WITHDRAWAL_CURRENCY = (
        ('GHS', 'GHS'),
        ('NGN', 'NGN'),
        ('KES', 'KES'),
        ('UGX', 'UGX'),
        ('USD', 'USD')
    ) 
ACCOUNT_TYPE = (
        ('MobileMoney', 'MobileMoney'),
        ('BankTransfer', 'Bank Transfer')
    ) 
FREE = 'Free'
BASIC = 'Basic'
ENTERPRISE = 'Enterprise',
TIER_TWO = 'Tier two'

PLAN_CHOICES = (
    (BASIC, 'Basic'),
    (ENTERPRISE, 'Enterprise'),
    (FREE, 'Free'),
    (TIER_TWO, 'Tier two')
)

PRICING = {
    FREE: 0.00,
    BASIC: 0.02,
    TIER_TWO: 0.01,
    ENTERPRISE: 0.01
    
}
PLATFORM_CURRENCY = (
        ('GHS', 'GHS'),
        ('USD', 'USD')
    ) 
# Api integration Transaction stages
API_INTEGRATION_STAGE = (
        ('initiated', 'Initiated'),
        ('created', 'Created'),
        ('ongoing', 'Ongoing'),
        ('funds_requested', 'Funds requested'),
        ("reversed", "Reversed"),
        ("reversing", "Reversing"),
        ("canceled", "Canceled"),
        ("in_dispute", "In dispute"),
        ("dispute_resolved", "Dispute resolved"),
        ("dispute", "Dispute"),
        ('completed', 'Completed')
    ) 