from django.shortcuts import render
from django.utils import timezone
from rest_framework import viewsets
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework import permissions, status, generics, authentication
from rest_framework.decorators import  action, permission_classes
from rest_framework.status import (HTTP_201_CREATED,
                                   HTTP_200_OK,
                                   HTTP_400_BAD_REQUEST, HTTP_401_UNAUTHORIZED,
                                   HTTP_404_NOT_FOUND)
from rest_framework.response import Response
from .models import LegalFirm, LegalRep, InviteLegalRep
from .serializers import LegalFirmSerializer, LegalFirmRepsSerializer, AvailableLegalReps

# Create your views here.
def invite_legal_rep(legal_rep, dispute, invitor, created_date):
    InviteLegalRep.objects.create(
        legal_rep = legal_rep,
        dispute = dispute,
        invitor = invitor,
        created_date =created_date
                )

class LegalFirmViewSet(viewsets.ModelViewSet):
    serializer_class = LegalFirmSerializer
    queryset = LegalFirm.objects.all().order_by('-id')

    @action(methods=('POST',), detail=False, url_path='create', 
        permission_classes=(IsAuthenticated,))
    def add_legal_firm(self, request, *args, **kwargs):
        """ Add new legal firm to PayLock
            Request for firm details
            Create a legal firm object with the details
        """
        user = request.user
        firm_details = {
            'name': request.data.get('name'),
            'address': request.data.get('address'),
            'location': request.data.get('location'),
            'contact': request.data.get('contact'),
            'user': user,
            'created_date': timezone.now()
        }
        create_legal_firm = LegalFirm.objects.create(**firm_details)
        serialize_firm = self.serializer_class(create_legal_firm)
        return Response({
            'status_code': status.HTTP_201_CREATED,
            'message': 'Your legal firm has been created successfully.',
            'result': {
                'data': serialize_firm.data
            }
        }, status=status.HTTP_201_CREATED)
    
    @action(methods=('POST',), detail=False, url_path='add-legal-rep',
        permission_classes=(IsAuthenticated,))
    def add_legal_firm_rep(self, request, *args, **kwargs):
        """ Add rep to each legal firm
            Request for reps details
            Create an object with those details

        """
        firm_id = request.data.get('firm_id')
        try:
            legal_firm = LegalFirm.objects.get(id=firm_id)
        except LegalFirm.DoesNotExist:
            return Response({
            'status_code': status.HTTP_404_NOT_FOUND,
            'message': 'Legal firm not found on PayLock',
        }, status=status.HTTP_404_NOT_FOUND)

        user = request.user
        reps_detail = {
            'firm_name': legal_firm,
            'rep_name': request.data.get('rep_name'),
            'rep_contact': request.data.get('rep_contact'),
            'status': request.data.get('status'),
            'speciality': request.data.get('speciality'),
            'charge_rate': request.data.get('charge_rate'),
            'user': user,
            'created_date': timezone.now()
        }
        legal_rep = LegalRep.objects.create(**reps_detail)
        serialize_rep = LegalFirmRepsSerializer(legal_rep)
        return Response({
            'status_code': status.HTTP_201_CREATED,
            'message': 'Legal representative has been added to legal firm',
            'result': {
                'data': serialize_rep.data
            }
        }, status=status.HTTP_201_CREATED)
    
    @action(methods=('GET',), detail=False, url_path='show-legal-reps',
        permission_classes=(IsAuthenticated,))
    def show_available_legal_reps(self, request, *args, **kwargs):
        """ Show available legals reps with their price they
            Charge when handling a dispute.
            User can select from these reps to join a dispute.
            
        """
        user = request.user
        reps = LegalRep.objects.all().order_by('-id')
        serializer_reps = AvailableLegalReps(reps, many=True)
        return Response({
            'status_code': status.HTTP_200_OK,
            'message': 'Legal reps available',
            'result': {
                'data': serializer_reps.data
            }

        }, status=status.HTTP_200_OK)