# Generated by Django 2.0.6 on 2019-05-15 23:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('legal_firm', '0008_auto_20190515_2119'),
    ]

    operations = [
        migrations.AlterField(
            model_name='legalrep',
            name='rep_name',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
    ]
