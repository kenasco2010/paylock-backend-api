from django.db import models
from django.conf import settings
from django.utils import timezone
from dispute.models import Dispute
from paylock.choices import LEGAL_REP_STATUS
AUTH_USER_MODEL = getattr(settings, 'AUTH_USER_MODEL', 'auth.User')

# Create your models here.
class LegalFirm(models.Model):
    name = models.CharField(
        max_length=200

    )
    address= models.CharField(
        max_length=200

    )
    location = models.CharField(
        max_length=200
    )
    contact = models.CharField(
        max_length=200
    )
    user = models.ForeignKey(
        AUTH_USER_MODEL, 
        related_name='legal_firm_user', 
        null=True, 
        on_delete=models.SET_NULL
        )
    created_date = models.DateTimeField(
        default=timezone.now
        )
    
    def __unicode__(self):
        return '{}'.format(self.name) or u"Firm deleted"

    def __str__(self):
        return '{}'.format(self.name) or u"Firm deleted"

    class Meta:
        verbose_name_plural = u'Registered Legal Firms' or ""


class LegalRep(models.Model):
    firm_name = models.ForeignKey(
        LegalFirm,
        related_name='legal_firm',
        null=True,
        on_delete=models.SET_NULL
        )
    rep_name = models.CharField(
        max_length=200,
        blank=True,
        null=True,
        default="Rep name"
    )
    rep_contact = models.CharField(
        max_length=20
    )
    status = models.CharField(
        max_length=15,
        choices=LEGAL_REP_STATUS,
        default='available'
    )
    speciality = models.CharField(
        max_length=200,

    )
    charge_rate = models.DecimalField(
        max_digits=7, 
        decimal_places=2, 
        default=0.00
    )
    user = models.ForeignKey(
        AUTH_USER_MODEL, 
        related_name='rep_user', 
        null=True, 
        on_delete=models.SET_NULL
        )
    created_date = models.DateTimeField(
        default=timezone.now
        )
    
    def __unicode__(self):
        return '{}'.format(self.rep_name) or u"Rep deleted"

    def __str__(self):
        return '{}'.format(self.rep_name) or u"Rep deleted"

    class Meta:
        verbose_name_plural = u'Arbitrators' or ""

class InviteLegalRep(models.Model):
    legal_rep = models.ForeignKey(
        LegalRep,
        related_name='legal_rep',
        null=True,
        blank=True,
        on_delete=models.SET_NULL
        )
    dispute = models.ForeignKey(Dispute, 
        related_name='dispute_legal_rep', 
        on_delete=models.CASCADE
    )
    invitor = models.ForeignKey(
        AUTH_USER_MODEL, related_name='invitor', null=True, 
        on_delete=models.SET_NULL
    )
    created_date = models.DateTimeField(
        default=timezone.now
        )
    
    def __unicode__(self):
        return '{}'.format(self.legal_rep.rep_name) or u"Arbitrator deleted"

    def __str__(self):
        return '{}'.format(self.legal_rep.rep_name) or u"Arbitrator deleted"

    class Meta:
        verbose_name = u'Invited Legal Reps' or " "
        verbose_name_plural = u'Invited Legal Reps' or " "