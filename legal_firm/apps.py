from django.apps import AppConfig


class LegalFirmConfig(AppConfig):
    name = 'legal_firm'
