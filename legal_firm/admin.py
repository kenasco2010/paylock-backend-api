from django.contrib import admin
from .models import LegalFirm, LegalRep, InviteLegalRep
# Register your models here.

class LegalFirmAdmin(admin.ModelAdmin):
    list_display = ('name', 'location', 'contact', 'user', 'created_date')

class LegalRepAdmin(admin.ModelAdmin):
    list_display = ('rep_name','firm_name', 'status', 'charge_rate', 'speciality')

class InviteRepToDisputeAdmin(admin.ModelAdmin):
    list_display = ('legal_rep', 'dispute', 'invitor', 'created_date')

    def legal_rep(self):
        return '{}'.format(self.legal_rep.rep_name) or 'Rep deleted'

    # def firm_name(self):
    #     return '{}'.format(self.legal_rep.firm_name)

admin.site.register(LegalFirm, LegalFirmAdmin)
admin.site.register(LegalRep, LegalRepAdmin)
admin.site.register(InviteLegalRep, InviteRepToDisputeAdmin)

