from rest_framework import serializers
from .models import LegalFirm, LegalRep, InviteLegalRep
class LegalFirmSerializer(serializers.ModelSerializer):
    class Meta:
        model = LegalFirm
        fields = '__all__'

class LegalFirmRepsSerializer(serializers.ModelSerializer):
    class Meta:
        model = LegalRep
        fields = '__all__'

class AvailableLegalReps(serializers.ModelSerializer):
    legal_firm = serializers.SerializerMethodField()
    # legal_firm = serializers.CharField(source='legalfirm.name')
    rep_profile_image = serializers.SerializerMethodField()
    class Meta:
        model = LegalRep
        fields = ('id',  'legal_firm', 'rep_name', 'rep_profile_image',
        'status', 'speciality', 'charge_rate',
        )
    
    def get_rep_profile_image(self, legalrep):
        return '{}'.format(
            legalrep.user.user_profile.profile_image
        )

    def get_legal_firm(self, legalrep):
        return '{}'.format(
            legalrep.firm_name.name
        )

class InvitedLegalRepSerializer(serializers.ModelSerializer):
    legal_rep = serializers.SerializerMethodField()
    firm_name = serializers.SerializerMethodField()
    speciality = serializers.SerializerMethodField()
    charge_rate = serializers.SerializerMethodField()

    class Meta:
        model = InviteLegalRep
        fields=( 'legal_rep', 'invitor', 'firm_name', 'speciality', 'charge_rate', 'created_date',)

    def get_legal_rep(self, invite):
        return '{}'.format(invite.legal_rep.rep_name)

    def get_firm_name(self, invite):
        return '{}'.format(invite.legal_rep.firm_name)

    def get_speciality(self, invite):
        return '{}'.format(invite.legal_rep.speciality)
    
    def get_charge_rate(self, invite):
        return '{}'.format(invite.legal_rep.charge_rate)