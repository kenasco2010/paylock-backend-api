from django.conf.urls import url
from django.conf.urls import include
from django.urls import path
from rest_framework.routers import DefaultRouter
from .views import PaymentMethodViewSet

router = DefaultRouter()
router.register(r'v1.0/payment-methods', PaymentMethodViewSet)

urlpatterns = [
    path(r'', include(router.urls))
]