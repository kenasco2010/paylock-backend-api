from django.test import TestCase, RequestFactory, Client
from rest_framework.test import APIRequestFactory
from rest_framework.test import APIClient
from rest_framework.test import force_authenticate
from django.contrib.auth import get_user_model
from django.utils import timezone
from rest_framework.authtoken.models import Token
import json
from .models import PaymentMethod
from .serializers import PaymentMethodSerializer

User = get_user_model()

class PaymentMethodTest(TestCase):

    def setUp(self):
        self.factory = RequestFactory()
        self.credentials = {
            'email': 'kenmartey89@gmail.com',
            'phone_number': '00233246424340',
            'password': 'ceremony',
            }
        self.user = User.objects.create_superuser(**self.credentials)
        self.payment_method_input = {
            'payment_type':'Mobile Money',
            'created_date': timezone.now()
        }
        self.payment_method_input_update ={
            'payment_type': 'Mobile Money Update',
            'created_date': timezone.now()
        }
        self.user = get_user_model().objects.get(email='kenmartey89@gmail.com')

    def test_add_payment_method(self, **kwargs):
        client = APIClient()
        headers = {
        'HTTP_AUTHORIZATION': 'Token {}'.format(self.user.auth_token),
        }
        request = self.client.post('/api/v1.0/payment-methods/add-payment-method/', 
            self.payment_method_input, format='json', **headers
            )
        print ('===================ADD PAYMENT METHOD=========================')
        print ('>>> >>> >>> /NEW PAYMENT METHOD TEST/ content', request.content)
        self.assertEqual(request.status_code, 201)
        print ('===================END ADD PAYMENT METHOD TEST=========================')
        print ('      ')

    def test_update_payment_method(self):
        client = APIClient()
        headers = {
            'HTTP_AUTHORIZATION': 'Token {}'.format(self.user.auth_token)
        }
        payment_method = PaymentMethod.objects.create(**self.payment_method_input, user=self.user)
        request = self.client.post('/api/v1.0/payment-methods/'+ str(payment_method.id) 
            +'/update-payment-method/', self.payment_method_input_update, 
            format='json', **headers
            )
        print ('===================UPDATED PAYMENT METHOD=========================')
        print ('>>> >>> >>> /UPDATE PAYMENT METHOD TEST/ content', request.content)
        self.assertEqual(request.status_code, 201)
        print ('===================END UPDATED PAYMENT METHOD TEST=========================')
        print ('      ')

    def test_delete_payment_method(self):
        client = APIClient()
        headers = {
            'HTTP_AUTHORIZATION': 'Token {}'.format(self.user.auth_token)
        }
        payment_method = PaymentMethod.objects.create(**self.payment_method_input, user=self.user)
        request = self.client.delete('/api/v1.0/payment-methods/'+ str(payment_method.id) 
            +'/delete-payment-method/', format='json', **headers
            )
        print ('===================DELETE PAYMENT METHOD=========================')
        print ('>>> >>> >>> /UPDATE PAYMENT METHOD TEST/ content', request)
        self.assertEqual(request.status_code, 200)
        print ('===================END DELETE PAYMENT METHOD TEST=========================')
        print ('      ')

    def test_empty_payment_method_field(self):
        client = APIClient()
        headers = {
            'HTTP_AUTHORIZATION': 'Token {}'.format(self.user.auth_token),
        }
        payment_type = ({'payment_type':'', 'user':self.user})
        request = self.client.post('/api/v1.0/payment-methods/add-payment-method/',
            payment_type, format='json', **headers
        )
        print ('===================EMPTY TRANSACTION TYPE FIELD TEST=========================')
        print ('>>> >>> >>> /EMPTY TRANSACTION TYPE FIELD TEST/ content', request.content)
        self.assertEqual(request.status_code, 404)
        print ('===================EMPTY TRANSACTION TYPE FIELD TEST=========================')
        print ('      ')