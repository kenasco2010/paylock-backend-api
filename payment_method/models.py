from django.db import models
from django.conf import settings

# Create your models here.

from django.utils import timezone
AUTH_USER_MODEL = getattr(settings, 'AUTH_USER_MODEL', 'auth.User')

class PaymentMethod(models.Model):
    payment_type = models.CharField(
        max_length=50)
    user = models.ForeignKey(AUTH_USER_MODEL,  
        on_delete=models.CASCADE)
    created_date = models.DateTimeField(
        default=timezone.now)
    modified = models.DateTimeField(
        auto_now=True)


    def __unicode__(self):
        return self.payment_type

    def __str__(self):
        return self.payment_type

    class Meta:
        verbose_name_plural = 'Payment Methods'