from django.shortcuts import render
from django.contrib.auth import authenticate, login, logout, get_user_model
from django.utils import timezone
from rest_framework.authentication import TokenAuthentication
from rest_framework.status import (HTTP_201_CREATED,
                                   HTTP_200_OK,
                                   HTTP_400_BAD_REQUEST, HTTP_401_UNAUTHORIZED,
                                   HTTP_404_NOT_FOUND, HTTP_409_CONFLICT)
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework import viewsets
from rest_framework import permissions, status, generics, authentication
from rest_framework.decorators import detail_route, \
                    list_route, permission_classes, api_view
from rest_framework.authtoken.models import Token
from user_account.models import User, UserManager
from paylock.permissions import IsSuperUserPermission
from .models import PaymentMethod
from .serializers import PaymentMethodSerializer
# Create your views here.


class PaymentMethodViewSet(viewsets.ModelViewSet):
    """Show all payment methods in the system"""
    permission_classes = (IsAuthenticated, )
    serializer_class = PaymentMethodSerializer
    queryset = PaymentMethod.objects.all()

    @list_route(methods=('POST',), url_path='add-payment-method', 
        permission_classes=(IsSuperUserPermission,))
    def add_payment_method(self, request, *args, **kwargs):
        """Add payment method to list of payment methods"""
        user = request.user
        payment_methods_data = ({
            'payment_type': request.data.get('payment_type'),
            'user': user,
            'created_date': timezone.now()
            })
        if not payment_methods_data['payment_type']:
            return Response({'status_code': status.HTTP_404_NOT_FOUND,
                        'message': 'Please enter a payment method',
                }, status=status.HTTP_404_NOT_FOUND)
        elif PaymentMethod.objects.filter(
            payment_type=payment_methods_data['payment_type']
            ).exists(): # check if payment method already exist
            return Response({'status_code': status.HTTP_409_CONFLICT, 
                'message': 'Payment method already exist'
                }, status=status.HTTP_409_CONFLICT)
        else:
            payment_methods = PaymentMethod.objects.create(
                **payment_methods_data
                )
            payment_methods = self.serializer_class(payment_methods)
            return Response({'status_code': status.HTTP_201_CREATED,
                            'message': 'You have added a payment method',
                            'result': {
                                'data': payment_methods.data
                            }
                    },status=status.HTTP_201_CREATED)

    @detail_route(methods=('POST',), url_path='update-payment-method', 
        permission_classes=(IsSuperUserPermission, ))
    def update_payment_method(self, request, pk=None, *args, **kwargs):
        """Edit payment method"""
        payment_method = self.get_object()
        payment_method.payment_type = request.data.get('payment_type')
        payment_method.user = request.user 
        payment_method.save()
        payment_method = self.serializer_class(payment_method)
        return Response({'status_code': status.HTTP_201_CREATED,
                        'message': 'Your payment method has been edited',
                        'result': {
                            'data': payment_method.data
                        }
                    }, status=status.HTTP_201_CREATED)

    @detail_route(methods=('DELETE',), url_path='delete-payment-method',
        permission_classes=(IsSuperUserPermission, ))
    def delete_payment_method(self, request, pk=None):
        payment_method = self.get_object()
        payment_method.delete()
        return Response({ 'status_code': status.HTTP_200_OK,
                        'message': 'You have successfully deleted payment method'

                    }, status=status.HTTP_200_OK)