from django.db import models
from django.conf import settings
from django.utils import timezone
from paylock.choices import WITHDRAW_STATUS, WITHDRAWAL_CURRENCY, ACCOUNT_TYPE

AUTH_USER_MODEL = getattr(settings, 'AUTH_USER_MODEL', 'auth.User')


# Create your models here.
class Withdraw(models.Model):
    account_type = models.CharField(
        max_length=50, 
        choices=ACCOUNT_TYPE, 
        default='BankTransfer')
    fullname = models.CharField(
        max_length=100,
        blank=True,
        null=True
        )
    email = models.CharField(
        max_length=50, null=True
        )
    country = models.CharField(
        max_length=50, 
        null=True
        )
    phone_number = models.CharField(
        max_length=20, 
        null=True
        )
    account_bank = models.CharField(
        max_length=50, 
        null=True,
        blank=True
        )
    bank_name = models.CharField(
        max_length=50, 
        null=True,
        blank=True
        )
    account_number = models.CharField(
        max_length=50, 
        null=True,
        blank=True
        )
    currency = models.CharField(
        max_length=6
        )
    amount = models.DecimalField(
        max_digits=7, 
        decimal_places=2,
        null=True,
        blank=True
        )
    fees = models.DecimalField(
        max_digits=7, 
        decimal_places=2,
        null=True,
        blank=True
        )
    other_currency_conversion = models.DecimalField( 
        max_digits=7, 
        decimal_places=2,
        null=True,
        blank=True
        ) # Converted amount in other currency other than USD
    swift_code = models.CharField(
        max_length=50, 
        null=True, 
        blank=True)
    routing_number = bank_address = models.CharField(
        max_length=50, 
        null=True,
        blank=True)
    bank_address = models.CharField(
        max_length=100,
        null=True,
        blank=True)
    bank_code = models.CharField(
        max_length=100,
        null=True,
        blank=True
    )
    destination_branch_code = models.CharField(
        max_length=100,
        null=True,
        blank=True
    )
    narration = models.CharField(
        max_length=100,
        default='Withdrawing from my PayLock wallet.' 
        )
    
    beneficiary_name = models.CharField(
        max_length=100,
        null=True,
        blank=True
        )
    status = models.CharField(max_length=15, 
        choices=WITHDRAW_STATUS, 
        default='pending')
    txref = models.CharField(max_length=150, blank=True)
    user = models.ForeignKey(
        AUTH_USER_MODEL, related_name='withdraw',
        on_delete=models.CASCADE)
    published_date = models.DateTimeField(blank=True, null=True)
    created_date = models.DateTimeField(default=timezone.now)
    modified = models.DateTimeField(auto_now=True)

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __unicode__(self):
        return '%s' % (self.account_type)

    def __str__(self):
        return '%s' % (self.account_type)
