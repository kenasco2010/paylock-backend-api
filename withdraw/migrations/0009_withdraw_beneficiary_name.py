# Generated by Django 2.0.6 on 2019-03-25 20:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('withdraw', '0008_withdraw_destination_branch_code'),
    ]

    operations = [
        migrations.AddField(
            model_name='withdraw',
            name='beneficiary_name',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
    ]
