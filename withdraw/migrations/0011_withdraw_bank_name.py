# Generated by Django 2.0.6 on 2019-03-28 16:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('withdraw', '0010_auto_20190325_2019'),
    ]

    operations = [
        migrations.AddField(
            model_name='withdraw',
            name='bank_name',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
    ]
