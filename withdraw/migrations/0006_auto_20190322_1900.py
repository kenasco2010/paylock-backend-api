# Generated by Django 2.0.6 on 2019-03-22 19:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('withdraw', '0005_auto_20190322_1854'),
    ]

    operations = [
        migrations.AlterField(
            model_name='withdraw',
            name='account_number',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
        migrations.AlterField(
            model_name='withdraw',
            name='bank_name',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
    ]
