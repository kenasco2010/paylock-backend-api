# Generated by Django 2.0.6 on 2019-03-30 17:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('withdraw', '0012_auto_20190328_1620'),
    ]

    operations = [
        migrations.AddField(
            model_name='withdraw',
            name='txref',
            field=models.CharField(blank=True, max_length=150),
        ),
    ]
