from rest_framework import serializers
from .models import Withdraw

class WithdrawSerializer(serializers.ModelSerializer):
    class Meta:
        model = Withdraw
        fields = ('id', 'account_type', 'fullname', 'email', 'country', 
        'phone_number','account_bank','bank_name', 'account_number', 
        'currency', 'amount', 'other_currency_conversion', 
        'swift_code', 'routing_number', 'bank_address', 'bank_code',
        'destination_branch_code','narration', 'beneficiary_name', 
        'status', 'txref', 'user', 'created_date',)