from django.utils import timezone
from django.conf import settings


def account_details(request):
    account_details = {
        'withdraw_type': request.data.get('withdraw_type'),
        'bank_name': request.data.get('bank_name'),
        'account_name': request.data.get('account_name'),
        'account_number': request.data.get('account_number'),
        'account_type': request.data.get('account_type'),
        'branch': request.data.get('branch'),
        'country': request.data.get('country'),
        'email': request.data.get('email'),
        'phone_number': request.data.get('phone_number'),
        'network_type': request.data.get('network_type'),
        'amount_usd': request.data.get('amount_usd'),
        'amount_cedi': request.data.get('amount_cedi'),
        'user': request.user,
        'published_date': timezone.now(),
        'created_date': timezone.now()
        }
    return account_details