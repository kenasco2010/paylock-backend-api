from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver
from django.utils import timezone
from django.conf import settings
from .models import Withdraw
from payment.models import Payment
import requests

# @receiver(post_save, sender=Withdraw)
# def update_payment_model_with_withdraw_request(instance, created=False, **kwargs):
#     if created:
#         # SEND SLACK NOTIFICATION IF FUNDS IS WITHDRAWN.
#         headers = {
#             'content_tyep': 'application/json'
#         }
#         webhook = settings.WITHDRAW_SLACK_WEBHOOK
#         content = instance.account_type +' Withdrawal Request '+ '\n' + 'Request by: ' + \
#             instance.user.user_profile.first_name + ' ' + instance.user.user_profile.last_name + \
#             '\n Amount: ' + str(instance.amount) + ' ' + instance.currency + \
#             '\n Country: ' + str(instance.country) + '\n ----------------------------------------------'
#         data = {
#             "text": content
#         }
#         request = requests.post(webhook, json=data, headers=headers)
        