from django.contrib import admin
from .models import Withdraw

# Register your models here.
class WithdrawAdmin(admin.ModelAdmin):
    list_display = ('account_type', 'account_bank', 'country', 'email', 'amount', 
        'phone_number', 'status', 'created_date')
admin.site.register(Withdraw, WithdrawAdmin)