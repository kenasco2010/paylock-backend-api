from django.shortcuts import render
from django.utils import timezone
from django.conf import settings
from rest_framework.status import (HTTP_201_CREATED,
                                   HTTP_200_OK,
                                   HTTP_400_BAD_REQUEST, HTTP_401_UNAUTHORIZED,
                                   HTTP_404_NOT_FOUND, HTTP_409_CONFLICT,
                                   HTTP_501_NOT_IMPLEMENTED)
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework import viewsets
from rest_framework import permissions, status, generics, authentication
from rest_framework.decorators import permission_classes, action
from user_account.models import User, UserManager
from paylock.permissions import IsSuperUserPermission, IsOwner
from payment.models import Payment
from .models import Withdraw
from wallet.models import UserWallet
from .serializers import WithdrawSerializer
from .signals import *
import decimal
from paylock_emails.signals import send_withdrawal_email_to_user
from payment.json_payload import local_account_withdraw_details, momo_withdraw_details, \
    foreign_account_withdraw_details
from payment.views import update_payment_history
# from .withdraw_funds_details import *

def withdraw_fee(currency):
    currency = currency
    response = requests.get('https://ravesandboxapi.flutterwave.com/v2/gpx/transfers/fee?seckey=' + settings.FLUTTERWAVE_SECRET_KEY +'&currency=' + currency)
    response_data = response.json()['data']
    check_currency = next(item for item in response_data if item["currency"] == currency)
    fee = check_currency['fee']
    return fee

def create_momo_transfer(request, txref):
    user = request.user
    momo_details = momo_withdraw_details(request)
    transfer = Withdraw.objects.create(
            account_bank = momo_details['account_bank'],
            account_number = momo_details['account_number'],
            beneficiary_name = momo_details['beneficiary_name'],
            amount = momo_details['amount'],
            narration = momo_details['narration'],
            currency = momo_details['currency'],
            account_type='MobileMoney',
            country = momo_details['country'],
            phone_number = momo_details['account_number'],
            user = user,
            txref= txref,
            fullname = momo_details['beneficiary_name'],
            email = user.email

        )
    return transfer

def create_african_bank_transfer(request, txref):
    user = request.user
    bank_details = local_account_withdraw_details(request)
    transfer = Withdraw.objects.create(
            account_bank = bank_details['account_bank'], # Code of the bank
            account_number = bank_details['account_number'], # Account number
            bank_name=bank_details['bank_name'],
            beneficiary_name = bank_details['beneficiary_name'], # For non NGN
            amount = bank_details['amount'],
            narration = bank_details['narration'], 
            currency = bank_details['currency'],
            account_type='BankTransfer',
            destination_branch_code=bank_details['destination_branch_code'],
            country = bank_details['country'],
            phone_number = request.user.phone_number,
            user = user,
            txref=txref,
            email = user.email,
            fullname = bank_details['beneficiary_name']

        )
    return transfer
def create_foreign_bank_transfer(request, txref, fees):
    user = request.user
    bank_details = foreign_account_withdraw_details(request)
    transfer = Withdraw.objects.create(
            account_number = bank_details['AccountNumber'], # Account number
            bank_name=bank_details['BankName'],
            beneficiary_name = bank_details['BeneficiaryName'], # For non NGN
            amount = bank_details['amount'],
            narration = bank_details['narration'], 
            currency = bank_details['currency'],
            account_type = 'BankTransfer',
            routing_number = bank_details['RoutingNumber'],
            swift_code= bank_details['SwiftCode'],
            bank_address = bank_details['BeneficiaryAddress'],
            country = bank_details['BeneficiaryCountry'],
            phone_number = request.user.phone_number,
            user = user,
            txref=txref,
            fees=fees,
            email = user.email,
            fullname = bank_details['BeneficiaryName']

    )
    return transfer

def update_payment_model(
    payment_type, description, mobile_money_amount,
    amount_usd, amount, appfee, payment_status, txref, user, published_date):
    update_payment =  Payment.objects.create(
        payment_type = payment_type,
        description = description,
        mobile_money_amount = mobile_money_amount,
        amount_usd = amount_usd,
        amount = amount,
        appfee = appfee,
        payment_status = payment_status,
        txref = txref,
        user = user,
        published_date = published_date
        )
    return update_payment
            
class WithdrawViewSet(viewsets.ModelViewSet):
    permission_classes = (AllowAny,)
    serializer_class = WithdrawSerializer
    queryset = Withdraw.objects.all()


    @action(methods=('GET',), detail=False, url_path='list-of-banks',
        permission_classes=(AllowAny,))
    def list_of_banks_for_transfer(self, request, *args, **kwargs):
        country = request.query_params.get('country')
        response = requests.get('https://ravesandboxapi.flutterwave.com/v2/banks/' + country +'?'+ 'public_key=' + settings.FLUTTERWAVE_PUBLIC_KEY)
        return Response({'status_code': HTTP_200_OK,
                            'message': 'List of banks with codes',
                            'data': response.json()

                        }, status=status.HTTP_200_OK
                        )

    @action(methods=('POST',), detail=False, url_path='via-local-bank-and-momo', 
        permission_classes=(IsAuthenticated,))
    def withdraw_funds_by_african_bank(self, request, *args, **kwargs):
        user = request.user
        momo_details = momo_withdraw_details(request)
        bank_details = local_account_withdraw_details(request)
        user_wallet = UserWallet.objects.get(user=user)
        account_type = request.data.get('account_type')
        # convert the amount
        if account_type == 'MobileMoney':
            response = requests.get(
                settings.CURRENCY_LAYER_API, '&from=' + momo_details['currency'] + 
                '&to=USD&amount=' + str(momo_details['amount']))
            rate = response.json()
            converted_amount = rate['result']

            # convert the fees 
            response = requests.get(
                settings.CURRENCY_LAYER_API, '&from=' + momo_details['currency'] +
                '&to=USD&amount=' + str(withdraw_fee(momo_details['currency'])))
            rate = response.json()
            converted_fee = rate['result']
            full_amount = float(converted_amount) + float(converted_fee)
            if user_wallet.balance < full_amount:
                    return Response({'status_code': HTTP_400_BAD_REQUEST,
                            'message': 'The amount in your wallet is less than the amount you '
                            'intend to withdraw including the withdraw charges.',
                            'result': {
                                'currency': momo_details['currency'],
                                'withdrawal_amount': momo_details['amount'],
                                'fee': withdraw_fee(momo_details['currency']),
                                'converted_amount_and_fee': float("%.2f" % full_amount),
                                'wallet_amount': user_wallet.balance,
                            }

                        }, status.HTTP_400_BAD_REQUEST
                    )
            intiate_local_transfer = requests.post(settings.FLUTTERWAVE_TRANSFER_URL, data=momo_details)
            response = intiate_local_transfer.json()
            momo_transfer = create_momo_transfer(request, response['data']['reference'])
        elif account_type == 'BankTransfer':
            response = requests.get(
                settings.CURRENCY_LAYER_API, '&from=' + bank_details['currency'] + 
                '&to=USD&amount=' + str(bank_details['amount']))
            rate = response.json()
            converted_amount = rate['result']

            # convert the fees 
            response = requests.get(
                settings.CURRENCY_LAYER_API, '&from=' + bank_details['currency'] +
                '&to=USD&amount=' + str(withdraw_fee(bank_details['currency'])))
            rate = response.json()
            converted_fee = rate['result']
            full_amount = float(converted_amount) + float(converted_fee)
            if user_wallet.balance < full_amount:
                    return Response({'status_code': HTTP_400_BAD_REQUEST,
                            'message': 'The amount in your wallet is less than the amount you '
                            'intend to withdraw including the withdraw charges.',
                            'result': {
                                'currency': bank_details['currency'],
                                'withdrawal_amount': bank_details['amount'],
                                'fee': withdraw_fee(bank_details['currency']),
                                'converted_amount_and_fee': float("%.2f" % full_amount),
                                'wallet_amount': user_wallet.balance,
                            }

                        }, status.HTTP_400_BAD_REQUEST
                    )
            intiate_local_transfer = requests.post(settings.FLUTTERWAVE_TRANSFER_URL, data=bank_details)
            response = intiate_local_transfer.json()
            bank_transfer = create_african_bank_transfer(request, response['data']['reference'])
        if response['status'] == "error":
            return Response({'status_code': HTTP_200_OK,
                        'message': 'There was an error with your transfer request.',
                        'result': {
                            'data': response
                        }
                    }, status=status.HTTP_200_OK)
        else:
            # Update withdraw db with data returned after the request.
            transfer = Withdraw.objects.get(txref=response['data']['reference'])
            transfer.bank_name = response['data']['bank_name']  
            transfer.fullname = response['data']['fullname']  
            transfer.bank_code = response['data']['bank_code']
            transfer.beneficiary_name = response['data']['fullname']
            transfer.fees = response['data']['fee']
            transfer.save()
            update_payment = update_payment_model(
                "Wallet funds withdrawal - " +  response['data']['currency'],
                response['data']['narration'],
                response['data']['amount'],
                converted_amount,
                converted_amount,
                response['data']['fee'],
                'initialized',
                response['data']['reference'],
                user,
                timezone.now()
            )
            # amount_to_charge = float(response['data']['amount']) + float(response['data']['fee'])

            # convert_response_fee = requests.get(
            #     settings.CURRENCY_LAYER_API, '&from=' + response['data']['currency'] +
            #     '&to=USD&amount=' + str(response['data']['fee']))
            # rate = convert_response_fee.json()
            # converted_fee = rate['result']
            # amount_to_charge = float(converted_amount) + float(converted_fee)
            # balance = user_wallet.balance - decimal.Decimal(amount_to_charge)
            # user_wallet.balance = balance 
            # user_wallet.save()
            send_withdrawal_email_to_user(transfer)
            return Response({'status_code': HTTP_200_OK,
                        'message': 'You have initiated a transfer. '
                        'Kindly wait for a transfer confirmation.',
                        'result': {
                            'data': response
                        }
                    }, status=status.HTTP_200_OK)
                # else:
        
     
 # Confirm transfer status by fetching the transfer and show appriopriate message to user
            # Make a request to fetch transfer to see the status
            # transfer = requests.get(
            #     'https://ravesandboxapi.flutterwave.com/v2/gpx/transfers/?seckey=' + settings.FLUTTERWAVE_SECRET_KEY + '&' + 'reference=' + payment_model_copy.txref)
                
            # transfer_response = transfer.json()
            # if transfer_response['data']['transfers'][0]['status'] == 'SUCCESSFUL':
            #     my_wallet.balance -= decimal.Decimal(payment.amount)
            #     my_wallet.save()



    @action(methods=('POST',), detail=False, url_path='foreign-transfers', 
        permission_classes=(IsAuthenticated,))
    def withdraw_funds(self, request, *args, **kwargs):
        bank_details = foreign_account_withdraw_details(request)
        account_type = request.data.get('account_type')
        user = request.user
        # get wallet
        try:
            user_wallet = UserWallet.objects.get(user=user)
        except UserWallet.DoesNotExist:
            return Response({'status_code': status.HTTP_404_NOT_FOUND,
                    'message': 'You don\'t have a wallet on PayLock'
                }, status=status.HTTP_404_NOT_FOUND) 
        if account_type == 'BankTransfer':
            full_amount = float(bank_details['amount']) + withdraw_fee(bank_details['currency'])
            print("First full_amount to charge --- >>", full_amount)
            if user_wallet.balance < full_amount:
                return Response({'status_code': HTTP_400_BAD_REQUEST,
                        'message': 'The amount in your wallet is less than the amount you '
                                'intend to withdraw including the withdraw charges.',
                    }, status=status.HTTP_400_BAD_REQUEST)
        intiate_local_transfer = requests.post(settings.FLUTTERWAVE_TRANSFER_URL, data=bank_details)
        response = intiate_local_transfer.json()
        bank_transfer = create_foreign_bank_transfer(request, response['data']['reference'], response['data']['fee'])
        if response['status'] == "error":
            return Response({'status_code': HTTP_200_OK,
                        'message': 'There was an error with your transfer request.',
                        'result': {
                            'data': response
                        }
                    }, status=status.HTTP_200_OK)
        else:
            transfer = Withdraw.objects.get(txref=response['data']['reference'])
            transfer.fullname = response['data']['fullname']  
            transfer.bank_code = response['data']['bank_code']
            transfer.beneficiary_name = response['data']['fullname']
            transfer.save()

            update_payment = update_payment_model(
                "Wallet funds withdrawal - " +  response['data']['currency'],
                response['data']['narration'],
                0.00,
                response['data']['amount'],
                response['data']['amount'],
                response['data']['fee'],
                'initialized',
                response['data']['reference'],
                user,
                timezone.now()
            )
            # amount_to_charge = float(response['data']['amount']) + float(response['data']['fee'])
            # balance = user_wallet.balance - decimal.Decimal(amount_to_charge)
            # user_wallet.balance = balance 
            # user_wallet.save()
            send_withdrawal_email_to_user(transfer)
            return Response({'status_code': HTTP_200_OK,
                        'message': 'You have initiated a transfer. '
                        'Kindly wait for a transfer confirmation.',
                        'result': {
                            'data': response
                        }
                    }, status=status.HTTP_200_OK)


            # serialize_response = self.serializer_class(request_withdraw)
            # # my_wallet.balance -= decimal.Decimal(withdraw_details['amount'])
            # # my_wallet.save()
            # return Response({'status_code': status.HTTP_201_CREATED,
            #             'message': 'Your request is being processed. We will notify you once transaction is completed.',
            #             'result': {
            #                 'data': serialize_response.data,
            #             }
            #         }, status=status.HTTP_201_CREATED)
    
   
    
    @action(methods=('POST',), detail=False, url_path='transfer-from-flutterwave', 
        permission_classes=(IsAuthenticated,))
    def load_wallet_square(self, request, *args, **kwargs):
        """ Transfer money from flutterwave automatically
        
        """
        user = request.user
        withdraw_details = momo_withdraw_details(request)
        try:
            my_wallet = UserWallet.objects.get(user=user)
        except UserWallet.DoesNotExist:
            return Response({'status_code': status.HTTP_404_NOT_FOUND,
                    'message': 'You don\'t have a wallet on PayLock'
                }, status=status.HTTP_404_NOT_FOUND) 
        withdraw = Withdraw.objects.create(
            account_bank = withdraw_details['account_bank'],
            account_number = withdraw_details['account_number'],
            amount = withdraw_details['amount'],
            narration = withdraw_details['narration'],
            currency = withdraw_details['currency'],
            account_type='BankTransfer',
            user = user,
            email = user.email
        )
        api_response = requests.post('https://api.ravepay.co/v2/gpx/transfers/create', json=withdraw_details)
        payment = Payment.objects.create(
            payment_type="Withdraw from wallet",
            amount=withdraw_details['amount'],
            user=request.user,
            description=withdraw_details['narration'],
            txref=withdraw_details['reference'],
            published_date=timezone.now()
        )
        serialize_response = self.serializer_class(withdraw)
        return Response({'status_code': status.HTTP_201_CREATED,
                        'message': 'Your request in being processed. We will notify you once transaction is completed.',
                        'result': {
                            'data': api_response.json(),
                        }
                    }, status=status.HTTP_201_CREATED)