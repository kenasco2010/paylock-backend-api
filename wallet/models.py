from django.db import models
from django.conf import settings
from django.utils import timezone
from decimal import Decimal
from django.utils.crypto import get_random_string
from django.contrib.contenttypes.fields import GenericRelation
from django.dispatch import receiver
from transaction.models import Transaction
from dashboard_notification.models import Notification
from paylock.choices import CURRENCY_CHOICES
import uuid
# Create your models here.
AUTH_USER_MODEL = getattr(settings, 'AUTH_USER_MODEL', 'auth.User')

def generate_wallet_ID():
    unique_id = get_random_string(length=21)
    return unique_id

def generate_escrow_wallet_uniqueID():
    unique_id = get_random_string(length=15)
    return unique_id

class UserWallet(models.Model):    
    wallet_number = models.CharField(
        max_length=255,
        unique=True,
        default=uuid.uuid4
        )    
    user = models.OneToOneField(
        AUTH_USER_MODEL, related_name='user_wallets', null=True, 
        on_delete=models.SET_NULL
        )
    backup_user = models.CharField(max_length=50, null=True)
    is_authorized = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    activation_code = models.CharField(max_length=200, blank=True, null=True)
    paylock_wallet_code = models.CharField(max_length=100, blank=True, 
        null=True, unique=True)
    notification = GenericRelation(Notification, related_query_name="user_wallet")
    date_created = models.DateTimeField(auto_now_add=True)
    last_used = models.DateTimeField(blank=True, null=True)
    currency = models.CharField(choices=CURRENCY_CHOICES, default='USD', max_length=3)
    balance = models.DecimalField(max_digits=9, decimal_places=2, default=0.0)

    # def __init__(self):
    #     super(UserWallet, self).__init__()
    #     self.wallet_number = str(uuid.uuid4())

    def __unicode__(self):
        return str(self.wallet_number)
    
    def __str__(self):
        return str(self.wallet_number)

    

    class Meta:
        ordering = ('-id',)
        verbose_name_plural = 'User Wallet'

    @classmethod
    def credit_wallet(self, amount):
        self.balance += Decimal(amount)
        self.save()
        return True

    def debit_wallet(self, amount):
        myamount = Decimal(amount)
        if self.balance >= myamount:
            self.balance -= myamount
            self.save()
            return True
        return False

    def create_wallet(self, user):
        self.user = user
        self.save()

def activate_wallet(user):
    user_wallet = UserWallet.objects.get(user=user)
    user_wallet.is_authorized = True
    user_wallet.save()



class EscrowWallet(models.Model):
    unique_transaction_code = models.CharField(
        max_length=250, 
        default=uuid.uuid4,
        unique=True
        )  
    transaction = models.ForeignKey(Transaction, 
        related_name='transactions',
        blank=True,
        null=True,
        on_delete=models.SET_NULL
        )
    sender = models.ForeignKey(
        AUTH_USER_MODEL, related_name='sender', null=True, 
        on_delete=models.SET_NULL
        )
    recipient = models.ForeignKey(
        AUTH_USER_MODEL, related_name='recipient', null=True, 
        on_delete=models.SET_NULL,
        help_text = 'The person receiving the funds after at the end of the transaction. It could be a Merchant in the case of Marketplace.'
        )
    # business_account = models.ForeignKey(
    #     AUTH_USER_MODEL, related_name='business_account_user', null=True, 
    #     on_delete=models.SET_NULL,
    #     help_text = 'The business providing the platform in the case of Marketplace or Ecommerce.'
    #     )
    amount = models.DecimalField(max_digits=7, decimal_places=2)
    payin_agreed = models.BooleanField(default=True)
    payout_agreed = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    can_cancel = models.BooleanField(default=True)
    transfer_date = models.DateTimeField(auto_now_add=True)
    withdraw_date = models.DateTimeField(blank=True, null=True)
    withdraw_status = models.CharField(max_length=50, blank=True)
    date_created = models.DateTimeField(auto_now_add=True)
    last_used = models.DateTimeField(blank=True, null=True)
    balance = models.DecimalField(max_digits=9, decimal_places=2, default=0.0)

    def __unicode__(self):
        return str(self.unique_transaction_code)
    
    def __str__(self):
        return str(self.unique_transaction_code)



# class WalletActivities(models.Model):
#     transaction_type = models.ForeignKey(max_length=100)  
#     sender = models.ForeignKey(
#         AUTH_USER_MODEL, related_name='sender', null=True, 
#         on_delete=models.SET_NULL
#         )
#     recipient = models.ForeignKey(
#         AUTH_USER_MODEL, related_name='recipient', null=True, 
#         on_delete=models.SET_NULL
#         )
#     amount = models.DecimalField(max_digits=7, decimal_places=2)
#     payin_agreed = models.BooleanField(default=True)
#     payout_agreed = models.BooleanField(default=False)
#     is_active = models.BooleanField(default=True)
#     can_cancel = models.BooleanField(default=True)
#     transfer_date = models.DateTimeField(auto_now_add=True)
#     withdraw_date = models.DateTimeField(blank=True, null=True)
#     withdraw_status = models.CharField(max_length=50, blank=True)
#     date_created = models.DateTimeField(auto_now_add=True)
#     last_used = models.DateTimeField(blank=True, null=True)
#     balance = models.DecimalField(max_digits=9, decimal_places=2, default=0.0)

#     def __unicode__(self):
#         return str(self.unique_transaction_code)
    
#     def __str__(self):
#         return str(self.unique_transaction_code)