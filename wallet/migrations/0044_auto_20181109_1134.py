# Generated by Django 2.0.6 on 2018-11-09 11:34

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('wallet', '0043_auto_20181109_1132'),
    ]

    operations = [
        migrations.AlterField(
            model_name='escrowwallet',
            name='unique_transaction_code',
            field=models.CharField(default=uuid.uuid4, max_length=250, unique=True),
        ),
    ]
