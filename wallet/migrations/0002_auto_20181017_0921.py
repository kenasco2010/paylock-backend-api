# Generated by Django 2.0.6 on 2018-10-17 09:21

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('wallet', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userwallet',
            name='author',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='user_wallets', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='userwallet',
            name='wallet_number',
            field=models.CharField(default='dEWPV7iaJUnE3LkzbIstZ', max_length=255),
        ),
    ]
