from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver
from django.utils import timezone
from transaction.models import Transaction
from .models import EscrowWallet

@receiver(post_save, sender=EscrowWallet)
def mark_has_paid_on_transaction(instance, created=False, **kwargs):
    if created:
        transaction = Transaction.objects.get(
            pk=instance.transaction.id
            )
        transaction.has_paid=True
        transaction.save()
