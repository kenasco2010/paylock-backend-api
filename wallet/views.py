from django.shortcuts import render
from django.conf import settings
from django.db.models import Q
from django.utils import timezone
from rest_framework.authentication import TokenAuthentication
from rest_framework.status import (HTTP_201_CREATED,
                                   HTTP_200_OK,
                                   HTTP_400_BAD_REQUEST, HTTP_401_UNAUTHORIZED,
                                   HTTP_404_NOT_FOUND)

from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework import viewsets
from rest_framework import permissions, status, generics, authentication
from rest_framework.decorators import permission_classes, action
from rest_framework.authtoken.models import Token
from paylock.paginations import StandardResultsSetPagination
from paylock.permissions import IsSuperUserPermission, IsOwner
import re
from payment.serializers import PaymentSerializer
from .serializers import UserWalletSerializer
from payment.models import Payment
from user_account.models import User, UserManager
from .models import UserWallet
from payment.json_payload import bank_account_details
from payment.views import getKey, encryptData
import json
import requests

# Create your views here.
# wallet = UserWallet()


class UserWalletViewSet(viewsets.ModelViewSet):
    queryset = UserWallet.objects.all().order_by('-id')
    permission_classes = (IsAuthenticated, )
    serializer_class = UserWalletSerializer
    pagination_class = StandardResultsSetPagination

    @action(methods=('GET',), detail=False, url_path='my-wallet', 
        permission_classes=(IsOwner,))
    def my_wallet(self, request, *args, **kwargs):
        try:
            my_wallet = UserWallet.objects.get(user=request.user)
            serialized_wallet = self.serializer_class(my_wallet)
            return Response({
                'status_code': HTTP_200_OK,
                'message': 'My wallet information',
                'result': {
                    'data': serialized_wallet.data
                    }
                }, status=status.HTTP_200_OK )
        except UserWallet.DoesNotExist:
            return Response({
                'status_code': HTTP_404_NOT_FOUND,
                'message': 'You do not have a wallet. Contact Administrator to create one.',
                }, status=status.HTTP_404_NOT_FOUND )
    

 
    # @action(methods=('POST',), detail=True, url_path='debit-wallet', 
    #     permission_classes=(IsOwner,))
    # def debit_my_wallet(self, request, *args, **kwargs):
    #     wallet = self.get_object()
    #     if wallet.ballance >= 0:
    #         wallet.ballance -= 1
    #         wallet.last_used = timezone.now()
    #         wallet.save()
    #         return Response({
    #             'status_code': HTTP_200_OK,
    #             'message': 'Your wallet has been debited with 2gh'
    #             }, status=status.HTTP_200_OK )



    

    @action(methods=('GET',), detail=False, url_path='activities', 
        permission_classes=(IsOwner,))
    def activities_on_my_wallet(self, request, *args, **kwargs):
        """ The list of Activities performed on a wallet. 
        
        """
        user = request.user
        paginator = StandardResultsSetPagination()
        activities = Payment.objects.filter(
            Q(user=user, payment_status='completed') | 
            Q(user=user, payment_status='in_progress') |
            Q(user=user, payment_status='initialized') |
            Q(user=user, payment_status='failed')      |
            Q(user=user, payment_status='canceled')).order_by('-id')
        result_page = paginator.paginate_queryset(activities, request)
        activities = PaymentSerializer(result_page, many=True)
        activities = paginator.get_paginated_response(activities.data)
        return Response({
            'status_code': HTTP_200_OK,
            'message': 'Your wallet activities',
            'result': {
                'data': activities.data
            }
        }, status=status.HTTP_200_OK)
    # @action(methods=('GET',), detail=True, url_path='detail',
    #     permission_classes=(IsOwner,))
    # def my_wallet_details(self, request, pk=None, *args, **kwargs):
    #     wallet_detail = self.get_object()
    #     serialized_wallet_detail = self.serializer_class(wallet_detail)
    #     return Response({
    #             'status_code': HTTP_200_OK,
    #             'message': 'My wallet detail',
    #             'result': {
    #                 'data': serialized_wallet_detail.data
    #                 }
    #             }, status=status.HTTP_200_OK )


    # @action(methods=('POST',), detail=False, url_path='transfer-funds-to-account',
    # permission_classes=(IsOwner,))
    # def withdraw_funds_from_wallet(self, request, *args, **kwargs):
    #     # specify the amount to withdraw
    #     account_details = bank_account_details(request)
    #     print('>>>>>>>> Account details', account_details)

        
    #     response = requests.post(settings.FLUTTERWAVE_AFRICAN_BANK_ACC_TRANSFER, 
    #         data=account_details)
    #     response_json = response.json()
    #     print('>>>>>>>> Response in json', response_json)

    #     return Response({"status_code": HTTP_200_OK,
    #                 'data': response_json
    #             })
        # Execute the function to withdraw
            # Get the user wallet 
            # Deduct the amount specified from the wallet
            # hit endpoint with amount and bank details. 
            # Wait for flutterwave to do it's thing.

