from django.contrib import admin
from wallet.models import UserWallet, EscrowWallet
# Register your models here.

class UserWalletAdmin(admin.ModelAdmin):
    list_display = ('wallet_number', 'user', 'balance', 'is_active', 
        'is_authorized', 'date_created')

class EscrowWalletAdmin(admin.ModelAdmin):
    list_display =('unique_transaction_code', 'sender', 
        'recipient', 'amount', 'transfer_date', 
        'can_cancel', 'withdraw_status', 'withdraw_date'
        )
admin.site.register(UserWallet, UserWalletAdmin)
admin.site.register(EscrowWallet, EscrowWalletAdmin)