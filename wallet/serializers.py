from rest_framework import serializers
from .models import UserWallet, EscrowWallet

class UserWalletSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserWallet
        fields = ('id','wallet_number','is_authorized','is_active',
            'date_created','currency','balance','last_used')

class EscrowWalletSerializer(serializers.ModelSerializer):
    class Meta:
        model = EscrowWallet
        fields=('__all__')