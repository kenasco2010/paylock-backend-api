from django.test import TestCase, RequestFactory, Client
from django.contrib.auth import get_user_model
from django.utils import timezone

from rest_framework.authtoken.models import Token
from rest_framework.test import APIRequestFactory
from rest_framework.test import APIClient
from rest_framework.test import force_authenticate
import json

from transaction_type.models import TransactionType
from payment_method.models import PaymentMethod
from transaction.models import Transaction
from user_profile.models import UserProfile
from transaction.serializers import TransactionSerializer
from .models import ReverseTransaction


User = get_user_model()
# Create your tests here.

class ReverseTransactionTest(TestCase):

    def setUp(self):
        self.credentials = {
            'email': 'kenmartey89@gmail.com',
            'phone_number': '00233246424340',
            'password': 'ceremony',
        }
        self.contractor = {
            'email': 'dacodemonk@gmail.com',
            'phone_number': '00233246424341',
            'password': 'ceremony',
        }
        self.payment_method_input = {
            'payment_type':'Mobile Money',
            'created_date': timezone.now()
        }
        self.transaction_type_input = {
            'transaction_type': 'Project',
            'created_date': timezone.now()
        }
        self.user_profile = {
            'first_name': 'Kennedy',
            'last_name': 'Anyinatoe',
            'date_of_birth': '1989-07-09',
            'current_address': 'East Legon',
            'country': 'Ghana',
            'profile_image': ''

        }
        self.contractor_profile = {
            'first_name': 'Dacode',
            'last_name': 'Monk',
            'date_of_birth': '1989-07-01',
            'current_address': 'East Legon',
            'country': 'Ghana',
            'profile_image': ''

        }
        self.user = User.objects.create_superuser(**self.credentials)
        self.contractor = User.objects.create_superuser(**self.contractor)
        self.user_profile = UserProfile.objects.create(**self.user_profile, 
            user=self.user)
        self.contractor_profile = UserProfile.objects.create(**self.contractor_profile, 
        user=self.contractor)
        self.payment_method = PaymentMethod.objects.create(**self.payment_method_input, 
        user=self.user)
        self.transaction_type = TransactionType.objects.create(**self.transaction_type_input, 
        user=self.user)
        self.transaction_info = {
            'name': 'Nike shoes',
            'transaction_type': self.transaction_type,
            'description': 'A pair of Nike shoes from Jumia',
            'payment_method': self.payment_method,
            'currency': 'GHS',
            'amount': '900',
            'contractor': self.contractor,
            'contractor_agreement': False,
            'project_owner_agreement': True,
            'created_date': timezone.now()
        }
        self.transaction = Transaction.objects.create(**self.transaction_info, user=self.user)
        self.reverse_transaction = {
            'transaction': self.transaction.id,
            'reverse_reason': 'Transaction reverse reasons stated here.',
            'confirm_reverse': False,
            'approver': self.user,
            'status':  'pending',
            'published_date': timezone.now()
        }
        self.reverse_transaction_create = {
            'transaction': self.transaction,
            'reverse_reason': 'Transaction reverse reasons stated here.',
            'confirm_reverse': False,
            'approver': self.user,
            'status':  'pending',
            'published_date': timezone.now()
        }
    def test_create_reverse_transaction(self):
        client = APIClient()
        headers = {
            'HTTP_AUTHORIZATION': 'Token {}'.format(
                self.user.auth_token)
        }
        request = self.client.post('/api/v1.0/reverse-transaction/create-reverse/',
        self.reverse_transaction, format='json', **headers
        )
        print ('===================CREATE REVERSE TRANSACTION=========================')
        print ('>>> >>> >>> /CREATE REVERSE TRANSACTION TEST/ content', request.content)
        self.assertEqual(request.status_code, 201)
        print ('===================CREATE REVERSE TRANSACTION TEST=========================')
        print ('      ')


    def test_approve_reverse_transaction(self):
        client = APIClient()
        headers = {
            'HTTP_AUTHORIZATION': 'Token {}'.format(
                self.user.auth_token)
        }
        reverse_transaction = ReverseTransaction.objects.create(**self.reverse_transaction_create)
        print (">>>>>>>>>>>>>>> Recent transaction", reverse_transaction)
        request = self.client.post('/api/v1.0/reverse-transaction/{0}/approve-reverse-transaction/'.format(reverse_transaction.id),
        self.reverse_transaction, format='json', **headers
        )
        print ('===================APPROVE REVERSE TRANSACTION=========================')
        print ('>>> >>> >>> /APPROVE REVERSE TRANSACTION TEST/ content', request.content)
        self.assertEqual(request.status_code, 200)
        print ('===================APPROVE REVERSE TRANSACTION TEST=========================')
        print ('      ')

    def test_decline_reverse_transaction(self):
        client = APIClient()
        headers = {
            'HTTP_AUTHORIZATION': 'Token {}'.format(
                self.user.auth_token)
        }
        reverse_transaction = ReverseTransaction.objects.create(**self.reverse_transaction_create)
        print (">>>>>>>>>>>>>>> Recent transaction", reverse_transaction)
        request = self.client.post('/api/v1.0/reverse-transaction/{0}/decline-reverse-transaction/'.format(reverse_transaction.id),
        self.reverse_transaction, format='json', **headers
        )
        print ('===================DECLINE REVERSE TRANSACTION=========================')
        print ('>>> >>> >>> /DECLINE REVERSE TRANSACTION TEST/ content', request.content)
        self.assertEqual(request.status_code, 200)
        print ('===================DECLINE REVERSE TRANSACTION TEST=========================')
        print ('      ')
        