from django.apps import AppConfig


class ReverseTransactionConfig(AppConfig):
    name = 'reverse_transaction'
