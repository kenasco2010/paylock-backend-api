from django.contrib import admin
from .models import ReverseTransaction
# Register your models here.

class ReverseTransactionAdmin(admin.ModelAdmin):
    list_display =('transaction','status', 'approver',
            'published_date', 'modified')

admin.site.register(ReverseTransaction, ReverseTransactionAdmin )