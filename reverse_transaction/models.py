from django.db import models

# Create your models here.
from django.db import models

# Create your models here.
from django.db import models
from django.conf import settings
from django.utils import timezone
from paylock.choices import REVERSE_TRANSACTION_STATUS
from transaction.models import Transaction
# Create your models here.

AUTH_USER_MODEL = getattr(settings, 'AUTH_USER_MODEL', 'auth.User')

class ReverseTransaction(models.Model):
    transaction = models.ForeignKey(Transaction, related_name='transaction',
        on_delete=models.CASCADE
        )
    reverse_reason = models.TextField() # Reasons for the transaction reverse
    confirm_reverse = models.BooleanField(default=False) # Owner of transaction should tick before creating the reverse transaction
    approver = models.ForeignKey(
        AUTH_USER_MODEL, related_name='reverse_transaction_approver', on_delete=models.CASCADE,
        blank=True, null=True
        )
    amount = models.DecimalField(
        max_digits=7, 
        decimal_places=2,
        null=True,
        blank=True,
        default=0.00
    )
    status = models.CharField(choices=REVERSE_TRANSACTION_STATUS,
                                    max_length=20, blank=True,
                                    default="pending")
   
    published_date = models.DateTimeField(blank=True, null=True)
    created_date = models.DateTimeField(default=timezone.now)
    modified = models.DateTimeField(auto_now=True)

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __unicode__(self):
        return '%s' % (self.transaction)

    def __str__(self):
        return '%s' % (self.transaction)
