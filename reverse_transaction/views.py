from django.shortcuts import render
from django.contrib.auth import authenticate, login, logout, get_user_model
from django.conf import settings
from django.utils import timezone
from django.dispatch import receiver
from decimal import Decimal
from rest_framework.authentication import TokenAuthentication
from rest_framework.status import (HTTP_201_CREATED,
                                   HTTP_200_OK,
                                   HTTP_400_BAD_REQUEST, HTTP_401_UNAUTHORIZED,
                                   HTTP_404_NOT_FOUND, HTTP_409_CONFLICT)
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework import viewsets
from rest_framework import permissions, status, generics, authentication
from rest_framework.decorators import detail_route, \
                    list_route, permission_classes, api_view, action
from rest_framework.authtoken.models import Token
from rest_framework.pagination import PageNumberPagination
from user_account.models import User, UserManager
from paylock.permissions import IsSuperUserPermission, IsOwner
from transaction.models import Transaction
from .models import ReverseTransaction
from wallet.models import EscrowWallet, UserWallet
from transaction.serializers import TransactionSerializer
from .serializers import ReverseTransactionSerializer, ReverseTransactionListSerializer, \
        ReverseTransactionAwaitingRespListSerializer, ReverseTransactionDetailsSerializer
                        
from paylock_emails.signals import  send_email_reverse_transaction_has_been_approved, \
                    send_email_reverse_transaction_has_been_declined



class ReverseTransactionViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated, )
    serializer_class = ReverseTransactionSerializer
    queryset = ReverseTransaction.objects.all().order_by('-id')


    # List of transactions that can be reversed without confirmation.
    @action(methods=('GET',), detail=False, url_path='awaiting-reverse-list', 
    permission_classes=(IsOwner,))
    def transactions_to_be_reversed(self, request, *args, **kwargs):
        """ These are the list of transactions that can be reversed without confirmation.
            This means the collaborator hasn't accepted or rejected the transaction.

        """
        user = request.user
        transaction = EscrowWallet.objects.filter(transaction__user=user, 
            is_active=True, can_cancel=True, transaction__is_archived=False, sender=user)
    
        if transaction:
            available_transactions = ReverseTransactionListSerializer(transaction, many=True)
            return Response({'status_code': HTTP_200_OK,
                'message': 'Your list of transactions you can reverse without approval',
                'result': {
                    'data': available_transactions.data
                }
            }, status=status.HTTP_200_OK)
        else:
            available_transactions = ReverseTransactionListSerializer(transaction, many=True)
            return Response({'status_code': HTTP_200_OK,
                'message': 'You have no transactions to reverse, All are in process by collaborator',
                'result':{
                    'data': available_transactions.data
                }
            }, status=status.HTTP_200_OK)

     # List of transactions that have been reversed but waiting a response. Either approval or rejection.
    @action(methods=('GET',), detail=False, url_path='reverse-list-awaiting-response', 
    permission_classes=(IsOwner,))
    def list_of_reverse_transactions_waiting_response(self, request, *args, **kwargs):
        """ These are list of transactions that have been reversed but waiting a 
            response. Either approval or rejection from collaborator

        """
        user = request.user
        reverse_list = ReverseTransaction.objects.filter(transaction__contractor=user, 
            transaction__is_archived=False, status='pending')
    
        if reverse_list:
            available_reverse_list = ReverseTransactionAwaitingRespListSerializer(reverse_list, many=True)
            return Response({'status_code': HTTP_200_OK,
                'message': 'Your list of reverse transactions you should approve or reject.',
                'result': {
                    'data': available_reverse_list.data
                }
            }, status=status.HTTP_200_OK)
        else:
            available_reverse_list = ReverseTransactionAwaitingRespListSerializer(reverse_list, many=True)
            return Response({'status_code': HTTP_200_OK,
                'message': 'You have no reverse list to respond to.',
                'result':{
                    'data': available_reverse_list.data
                }
            }, status=status.HTTP_200_OK)


    # Reversed without confirmation.
    @action(methods=('POST',), detail=False, url_path='without-approval', 
    permission_classes=(IsOwner,))
    def reverse_transaction_without_collaborator_approval(self, request, *args, **kwargs):
        """ End and reverse a transaction without approval from collaborator.
            Get transaction by id
            Fetch data from Escrow wallet and check if eligible for refund without approval
            Get user's wallet and Credit User's wallet with amount from Escrow Wallet.
            Save current changes on both escrow wallet and user wallet.
            Delete transaction.
            
        """
        user = request.user
        transaction_id = request.data.get('transaction_id')

        reverse_condition = ['reversed', 'reversing', 'canceled', 'dispute']
        transaction = Transaction.objects.get(pk=transaction_id)
        if transaction.stage in reverse_condition:
            return Response({'status_code': HTTP_400_BAD_REQUEST,
                'message': 'Your transaction is not in the right stage for reverse. '
                'Kindly contact your collaborator to agree on terms.',
            }, status=status.HTTP_400_BAD_REQUEST)

        escrow_wallet_data = EscrowWallet.objects.get(transaction__id=transaction_id)
        if escrow_wallet_data.is_active==True and escrow_wallet_data.can_cancel==True and escrow_wallet_data.sender==user:
            
            try:
                user_wallet = UserWallet.objects.get(user=user)
            except UserWallet.DoesNotExist:
                return Response({'status_code': HTTP_404_NOT_FOUND,
                'message': 'You do not have wallet attached to your account. Kindly contact admin to help you set up.',
            }, status=status.HTTP_404_NOT_FOUND)
            #  credit wallet and save
            user_wallet.balance += Decimal(escrow_wallet_data.amount)
            user_wallet.save()
            # save escrow wallet changes
            escrow_wallet_data.is_active = False
            escrow_wallet_data.can_cancel = False
            escrow_wallet_data.withdraw_status = "Reversed transaction without approval"
            escrow_wallet_data.save()
            # delete transaction
            transaction = Transaction.objects.get(id=transaction_id)
            transaction.stage = 'reversed'
            transaction.is_archived = True
            transaction.save()
            return Response({'status_code': HTTP_200_OK,
                'message': 'You have reversed your transaction successfully. Kindly check your wallet balance to confirm.',
            }, status=status.HTTP_200_OK)
        else:
            return Response({'status_code': HTTP_400_BAD_REQUEST,
                'message': 'You are not eligible for refund on this transaction.',
            }, status=status.HTTP_400_BAD_REQUEST)


    # Create a reverse transaction and wait for approval form collaborator.
    @action(methods=('POST',), detail=False, url_path='create-reverse')
    def create_reverse_transaction(self, request, *args, **kwargs):
        """ Request a reverse or cancel transaction due to any reasons.

        """
        user = request.user
        reverse_condition = ['reversed', 'reversing', 'canceled', 'dispute']
        transaction = Transaction.objects.get(pk=request.data.get("transaction"))
        if transaction.user == user:
            if transaction.stage in reverse_condition:
                return Response({'status_code': HTTP_400_BAD_REQUEST,
                    'message': 'Your transaction is not in the right stage for reverse.'
                    'Kindly contact your collaborator to agree on terms.',
                }, status=status.HTTP_400_BAD_REQUEST)

            reverse_transaction_details = {
                'transaction': transaction,
                'reverse_reason': request.data.get('reverse_reason'),
                'amount': request.data.get('amount'),
                'confirm_reverse': False,
                'published_date': timezone.now(),
                
            }
            transaction_escrow_amount = EscrowWallet.objects.get(transaction__id=transaction.id)
            if Decimal(reverse_transaction_details['amount']) > Decimal(transaction_escrow_amount.amount):
                return Response({'status_code': HTTP_400_BAD_REQUEST,
                    'message': 'Kindly check the amount you entered,'
                    'it does not correspond with the transaction amount',
                }, status=status.HTTP_400_BAD_REQUEST)
            
            reverse_transaction = ReverseTransaction.objects.create(
                    **reverse_transaction_details
                    )
            # Update the stage of the transaction
            transaction.stage = 'reversing'
            transaction.save()
            # Serialize reverse transaction.
            reverse_transaction = self.serializer_class(reverse_transaction)
            return Response({'status_code': HTTP_201_CREATED,
                    'message': 'You have requested to reverse your transaction. '
                    'Your collaborator needs to confirm your reverse process',
                    'result': {
                        'data': reverse_transaction.data
                    }
                }, status=status.HTTP_201_CREATED)
        else:
            return Response({'status_code': HTTP_400_BAD_REQUEST,
                    'message': 'You are not authorized to reverse a transaction'
                }, status=status.HTTP_400_BAD_REQUEST)


    @action(methods=('POST',), detail=False, url_path='approve-reverse-transaction', 
        permission_classes=(IsAuthenticated,))
    def approve_reverse_transaction(self, request, pk=None, *args, **kwargs):
        """ Approve reverse transaction request by collaborator or contractor.
        
        """
        user = request.user
        reverse_id = request.data.get('reverse_id')
        try:
            reverse_transaction = ReverseTransaction.objects.get(id=reverse_id)
        except ReverseTransaction.DoesNotExist:
            return Response({'status_code': HTTP_404_NOT_FOUND,
                    'message': 'No transaction to be reversed'
                })
        if reverse_transaction.transaction.contractor == user:
            reverse_transaction.approver = user
            reverse_transaction.status = 'approved'
            reverse_transaction.confirm_reverse = True
            reverse_transaction.save()
            # reverse the money into users account
            transaction_id = reverse_transaction.transaction.id
            escrow_wallet_data = EscrowWallet.objects.get(transaction__id=transaction_id)

            transaction_user = reverse_transaction.transaction.user
            if escrow_wallet_data.is_active==True and escrow_wallet_data.can_cancel==False and escrow_wallet_data.transaction.contractor==user:
                try:
                    transction_user_wallet = UserWallet.objects.get(user=transaction_user)
                except UserWallet.DoesNotExist:
                    return Response({'status_code': HTTP_404_NOT_FOUND,
                    'message': 'Transaction owner does not have wallet on PayLock. Kindly contact admin to help you set up.',
                }, status=status.HTTP_404_NOT_FOUND)
                #  credit wallet and save
                transction_user_wallet.balance += Decimal(reverse_transaction.amount)
                transction_user_wallet.save()
                # subtract the amount from the escrow wallet
                escrow_wallet_data.amount -=Decimal(reverse_transaction.amount)
                escrow_wallet_data.save()
                #  if there is no amount left in the transaction escrow wallet
                if escrow_wallet_data.amount <= 0:
                # save escrow wallet changes
                    escrow_wallet_data.is_active = False
                    escrow_wallet_data.can_cancel = False
                    escrow_wallet_data.withdraw_status = "Reversed transaction approved"
                    escrow_wallet_data.save()
                    # delete transaction
                    transaction = Transaction.objects.get(id=transaction_id)
                    transaction.stage = 'reversed'
                    transaction.is_archived = True
                    transaction.save()
                else:
                    escrow_wallet_data.is_active = True
                    escrow_wallet_data.can_cancel = False
                    escrow_wallet_data.withdraw_status = "Specified amount Reversed"
                    escrow_wallet_data.save()
                    # delete transaction
                    transaction = Transaction.objects.get(id=transaction_id)
                    transaction.stage = 'ongoing'
                    transaction.is_archived = False
                    transaction.save()
                send_email_reverse_transaction_has_been_approved(reverse_transaction) # Send email function
                return Response({
                    'status_code': HTTP_200_OK,
                    'message': 'Reverse transaction approved by collaborator.'
                    }, status=status.HTTP_200_OK)
            else:
                return Response({
                    'status_code': HTTP_400_BAD_REQUEST,
                    'message': 'This transaction cannot be reversed. The transaction is inactive.'
                    }, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({
                    'status_code': HTTP_400_BAD_REQUEST,
                    'message': 'You are not a collaborator to this transaction, hence you cannot approve this transaction reverse request.'
                    }, status=status.HTTP_400_BAD_REQUEST)


    @action(methods=('POST',), detail=False, url_path='decline-reverse-transaction', 
        permission_classes=(IsAuthenticated,))
    def decline_reverse_transaction(self, request, pk=None, *args, **kwargs):
        """ Decline reverse transaction request by collaborator or contractor.
        
        """
        user = request.user
        reverse_id = request.data.get('reverse_id')
        try:
            reverse_transaction = ReverseTransaction.objects.get(id=reverse_id)
        except ReverseTransaction.DoesNotExist:
            return Response({'status_code': HTTP_404_NOT_FOUND,
                    'message': 'No transaction to be reversed'
                })
        if reverse_transaction.transaction.contractor == user:
            reverse_transaction.approver = user
            reverse_transaction.status = 'decline'
            reverse_transaction.save()
            send_email_reverse_transaction_has_been_declined(reverse_transaction) # Send email function
            transaction = Transaction.objects.get(pk=reverse_transaction.transaction.id)
            transaction.stage = 'dispute'
            transaction.save()
            return Response({
                'status_code': HTTP_200_OK,
                'message': 'You have declined the reverse transaction request. '
                }, status=status.HTTP_200_OK)
        else:
            return Response({
                'status_code': HTTP_400_BAD_REQUEST,
                'message': 'You are not a collaborator to this transaction, hence you cannot decline this transaction reverse request.'
                }, status=status.HTTP_400_BAD_REQUEST)


    @action(methods=('GET',), detail=False, url_path='details', permission_classes=(IsAuthenticated,))
    def reverse_transaction_details(self, request, *args, **kwargs):
        """ Reverse transaction details page
            Get ID of reverse transaction object
            View details of reverse transaction with transaction details

        """
        user = request.user 
        reverse_id = request.query_params.get('reverse_id')
        try:
            reverse_obj = ReverseTransaction.objects.get(id=reverse_id)
        except ReverseTransaction.DoesNotExist:
            return Response({
                'status_code': HTTP_400_BAD_REQUEST,
                'message': 'The reverse transaction object cannot be found.'
                }, status=status.HTTP_400_BAD_REQUEST)
        serialize = ReverseTransactionDetailsSerializer(reverse_obj)
        return Response({
                'status_code': HTTP_200_OK,
                'message': 'Reverse transaction details',
                'result': {
                    'data': serialize.data
                }
                }, status=status.HTTP_200_OK)