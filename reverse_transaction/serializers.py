from rest_framework import serializers
from .models import ReverseTransaction
from wallet.models import EscrowWallet
from transaction.serializers import TransactionSerializer
from transaction.serializers import TransactionSerializer

class ReverseTransactionSerializer(serializers.ModelSerializer):
    reverse_user = serializers.SerializerMethodField()
    reverse_user_id = serializers.SerializerMethodField()
    transaction_name = serializers.SerializerMethodField()

    class Meta:
        model  = ReverseTransaction
        fields = ('id', 'transaction', 'transaction_name', 'reverse_reason','status', 'approver', 'status', 
            'published_date', 'created_date', 'modified', 'reverse_user_id','reverse_user',
            )

    def get_transaction_name(self, reverse_obj):
        return '{}'.format(
            reverse_obj.transaction.name
            )

    def get_reverse_user(self, reverse_obj):
        return '{} {}'.format(
            reverse_obj.transaction.user.user_profile.first_name,
            reverse_obj.transaction.user.user_profile.last_name,
            )
    def get_reverse_user_id(self, reverse_obj):
        return '{}'.format(
            reverse_obj.transaction.user.id
            )
class ReverseTransactionListSerializer(serializers.ModelSerializer):
    transaction = TransactionSerializer()
    class Meta:
        model = EscrowWallet
        fields = ('id','unique_transaction_code','transaction','is_active','can_cancel',
            'sender',)

class ReverseTransactionAwaitingRespListSerializer(serializers.ModelSerializer):
    name = serializers.CharField(source='transaction.name')
    transaction_owner = serializers.CharField(source='transaction.user_fullname')
    class Meta:
        model = ReverseTransaction
        fields = ('id','name','reverse_reason','amount',
            'status','transaction_owner','created_date')
    
class ReverseTransactionDetailsSerializer(serializers.ModelSerializer):
    transaction = TransactionSerializer()
    transaction_owner_id = serializers.IntegerField(source='transaction.user.id')
    collaborator_id = serializers.IntegerField(source='transaction.contractor.id')

    class Meta:
        model = ReverseTransaction
        fields = ('id', 'reverse_reason','status', 'approver', 'status', 
            'published_date', 'created_date', 'modified', 'transaction',
            'transaction_owner_id', 'collaborator_id')