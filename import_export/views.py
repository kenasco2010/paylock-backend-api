from django.shortcuts import render
from django.utils import timezone
from django.db.models import Sum
from django.db.models import Q
from django.dispatch import receiver
from django.core.exceptions import ObjectDoesNotExist
from rest_framework.decorators import action
from rest_framework.status import (HTTP_201_CREATED,
                                   HTTP_200_OK,
                                   HTTP_400_BAD_REQUEST, HTTP_401_UNAUTHORIZED,
                                   HTTP_404_NOT_FOUND, HTTP_409_CONFLICT)
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework import viewsets, status
from .models import ImportExport
from transaction.models import Transaction
from .import_export_json import import_export_detail
from .serializers import ImportExportSerializer

# Create your views here.

class ImportExportViewSet(viewsets.ModelViewSet):
    queryset = ImportExport.objects.all()
    serializer_class = ImportExportSerializer
    permission_class = (AllowAny,)

    @action(methods=('POST',), detail=False, url_path='create-import-export', 
        permission_class=(IsAuthenticated,))
    def create_import_or_export(self, request, *args, **kwargs):
        """ This function adds a an import or export to a transaction.

        """
        user = request.user
        import_export_info = import_export_detail(request)
        transaction = Transaction.objects.get(pk=request.data.get('transaction'))
        
        import_export = ImportExport.objects.create(
           **import_export_info, 
           transaction=transaction
        )
        import_export_serializer = self.serializer_class(import_export)
        return Response({
            'status_code': status.HTTP_200_OK,
            'message': 'You have successfully added import or export data to this transaction.',
            'result': {
                'data': import_export_serializer.data
            }
        })
