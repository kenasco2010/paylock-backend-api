from django.db import models
from django.conf import settings
from django.utils import timezone
from datetime import date
from paylock.choices import TRADE_TYPE 
from transaction.models import Transaction
AUTH_USER_MODEL = getattr(settings, 'AUTH_USER_MODEL', 'auth.User')

# Create your models here.
class ImportExport(models.Model):
    transaction = models.OneToOneField(
        Transaction,
        related_name='import_exports',
        on_delete=models.CASCADE
    )
    trade_type = models.CharField(
        max_length=50,
        choices=TRADE_TYPE, 
        default='export'
    )
    nature_of_goods = models.TextField(
        blank=True
    )
    country_of_origin = models.CharField(
        max_length=50
        )
    quantity = models.IntegerField(
        default=0
    )
    weight = models.DecimalField(
        decimal_places=2,
        max_digits=7,
        default=0.00,
        blank=True,
        null=True
        )
    description_of_goods = models.TextField(

    )
    destination_country = models.CharField(
        max_length=50
    )
    date_of_shipment = models.DateField(
        default=date.today
    )
    estimated_date_of_arrival = models.DateField(
        default=date.today
    )
    # Clearance
    container_number = models.CharField(
        max_length=100

    )
    tracking_number = models.CharField(
        max_length=100

    )
    receipient_information = models.TextField(
        blank=True
    )

    # Upload any document.
    bill_of_lading = models.FileField(
        upload_to='bill_of_lading',
        blank=True, null=True
        )
    import_export_permit = models.FileField(
        upload_to='import_export_permits',
        blank=True, null=True
        )
    invoices_and_payment = models.FileField(
        upload_to='invoices_and_payments',
        blank=True, null=True
        )
    additional_documents = models.FileField(
        upload_to='additional_documents',
        blank=True, null=True
        )
    stage = models.CharField(
        max_length=50,
        default='ongoing'
        )
    created_date = models.DateTimeField(
        auto_now_add=True
        )
    modified_date = models.DateTimeField(
        auto_now=True
    )
    user = models.ForeignKey(
        AUTH_USER_MODEL, 
        related_name='import_exports', 
        null=True, 
        on_delete=models.SET_NULL
        )

    def __str__(self):
        return '{}'.format(self.trade_type) or u"Import / Export"

    class Meta:
        verbose_name_plural = u"Import / Export" or " "