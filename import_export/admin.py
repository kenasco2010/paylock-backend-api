from django.contrib import admin
from .models import ImportExport
# Register your models here.

class ImportExportAdmin(admin.ModelAdmin):
    list_display =('transaction', 'trade_type', 'stage', 'created_date', 'user', )
admin.site.register(ImportExport, ImportExportAdmin)