from django.utils import timezone
from django.conf import settings

def import_export_detail(request):
    import_export = {
        'trade_type': request.data.get('trade_type'),
        'nature_of_goods': request.data.get('nature_of_goods'),
        'country_of_origin': request.data.get('country_of_origin'),
        'quantity': request.data.get('quantity'),
        'weight': request.data.get('weight'),
        'description_of_goods': request.data.get('description_of_goods'),
        'destination_country': request.data.get('destination_country'),
        'date_of_shipment': request.data.get('date_of_shipment'),
        'estimated_date_of_arrival': request.data.get('estimated_date_of_arrival'),

        'container_number': request.data.get('container_number'),
        'tracking_number': request.data.get('tracking_number'),
        'receipient_information': request.data.get('receipient_information'),

        'bill_of_lading':  request.data.get('bill_of_lading'),
        'import_export_permit': request.data.get('import_export_permit'),
        'invoices_and_payment': request.data.get('invoices_and_payment'),
        'additional_documents': request.data.get('additional_documents'),
        'stage': request.data.get('stage'),
        'user': request.user,
        }
    return import_export

    