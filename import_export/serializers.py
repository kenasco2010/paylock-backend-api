from rest_framework import serializers
from .models import ImportExport

class ImportExportSerializer(serializers.ModelSerializer):
    class Meta:
        model = ImportExport
        fields = "__all__"