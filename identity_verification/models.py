from __future__ import unicode_literals
from django.contrib.auth.models import User
from django.utils import timezone
from django.conf import settings
from django.db import models
from paylock.choices import ID_TYPE

AUTH_USER_MODEL = getattr(settings, 'AUTH_USER_MODEL', 'auth.User')

# Create your models here.

class IdentityVerification(models.Model):
    user = models.OneToOneField(AUTH_USER_MODEL,  on_delete=models.CASCADE,
    related_name='identity_verification')
    id_number = models.CharField(
        max_length=50, 
        unique=True,
        blank=True, 
        null=True)
    id_type = models.CharField(choices=ID_TYPE, max_length=20,
                                    default="passport")
    verification_status = models.BooleanField(default=False)
    published_date = models.DateTimeField(blank=True, null=True)
    verification_document = models.FileField(
        upload_to='Identity_verification_document',
        blank=True, 
        null=True
        )
    created_date = models.DateTimeField(default=timezone.now)
    modified = models.DateTimeField(auto_now=True)

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __unicode__(self):
        first_name = self.user.user_profile.first_name
        last_name = self.user.user_profile.last_name
        return '{} {}'.format(first_name, last_name)

    def __str__(self):
        first_name = self.user.user_profile.first_name
        last_name = self.user.user_profile.last_name
        return '{} {}'.format(first_name, last_name)

    def user_name(self):
        first_name = self.user.user_profile.first_name
        last_name = self.user.user_profile.last_name
        return '{} {}'.format(first_name, last_name)