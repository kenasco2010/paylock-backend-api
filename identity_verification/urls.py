

from django.conf.urls import url
from django.conf.urls import include
from django.urls import path
from rest_framework.routers import DefaultRouter
from .views import IdentityVerificationViewSet

router = DefaultRouter()
router.register(r'v1.0/identity', IdentityVerificationViewSet)

urlpatterns = [
    path(r'', include(router.urls))
]