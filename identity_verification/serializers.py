from rest_framework import serializers
from .models import IdentityVerification

class IdentityVerificationSerializer(serializers.ModelSerializer):

    class Meta:
        model = IdentityVerification
        fields = ('user', 'id_number', 'id_type', 
        'verification_status', 'published_date', 'created_date')