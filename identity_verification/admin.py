from django.contrib import admin
from identity_verification.models import IdentityVerification 

# Register your models here.
class IdentityVerificationAdmin(admin.ModelAdmin):
    list_display = (
        'user_name', 'user', 'id_type', 'verification_status', 'created_date'
    )

admin.site.register(IdentityVerification, IdentityVerificationAdmin)
