from django.shortcuts import render
from django.contrib.auth import authenticate, login, logout, get_user_model
from django.conf import settings
from django.utils import timezone
from rest_framework.authentication import TokenAuthentication
from rest_framework.status import (HTTP_201_CREATED,
                                   HTTP_200_OK,
                                   HTTP_400_BAD_REQUEST, HTTP_401_UNAUTHORIZED,
                                   HTTP_404_NOT_FOUND)
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework import viewsets
from rest_framework import permissions, status, generics, authentication
from rest_framework.decorators import detail_route, \
                    list_route, permission_classes, api_view, action
from rest_framework.authtoken.models import Token
from user_account.models import User, UserManager
from .models import IdentityVerification
from .serializers import IdentityVerificationSerializer
import requests
import datetime
import re

import os
# Create your views here.


class IdentityVerificationViewSet(viewsets.ModelViewSet):
    """Show a list verifications"""
    permission_classes = (IsAuthenticated, )
    serializer_class = IdentityVerificationSerializer
    queryset = IdentityVerification.objects.all()

    @action(methods=('POST',), detail=False, url_path='verify', permission_classes=(IsAuthenticated,))
    def create_verification(self, request, *args, **kwargs):
        user = request.user
        country = user.user_profile.country

        
        verification_detail = {
            'id_number': request.data.get('id_number'),
            'id_type': request.data.get('id_type'),
            'verification_document': request.data.get('verification_document'),
            'first_name': user.user_profile.first_name, #not in use
            'last_name': user.user_profile.last_name,   #not in use
            'date_of_birth': user.user_profile.date_of_birth.strftime('%d/%m/%Y') #not in use
        }
        if not verification_detail['id_number'] or verification_detail['id_type'] is None:
            return Response({'status_code': status.HTTP_404_NOT_FOUND,
                    'message': 'Please provide required information.'
                    }, status=status.HTTP_404_NOT_FOUND)

        passport_number = IdentityVerification.objects.filter(id_number=verification_detail['id_number'])
        if passport_number.exists():
            return Response({'status_code': status.HTTP_409_CONFLICT,
                    'message': 'This passport number already exists. Please check the passport number.'
                    }, status=status.HTTP_409_CONFLICT)

        try:
            #  Check for verified account by user
            verified_user = IdentityVerification.objects.get(user=request.user, verification_status=True)
            if verified_user:
                return Response({'status_code': status.HTTP_409_CONFLICT,
                    'message': 'You have already verified your account.'
                    }, status=status.HTTP_409_CONFLICT)
        except IdentityVerification.DoesNotExist:
            create_verification = IdentityVerification.objects.create(
                user=user,
                id_number=verification_detail['id_number'],
                id_type=verification_detail['id_type'],
                verification_document=verification_detail['verification_document'],
                verification_status=True,
                published_date=timezone.now()
                )
            return Response({
                'status_code': status.HTTP_201_CREATED,
                'message': 'Thanks for verifying. We will notify you once we are done with the verification process.'
            }, status=status.HTTP_201_CREATED)


    @action(methods=('GET',), detail=False, url_path='check-verification', 
        permission_classes=(IsAuthenticated,))
    def check_if_user_is_verified(self, request, *args, **kwargs):
        """Check if the user is verified on load of the verify page."""
        user = request.user
        try:
            verified_user = IdentityVerification.objects.get(user=request.user, verification_status=True)
            serialized_verification = self.serializer_class(verified_user)
            if verified_user:
                return Response({'status_code': status.HTTP_200_OK,
                    'message': 'Your account is verified.',
                    'result': {
                        'data': serialized_verification.data
                    }
                    }, status=status.HTTP_200_OK)
        except IdentityVerification.DoesNotExist:
            return Response({
                'status_code': status.HTTP_404_NOT_FOUND,
                'message': 'Your account is not verified. Kindly provide your details to verify.'
                }, status=status.HTTP_404_NOT_FOUND)
            
