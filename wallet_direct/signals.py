from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver
from django.utils import timezone
from django.conf import settings
from paylock.send_text import send_text_message_about_transaction
from wallet.models import UserWallet
from .models import WalletDirect
from decimal import Decimal
# from transaction.views import markup_value
from payment.models import Payment
from paylock_emails.signals import funds_received_by_wallet_direct_by_email
from wallet import signals
import urllib
import requests


@receiver(post_save, sender=WalletDirect)
def move_money_to_receivers_account(instance, created=False, **kwargs):
    """ Move money to user wallet when a wallet direct transaction is 
        created.
    """
    if created:
        receiver_wallet = UserWallet.objects.get(user=instance.receiver)
        receiver_wallet.balance += Decimal(instance.amount)
        receiver_wallet.last_used = timezone.now()
        receiver_wallet.save()
        instance.stage = 'completed'
        instance.save()
        Payment.objects.create(
            payment_type="Wallet direct payment",
            description=instance.description,
            amount=instance.amount,
            appfee = instance.markup,
            payment_status='completed',
            user=instance.user,
            published_date=timezone.now()
        )
        # SEND EMAIL TO RECIPIENT OF FUNDS BY WALLET DIRECT.
        funds_received_by_wallet_direct_by_email(instance)
        