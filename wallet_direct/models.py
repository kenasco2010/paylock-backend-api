from django.db import models
from django.conf import settings
from django.utils import timezone
from decimal import Decimal
from django.utils.crypto import get_random_string
from django.contrib.contenttypes.fields import GenericRelation
from django.dispatch import receiver
from paylock.choices import CURRENCY_CHOICES, ESCROW_FEE_PAYMENT
import uuid
# Create your models here.
AUTH_USER_MODEL = getattr(settings, 'AUTH_USER_MODEL', 'auth.User')

def generate_wallet_ID():
    unique_id = get_random_string(length=21)
    return unique_id

def generate_escrow_wallet_uniqueID():
    unique_id = get_random_string(length=15)
    return unique_id

class WalletDirect(models.Model):
    name = models.CharField(
        max_length=200,
        default='Wallet direct transfer')
    description = models.TextField(
        help_text = 'Purpose of the transfer'
    )
    initial_amount = models.DecimalField(
        max_digits=7, 
        decimal_places=2
        )
    amount = models.DecimalField(max_digits=7, decimal_places=2, 
        default=0)
    
    markup = models.DecimalField(
        help_text = 'Percentage of the amount as fee',
        max_digits=7, 
        decimal_places=2,
        default=0
        )
    receiver = models.ForeignKey(
        AUTH_USER_MODEL, related_name='wallet_direct_receiver',
        on_delete=models.SET_NULL,
        blank = True,
        null=True
        )
    stage = models.CharField(
        max_length=20,
        default='initialized'
    )
    is_archived = models.BooleanField(
        default=False
    )
    user = models.ForeignKey(
        AUTH_USER_MODEL, related_name='wallet_direct_sender',
        on_delete=models.CASCADE
        )
    has_paid = models.BooleanField(
        default=False
    )
    published_date = models.DateTimeField(blank=True, null=True)
    created_date = models.DateTimeField(default=timezone.now)
    modified = models.DateTimeField(auto_now=True)
    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __str__(self):
        return '%s' % (self.name)

    class Meta:
        verbose_name_plural = u"Wallet Direct" or " "