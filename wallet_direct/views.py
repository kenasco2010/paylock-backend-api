from django.shortcuts import render
from django.conf import settings
from django.db.models import Q
from django.utils import timezone
from rest_framework.authentication import TokenAuthentication
from rest_framework.status import (HTTP_201_CREATED,
                                   HTTP_200_OK,
                                   HTTP_400_BAD_REQUEST, HTTP_401_UNAUTHORIZED,
                                   HTTP_404_NOT_FOUND)

from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework import permissions, status, generics, authentication, viewsets
from rest_framework.decorators import permission_classes, action
from rest_framework.authtoken.models import Token
from paylock.paginations import StandardResultsSetPagination
from paylock.permissions import IsSuperUserPermission, IsOwner
import re
from user_account.models import User, UserManager
from identity_verification.models import IdentityVerification
from wallet.models import UserWallet
from .serializers import WalletDirectSerializer
from wallet_direct import signals
from paylock_emails.signals import funds_sent_by_wallet_direct_by_email
from .models import WalletDirect
import requests

# Create your views here.
def total_transaction_amount(amount, user):
    pricing = user.get_pricing
    if amount <= 10000:
        markup = float(amount) * 0.015
        total_amount = markup + float(amount)
    elif amount > 10000:
        markup = float(amount) * 0.01
        total_amount = markup + float(amount)
    return total_amount

def markup_value(amount, user):
    """ We get the user pricing 
        and multiply by the amount 
        to get the escrow / markup 
        value.
    """
    if float(amount) <= 1000: 
        markup = float(amount) * 0.015
    elif float(amount) > 1000:
        markup = float(amount) * 0.01
    return markup

class WalletDirectViewSet(viewsets.ModelViewSet):
    queryset = WalletDirect.objects.all().order_by('-id')
    permission_classes = (IsAuthenticated, )
    serializer_class = WalletDirectSerializer
    pagination_class = StandardResultsSetPagination

    @action(methods=('POST',), detail=False, url_path='send', 
        permission_classes=(IsAuthenticated,))
    def send_wallet_direct(self, request, *args, **kwargs):
        """
            Sending money from wallet to wallet.(Wallet direct.)
        """
        user = request.user
        receiver = request.data.get('receiver')
        try:
            receiver = User.objects.get(email=receiver)
        except User.DoesNotExist:
            return Response({'status_code': status.HTTP_404_NOT_FOUND,
                'message': 'Your receiver doesn\'t have an account on PayLock'
                },status=status.HTTP_404_NOT_FOUND)

        try:
            verified = IdentityVerification.objects.get(user=user, verification_status=True)
        except IdentityVerification.DoesNotExist:
            return Response({
                'status_code': status.HTTP_403_FORBIDDEN,
                'message': 'You haven\'t verified your account. Verify your account to proceed with the transaction.' 
            }, status=status.HTTP_403_FORBIDDEN)

        # Error while creating transaction with same owner email address
        if receiver == request.user:
            return Response({'status_code': status.HTTP_400_BAD_REQUEST,
                'message': 'You can not make a transaction with your own email.'
                ' Please select another receiver email address'
                }, status=status.HTTP_400_BAD_REQUEST)
    
        try:
            my_wallet = UserWallet.objects.get(user=user, is_authorized=True)
        except UserWallet.DoesNotExist:
            return Response({'status_code': status.HTTP_400_BAD_REQUEST,
                'message': 'You do\'t have a wallet or not activated, Check your email for activation link or contact paylock admin.',
                }, status=status.HTTP_400_BAD_REQUEST)

        try:
            receiver_wallet = UserWallet.objects.get(user=receiver, is_authorized=True)
        except UserWallet.DoesNotExist:
            return Response({'status_code': status.HTTP_400_BAD_REQUEST,
                'message': 'Recipient\'s wallet is not activated. Activation link has been ' 
                'sent to recipient email. Try transfer again once recipient wallet is activated',
                }, status=status.HTTP_400_BAD_REQUEST)

        transaction_detail = {
            'description': request.data.get('description'),
            'amount': request.data.get('amount'),
            'receiver': receiver, #the person receiving the money
            'user': user,
            'published_date': timezone.now()
        }
        
        # Custom validations ### Refactor this code!
       
        if not transaction_detail['description']:
            return Response({'status_code': status.HTTP_404_NOT_FOUND,
            'message': 'Type the reason for the wallet direct transfer'
            }, status=status.HTTP_404_NOT_FOUND)
   
        elif not transaction_detail['amount']:
            return Response({'status_code': status.HTTP_404_NOT_FOUND,
            'message': 'Please add an amount'
            }, status=status.HTTP_404_NOT_FOUND)
        elif my_wallet.balance <= total_transaction_amount(
            float(transaction_detail['amount']), user):          
            return Response({'status_code': status.HTTP_404_NOT_FOUND,
            'message': 'The amount in your wallet is very low for this transaction. '
            'Kindly top up your wallet to proceed.',
            }, status=status.HTTP_404_NOT_FOUND)
    
        wallet_direct_transaction = WalletDirect.objects.create(
                **transaction_detail,
                initial_amount=transaction_detail['amount'],
                    markup=markup_value(
                    transaction_detail['amount'], user)
                    )
        total_amount = float(wallet_direct_transaction.amount) + float(wallet_direct_transaction.markup)
        amount_to_pay = float(my_wallet.balance) - total_amount
        my_wallet.balance = amount_to_pay
        my_wallet.last_used = timezone.now()
        my_wallet.save()
        funds_sent_by_wallet_direct_by_email(wallet_direct_transaction)
        wallet_direct = self.serializer_class(wallet_direct_transaction)
        return Response({'status_code': status.HTTP_201_CREATED,
            'message': 'Amount has been sent to receiver\'s wallet.',
            'result': {
                'data': wallet_direct.data
                }
            }, status=status.HTTP_201_CREATED)


    @action(methods=('GET',), detail=False, url_path='transfer-list', 
        permission_classes=(IsAuthenticated,))
    def wallet_direct_transfer_list(self, request, *args, **kwargs):
        """
            List of wallet direct transfers.
        """
        user = request.user
        wallet_direct = WalletDirect.objects.filter(
            Q(user=user) | Q(receiver=user)
        )
        transfers = self.serializer_class(wallet_direct, many=True)
        return Response({'status_code': status.HTTP_200_OK,
            'message': 'Your wallet direct transfers',
            'result': {
                'data': transfers.data
                }
            }, status=status.HTTP_200_OK)