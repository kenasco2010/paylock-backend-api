from rest_framework import serializers
from .models import WalletDirect

class WalletDirectSerializer(serializers.ModelSerializer):
    class Meta:
        model = WalletDirect
        fields = "__all__"