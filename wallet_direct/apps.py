from django.apps import AppConfig


class WalletDirectConfig(AppConfig):
    name = 'wallet_direct'
