
from django.contrib import admin
from .models import WalletDirect
# Register your models here.

class WalletDirectAdmin(admin.ModelAdmin):
    list_display =('name', 'description', 'amount', 'markup', 'stage', 'created_date', 'user', )
admin.site.register(WalletDirect, WalletDirectAdmin)