from rest_framework import serializers
from .models import TransactionInvitation
from transaction.serializers import TransactionSerializer
from transaction.models import Transaction

class TransactionInvitationSerializer(serializers.ModelSerializer):
    transaction = TransactionSerializer()
    class Meta:
        model = TransactionInvitation
        fields = ('id', 'transaction', 'acceptor', 'status', 
                'published_date', 'created_date', 'modified')