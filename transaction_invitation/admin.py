from django.contrib import admin
from .models import TransactionInvitation
# Register your models here.

class TransactionInvitationAdmin(admin.ModelAdmin):
    list_display = (
        'transaction', 'acceptor', 'status', 'created_date',
        'modified'
    )


admin.site.register(TransactionInvitation, TransactionInvitationAdmin)
