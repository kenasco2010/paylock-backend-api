from django.shortcuts import render
from django.contrib.auth import authenticate, login, logout, get_user_model
from django.utils import timezone
from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver
from django.conf import settings
from rest_framework.authentication import TokenAuthentication
from rest_framework.status import (HTTP_201_CREATED,
                                   HTTP_200_OK,
                                   HTTP_400_BAD_REQUEST, HTTP_401_UNAUTHORIZED,
                                   HTTP_404_NOT_FOUND, HTTP_409_CONFLICT)
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework import viewsets
from rest_framework import permissions, status, generics, authentication
from rest_framework.decorators import detail_route, \
                    list_route, permission_classes, api_view
from rest_framework.decorators import action
from rest_framework.authtoken.models import Token
from decimal import Decimal
from user_account.models import User, UserManager
from paylock.permissions import IsSuperUserPermission
from transaction_type.models import TransactionType
from payment_method.models import PaymentMethod
from user_account.models import User, UserManager
from .models import TransactionInvitation
from wallet.models import EscrowWallet, UserWallet
from .serializers import TransactionInvitationSerializer
from transaction.models import Transaction
# from dashboard_notification.signals import accept_transaction_invitation_notification

# Create your views here.


@receiver(post_save, sender=Transaction)
def create_product_invitation_after_transaction(sender, instance, created=False, **kwargs):
    """ Transaction invitation model is created when Transaction 
        is created and invitation sent to others
    
    """
    if created:
        transaction = sender.objects.get(pk=instance.pk)
        transaction_invitation = TransactionInvitation.objects.create(
            transaction = transaction,
            published_date= timezone.now()
        )

class TransactionInvitationViewSet(viewsets.ModelViewSet):
    """ This handles the transaction detials page.
        Just add the id of the invitation to get the details 
        of the transaction.

    """
    permission_classes = (IsAuthenticated, )
    serializer_class = TransactionInvitationSerializer
    queryset = TransactionInvitation.objects.all()

    @action(methods=('POST',), detail=False,  url_path='accept', 
        permission_classes=(IsAuthenticated,))
    def approve_invitation(self, request, *args, **kwargs):
        """ Endpoint to Approve invitation for a product or project transaction.
            Supply the invitation id
            Approve by invitation id
            Get transaction id from invitation
            Move can_cancel to False on EscrowWallet Model.

        """
        user = request.user
        invitation_id = request.data.get('invitation_id')
        escrow_fee = ['owner', 'both']
        try:
            invitation = TransactionInvitation.objects.get(id=invitation_id)
        except TransactionInvitation.DoesNotExist:
            return Response({'status_code': status.HTTP_200_OK,
                        'message': 'Transaction invitation not found. Please check the invitation ID'
                }, status=status.HTTP_200_OK)  

        transaction = Transaction.objects.get(pk=invitation.transaction.id)
        
        if invitation.transaction.contractor == user and invitation.transaction.transaction_role == 'owner' and  invitation.transaction.escrow_fee_payment in escrow_fee:
            invitation.status = 'approved'
            invitation.acceptor = user
            invitation.save()
            
            # Update the transaction contractor agreement field
            # transaction = Transaction.objects.get(pk=invitation.transaction.id)
            transaction.contractor_agreement = True
            transaction.stage = 'ongoing'
            transaction.save()
            # Update the Escrow wallet by moving the can_cancel to False.
            try:
                escrow_wallet = EscrowWallet.objects.get(
                    transaction=invitation.transaction.id, is_active=True
                    )
            except EscrowWallet.DoesNotExist:
                return Response({'status_code': status.HTTP_404_NOT_FOUND,
                            'message': 'No money was deposited in the escrow '
                            'account for you. Kindly contact the transaction'
                            'owner'
            }, status=status.HTTP_404_NOT_FOUND) 
            escrow_wallet.last_used = timezone.now()
            escrow_wallet.can_cancel = False
            escrow_wallet.save()
            return Response({'status_code': status.HTTP_200_OK,
                            'message': 'You have accepted this invitation'
            }, status=status.HTTP_200_OK)
        elif invitation.transaction.contractor == user and invitation.transaction.transaction_role == 'owner' and invitation.transaction.escrow_fee_payment == 'collaborator':
            invitation.status = 'approved'
            invitation.acceptor = user
            invitation.save()
            
            # Update the transaction contractor agreement field
            # transaction = Transaction.objects.get(pk=invitation.transaction.id)
            transaction.contractor_agreement = True
            transaction.stage = 'ongoing'
            transaction.amount -= transaction.markup # Deduct markup from amount.
            transaction.save()
            # Update the Escrow wallet by moving the can_cancel to False.
            try:
                escrow_wallet = EscrowWallet.objects.get(
                    transaction=invitation.transaction.id, is_active=True
                    )
            except EscrowWallet.DoesNotExist:
                return Response({'status_code': status.HTTP_404_NOT_FOUND,
                            'message': 'No money was deposited in the escrow '
                            'account for you. Kindly contact the transaction'
                            'owner'
            }, status=status.HTTP_404_NOT_FOUND)
            escrow_wallet.amount -= invitation.transaction.markup # Deduct markup from amount in escrow wallet.
            escrow_wallet.last_used = timezone.now()
            escrow_wallet.can_cancel = False
            escrow_wallet.save()
            return Response({'status_code': status.HTTP_200_OK,
                            'message': 'You have accepted this Invitation and escrow fee deducted from transaction amount'
            }, status=status.HTTP_200_OK)
        elif invitation.transaction.contractor == user and invitation.transaction.transaction_role == 'collaborator' and invitation.transaction.escrow_fee_payment in escrow_fee:
            try:
                collaborator_wallet = UserWallet.objects.get(user=invitation.transaction.contractor)
            except UserWallet.DoesNotExist:
                return Response({
                    'status_code': HTTP_404_NOT_FOUND,
                    'message': 'Your wallet is not active on PayLock'
                })
            total_amount = float(invitation.transaction.amount)
            if collaborator_wallet.balance < total_amount:
                return Response({
                    'status_code': HTTP_400_BAD_REQUEST,
                    'message': 'You don\'t have enough funds in your wallet to accept this transaction'
                })
            collaborator_wallet.balance -= Decimal(total_amount)
            collaborator_wallet.last_used = timezone.now()
            collaborator_wallet.save()

            invitation.status = 'approved'
            invitation.acceptor = user
            invitation.save()
            
            # Update the transaction contractor agreement field
            # transaction = Transaction.objects.get(pk=invitation.transaction.id)
            transaction.contractor_agreement = True
            transaction.amount -= transaction.markup
            transaction.stage = 'ongoing'
            transaction.save()
            # Update the Escrow wallet by moving the can_cancel to False.
            try:
                escrow_wallet = EscrowWallet.objects.get(
                    transaction=invitation.transaction.id, is_active=True
                    )
            except EscrowWallet.DoesNotExist:
                return Response({'status_code': status.HTTP_404_NOT_FOUND,
                            'message': 'No money was deposited in the escrow '
                            'account for you. Kindly contact the transaction'
                            'owner'
            }, status=status.HTTP_404_NOT_FOUND) 
            escrow_wallet.amount += transaction.amount
            escrow_wallet.last_used = timezone.now()
            escrow_wallet.can_cancel = False
            escrow_wallet.save()
            return Response({'status_code': status.HTTP_200_OK,
                            'message': 'You have accepted this invitation. The money will be moved from your wallet and escrow fee deducted as well.'
            }, status=status.HTTP_200_OK)
        
        elif invitation.transaction.contractor == user and invitation.transaction.transaction_role == 'collaborator' and invitation.transaction.escrow_fee_payment == 'collaborator':
            try:
                collaborator_wallet = UserWallet.objects.get(user=invitation.transaction.contractor)
            except UserWallet.DoesNotExist:
                return Response({
                    'status_code': HTTP_404_NOT_FOUND,
                    'message': 'Your wallet is not active on PayLock'
                })
            total_amount = float(invitation.transaction.amount)
            if collaborator_wallet.balance < total_amount:
                return Response({
                    'status_code': HTTP_400_BAD_REQUEST,
                    'message': 'You don\'t have enough funds in your wallet to accept this transaction'
                })
            collaborator_wallet.balance -= Decimal(total_amount)
            collaborator_wallet.last_used = timezone.now()
            collaborator_wallet.save()

            invitation.status = 'approved'
            invitation.acceptor = user
            invitation.save()
            
            # Update the transaction contractor agreement field
            # transaction = Transaction.objects.get(pk=invitation.transaction.id)
            transaction.contractor_agreement = True
            transaction.stage = 'ongoing'
            # transaction.amount = total_amount
            transaction.save()
            # Update the Escrow wallet by moving the can_cancel to False.
            try:
                escrow_wallet = EscrowWallet.objects.get(
                    transaction=invitation.transaction.id, is_active=True
                    )
            except EscrowWallet.DoesNotExist:
                return Response({'status_code': status.HTTP_404_NOT_FOUND,
                            'message': 'No money was deposited in the escrow '
                            'account for you. Kindly contact the transaction'
                            'owner'
            }, status=status.HTTP_404_NOT_FOUND)
            escrow_wallet.amount = total_amount
            escrow_wallet.last_used = timezone.now()
            escrow_wallet.can_cancel = False
            escrow_wallet.save()
            return Response({'status_code': status.HTTP_200_OK,
                            'message': 'You have accepted this Invitation and only transaction amount deducted from your wallet'
            }, status=status.HTTP_200_OK)

        else:
            return Response({'status_code': HTTP_400_BAD_REQUEST,
                    'message': 'You are not authorized to accept this transaction invitation.'
                }, status=status.HTTP_400_BAD_REQUEST)

        
    @action(methods=('POST',), detail=False, url_path='reject', 
        permission_classes=(IsAuthenticated,))
    def decline_invitation(self, request, *args, **kwargs):
        """ Endpoint to reject invitation for a product or project
            Get escrow wallet transaction details by invitation id
            Make escrow wallet transaction inactive, 
            Shift escrow wallet Can cancel to False
            Credit transaction owner wallet back to amount spent on transaction.

        """ 
        user = request.user
        invitation_id = request.data.get('invitation_id')
        invitation = TransactionInvitation.objects.get(id=invitation_id)
        if invitation.transaction.contractor == user:
            invitation.status = 'decline'
            invitation.acceptor = request.user
            invitation.save()
            # Update the transaction contractor agreement field
            transaction = Transaction.objects.get(pk=invitation.transaction.id)
            transaction.contractor_agreement = False
            transaction.stage = 'canceled'
            transaction.save()    

            # Get transaction moved to escrow wallet.
            try:
                escrow_wallet = EscrowWallet.objects.get(
                    transaction=invitation.transaction.id, is_active=True
                    )
            except EscrowWallet.DoesNotExist:
                return Response({'status_code': status.HTTP_404_NOT_FOUND,
                            'message': 'Transaction is in an invalid state. '
                            'Kindly contact the transaction owner.'
            }, status=status.HTTP_404_NOT_FOUND)

            # Get the escrow details, make it inactive and prevent transaction _
            # owner from canceling the transaction.      
            amount = escrow_wallet.amount
            escrow_wallet.can_cancel = False
            escrow_wallet.is_active = False
            escrow_wallet.withdraw_status ="Transaction Invitation was declined"
            escrow_wallet.last_used = timezone.now()
            escrow_wallet.save()

            # Credit back the transaction owner escrow Account
            user_wallet = UserWallet.objects.get(user=invitation.transaction.user)
            user_wallet.balance += Decimal(amount)
            user_wallet.last_used = timezone.now()
            user_wallet.save()
            return Response({'status_code': status.HTTP_200_OK,
                            'message': 'You have rejected this invitation'
            }, status=status.HTTP_200_OK)
        else:
            return Response({'status_code': HTTP_400_BAD_REQUEST,
                    'message': 'You are not authorized to reject this transaction invitation.'
                }, status=status.HTTP_400_BAD_REQUEST)

    
    @action(methods=('GET',), detail=False, url_path='sent-list', 
            permission_classes=(IsAuthenticated,))
    def transaction_invitations_sent(self, request, *args, **kwargs):
        """ Transaction invitations sent by project owner with their response.
        
        """
        user = request.user
        invitation = TransactionInvitation.objects.filter(transaction__user=user, 
            transaction__is_archived=False).order_by('-id')
        transaction_invitation = self.serializer_class(invitation, many=True)
        return Response({'status_code': HTTP_200_OK,
                        'message': 'Transaction invitations sent out with their status',
                        'result': {
                            'data': transaction_invitation.data
                        }
            })
   
    @action(methods=('GET',), detail=False, url_path='received-list', 
            permission_classes=(IsAuthenticated,))
    def received_transaction_invitation_list(self, request,*args, **kwargs):
        """ Transaction invitation received by collaborator to either accept or
            reject.
        
        """
        user = request.user
        invitation = TransactionInvitation.objects.filter(transaction__contractor=user, 
            transaction__is_archived=False, status='pending').order_by('-id')
        transaction_invitation = self.serializer_class(invitation, many=True)
        return Response({'status_code': HTTP_200_OK,
                        'message': 'Received transaction invitations',
                        'result': {
                            'data': transaction_invitation.data
                        }
            })
    
    @action(methods=('GET',), detail=False, url_path='recieved-list-with-status', 
            permission_classes=(IsAuthenticated,))
    def transaction_invitation_received_list(self, request, *args, **kwargs ):
        """ All transaction invitation received by collaborator and acted upon
        
        """
        user = request.user
        invitation = TransactionInvitation.objects.filter(transaction__contractor=user, 
            transaction__is_archived=False).order_by('-id')
        transaction_invitation = self.serializer_class(invitation, many=True)
        return Response({'status_code': HTTP_200_OK,
                        'message': 'Approved transaction invitations list',
                        'result': {
                            'data': transaction_invitation.data
                        }
            })
