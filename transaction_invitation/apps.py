from django.apps import AppConfig


class TransactionInvitationConfig(AppConfig):
    name = 'transaction_invitation'
