from django.db import models
from django.conf import settings
from django.utils import timezone
from paylock.choices import INVITATION_STATUS
from transaction.models import Transaction
# Create your models here.

AUTH_USER_MODEL = getattr(settings, 'AUTH_USER_MODEL', 'auth.User')

class TransactionInvitation(models.Model):
    transaction = models.ForeignKey(Transaction, related_name='invitation',
        on_delete=models.CASCADE
        )
    acceptor = models.ForeignKey(
        AUTH_USER_MODEL, related_name='transaction_invitation', on_delete=models.CASCADE,
        blank=True, null=True
        )
    status = models.CharField(choices=INVITATION_STATUS,
                                    max_length=20, blank=True,
                                    default="pending")
    published_date = models.DateTimeField(blank=True, null=True)
    created_date = models.DateTimeField(default=timezone.now)
    modified = models.DateTimeField(auto_now=True)

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __unicode__(self):
        return '%s' % (self.transaction)

    def __str__(self):
        return '%s' % (self.transaction)