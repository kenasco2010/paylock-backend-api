from django.db import models
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.conf import settings
from django.db.models.signals import post_save
from django.core.exceptions import ObjectDoesNotExist
from django.dispatch import receiver
from rest_framework.authtoken.models import Token

from paylock import choices as c
from decimal import Decimal


# Create auth token for new users
@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)
        

class UserManager(BaseUserManager):
    """Class required by Django for managing our users from the management
    command.
    """

    def create_user(self, email, phone_number, password=None):
        """Creates a new user with the given detials."""

        # Check that the user provided an email.
        if not email:
            raise ValueError('Users must have an email address.')

        if not phone_number:
            raise ValueError('Provide valid phone number')

        # Create a new user object.
        user = self.model(
            email=self.normalize_email(email),
            phone_number=phone_number,
        )

        # Set the users password. We use this to create a password
        # hash instead of storing it in clear text.
        user.set_password(password)
        user.save(using=self._db)

        return user

    def create_superuser(self, email, phone_number, password):
        """Creates and saves a new superuser with given detials."""

        # Create a new user with the function we created above.
        user = self.create_user(
            email,
            phone_number,
            password
        )

        # Make this user an admin.
        user.is_superuser = True
        user.is_staff = True
        user.save(using=self._db)

        return user


class User(AbstractBaseUser, PermissionsMixin):
    """A user profile in our system."""

    email = models.EmailField(max_length=255, unique=True)
    phone_number = models.CharField(max_length=20, unique=True)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    is_phone_verified = models.BooleanField(default=False)
    account_type = models.CharField(max_length=20, default='personal')
    phone_verification_token = models.CharField(max_length=50, blank=True, null=True)
    reset_pass_token = models.CharField(max_length=255, blank=True, null=True)
    invite_ref = models.CharField(max_length=255, blank=True, null=True)
    created_date = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    plan = models.CharField(
        max_length=12,
        choices=c.PLAN_CHOICES,
        default=c.FREE
    )
    pricing = models.DecimalField(
        max_digits=7,
        decimal_places=2,
        default=c.PRICING[c.FREE]
    )
    # tier_two = models.DecimalField(
    #     max_digits = 7,
    #     decimal_places = 3,
    #     default=c.PRICING[c.TIER_TWO]
    # )
    trial_period = models.IntegerField(default=2)

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['phone_number']


    def __str__(self):
        """What to show when we output an object as a string."""

        return self.email

    class Meta:
        verbose_name = "User"
        verbose_name_plural = "Users"

    @property
    def get_pricing(self):
        trial_left = self.trial_period - self.transactions.count()
        if trial_left >= 0:
            
            return c.PRICING[c.FREE]
       
        return c.PRICING[c.BASIC]

    @property
    def change_plan(self):
        if self.trial_period > 0:
            self.trial_period -= 1
            if self.trial_period ==0:
                self.pricing = c.PRICING[c.BASIC]
                self.plan = c.BASIC
                self.save()
            else:
                self.pricing = c.PRICING[c.FREE]
                self.plan = c.FREE
                self.save()
        else:
            self.pricing = c.PRICING[c.BASIC]
            self.plan = c.BASIC
            self.save()
