from django.test import TestCase, RequestFactory
from django.test import Client
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.utils import six
from django.shortcuts import render
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, \
    urlsafe_base64_decode
from rest_framework.test import APIClient, APIRequestFactory
from django.contrib.auth import get_user_model
from django.conf import settings
from django.utils import timezone
from user_account_setting.models import UserAccountSetting
import boto3
import json
# client = boto3.client(
#     "sns",
#     aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
#     aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY,
#     region_name="us-west-2"
# )

# This is because am using a custom user model
User = get_user_model()

# class TokenGeneratorTest(PasswordResetTokenGenerator):
#     def _make_hash_value(self, user, timestamp):
#         return (
#             six.text_type(user.pk) + six.text_type(timestamp) +
#             six.text_type(user.is_active)
#         )
# account_activation_token = TokenGeneratorTest()


class SignUpTest(TestCase):
    def setUp(self):
        self.credentials = {
            'email': 'kennedy@paylock.io',
            'phone_number': '+233246424340',
            'password': 'Password.2019',
            'is_active': True,
            'invite_ref': '',
            }
        self.login = {
            'email': 'kenmartey89@gmail.com',
            'password': 'Password.2019'
        }
        self.client = APIClient()
        # login_user = self.client.login(email='kenmartey89@gmail.com', password='Password.2019')
        self.user = User.objects.create_user(
            email='kenmartey89@gmail.com', password='Password.2019', phone_number='+233246424340')
       
        # print(">>÷>>>>>>>>>>>>",self.user)

    def test_signup(self):
        # send signup data
        # response = self.client.post('/api/v1.0/user/signup/', self.credentials
        #     )
        UserAccountSetting.objects.create(
            newsletter= True, 
            general_promotional_emails=True,
            user=self.user
        )
        # Should signup and return status code of 200
        print ('===================SIGN UP TEST=========================')
        # print ('>>> >>> >>> /SIGN UP TEST/ content: ', response.content)
        # self.assertEqual(response.status_code, 201)
        print ('===================END OF SIGN UP TEST=========================')
        print ('      ')

    
    def test_login(self):
        response = self.client.post('/api/v1.0/user/login/', self.login
            )
        print ('===================LOGIN  TEST=========================')
        print ('>>> >>> >>> /LOGIN TEST/ content: ', response)
        self.assertEqual(response.status_code, 200)
        print ('===================LOGIN TEST=========================')
        print ('      ')
    
# def test_activate_account_requirement(request, uidb64, token):
#     try:
#         uid = force_text(urlsafe_base64_decode(uidb64))
#         user = User.objects.get(pk=uid)
#     except(TypeError, ValueError, OverflowError, User.DoesNotExist):
#         user = None
#     if user is not None and account_activation_token.check_token(user, token):
#         user.is_active = True
#         user.save()
#         # login(request, user)
#         # return redirect('home')
#         rendered = test_after_activation_email_template(request)
#         return HttpResponse(rendered)
#     else:
#         return HttpResponse('Activation link is invalid!')
#         pass

# def test_after_activation_email_template(request, **kwargs):
#     """Activation page afer activating user account"""
#     template = 'account_activation_email/after_account_activation.html'
#     return render(request, template)


# def test_activate_account(self, parameter_list):
#     response = self.client.post('/api/v1.0/activate/', 
#             test_activate_account_requirement()
#             )
#     # Should signup and return status code of 200
#     print ('===================EMAIL ACTIVATION TEST=========================')
#     print ('>>> >>> >>> /SIGN UP TEST/ content: ', response.content)
#     self.assertEqual(response.status_code, 200)
#     print ('===================EMAIL ACTIVATION TEST========================')
#     print ('      ')

    # def test_welcome_user_with_text_message_after_signup(self):
    #     api_client = APIClient()
    #     headers = {
    #         'Content-Type':'application/json', 
    #         'Authorization': settings.INFOBIP_TOKEN
    #     }
    #     message = "Welcome to PayLock! "\
    #         "This platform helps secure your funds during a transaction. " \
    #         "Kindly complete your profile and enjoy our services. You can " \
    #         "send us an email on support@paylock.io for any assistance. " \
    #         "Thank you."   
    #     text_message = client.publish(
    #         PhoneNumber=instance.phone_number,
    #         Message=message
    #     )
    #     print("Headers >>>>>>>>", text_message)



# class LoginTest(TestCase):

#     def setUp(self):
#         # get login data
#         self.credentials = {
#             'email': 'kenmartey89@gmail.com',
#             'phone_number': '0246424340',
#             'password': 'ceremony',
#             # 'is_active': True
#             }
#         User.objects.create_user(**self.credentials)

#     def test_login(self):
#         global response
#         # instance of the test client
#         c = Client()
#         login_credentials =  c.login(email='kenmartey89@gmail.com', password='ceremony', is_active=True)
#         # user_login = self.assertTrue(login_credentials)
#         response = self.client.post('/api/v1.0/user/login/', 
#             self.credentials)
#         print ('===================LOGIN TEST=========================')
#         print (">>>> >>> >>> /LOGIN TEST/ content: ",response.content)
#         self.assertEqual(response.status_code, 200)
#         print ('===================END OF LOGIN TEST=========================')
        # print ('      ')

    # #  TEST TEXT MESSAGE WITH MNOTIFY
    # def test_welcome_user_with_text_message_after_signup(self):
        
    #     print("Text message information", request.text)

