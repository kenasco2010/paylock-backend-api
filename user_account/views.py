from django.shortcuts import render
from django.contrib.auth import authenticate, login, logout, get_user_model
from django.conf import settings
from django.db.models import Q
from rest_framework.authentication import TokenAuthentication
from rest_framework.status import (HTTP_201_CREATED,
                                   HTTP_200_OK,
                                   HTTP_400_BAD_REQUEST, HTTP_401_UNAUTHORIZED,
                                   HTTP_404_NOT_FOUND, HTTP_409_CONFLICT)
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework import viewsets
from rest_framework import permissions, status, generics, authentication
from rest_framework.decorators import detail_route, \
                    list_route, permission_classes, api_view, action
from rest_framework.authtoken.models import Token
import re
from .serializers import UserSerializer, ChangePasswordSerializer
from .models import User, UserManager
from .signals import *
from paylock_emails.views import send_wallet_activation_email, \
    after_activation_email_template, send_email_to_invite_collaborator, \
    email_transaction_partner_about_account

from paylock_emails.signals import invite_new_users, reset_paylock_account_password

# Activate your account
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from .tokens import account_activation_token
from django.http import HttpResponse
from wallet.models import activate_wallet, UserWallet
from user_account_setting.views import save_user_settings
from paylock.send_text import update_phone_number_text_message, \
        update_user_on_phone_number_change_attempt, verify_phonenumber_on_paylock_registration, \
        resend_phone_num_verification_token, phone_number_verification_after_invitation
from user_profile.views import create_collaborator_profile
import base64
import uuid
# Create your views here.
import random

def generate_random_phone_verification_token():
    """Generate random token for phone verification"""
    randtoken = ''.join(random.choice('0123456789ABCDEFXZSHYTINFHDHJWEKFNWJFHEFHGLMPQRUVAJ') for i in range(6))
    return randtoken


# def activate_account(request, uidb64, token):
#     """This handles the activation of paylock account and wallet after signup
    
#     """
#     try:
#         uid = force_text(urlsafe_base64_decode(uidb64))
#         user = User.objects.get(pk=uid)
#     except(TypeError, ValueError, OverflowError, User.DoesNotExist):
#         user = None
#     if user is not None and account_activation_token.check_token(user, token):
#         user.is_active = False # CHANGED IT FROM TRUE TO FALSE BECAUSE I WANT TO FORCE PEOPLE TO VERIFY WITH PHONE NUMBERS
#         user.save()

#         activate_wallet(user) # this piece of function activates the wallet once the acount is activated
#         # login(request, user)
#         # return redirect('home')
#         rendered = after_activation_email_template(request)
#         return HttpResponse(rendered)
#     else:
#         return HttpResponse('Activation link is invalid!')
def activate_wallet(request, uuid, wlc):
    """ This piece of code handles activation of user wallet 
        When user confirms his/her email
    """
    user_id = force_text(urlsafe_base64_decode(uuid))
    wallet_activation_code = force_text(urlsafe_base64_decode(wlc))
    try:
        user = User.objects.get(pk=user_id)
        # user_wallet = UserWallet.objects.get(user=user, activation_code=wallet_activation_code)
    except (TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None

    if user is None:
        return HttpResponse('Activation link is invalid! Contact admin on support@paylock.io')
    
    try:
        user_wallet = UserWallet.objects.get(user=user, activation_code=wallet_activation_code)
    except (TypeError, ValueError, OverflowError, UserWallet.DoesNotExist):
        user_wallet = None

    if user_wallet is None:
        return HttpResponse('Activation link is invalid! Signup or contact admin on support@paylock.io')
    
    if user_wallet:
        user_wallet.is_authorized = True
        user_wallet.activation_code = "activated"
        user_wallet.save()
        rendered = after_activation_email_template(request)
        return HttpResponse(rendered)
    # elif user_wallet.activation_code == "activated":
    #     return HttpResponse('Your wallet is already activated!, continue to load your wallet and create a transaction.')

def generate_reset_password_token():
    unique_id = get_random_string(length=50)
    return unique_id

def generate_wallet_activation_code():
    unique_id = get_random_string(length=50)
    return unique_id

def generate_user_password():
    unique_id = get_random_string(length=8)
    return unique_id


# def reset_password(request, uidb64, token):
#     """ This resetting of password after sending reset password email
    
#     """
#     try:
#         uid = force_text(urlsafe_base64_decode(uidb64))
#         user = User.objects.get(pk=uid)
#     except(TypeError, ValueError, OverflowError, User.DoesNotExist):
#         user = None
#     if user is not None and account_activation_token.check_token(user, token):
#         rendered = reset_password_form(request)
#         return HttpResponse(rendered)
#     else:
#         return HttpResponse('Reset password link is invalid!')


class UserViewSet(viewsets.ModelViewSet):
    """Handles creating a user and login"""
    permission_classes = (AllowAny,)
    serializer_class = UserSerializer
    queryset = User.objects.all().order_by('-id')

    @list_route(methods=('POST',),  permission_classes=(AllowAny,))
    def signup(self, request, *args, **kwargs):
        """
        Allows a user to signup to the platform using email
        POST PARAMETERS:
        email = email of the user
        phone_number = phone_number of the user
        password = Password of User
        invite_ref = get invite reference from user.
        Decode invite reference and save.
        """
        email = request.data.get('email', None)
        phone_number = request.data.get('phone_number', None)
        password = request.data.get('password', None)
        invite_ref = request.data.get('invite_ref')
        newsletter =request.data.get('newsletter')
        general_emails = request.data.get('general_emails')

        # checks if the input in the email field is valid with regex (re)
        validate_email = re.search(r'[\w.-]+@[\w.-]+.\w+', email)

        # check if email and phone_number is not empty
        if not email or not phone_number:
            return Response({'status_code': status.HTTP_401_UNAUTHORIZED,
                            'message': 'All fields are required, kindly fill them'
                            }, status=HTTP_401_UNAUTHORIZED)

        if not email or not validate_email:
            return Response({'status_code': status.HTTP_401_UNAUTHORIZED,
                             'message': 'please enter a valid email address',
                             }, status=status.HTTP_401_UNAUTHORIZED)

        if User.objects.filter(email=email).exists():
            return Response({'status_code': status.HTTP_409_CONFLICT,
                            'message': 'Email already exists. Please use a different email'
                             }, status=status.HTTP_409_CONFLICT)
        elif User.objects.filter(phone_number=phone_number).exists():
            return Response({'status_code': status.HTTP_409_CONFLICT,
                            'message': 'Phone number already exists. Please use a different number'
                             }, status=status.HTTP_409_CONFLICT)

        if email is not None and phone_number is not None \
            and password is not None:

            mymodel = get_user_model()
            user = User.objects.create_user(
                email=email,
                phone_number=phone_number
                
            )
            # decode invite ref and store.
            decoded_invite_ref = base64.b64decode(invite_ref)
            user.set_password(password)
            user.is_active = True
            user.invite_ref=decoded_invite_ref
            user.save()
            # wallet.create_wallet(user) # creates wallet afer a user is created.
            create_wallet = UserWallet.objects.create(
                user=user,
                backup_user = user.email,
                activation_code = generate_wallet_activation_code()
                )
            save_user_settings(user, newsletter, general_emails)
            send_wallet_activation_email(request, user, create_wallet) # Send account activation email
            randomtoken = generate_random_phone_verification_token()
            encoded_token = base64.b64encode(force_bytes(randomtoken))
            user.phone_verification_token = encoded_token
            user.save()
            # Send user verification to phone
            verify_phonenumber_on_paylock_registration(user, randomtoken)
            # user = authenticate(email=user.email,  password=password)
            # login(request, user)
            user = self.serializer_class(user)
            return Response({'status_code': status.HTTP_201_CREATED,
                             'message': 'You have successfully signed up. Please check your email to activate your account.',
                             'data': user.data
                             }, status=status.HTTP_201_CREATED)
        else:
            return Response({'status_code': status.HTTP_400_BAD_REQUEST,
                            'message': 'Please supply required parameters'
                             },status=status.HTTP_400_BAD_REQUEST)

    

    @list_route(methods=('POST',),  permission_classes=(AllowAny,))
    def login(self, request, *args, **kwargs):
        """
        Allow users to login using email and password

        POST PARAMETERS:
        email = Typed Email
        password = Of course Password typed
        """
        email = request.data.get('email', None)
        password = request.data.get('password', None)
        user = authenticate(email=email, password=password)
    
        if user is not None:
            if user.is_active == True:
                Token.objects.get(user=user).delete()
                Token.objects.create(user=user)
                # login(request, user)
                user.last_login = timezone.now()
                user.save()
                user = UserSerializer(user)
                return Response({"status_code": status.HTTP_200_OK,
                                 "message": "you have successfully logged in",
                                 "data": user.data
                                }, status=status.HTTP_200_OK)
            else:
                return Response({"status_code": status.HTTP_404_NOT_FOUND,
                                 "message": "This account is not activated yet. Confirm your phone token to activate or contact admin."
                                 }, status=status.HTTP_404_NOT_FOUND)
        else:
            return Response({"status_code": status.HTTP_400_BAD_REQUEST,
                            "message": "Invalid email or password"
                             }, status=status.HTTP_400_BAD_REQUEST)


    @action(methods=('POST',), detail=False, url_path='reset-password-email',
        permission_classes=(AllowAny,))
    def send_reset_password_email(self, request, *args, **kwargs):
        user = request.data.get('email')
        try:
            user = User.objects.get(email=user)
        except User.DoesNotExist:
            return Response({
                'status_code': HTTP_404_NOT_FOUND,
                'message': 'User email not found on PayLock'
            })
        if user:
            # Generate user token and store
            user.reset_pass_token = generate_reset_password_token()
            user.save()
            
            # Send email to paylock user to reset his email
            reset_paylock_account_password(user)
            return Response({
                'status_code': HTTP_200_OK,
                'message': 'We have sent a password reset link to your email.'
            })
    

    @action(methods=('POST',), detail=False, url_path='check-if-can-reset-password',
        permission_classes=(AllowAny,))
    def check_if_user_can_reset_password(self, request, *args, **kwargs):
        """

        """

        token = request.data.get('token', None)
        user_id = request.data.get('user_id', None)
        decoded_id = base64.b64decode(user_id)
        # Decode it to normal string
        normal_id = decoded_id.decode("utf-8")
        try:
            if token is not None and user_id is not None:
                user = User.objects.get(pk=normal_id, reset_pass_token=token)
            else:
                return Response({
                'status_code': HTTP_404_NOT_FOUND,
                'message': 'We are sorry something went wrong with your account credentials'
            })
        except User.DoesNotExist:
            return Response({
                'status_code': HTTP_404_NOT_FOUND,
                'message': 'We are sorry you cannot perform this operation '
                'since you are not registered in the system'
            }, status=status.HTTP_404_NOT_FOUND)
        if user:
            return Response({
                'status_code': HTTP_200_OK,
                'message': 'You can proceed to reset your password',
                'result': {
                    'id': normal_id,
                    'token': token

                }
            })


    @action(methods=('POST',), detail=False, url_path='reset-password',
        permission_classes=(AllowAny,))
    def reset_password(self, request, *args, **kwargs):
        """

        """
        user_id = request.data.get('user_id', None)
        token = request.data.get('token', None)

        new_password = request.data.get('new_password')
        try:
            user = User.objects.get(id=user_id, reset_pass_token=token)
        except User.DoesNotExist:
            return Response({
                'status_code': HTTP_404_NOT_FOUND,
                'message': 'We are sorry you cannot perform this operation '
                'since you are not registered in the system'
            })
        if user:
            user.set_password(new_password)
            user.reset_pass_token = None
            user.save()
            return Response({
                'status_code': HTTP_200_OK,
                'message': 'You have successfully reset your password'
            })
        else:
            return Response({
                'status_code': HTTP_404_NOT_FOUND,
                'message': 'Your password link has expired.'
            })
        


    @action(methods=('POST',), detail=False, url_path='invite-user-to-paylock',
        permission_classes=(IsAuthenticated,))
    def refer_user_to_join_paylock(self, request, *args, **kwargs):
        """ Invite users not on PayLock to join PayLock.
            Call the send email function to call
            Get invite user email
            Encode invitee user id
            Send email to invite email.
        """
        email = request.data.get('email')
        user_id = request.user.id
        encode_id = base64.b64encode(force_bytes(user_id))
        invitee_full_name = request.user.user_profile.first_name + ' ' + request.user.user_profile.last_name
        invitation_link = settings.DOMAIN_URL + 'account/signup?ref={}'.format(encode_id)
        invite_new_users(email, invitee_full_name, invitation_link,)
        return Response({"status_code": HTTP_200_OK,
                'message': 'You have successfully made a referral',
                
        })

    @action(methods=('POST',), detail=False, url_path='invite-collaborator-onboarding',
        permission_classes=(AllowAny,))
    def invite_collaborator_onboarding(self, request, *args,**kwargs):
        """Invite a collaborator by email in the onboarding process"""
        email = request.data.get('email')
        invitee_full_name = request.user.user_profile.first_name + ' ' + request.user.user_profile.last_name
        send_email_to_invite_collaborator(email, invitee_full_name)

        if not email:
            return Response({
            'status_code': HTTP_404_NOT_FOUND,
            'message': 'No email provided to invite collaborator'  
        })

        return Response({
            'status_code': HTTP_200_OK,
            'message': 'Invitation sent to {} to collaborate with you on your transaction'.format(email)     
        })

    @action(methods=('GET',), detail=False, url_path='generate-invite-link',
        permission_classes=(IsAuthenticated,))
    def invite_link(self, request, *args, **kwargs):
        user_id = request.user.id
        encoded_user_id = base64.b64encode(force_bytes(user_id))
        return Response({
            'status_code': HTTP_200_OK,
            'result': {
                'data': encoded_user_id
            }
            
        })

    @action(methods=('POST',), detail=False, url_path='edit-phone-number',
        permission_classes=(IsAuthenticated,))
    def edit_phone_number(self, request, *args, **kwargs):
        current_user = request.user
        user_id = request.user.id
        phone_number = request.data.get('phone_number')
        try:
            user = User.objects.get(id=user_id)
        except User.DoesNotExist:
            return Response({
                'status_code': HTTP_404_NOT_FOUND,
                'mesage': 'User does not exist'
            })
        if current_user:
            user.phone_number = phone_number
            user.save()
            update_phone_number_text_message(user)
        else:
            update_user_on_phone_number_change_attempt(user)
            return Response({
                'status_code': HTTP_400_BAD_REQUEST,
                'mesage': 'You are not authorized for this operation'
            })
        return Response({
                'status_code': HTTP_200_OK,
                'mesage': 'Phone number updated successfully.',
                
                
            })

    @action(methods=('POST',), detail=False, url_path='verify-phone-number', permission_classes=(AllowAny,))
    def activate_user_account_by_phone_token(self, request, *args, **kwargs):
        """ Activate user account by checking the token
            Accept token from user
            encode token
            Match encoded token in db
            activate user.
        """
        token = request.data.get('token')
        email = request.data.get('email')
        encoded_token = base64.b64encode(force_bytes(token))
        # base64.b64decode
        try:
            user = User.objects.get(email=email)  
        except User.DoesNotExist:
            return Response({
                'status_code': HTTP_404_NOT_FOUND,
                'message': "Please sign up if you haven't. " 
                "Your data is not currently on PayLock",
            })

        if str(encoded_token) != str(user.phone_verification_token):
            return Response({
                    'status_code': HTTP_404_NOT_FOUND,
                    'message': "Please check your verification code to make sure you have it right."
                }, status=status.HTTP_404_NOT_FOUND)

        if user.is_phone_verified == True:
            return Response({
                    'status_code': HTTP_200_OK,
                    'message': "You have already activated your account."
                }, status=status.HTTP_200_OK)
        else:
            print ("This should pass through the verification code.")
            user.is_active = True
            user.is_phone_verified = True
            user.save()
            return Response({
                'status_code': HTTP_200_OK,
                'message': "You have successfully activated your account."
            }, status=status.HTTP_200_OK) 

    @action(methods=('POST',), detail=False, url_path='resend-phone-verification-token', 
        permission_classes=(AllowAny,))
    def resend_phone_verification_token(self, request, *args, **kwargs):
        """ Resend Phone verification token.
            Accept user email and phone number
            call generate token function
            encode token, fetch user by email, save the token
            call a funtion to send email to the same number
        """
        email = request.data.get('email')  
        token = generate_random_phone_verification_token()
        encode_token = base64.b64encode(force_bytes(token))
        try:
            user = User.objects.get(email=email, is_active=True)  
        except User.DoesNotExist:
            return Response({
                'status_code': HTTP_404_NOT_FOUND,
                'message': "Please sign up if you haven't. " 
                "Your data is not currently on PayLock",
            }, status=status.HTTP_404_NOT_FOUND)
        user.phone_verification_token = encode_token
        user.save()
        resend_phone_num_verification_token(user, token)
        return Response({
                'status_code': HTTP_200_OK,
                'message': "Verification token has been sent to your number.",
            }, status=status.HTTP_200_OK)
    

    @action(methods=('POST',), detail=False, url_path='resend-wallet-activation-link', 
        permission_classes=(AllowAny,))
    def resend_email_verification_link(self, request, *args, **kwargs):
        """ Resend email verification link to user.
            Accept user email
            Check for user and wallet in the db, if they exist,
            call the send wallet activation email function
            
        """
        email = request.data.get('email')  
        # token = generate_random_phone_verification_token()
        # encode_token = base64.b64encode(force_bytes(token))
        try:
            user = User.objects.get(email=email, is_active=True)
            user_wallet = UserWallet.objects.get(user__email=email, is_authorized=False) 
        except User.DoesNotExist:
            return Response({
                'status_code': HTTP_404_NOT_FOUND,
                'message': "Please sign up if you haven't. " 
                "Your data is not currently on PayLock",
            }, status=status.HTTP_404_NOT_FOUND)
        send_wallet_activation_email(request, user, user_wallet) # Send account activation email
        return Response({
                'status_code': HTTP_200_OK,
                'message': "Email verification link has been sent to your email.",
            }, status=status.HTTP_200_OK)



    @action(methods=('POST',), detail=False, url_path='create-collaborator-account', 
        permission_classes=(AllowAny,))
    def create_collaborator_account(self, request, *args, **kwargs):
        """ This is to create the collaborators account when not on 
            PayLock during a transaction process.
        """
        user = request.user
        email = request.data.get('email')
        phone_number = request.data.get('phone_number')
        collab_firstname = request.data.get('first_name')
        collab_lastname = request.data.get('last_name')
        invitee_full_name = user.user_profile.first_name + ' ' + user.user_profile.last_name
        collaborator = User.objects.filter(email=email, phone_number=phone_number)
        phonenumber_or_email =User.objects.filter(
            Q(email=email) |
            Q(phone_number=phone_number))
        if phonenumber_or_email.exists():
            return Response({
                'status_code': HTTP_409_CONFLICT,
                'message': 'Email or phone number already exists on PayLock. ' 
                'Please use a different email or phone number. '
            }, status=status.HTTP_409_CONFLICT)
        if collaborator.exists():
            return Response({
                'status_code': HTTP_409_CONFLICT,
                'message': 'Transaction partner already exists on PayLock.'
            }, status=status.HTTP_409_CONFLICT)
            
        user = User.objects.create_user(
            email=email,
            phone_number=phone_number
        )
        save_user_settings(user, True, True) # Save user account settings.
        create_collaborator_profile(collab_firstname, collab_lastname, user) # Create collaborator profile after invitation.
        invite_ref = base64.b64encode(force_bytes(user.id))
        # Generate password and set
        temporal_password = generate_user_password()
        randomtoken = generate_random_phone_verification_token()
        encoded_token = base64.b64encode(force_bytes(randomtoken))
        user.phone_verification_token = encoded_token
        user.invite_ref = invite_ref
        user.set_password(temporal_password)
        user.save()
        # send activation email with password
        wallet = UserWallet.objects.create(
                user=user,
                backup_user = user.email,
                activation_code = generate_wallet_activation_code()
                )
        email_transaction_partner_about_account(request, user, invitee_full_name, temporal_password, wallet)
        # send phone number validation
        phone_number_verification_after_invitation(user, randomtoken)
        return Response({
            'status_code': HTTP_201_CREATED,
            'message': 'Collaborator has been created and added to the transaction.'
        })
        



class ChangePasswordViewSet(viewsets.ModelViewSet):
    """
    An endpoint for changing password.
    """
    serializer_class = ChangePasswordSerializer
    queryset = User.objects.all()
    permission_classes = (IsAuthenticated,)

    def get_object(self, queryset=None):
        obj = self.request.user
        return obj

    @action(methods=('POST',), detail=True, url_path='change')
    def change_password(self, request, *args, **kwargs):
        """This handles changing of user account password """

        self.object = self.get_object()
        serializer = self.get_serializer(data=request.data)

        if serializer.is_valid():
            # Check old password
            if not self.object.check_password(serializer.data.get("old_password")):
                return Response({"old_password": ["Wrong password."]}, status=status.HTTP_400_BAD_REQUEST)
            # set_password also hashes the password that the user will get
            self.object.set_password(serializer.data.get("new_password"))
            self.object.save()
            return Response("Password changed Successfully.", status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


#    About for merging sake