import string
from django.utils.crypto import get_random_string
from django.conf import settings
# from celery import shared_task
from celery.decorators import task
from slackclient import SlackClient
import urllib
import requests
# from paylock.send_text import verify_phonenumber_on_paylock_registration
from user_account.models import User

@task
def slack_notification_background_job(sender, instance, created):
    if created:
        slack_token = settings.SLACK_API_TOKEN
        sc = SlackClient(slack_token)
        sc.api_call(
        "chat.postMessage",
        username=settings.SLACK_USERNAME,
        icon_url='https://s3-us-west-2.amazonaws.com/paylock-assets/paylock_icon.png',
        channel=settings.SLACK_NEW_SIGNUPS_CHANNEL_ID,
        text="New user signed up on PayLock :tada:",
        attachments=[{
            "pretext": "email: " + instance.email, 
            "text": "phone number: " + instance.phone_number
            }]
        )
        # verify_phonenumber_on_paylock_registration(user, randomtoken)
        # welcome_user_text_message(instance)

@task
def send_text_msg_to_user_job(sender, instance, created):
    if created:
        headers = {
            'Authorization': settings.INFOBIP_TOKEN,
            'Content-Type':'application/json',
        }
        payload = {
        "to": instance.phone_number,
        "text": "Welcome to PayLock! "\
        "This platform helps secure your funds during a transaction. " \
        "Kindly complete your profile and enjoy our services. You can " \
        "send us an email on support@paylock.io for any assistance. " \
        "Thank you."
        }
        r = requests.post(
            'https://wnk41.api.infobip.com/sms/1/text/single',
            json=payload,
            headers=headers
            )