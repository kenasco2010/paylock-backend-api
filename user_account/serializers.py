from rest_framework import serializers
from . import models
from user_profile.models import UserProfile
from user_account_setting.serializers import UserAccountSettingSerializer
from wallet.serializers import UserWalletSerializer
from api_integration.serializers import ApiIntegrationSettingsSerializer


class UserProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserProfile
        fields = ('id', 'first_name', 'last_name', 'date_of_birth', 
                    'current_address', 'country','profile_image', 
                    'user','profile_completed','created_date' )

class UserSerializer(serializers.ModelSerializer):
    """A serializer for our user objects."""
    user_profile = UserProfileSerializer()
    user_setting = UserAccountSettingSerializer()
    wallet_authorized = serializers.ReadOnlyField(source='user_wallets.is_authorized')
    api_settings_user = ApiIntegrationSettingsSerializer()
    class Meta:
        model = models.User
        fields =('id', 'email', 'phone_number', 'password', 'account_type',
                'is_active', 'is_phone_verified', 'plan', 'pricing', 'invite_ref', 'trial_period', 
                'auth_token', 'user_profile','user_setting', 'wallet_authorized', 'api_settings_user'
                )
        extra_kwargs = {'password': {'write_only': True}} #

    def create(self, validated_data): # override the user creation
        """Create and return a new user."""

        user = models.User(
            email = validated_data['email'],
            phone_number = validated_data['phone_number']
            )

        user.set_password(validated_data['password'])
        user.save()

        return user


class ChangePasswordSerializer(serializers.Serializer):
    """
    Serializer for password change endpoint.
    """
    old_password = serializers.CharField(required=True)
    new_password = serializers.CharField(required=True)
