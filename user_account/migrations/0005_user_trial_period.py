# Generated by Django 2.0.6 on 2018-10-29 14:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user_account', '0004_auto_20181029_1158'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='trial_period',
            field=models.IntegerField(default=3),
        ),
    ]
