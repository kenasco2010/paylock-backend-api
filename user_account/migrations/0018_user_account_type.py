# Generated by Django 2.0.6 on 2019-10-28 15:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user_account', '0017_auto_20191015_1538'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='account_type',
            field=models.CharField(default='personal', max_length=20),
        ),
    ]
