# Generated by Django 2.0.6 on 2019-01-10 11:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user_account', '0011_user_invite_ref'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='reset_pass_token',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
    ]
