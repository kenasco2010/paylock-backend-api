# Generated by Django 2.0.6 on 2018-10-29 11:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user_account', '0003_auto_20181005_0908'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='plan',
            field=models.CharField(choices=[('B', 'Basic'), ('E', 'Enterprise')], default='B', max_length=1),
        ),
        migrations.AddField(
            model_name='user',
            name='pricing',
            field=models.DecimalField(decimal_places=2, default=0.05, max_digits=7),
        ),
    ]
