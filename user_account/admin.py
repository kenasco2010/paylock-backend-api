from django.contrib import admin
import csv
from django.http import HttpResponse
# Register your models here.
from .models import User
# from import_export import resources

class UserAdmin(admin.ModelAdmin):
    actions = ['export_as_csv']
    list_display  = ('email', 'phone_number', 'created_date', 'is_active', 'is_staff')
    
    def export_as_csv(self, request, queryset):
        meta = self.model._meta
        field_names = ['id','email', 'phone_number', 'created_date', 'is_active', 'is_staff']
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename={}.csv'.format(meta)
        writer = csv.writer(response)
        print(writer)
        writer.writerow(field_names)
        for obj in queryset:
            row = writer.writerow([getattr(obj, field) for field in field_names])
        return response
    export_as_csv.short_description = "Export Selected {}".format(User._meta.verbose_name_plural)
admin.site.register(User, UserAdmin)