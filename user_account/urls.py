from django.conf.urls import url
from django.conf.urls import include
from django.urls import path
from rest_framework.routers import DefaultRouter
from .views import UserViewSet, ChangePasswordViewSet
from user_account.views import activate_wallet

router = DefaultRouter()
router.register(r'v1.0/user', UserViewSet) # you don't need to specify base_name cos the models handle that for you.
router.register(r'v1.0/user/password', ChangePasswordViewSet)

urlpatterns = [
    path(r'', include(router.urls)),
    url(r'^activate/(?P<uuid>[0-9A-Za-z_\-]+)/(?P<wlc>[0-9A-Za-z]+)/$',
        activate_wallet, name='activate_wallet'),
    
    
]