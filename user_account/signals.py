from django.shortcuts import render
from django.core.mail import EmailMultiAlternatives
from django.template.loader import get_template
from django.template import Context
from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver
from django.conf import settings
from slackclient import SlackClient
import urllib
import requests
from django.utils import timezone
from user_account.models import User
from wallet.models import UserWallet
from user_account_setting.models import UserAccountSetting
# from paylock.send_text import welcome_user_text_message
from user_account_setting.views import account_settings_data

from user_account.tasks import *

# Create your views here.

@receiver(post_save, sender=User)
def send_slack_notification_on_new_user_signup(sender, instance, created=False, *args, **kwargs):
    """Send notification to slack when a new user joins PayLock"""
    slack_notification_background_job.delay(sender, instance, created=created)


@receiver(post_save, sender=User)
def send_text_msg_to_user(sender, instance, created=False, *args, **kwargs):
    """ Send text message to users when they sign up on PayLock"""
    send_text_msg_to_user_job.delay(sender, instance, created=created)



# @receiver(post_save, sender=User)
# def save_user_settings(user, newsletter, general_emails, created=False):
#     """ Check if user has a setting model
#         Update if user setting model exist
#         Create a new model and save setting if not existing.

#     """
#     if created:
#         UserAccountSetting.objects.create(
#             newsletter = newsletter,
#             general_promotional_emails =  general_emails,
#             user = user
#         )
#     user = user
#     newsletter = newsletter
#     general_emails = general_emails
#     #  Check if user setting model already exist and update, 
#     # else create a new user settings model and update
#     try:
#         user = UserAccountSetting.objects.get(user=user)
#         if user:
#             user.newsletter = newsletter
#             user.general_promotional_emails = general_emails
#             user.save()
#             return Response({'statu_code': HTTP_200_OK})
#     except UserAccountSetting.DoesNotExist:
#         UserAccountSetting.objects.create(
#             newsletter = newsletter,
#             general_promotional_emails =  general_emails,
#             user = user
#         )
#         return Response({'status_code': HTTP_200_OK})