from django.core.management.base import BaseCommand
from user_account.models import User


class Command(BaseCommand):

    def handle(self, *args, **options):
        if not User.objects.filter(phone_number="+233246424340").exists():
            User.objects.create_superuser("kenmartey89@gmail.com", "+233246424340", "ceremony")
