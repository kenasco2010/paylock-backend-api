from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver
from django.utils import timezone
from django.conf import settings
from .models import Dispute
import requests

@receiver(post_save, sender=Dispute)
def send_dispute_info_to_slack(instance, created=False, **kwargs):
    # balance = float(instance.amount) + float(instance.markup)
    if created:
        # SEND SLACK NOTIFICATION IF FUNDS IS WITHDRAWN.
        headers = {
            'content_tyep': 'application/json'
        }
        webhook = settings.DISPUTE_WEBHOOK
        content = 'Transaction' +' Dispute '+ '\n' + 'request: ' + \
            instance.transaction.name + ' - ' + str(instance.transaction.transaction_type) + \
            '\n Transaction Amount: $' + str(instance.transaction.amount) + \
            '\n Dispute number: ' + instance.dispute_number + '\n ----------------------------------------------'
        data = {
            "text": content
        }
        request = requests.post(webhook, json=data, headers=headers)
