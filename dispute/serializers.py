from rest_framework import serializers
from transaction.models import Transaction
from transaction.serializers import TransactionSerializer
from .models import Dispute, Comment
from legal_firm.serializers import InvitedLegalRepSerializer

class DisputeTransactionListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Transaction
        fields =('id', 'name')

class CommentSerializer(serializers.ModelSerializer):
    user = serializers.EmailField()
    class Meta:
        model = Comment
        fields = ('id', 'comment', 'user', 'current_user_id', 'approved_comment',
        'first_name','user_profile_image', 'date_created', 
        'comment_document',
        )

class DisputeSerializer(serializers.ModelSerializer):
    transaction_name = serializers.CharField(source='transaction.name')
    transaction_type  = serializers.CharField(source='transaction.transaction_type')
    amount  = serializers.CharField(source='transaction.amount')
    transaction_stage  = serializers.CharField(source='transaction.stage')
   

    class Meta:
        model = Dispute
        fields = ('id', 'dispute_number', 'reason', 'stage', 'user', 'dispute_creator',
            'dispute_against', 'supporting_document','date_created', 'modified', 
            'transaction_name', 'transaction_type', 'amount', 'transaction_stage', 'comments')




class DisputeDetailSerializer(serializers.ModelSerializer):
    # comments = CommentSerializer(many=True)
    transaction_name = serializers.CharField(source='transaction.name')
    transaction_type  = serializers.CharField(source='transaction.transaction_type')
    amount  = serializers.CharField(source='transaction.amount')
    markup = serializers.CharField(source='transaction.markup')
    transaction_stage  = serializers.CharField(source='transaction.stage')
    transaction_owner_email = serializers.CharField(source='transaction.user')
    collaborator_email = serializers.CharField(source='transaction.contractor')
    owner_full_name = serializers.SerializerMethodField()
    collaborator_full_name = serializers.SerializerMethodField()
    dispute_legal_rep = InvitedLegalRepSerializer(many=True)

    class Meta:
        model = Dispute
        fields = ('id', 'dispute_number', 'reason', 'stage', 'user', 'dispute_creator',
            'dispute_against', 'supporting_document','date_created', 'modified',
            'transaction_name', 'transaction_type', 'amount', 'markup', 'transaction_stage', 
            'transaction_owner_email','collaborator_email','owner_full_name',
            'collaborator_full_name', 'dispute_legal_rep'
            )

    def get_owner_full_name(self, dispute):
        return '{} {}'.format(
            dispute.transaction.user.user_profile.first_name, 
            dispute.transaction.user.user_profile.last_name
            ) 
    
    def get_collaborator_full_name(self, dispute):
        return '{} {}'.format(
            dispute.transaction.contractor.user_profile.first_name,
            dispute.transaction.contractor.user_profile.last_name
        )
# To show dispute related documents on separate endpoint
class DisputeCommentDocumentsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comment
        fields = ('id', 'comment_document')

class DisputeDocumentSerializer(serializers.ModelSerializer):
    comments = DisputeCommentDocumentsSerializer(many=True)
    
    class Meta:
        model = Dispute
        fields = ('id', 'dispute_number', 'supporting_document','date_created', 'modified',
            'comments',
            )

# To show dispute comments on separate endpoint
class DisputeCommentsOnlySerializer(serializers.ModelSerializer):
    username = serializers.SerializerMethodField()
    class Meta:
        model = Comment
        fields = ('id', 'comment','user',  'username','date_created','comment_document',
        'approved_comment'
         )

    def get_username(self, dispute):
        return '{} {}'.format(
            dispute.user.user_profile.first_name,
            dispute.user.user_profile.last_name
            ) 
class DisputeDetailCommentSerializer(serializers.ModelSerializer):
    comments = DisputeCommentsOnlySerializer(many=True)
    class Meta:
        model = Dispute
        fields = ('id', 'dispute_number', 'date_created', 'modified',
            'comments',
            )
    