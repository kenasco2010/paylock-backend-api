from django.contrib import admin
from .models import Dispute, Comment
# Register your models here.
class DisputeAdmin(admin.ModelAdmin):
    list_display = ('dispute_number', 'transaction', 'stage', 'user', 'date_created')

admin.site.register(Dispute, DisputeAdmin)
admin.site.register(Comment)