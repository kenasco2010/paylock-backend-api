# Generated by Django 2.0.6 on 2019-05-07 18:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dispute', '0038_auto_20190507_1536'),
    ]

    operations = [
        migrations.AlterField(
            model_name='dispute',
            name='dispute_number',
            field=models.CharField(default='4CC9716', max_length=15),
        ),
    ]
