# Generated by Django 2.0.6 on 2019-04-03 21:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dispute', '0027_auto_20190330_1855'),
    ]

    operations = [
        migrations.AlterField(
            model_name='dispute',
            name='dispute_number',
            field=models.CharField(default='9B28D99', max_length=15),
        ),
    ]
