# Generated by Django 2.0.6 on 2019-05-15 21:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dispute', '0043_auto_20190515_2111'),
    ]

    operations = [
        migrations.AlterField(
            model_name='dispute',
            name='dispute_number',
            field=models.CharField(default='EFDC576', max_length=15),
        ),
    ]
