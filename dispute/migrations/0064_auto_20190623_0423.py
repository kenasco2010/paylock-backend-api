# Generated by Django 2.0.6 on 2019-06-23 04:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dispute', '0063_auto_20190621_2217'),
    ]

    operations = [
        migrations.AlterField(
            model_name='dispute',
            name='dispute_number',
            field=models.CharField(default='A36DBF4', max_length=15),
        ),
    ]
