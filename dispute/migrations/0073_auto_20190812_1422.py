# Generated by Django 2.0.6 on 2019-08-12 14:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dispute', '0072_auto_20190807_1524'),
    ]

    operations = [
        migrations.AlterField(
            model_name='dispute',
            name='dispute_number',
            field=models.CharField(default='DBCE410', max_length=15),
        ),
    ]
