# Generated by Django 2.0.6 on 2019-02-28 16:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dispute', '0007_auto_20190228_1522'),
    ]

    operations = [
        migrations.AlterField(
            model_name='dispute',
            name='dispute_number',
            field=models.CharField(default='A8F928C', max_length=15),
        ),
    ]
