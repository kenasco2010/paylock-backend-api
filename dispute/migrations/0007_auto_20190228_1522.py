# Generated by Django 2.0.6 on 2019-02-28 15:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dispute', '0006_auto_20190226_1943'),
    ]

    operations = [
        migrations.AlterField(
            model_name='dispute',
            name='dispute_number',
            field=models.CharField(default='C6CD149', max_length=15),
        ),
    ]
