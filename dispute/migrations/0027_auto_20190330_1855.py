# Generated by Django 2.0.6 on 2019-03-30 18:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dispute', '0026_auto_20190330_1705'),
    ]

    operations = [
        migrations.AlterField(
            model_name='dispute',
            name='dispute_number',
            field=models.CharField(default='13D5512', max_length=15),
        ),
    ]
