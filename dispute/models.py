from django.db import models

from django.conf import settings
from django.utils import timezone
from decimal import Decimal
from django.utils.crypto import get_random_string
from django.contrib.contenttypes.fields import GenericRelation
from transaction.models import Transaction
from dashboard_notification.models import Notification
from paylock.choices import DISPUTE_STAGE
import uuid

AUTH_USER_MODEL = getattr(settings, 'AUTH_USER_MODEL', 'auth.User')



def unique_dispute_id(string_length=10):
    """
        Returns a random string of length string_length.
    
    """
    random = str(uuid.uuid4()) # Convert UUID format to a Python string.
    random = random.upper() # Make all characters uppercase.
    random = random.replace("-","") # Remove the UUID '-'.
    return random[0:string_length] # Return the random string.


class Dispute(models.Model):
    dispute_number = models.CharField(
        max_length=15,
        default=unique_dispute_id(7)
        )
    transaction = models.ForeignKey(Transaction, 
        related_name='dispute_transaction', 
        on_delete=models.CASCADE
    )
    reason = models.TextField(
        default='Not satisfied with transaction'
    )
    stage = models.CharField(
        max_length=50,
        choices=DISPUTE_STAGE,
        default='pending'
    )
    dispute_creator = models.ForeignKey(
        AUTH_USER_MODEL, related_name='dispute_creator', null=True, 
        on_delete=models.SET_NULL
        )
    dispute_against = models.ForeignKey(
        AUTH_USER_MODEL, related_name='dispute_against', null=True, 
        on_delete=models.SET_NULL
        )
    dispute_legal_advisor = models.ForeignKey(
        AUTH_USER_MODEL, related_name='legal_rep', null=True, 
        on_delete=models.SET_NULL, blank=True
        ) 
    user = models.ForeignKey(
        AUTH_USER_MODEL, related_name='dispute', null=True, 
        on_delete=models.SET_NULL
        )
    supporting_document = models.FileField(
        upload_to='dispute_supporting_document',
        blank=True, null=True
        )
    notification = GenericRelation(Notification, related_query_name="dispute")
    date_created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.dispute_number

    def __str__(self):
        return self.dispute_number


class Comment(models.Model):
    dispute = models.ForeignKey(Dispute, 
        related_name='comments', 
        on_delete=models.CASCADE
    )
    comment = models.TextField(

    )
    user = models.ForeignKey(
        AUTH_USER_MODEL, related_name='comment_user', null=True, 
        on_delete=models.SET_NULL
        )
    comment_document = models.FileField(
        upload_to='dispute_comment_document',
        blank=True, null=True
        )
    approved_comment = models.BooleanField(default=False)
    date_created = models.DateTimeField(auto_now_add=True)
    
    def __unicode__(self):
        return self.dispute.dispute_number

    def __str__(self):
        return self.dispute.dispute_number
    
    @property
    def user_profile_image(self):
        return '{}'.format(
            self.user.user_profile.profile_image
            )

    @property
    def current_user_id(self):
        return '{}'.format(
            self.user.id
            )
            
    @property
    def first_name(self):
        return '{}'.format(
            self.user.user_profile.first_name
        )






