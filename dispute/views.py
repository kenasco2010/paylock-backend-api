from django.shortcuts import render
from django.db.models import Q
from django.contrib.auth import authenticate, login, logout, get_user_model
from django.utils import timezone
from rest_framework.authentication import TokenAuthentication
from rest_framework.status import (HTTP_201_CREATED,
                                   HTTP_200_OK,
                                   HTTP_400_BAD_REQUEST, HTTP_401_UNAUTHORIZED,
                                   HTTP_404_NOT_FOUND)
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework import viewsets
from rest_framework import permissions, status, generics, authentication
from rest_framework.decorators import action
from decimal import Decimal
from paylock.permissions import IsOwner, IsSuperUserPermission
from paylock.paginations import StandardResultsSetPagination
from .models import Dispute, Comment
from transaction.models import Transaction
from .serializers import *
from paylock_emails.signals import send_email_to_paylock_on_dispute, respond_to_dispute_email, \
        resolve_dispute_email, send_email_to_legal_rep_about_dispute, \
        send_email_about_legal_fees_payment, send_email_to_dispute_against_about_comment, \
        send_email_to_dispute_owner_about_comment, send_email_to_both_dipuste_parties_about_legal_rep_comment
from paylock_emails.views import send_email_to_collaborator_about_dispute, \
        email_transaction_owner_about_dispute,  copy_of_dispute_to_transaction_owner, \
        copy_of_dispute_to_collaborator
from .signals import send_dispute_info_to_slack
from legal_firm.models import LegalRep, InviteLegalRep
from legal_firm.views import invite_legal_rep
from wallet.models import UserWallet
from payment.views import update_payment_history

    
class DisputeViewSet(viewsets.ModelViewSet):
    """ Show a list of all disputes on PayLock.

    """
    permission_classes = (IsAuthenticated,)
    serializer_class = DisputeSerializer
    queryset = Dispute.objects.all()

    @action(methods=('GET',), detail=False, url_path='transactions', 
        permission_classes=(IsAuthenticated,))
    def dispute_transactions(self, request, *args, **kwargs):
        """ List of transactions that can undergo dispute resolution.
            These lists are supposed to be loaded in the select box 
            in the dispute form.
        """
        user = request.user
        transactions = Transaction.objects.filter(
            Q(user=user, is_archived=False, stage='dispute') |
            Q(contractor=user, is_archived=False, stage='dispute')
            ).order_by('-id')
        serialized_transactions = DisputeTransactionListSerializer(transactions, many=True)
        return Response({
            'status_code': HTTP_200_OK,
            'message': 'Transactions available for dispute resolution',
            'result': {
                'data': serialized_transactions.data
            }
            })

    @action(methods=('POST',), detail=False, url_path='create', 
        permission_classes=(IsAuthenticated,))
    def create_a_dispute(self, request, *args, **kwargs):
        """
            Get the dispute details from the user
            Check if the transaction is not already in dispute.
            Create a dispute.

        """
        user = request.user
        transaction = Transaction.objects.get(pk=request.data.get("transaction"))
        transaction_stage = ['in_dispute', 'initialized', 'ongoing', 'completed', 'canceled', 'dispute_resolved']
        if transaction.stage in transaction_stage:
            return Response({
                'status_code': HTTP_400_BAD_REQUEST,
                'message': 'This transaction is not qualified for dispute resolution.'
                })
        else:
            dispute_details = {
                'transaction': transaction,
                'reason': request.data.get('reason'),
                'user': user,
                'supporting_document': request.FILES.get('supporting_document'),
                'date_created': timezone.now()
            }
            dispute = Dispute.objects.create(**dispute_details)
            transaction.stage = 'in_dispute'
            transaction.save()
            # Write a function to do the following:
            send_email_to_paylock_on_dispute(dispute)
            # Send email transaction owner about dispute or collaborator
            if dispute.user == transaction.user:
                dispute.dispute_creator = transaction.user
                dispute.dispute_against = transaction.contractor
                dispute.save()
                # print ('The transaction owner is the one creating the dispute')
                # meaning it's the transaction owner creating the dispute
                # send email to contractor about the dispute 
                send_email_to_collaborator_about_dispute(dispute)
                # a copy of the dispute to transaaction owner.
                copy_of_dispute_to_transaction_owner(dispute)
                # and to transaction owner about what he has done and what to do next
            elif dispute.user == transaction.contractor:
                dispute.dispute_creator = transaction.contractor
                dispute.dispute_against = transaction.user
                dispute.save()
                # print ('The Contractor is the one creating the dispute')
                # send email to transaction owner that a collaborator has raised 
                email_transaction_owner_about_dispute(dispute)
                # send copy of the dispute info to collaborator
                copy_of_dispute_to_collaborator(dispute)
                # dispute against them.
            serialize = self.serializer_class(dispute)
            return Response({
                'status_code': HTTP_200_OK,
                'message': 'Dispute successfully created.',
                'result': {
                    'data': serialize.data
                }
                })


    @action(methods=('GET',), detail=False, url_path='list', 
        permission_classes=(IsAuthenticated,))
    def list_of_disputes(self, request, *args, **kwargs):
        """ List of disputes you created and 
            list of disputes you have been added to.

        """
        user = request.user
        paginator = StandardResultsSetPagination()
        disputes = Dispute.objects.filter(Q(dispute_creator=user) | 
            Q(dispute_against=user) | Q(dispute_legal_advisor=user)
        ).order_by('-id')
        result_page = paginator.paginate_queryset(disputes, request)
        serialize_disputes = self.serializer_class(result_page, many=True)
        paginated_disputes = paginator.get_paginated_response(serialize_disputes.data)
        return Response({
                'status_code': HTTP_200_OK,
                'message': 'Your disputes list',
                'result': {
                    'data': paginated_disputes.data
                }
        })

# Function to display the dispute details page with comments and other information related to the dispute.
    @action(methods=('GET',), detail=False, url_path='detail', 
        permission_classes=(IsAuthenticated,))
    def dispute_detail(self, request, *args, **kwargs):
        """ View details page for a particular dispute
            This details page points to a Dispute Detail serializer.
        """
        user = request.user
        #Replace functions with decorators
        
        dispute_id = request.query_params.get('dispute_id') 
        try:
            dispute = Dispute.objects.prefetch_related(
            'comments').get(id=dispute_id)
        except Dispute.DoesNotExist:
            return Response({
                'status_code': HTTP_404_NOT_FOUND,
                'message': 'Dispute information not found'
            })
            
        if dispute.dispute_creator == user or dispute.dispute_against == user or user.is_staff == True:
            comment_count = Comment.objects.filter(dispute__id=dispute_id).count()
            serialize = DisputeDetailSerializer(dispute)
            return Response({
                    'status_code': HTTP_200_OK,
                    'message': 'Dispute detail',
                    'result': {
                        'data': serialize.data,
                        'comment_count': comment_count
                    }
            })
        else:
            return Response({
                    'status_code': HTTP_401_UNAUTHORIZED,
                    'message': 'You are not authorized to view this page',
            })
        


# Function to display all documents associated to a particular dispute in the dispute detail page.
    @action(methods=('GET',), detail=False, url_path='detail/comments', 
        permission_classes=(IsAuthenticated,))
    def dispute_detail_comments_only(self, request, *args, **kwargs):
        """ View all comments made on a particular dispute in the 
            details page.
            This function points to dispute document serializer.
        """
        user = request.user
        #Replace functions with decorators
        
        dispute_id = request.query_params.get('dispute_id')
        try:
            dispute = Dispute.objects.prefetch_related(
            'comments').get(id=dispute_id)
        except Dispute.DoesNotExist:
            return Response({
                'status_code': HTTP_404_NOT_FOUND,
                'message': 'Dispute information not found'
            })
        
        if dispute.dispute_creator == user or dispute.dispute_against == user or user.is_staff == True:
            comment_count = Comment.objects.filter(dispute__id=dispute_id).count()
            serialize = DisputeDetailCommentSerializer(dispute)
            return Response({
                    'status_code': HTTP_200_OK,
                    'message': 'Dispute comments',
                    'result': {
                        'data': serialize.data,
                        'comment_count': comment_count
                    }
            })
        else:
            return Response({
                    'status_code': HTTP_401_UNAUTHORIZED,
                    'message': 'You are not authorized to view this page',
            })
        


# Function to display all documents associated to a particular dispute in the dispute detail page.
    @action(methods=('GET',), detail=False, url_path='detail/supporting-documents', 
        permission_classes=(IsAuthenticated,))
    def dispute_detail_documents(self, request, *args, **kwargs):
        """ View documents attached to a particular dispute in the 
            details page.
            This function points to dispute document serializer.
        """
        user = request.user
        #Replace functions with decorators
        
        dispute_id = request.query_params.get('dispute_id')
        try:
            dispute = Dispute.objects.prefetch_related(
            'comments').get(id=dispute_id)
        except Dispute.DoesNotExist:
            return Response({
                'status_code': HTTP_404_NOT_FOUND,
                'message': 'Dispute information not found'
            })
        
        if dispute.dispute_creator == user or dispute.dispute_against == user or user.is_staff == True:
            comment_count = Comment.objects.filter(dispute__id=dispute_id).count()
            serialize = DisputeDocumentSerializer(dispute)
            return Response({
                    'status_code': HTTP_200_OK,
                    'message': 'Dispute documents',
                    'result': {
                        'data': serialize.data,
                        'comment_count': comment_count
                    }
            })
        else:
            return Response({
                    'status_code': HTTP_401_UNAUTHORIZED,
                    'message': 'You are not authorized to view this page',
            })
        

    # This function allows a dispute against person to respond to dispute raised against him/her
    @action(methods=('POST',), detail=False, url_path='respond', 
        permission_classes=(IsAuthenticated,))
    def confirm_dispute(self, request, *args, **kwargs):
        """ Responding to the dispute created on PayLock.
            Accept dispute ID
            Accept dispute against user ID
            Check system to see if dispute is found and
            dispute againt ID is same to Logged in user.
        """
        user = request.user
        dispute_id = request.data.get('dispute_id')
        dispute_against_id = request.data.get('dispute_against_id')
        try:
            dispute = Dispute.objects.get(id=dispute_id, dispute_against__id=dispute_against_id)
            if dispute.dispute_against == user:
                dispute.stage = 'in_progress'
                dispute.save()
                respond_to_dispute_email(dispute)
                return Response({
                    'status_code': HTTP_200_OK,
                    'message': 'You have responded to this dispute. The two parties have 16 days' 
                    ' to respond to before going for arbitration option'
                    })
            else:
                return Response({
                    'status_code': HTTP_404_NOT_FOUND,
                    'message': 'Operation not allowed. You are either not the respondent to '
                    'this dispute or it\'s not in the right state to be responded to.'
                    })
        except Dispute.DoesNotExist:
            return Response({
                'status_code': HTTP_404_NOT_FOUND,
                'message': 'Operation not allowed. '
                'Dispute doesn\'t exist'
                })
    
    
    @action(methods=('POST',), detail=False, url_path='resolve',
        permission_classes=(IsAuthenticated,))
    def resolve_dispute(self, request, *args, **kwargs):
        """ Resolving to the dispute created on PayLock.
            Accept dispute ID
            Accept dispute creator user ID
            Check system to see if dispute is found and
            dispute againt ID is same to Logged in user.
        
        """
        user = request.user 
        dispute_id = request.data.get('dispute_id')
        dispute_creator_id = request.data.get('dispute_creator_id')
        try:
            dispute = Dispute.objects.get(id=dispute_id, dispute_creator__id=dispute_creator_id)
            if dispute.dispute_creator == user and dispute.stage == 'in_progress':
                dispute.stage = 'resolved'
                dispute.save()
                dispute.transaction.stage='dispute_resolved'
                dispute.transaction.save()
                resolve_dispute_email(dispute)
                return Response({
                    'status_code': HTTP_200_OK,
                    'message': 'You have resolved this dispute'
                    })
            else:
                return Response({
                    'status_code': HTTP_404_NOT_FOUND,
                    'message': 'Operation not allowed. This dispute is not in the right stage'
                    ' to be resolved or you are not the creator of the dispute.'
                    })
        except Dispute.DoesNotExist:
            return Response({
                'status_code': HTTP_404_NOT_FOUND,
                'message': 'Operation not allowed. '
                'Dispute doesn\'t exist'
                })
    
    @action(methods=('POST',), detail=False, url_path='add-comment',
        permission_classes=(IsAuthenticated,))
    def add_dispute_comment(self, request, *args, **kwargs):
        """ Add comments related to a particular dispute.
            Get the dispute ID
            Fetch the dispute
            Add comment / attach document for proof.
        """
        user = request.user
        dispute_id = request.data.get('dispute_id')
        try: # Check if Dispute is on PayLock
            dispute = Dispute.objects.get(pk=dispute_id)
        except Dispute.DoesNotExist:
            return Response({
                'status_code': HTTP_404_NOT_FOUND,
                'message': 'Dispute information not found.',
                })

        # try: # Check if Legal person has been invited to a dispute on PayLock
        #     legal_invite = InviteLegalRep.objects.get(dispute=dispute)
        # except InviteLegalRep.DoesNotExist:
        #     return Response({
        #         'status_code': HTTP_404_NOT_FOUND,
        #         'message': 'Legal rep not added to a dispute on PayLock.',
        #         })

        comment = {
            'dispute': dispute,
            'comment': request.data.get('comment'),
            'comment_document': request.FILES.get('comment_document'),
            'user': user,
            'approved_comment': True,
            'date_created': timezone.now()
        }
        if not comment['comment']:
            return Response({
                'status_code': HTTP_400_BAD_REQUEST,
                'message': 'Dispute comment cannot be left empty. Please write your opinion.'
            }, status=status.HTTP_400_BAD_REQUEST)

        comment = Comment.objects.create(**comment)
        print (comment)
        if user == dispute.dispute_creator:
            # send email to dispute against about comment
            send_email_to_dispute_against_about_comment(dispute, comment)
        elif user == dispute.dispute_against:
            send_email_to_dispute_owner_about_comment(dispute, comment)
            # send email to dispute creator about comment
        # elif user == legal_invite.legal_rep.user:
        # send email to both owner and against
            send_email_to_both_dipuste_parties_about_legal_rep_comment(dispute, comment)
        serialized_comment = CommentSerializer(comment)
        return Response({
                'status_code': HTTP_201_CREATED,
                'message': 'Comment added successfully',
                'result': {
                    'data': serialized_comment.data
                }
                }, status=status.HTTP_201_CREATED)

    @action(methods=('POST',), detail=False, url_path='invite-legal-rep',
            permission_classes=(IsAuthenticated,))
    def invite_legal_rep_to_dispute(self, request, *args, **kwargs):
        """ Invite a legal representative to join a dispute
            Get the ID of the lawyer
            Get the ID of the invitor and Check his/her account balance
            Deduct the money being charged by the lawyer 
            Create InviteLegalRep object
            Allow legal person be able to add comment in the conversation.
        """
        user = request.user
        # user_balance = request.user.user_wallets.balance
        legal_rep_id = request.data.get('rep_id')
        dispute_id = request.data.get('dispute_id')

        try:
            dispute = Dispute.objects.get(id=dispute_id)
        except Dispute.DoesNotExist:
            return Response({
                'status_code': HTTP_404_NOT_FOUND,
                'message': 'Dispute not found with the ID'

                }, status=status.HTTP_404_NOT_FOUND)

        try:
            legal_rep = LegalRep.objects.get(id=legal_rep_id)
        except LegalRep.DoesNotExist:
            return Response({
                'status_code': HTTP_404_NOT_FOUND,
            'message': 'Legal Rep not found with the ID'

                }, status=status.HTTP_404_NOT_FOUND)
        # wallet = UserWallet.objects.all()

        # Check if this arbitrator has been invited already.
        try:
            invted_legal_rep = InviteLegalRep.objects.get(dispute=dispute, legal_rep=legal_rep)
            return Response({
                'status_code': HTTP_400_BAD_REQUEST,
                'message': 'You have already invited this arbitrator, you can select another availabel arbitrator.'

                }, status=status.HTTP_400_BAD_REQUEST)
        except InviteLegalRep.DoesNotExist:
            pass

        legal_rep_wallet= UserWallet.objects.get(user=legal_rep.user)
        user_balance = UserWallet.objects.get(user=user)
        if legal_rep.status == 'available':
            rep_charge_rate = legal_rep.charge_rate
            
            if user_balance.balance < rep_charge_rate:
                return Response({
                'status_code': HTTP_400_BAD_REQUEST,
                'message': 'Please top up your wallet to enable you invite an arbitrator'

                }, status=status.HTTP_400_BAD_REQUEST)
            else:
                invite_legal_rep_details = InviteLegalRep.objects.create( # Import function from legal_firm views
                    legal_rep = legal_rep,
                    dispute = dispute,
                    invitor = user,
                    created_date = timezone.now()
                )
                user_balance.balance -= rep_charge_rate #Deduct the legal rep charge rate from user wallet balance
                user_balance.last_used = timezone.now()
                user_balance.save()
                # Update legal rep wallet balance
                legal_rep_wallet.balance +=Decimal(rep_charge_rate)
                legal_rep_wallet.last_used = timezone.now()
                legal_rep_wallet.save()
                # Update dispute with legal advisor
                dispute.dispute_legal_advisor = legal_rep.user
                dispute.save()
                update_payment_history(
                    'Legal Rep Payment',
                    'Paying legal rep for inviting to join dispute',
                    rep_charge_rate,
                    'completed',
                    user,
                    timezone.now(),
                    timezone.now())
                # Send email to legal officer about dispute.
                send_email_to_legal_rep_about_dispute(invite_legal_rep_details)
                # Send email to invitor about amount deducted from his account
                send_email_about_legal_fees_payment(invite_legal_rep_details)
                return Response({
                'status_code': HTTP_201_CREATED,
                'message': 'Legal rep has been added to your conversation.'

                }, status=status.HTTP_201_CREATED)
        return Response({
                'status_code': HTTP_200_OK,
                'message': 'Legal rep is not available at the moment.'

                }, status=status.HTTP_200_OK)
        
        # invitor_id = request.data.get('invitor_id')
        # legal_rep_id = request.data.get('legal_rep_id')
