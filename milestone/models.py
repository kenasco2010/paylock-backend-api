from django.db import models
from django.conf import settings
from django.utils import timezone
from datetime import date
from transaction.models import Transaction
AUTH_USER_MODEL = getattr(settings, 'AUTH_USER_MODEL', 'auth.User')

# Create your models here.
class Milestone(models.Model):
    transaction = models.ForeignKey(
        Transaction,
        related_name='milestones',
        on_delete=models.CASCADE
    )
    feature = models.CharField(
        max_length=400

    )
    description = models.TextField(
        blank=True
    )
    amount = models.DecimalField(
        max_digits=9, 
        decimal_places=2, 
        default=0.0)
    due_date = models.DateField(
        default=date.today
    )
    stage = models.CharField(
        max_length=50,
        default='ongoing'
        )
    created_date = models.DateTimeField(
        auto_now_add=True
        )
    modified_date = models.DateTimeField(
        auto_now=True
    )
    user = models.ForeignKey(
        AUTH_USER_MODEL, 
        related_name='user_milestones', 
        null=True, 
        on_delete=models.SET_NULL
        )
    def __str__(self):
        return '{}'.format(self.feature) or u"Milestone Features"

    class Meta:
        verbose_name_plural = u"Milestone Features" or " "