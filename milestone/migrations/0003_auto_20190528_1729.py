# Generated by Django 2.0.6 on 2019-05-28 17:29

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('milestone', '0002_milestone_description'),
    ]

    operations = [
        migrations.AlterField(
            model_name='milestone',
            name='amount',
            field=models.DecimalField(decimal_places=2, default=0.0, max_digits=9),
        ),
    ]
