from django.contrib import admin
from .models import Milestone

# Register your models here.
class MilestoneAdmin(admin.ModelAdmin):
    list_display = ('transaction', 'feature','amount',
        'due_date', 'stage', 'created_date',
        'modified_date'
        )

admin.site.register(Milestone, MilestoneAdmin)