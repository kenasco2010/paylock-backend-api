from django.shortcuts import render
from django.utils import timezone
from django.db.models import Sum
from django.db.models import Q
from django.dispatch import receiver
from django.core.exceptions import ObjectDoesNotExist
from rest_framework.decorators import action
from rest_framework.status import (HTTP_201_CREATED,
                                   HTTP_200_OK,
                                   HTTP_400_BAD_REQUEST, HTTP_401_UNAUTHORIZED,
                                   HTTP_404_NOT_FOUND, HTTP_409_CONFLICT)
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework import viewsets, status
from transaction.models import Transaction
from .models import Milestone
from .serializers import MilestoneSerializer
import decimal
# Create your views here.

def milestone_details_function(request):
    milestone_details_dictionary = {
        'feature': request.data.get('feature'),
        'description': request.data.get('description'),
        'amount': request.data.get('amount'),
        'due_date': request.data.get('due_date')
    }
    return milestone_details_dictionary

class MilestoneViewSet(viewsets.ModelViewSet):
    queryset = Milestone.objects.all()
    serializer_class = MilestoneSerializer
    permission_class = (AllowAny,)

    @action(methods=('POST',), detail=False, url_path='add-milestone', 
        permission_class=(AllowAny))
    def add_milestones_to_transaction(self, request, *arg, **kwargs):
        """
            Add milestones to transaction already created
        """
        user = request.user
        user_wallet = user.user_wallets
        transaction = Transaction.objects.get(pk=request.data.get("transaction"))
        milestone_details = milestone_details_function(request)
        
        # Check if enough funds in wallet to set this milestone.
        if float(milestone_details['amount']) > user_wallet.balance:
            return Response({
            'status_code': status.HTTP_400_BAD_REQUEST,
            'message': 'Not enough funds in wallet to set this milestone. Top up your wallet to proceed.',
            }, status=status.HTTP_400_BAD_REQUEST)
       
        milestone = Milestone.objects.filter(transaction=transaction)
        milestone_sum = milestone.aggregate(Sum('amount'))['amount__sum']
        print(milestone_sum, transaction.amount)
        if  milestone_sum is not None and milestone_sum + decimal.Decimal(milestone_details['amount']) > transaction.amount:
            return Response({
            'status_code': status.HTTP_409_CONFLICT,
            'message': 'Your total funds set for this transaction is gettting lower. '
            'Update the transaction amount to continue setting this milestone.',
            }, status=status.HTTP_409_CONFLICT)
        
        milestone = Milestone.objects.create(
            transaction = transaction,
            feature = milestone_details['feature'],
            description = milestone_details['description'],
            amount = milestone_details['amount'],
            due_date = milestone_details['due_date'],
            user=user
        )
        serialize_milestone = self.serializer_class(milestone)
        return Response({
            'status_code': status.HTTP_201_CREATED,
            'message': 'Successfully added milestone to transaction',
            'result': {
                'data': serialize_milestone.data
            }
        }, status=status.HTTP_201_CREATED)
    

    @action(methods=('POST',), detail=False, url_path='edit-milestone', permission_class=(IsAuthenticated,))
    def edit_milestone(self, request, *arg, **kwargs):
        """ Edit milestones.
            Check if current user set the milestone before you allow operation.
            Check amount to make sure it's not more than transaction amount.
        """
        user = request.user
        milestone_details = milestone_details_function(request)
        milestone_id = request.data.get('milestone_id')
        transaction = request.data.get("transaction")

        milestone =  Milestone.objects.get(
            Q (pk=milestone_id) | Q (user=user))
        if milestone.user == user:
            transaction = Transaction.objects.get(pk=transaction)
            milestone_sum = Milestone.objects.aggregate(Sum('amount'))
            if milestone_sum['amount__sum'] + milestone_details['amount'] > transaction.amount:
                return Response({
                'status_code': status.HTTP_409_CONFLICT,
                'message': 'Your total funds set for this transaction is gettting lower. '
                'Update the transaction amount to continue setting this milestone.',
                }, status=status.HTTP_409_CONFLICT)
            else:
                milestone.feature = milestone_details['feature']
                milestone.description = milestone_details['description']
                milestone.amount = milestone_details['amount']
                milestone.due_date = milestone_details['due_date']
                milestone.save()
                serialize_milestone = self.serializer_class(milestone)
                return Response({
                    'status_code': status.HTTP_201_CREATED,
                    'message': 'Successfully updated your milestone.',
                    'result': {
                        'data': serialize_milestone.data
                    }
                }, status=status.HTTP_201_CREATED)
        else:
            return Response({
                'status_code': status.HTTP_400_BAD_REQUEST,
                'message': 'You are not authorized to edit this milestone.'
            }, status = status.HTTP_400_BAD_REQUEST)


    @action(methods=('DELETE',), detail=False, url_path='delete-milestone', permission_class=(IsAuthenticated,))
    def delete_milestone(self, request, *arg, **kwargs):
        """ Delete a milestone
            Milestone ID
            
        """
        user = request.user
        milestone_id = request.data.get('milestone_id')
        try:
             milestone = Milestone.objects.get(
            Q(pk=milestone_id) | Q (user=user))
        except Milestone.DoesNotExist:
            return Response({
                'status_code': status.HTTP_400_BAD_REQUEST,
                'message': 'This milestone does not exist.'
            }, status=status.HTTP_400_BAD_REQUEST)
       
        if milestone.user == user:
            milestone.delete()
            return Response({
                'status_code': status.HTTP_200_OK,
                'message': 'Milestone stone deleted successfully. '
            }, status=status.HTTP_200_OK)
        else:
            return Response({
                'status_code': status.HTTP_400_BAD_REQUEST,
                'message': 'You are not authorized to delete this milestone.'
            }, status=status.HTTP_400_BAD_REQUEST)
    