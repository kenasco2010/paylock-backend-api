from django.db import models
from django.conf import settings
from django.utils import timezone

# Create your models here.

AUTH_USER_MODEL = getattr(settings, 'AUTH_USER_MODEL', 'auth.User')

class UserAccountSetting(models.Model):
    user = models.OneToOneField(AUTH_USER_MODEL,  on_delete=models.CASCADE,
        related_name='user_setting')
    newsletter = models.BooleanField(
        default=False
        )
    general_promotional_emails = models.BooleanField(
         default=False
         )
    is_new_user = models.BooleanField(
        default = True
        )
    date_created = models.DateTimeField(
        default=timezone.now
        )
    modified = models.DateTimeField(
        auto_now=True
        )
    
    def __unicode__(self):
        return self.user

    def __str__(self):
        return str(self.user)