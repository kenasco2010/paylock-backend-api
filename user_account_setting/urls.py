from django.conf.urls import url
from django.conf.urls import include
from django.urls import path
from rest_framework.routers import DefaultRouter
from .views import UserAccountSettingViewSet

router = DefaultRouter()
router.register(r'v1.0/settings', UserAccountSettingViewSet)

urlpatterns = [
    path(r'', include(router.urls))
]