from rest_framework import serializers
from .models import UserAccountSetting

class UserAccountSettingSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = UserAccountSetting
        fields = ('id', 'newsletter', 'general_promotional_emails', 'is_new_user')