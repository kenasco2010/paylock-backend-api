# Generated by Django 2.0.6 on 2019-02-11 13:50

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('user_account_setting', '0005_useraccountsetting_new_user'),
    ]

    operations = [
        migrations.RenameField(
            model_name='useraccountsetting',
            old_name='new_user',
            new_name='is_new_user',
        ),
    ]
