from django.apps import AppConfig


class UserAccountSettingConfig(AppConfig):
    name = 'user_account_setting'
