from django.shortcuts import render
from django.utils import timezone
from django.conf import settings
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.status import (HTTP_201_CREATED,
                                   HTTP_200_OK,
                                   )
from .models import UserAccountSetting
from .serializers import UserAccountSettingSerializer

# Create your views here.

def account_settings_data(request):
    account_settings = {
        "newsletter": request.data.get('newsletter'),
        "general_emails": request.data.get('general_emails'),      
        }
    return account_settings

def save_user_settings(user, newsletter, general_emails):
    """ Check if user has a setting model
        Update if user setting model exist
        Create a new model and save setting if not existing.

    """
    user = user
    newsletter = newsletter
    general_emails = general_emails
    #  Check if user setting model already exist and update, 
    # else create a new user settings model and update
    try:
        user = UserAccountSetting.objects.get(user=user)
        if user:
            user.newsletter = newsletter
            user.general_promotional_emails = general_emails
            user.save()
            return Response({'status_code': HTTP_200_OK})
    except UserAccountSetting.DoesNotExist:
        UserAccountSetting.objects.create(
            newsletter = newsletter,
            general_promotional_emails =  general_emails,
            user = user
        )
        return Response({'status_code': HTTP_200_OK})

class UserAccountSettingViewSet(viewsets.ModelViewSet):
    permission_classes = (AllowAny,)
    serializer_class = UserAccountSettingSerializer
    queryset = UserAccountSetting.objects.all()
    
#     @action(methods=('POST',), detail=False, url_path='user-account', permission_classes=(AllowAny,))
#     def save_user_settings(self, request, newsletter, general_emails, *args, **kwargs):
#         """ Check if user has a setting model
#             Update if user setting model exist
#             Create a new model and save setting if not existing.

#         """
#         user = request.user
#         newsletter = newsletter
#         general_emails = general_emails
#         #  Check if user setting model already exist and update, 
#         # else create a new user settings model and update
#         try:
#             user = UserAccountSetting.objects.get(user=user)
#             if user:
#                 user.newsletter = newsletter
#                 user.general_promotional_emails = general_emails
#                 user.save()
#                 return Response({'statu_code': HTTP_200_OK})
#         except UserAccountSetting.DoesNotExist:
#             UserAccountSetting.objects.create(
#                 newsletter = newsletter,
#                 general_promotional_emails =  general_emails,
#                 user = user
#             )
#             return Response({'status_code': HTTP_200_OK})
    
    
    @action(methods=('GET',), detail=False, url_path='mark-new-user-as-false', 
    permission_classes=(IsAuthenticated,))
    def mark_new_user_as_false(self, request, *args, **kwargs):
        """ This function marks new user to false, 
            meaning the user has already gone through the
            onboarding process and has accessed the system.
        """
        user = request.user
        user_setting = UserAccountSetting.objects.get(user = user)
        user_setting.is_new_user = False
        user_setting.save()
        return Response({
            'status_code': HTTP_200_OK
        })
        

