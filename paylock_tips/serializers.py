from rest_framework import serializers
from .models import PayLockTip

class PayLockTipSerializer(serializers.ModelSerializer):
    class Meta:
        model = PayLockTip
        fields =('id', 'tip')
