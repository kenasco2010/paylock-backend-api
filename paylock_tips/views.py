from django.shortcuts import render
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.permissions import AllowAny, IsAuthenticated
from .serializers import PayLockTipSerializer
from .models import PayLockTip
from rest_framework.status import (HTTP_200_OK)
from rest_framework.decorators import action
import random

# Create your views here.

class PayLockTipViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated, )
    serializer_class = PayLockTipSerializer
    queryset = PayLockTip.objects.all().order_by('-id')

    @action(methods=('GET',), detail=False, url_path='randomized', 
        permission_classes=(IsAuthenticated,))
    def ramdomize_tips(self, request, *args, **kwargs):
        tips = PayLockTip.objects.all().order_by('?')[:3]
        # randomized_tips = random.shuffle(tips)
        serialize = self.serializer_class(tips, many=True)
        return Response ({'status_code': HTTP_200_OK,
                        'message': 'Randomized tips',
                        'result': {
                            'data': serialize.data
                        }
             })