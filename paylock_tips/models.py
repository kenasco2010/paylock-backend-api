from django.db import models
from django.conf import settings
from django.utils import timezone

AUTH_USER_MODEL = getattr(settings, 'AUTH_USER_MODEL', 'auth.User')

# Create your models here.
class PayLockTip(models.Model):
    tip = models.CharField(max_length=200)
    created_date = models.DateTimeField(default=timezone.now)
    modified = models.DateTimeField(auto_now=True)

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __unicode__(self):
        return '%s' % (self.tip)

    def __str__(self):
        return '%s' % (self.tip)