from django.apps import AppConfig


class PaylockTipsConfig(AppConfig):
    name = 'paylock_tips'
