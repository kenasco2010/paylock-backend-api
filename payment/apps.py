from django.apps import AppConfig
from suit.apps import DjangoSuitConfig

class PaymentConfig(AppConfig):
    name = 'payment'

class SuitConfig(DjangoSuitConfig):
    layout = 'vertical'

