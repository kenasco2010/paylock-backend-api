from django.db import models
from django.conf import settings
from django.utils import timezone

AUTH_USER_MODEL = getattr(settings, 'AUTH_USER_MODEL', 'auth.User')

PAYMENT_STATUS = (
    ('initialized', 'initialized'),
    ('failed', 'failed'),
    ('canceled', 'canceled'),
    ('in_progress', 'in progress'),
    ('verifying', 'verifying'),
    ('completed', 'completed'),
    ('outstanding', 'Outstanding')
    
)
# Create your models here.
class Payment(models.Model):
    payment_type = models.CharField(
        max_length=100, 
        null=True
        )
    description = models.CharField(
        max_length=500, 
        null=True
        )
    amount = models.DecimalField(
        max_digits=7, 
        decimal_places=2,
        default=0.00,
        null=True,
        blank=True
        )
    amount_usd = models.DecimalField(
        max_digits=7, 
        decimal_places=2,
        null=True,
        blank=True,
        default=0.00
        )
    mobile_money_amount = models.DecimalField(
        max_digits=7, 
        decimal_places=2,
        null=True,
        blank=True,
        default=0.00
        )
    appfee = models.DecimalField( # how much you will be charged for card & momo
        max_digits=7, 
        decimal_places=2,
        null=True,
        blank=True,
        default=0.00
    )
    actual_amount = models.DecimalField( # This is the actual amount you get when the app fee is deducted.
        max_digits=7, 
        decimal_places=2,
        null=True,
        blank=True,
        default=0.00
    )
    payment_status = models.CharField(
        max_length=15, 
        choices=PAYMENT_STATUS, 
        default='initialized'
        )
    webhook_status =  models.CharField(
        max_length=50, 
        null=True,
        blank=True
        )
    activity_type = models.CharField(
        max_length=20,
        default = 'personal',
        blank=True
    )
    api_transaction_id = models.CharField(
        max_length=60,
        null = True,
        blank=True
    )
    user = models.ForeignKey(
        AUTH_USER_MODEL, related_name='payment',
        on_delete=models.CASCADE)
    charge_response_code = models.CharField(max_length=128, blank=True)    
    charge_response_message = models.CharField(max_length=100, blank=True)
    transaction_reference = models.CharField(max_length=200, blank=True)
    txref = models.CharField(max_length=150, blank=True)
    orderRef = models.CharField(max_length=200, blank=True)
    verification_status = models.CharField(max_length=100, blank=True)
    save_card = models.BooleanField(default=False)
    embedtoken = models.CharField(max_length=200, blank=True)
    card_type = models.CharField(max_length=50, blank=True)
    last_four_digit = models.CharField(max_length=50, blank=True)
    published_date = models.DateTimeField(blank=True, null=True)
    created_date = models.DateTimeField(default=timezone.now)
    modified = models.DateTimeField(auto_now=True)

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __unicode__(self):
        return '%s %s' % (self.user.email, str(self.amount))

    def __str__(self):
        return '%s %s' % (self.user.email, str(self.amount))




class SavedCardToken(models.Model):
    user = models.ForeignKey(
        AUTH_USER_MODEL, related_name='card_tokens',
        on_delete=models.CASCADE)
    card_token = models.CharField(
        max_length=200,
        unique=True,
        blank=True
        )
    card_type = models.CharField(
        max_length=20,
        blank=True
        )
    last_four_digits = models.CharField(
        max_length=5,
        blank=True
        )
    created_date = models.DateTimeField(default=timezone.now)
    modified = models.DateTimeField(auto_now=True)
    def __unicode__(self):
        return '%s' % (self.card_type)

    def __str__(self):
        return '%s' % (self.card_type)