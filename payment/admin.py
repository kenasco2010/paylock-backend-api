from django.contrib import admin

# Register your models here.
from .models import Payment, SavedCardToken

class PaymentAdmin(admin.ModelAdmin):
    list_display = ('payment_type', 'amount', 'amount_usd', 'mobile_money_amount',
    'appfee', 'actual_amount', 'payment_status', 'user', 
    'charge_response_code', 'transaction_reference', 'created_date', 'modified')

class SavedCardTokensAdmin(admin.ModelAdmin):
    list_display = ('card_type', 'last_four_digits', 'card_token', 'user')

admin.site.register(Payment, PaymentAdmin)
admin.site.register(SavedCardToken, SavedCardTokensAdmin)