# Generated by Django 2.0.6 on 2018-11-19 23:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('payment', '0013_payment_txref'),
    ]

    operations = [
        migrations.AlterField(
            model_name='payment',
            name='payment_type',
            field=models.CharField(max_length=50, null=True),
        ),
    ]
