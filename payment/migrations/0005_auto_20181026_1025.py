# Generated by Django 2.0.6 on 2018-10-26 10:25

from django.conf import settings
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('payment', '0004_payment_orderref'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Payment',
            new_name='LoadWallet',
        ),
    ]
