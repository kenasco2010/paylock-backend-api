# Generated by Django 2.0.6 on 2018-10-30 16:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('payment', '0010_auto_20181030_1615'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='payment',
            name='otp',
        ),
        migrations.AddField(
            model_name='payment',
            name='description',
            field=models.CharField(max_length=15, null=True),
        ),
    ]
