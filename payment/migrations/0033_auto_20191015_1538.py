# Generated by Django 2.0.6 on 2019-10-15 15:38

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('payment', '0032_auto_20191013_0010'),
    ]

    operations = [
        migrations.CreateModel(
            name='SavedCardTokens',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('card_token', models.CharField(blank=True, max_length=200, unique=True)),
                ('card_type', models.CharField(blank=True, max_length=20)),
                ('last_four_digits', models.CharField(blank=True, max_length=5)),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='card_tokens', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AlterField(
            model_name='payment',
            name='payment_status',
            field=models.CharField(choices=[('initialized', 'initialized'), ('failed', 'failed'), ('canceled', 'canceled'), ('in_progress', 'in progress'), ('verifying', 'verifying'), ('completed', 'completed')], default='initialized', max_length=15),
        ),
    ]
