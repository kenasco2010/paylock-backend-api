from django.conf.urls import url
from django.conf.urls import include
from django.urls import path
from rest_framework.routers import DefaultRouter
from .views import PaymentViewSet

# from payment import views

router = DefaultRouter()
router.register(r'v1.0/payments', PaymentViewSet)



urlpatterns = [
    path(r'', include(router.urls)),
    # path(r'api/payments/webhook/', views.payment_webhook_url, name='webhook')
]