from rest_framework import serializers
from .models import Payment, SavedCardToken

class PaymentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Payment
        fields = (
        'id', 'payment_type', 'amount', 'actual_amount', 'amount_usd', 'appfee', 'description', 'payment_status', 
        'charge_response_code', 'charge_response_message', 'txref', 'activity_type',
        'published_date', 'created_date', 'modified'
        )

class ApiIntegrationPaymentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Payment
        fields = (
        'id', 'payment_type', 'description', 'amount', 'payment_status', 'txref',
        'created_date'
        )

class SavedCardsTokenSerializer(serializers.ModelSerializer):
    class Meta:
        model = SavedCardToken
        fields = (
        'id', 'card_type', 'last_four_digits', 'card_token'
        )