from payment.models import SavedCardToken
from rest_framework.response import Response
import requests

def save_cards_token(user, card_type, last_four_digits, email, card_token):
    """ Function to save cards token after webhook.
    CHeck if card token already saved
    If saved:
        Don't save
    else: 
        save()
    """
    try:
        user_cards = SavedCardToken.objects.filter(user=user, last_four_digits=last_four_digits)
        # requests.post('http://190dce96.ngrok.io', json={'Found user cards'})
        if user_cards.exists():
            # requests.post('http://190dce96.ngrok.io', json={'User cards exist'})
            for cards in user_cards:
                if  cards.last_four_digits == last_four_digits and  cards.user.email == email:
                    # requests.post('http://190dce96.ngrok.io', json={'Before break point'})
                    break
                    # requests.post('http://190dce96.ngrok.io', json={'After break point'})
                else:
                    # requests.post('http://190dce96.ngrok.io', json={'1st else statement...'})
                    SavedCardToken.objects.create(
                        user=user,
                        card_type=card_type,
                        last_four_digits=last_four_digits,
                        card_token=card_token   
                    )
                    # requests.post('http://190dce96.ngrok.io', json={'2st else statement......s'})
        else:
            # requests.post('http://190dce96.ngrok.io', json={'3st else statement...s'})
            SavedCardToken.objects.create(
                user=user,
                card_type=card_type,
                last_four_digits=last_four_digits,
                card_token=card_token
                
            )
            # requests.post('http://190dce96.ngrok.io', json={'4st else statement...'})
    except SavedCardToken.DoesNotExist:
        # requests.post('http://190dce96.ngrok.io', json={'5st else statement...'})
        SavedCardToken.objects.create(
            user=user,
            card_type=card_type,
            last_four_digits=last_four_digits,
            card_token=card_token
        )
        # requests.post('http://190dce96.ngrok.io', json={'6st else statement...'})
    