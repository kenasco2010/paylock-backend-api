from django.utils import timezone
from django.conf import settings
import uuid

def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip

def payment_detail(request):
    payment_detail = {
        "cardno": request.data.get('cardno'),
        "cvv": request.data.get('cvv'),
        "expirymonth": request.data.get('expirymonth'),
        "expiryyear": request.data.get('expiryyear'),
        "currency": "USD",
        "country": request.data.get('country'),
        "amount": request.data.get('amount'),
        "paymentType": request.data.get('paymentType'),
        "email": request.user.email,
        "phonenumber": request.user.phone_number,
        "firstname": request.data.get('firstname'),
        "lastname": request.data.get('lastname'),
        "activity_type": request.data.get('activity_type'),
        "IP": get_client_ip(request),
        "txRef":  "PAYL-card_Tref" + str(uuid.uuid4()),
        "redirect_url": settings.DOMAIN_URL + "client/wallet/success"
        }
    return payment_detail


def initial_payload(request):
    initial_payload = {
            "PBFPubKey": settings.FLUTTERWAVE_PUBLIC_KEY,
            # "IP": "355426087298442",
            # "txRef": "MC-" + str(timezone.now()),
            # "redirect_url": ""#callback url.
            "redirect_url": settings.DOMAIN_URL + "client/wallet/success"
            }
    initial_payload_info = initial_payload
    payment_detail_payload = dict(payment_detail(request))
    combine_dict = {**payment_detail_payload, **initial_payload_info}
    return combine_dict


def final_payment_payload_with_pin(request):
    second_request_payload = {
        "PBFPubKey": settings.FLUTTERWAVE_PUBLIC_KEY,
        "pin": request.data.get('pin'),
        "suggested_auth": request.data.get('suggested_auth'),
        "IP": "355426087298442",
        # "txRef": "MC-" + str(timezone.now()),
        "redirect_url": settings.DOMAIN_URL + "client/wallet/success"
        }
    payload = second_request_payload
    card_input_payload = dict(payment_detail(request))
    final_payload = {**card_input_payload, **payload}
    return final_payload

def final_payment_payload_with_billing_address(request):
    billing_address = {
        "currency": "USD",
        "billingzip": request.data.get('billingzip'),
        "billingcity": request.data.get('billingcity'),
        "billingaddress": request.data.get('billingaddress'),
        "billingstate": request.data.get('billingstate'),
        "billingcountry": request.data.get('billingcountry'),
        "redirect_url": settings.DOMAIN_URL + "client/wallet/success",
        "suggested_auth": request.data.get('suggested_auth')
    }
    avs_vbvsecurecode_payload = {

        # "PBFPubKey": settings.FLUTTERWAVE_PUBLIC_KEY,
        # "suggested_auth": "NOAUTH_INTERNATIONAL"

        "PBFPubKey": settings.FLUTTERWAVE_PUBLIC_KEY

       }
    billing_address_payload = billing_address
    vbv_payload = avs_vbvsecurecode_payload
    final_vbv_auth_payload = {**billing_address_payload, 
    **vbv_payload, **payment_detail(request)}
    return final_vbv_auth_payload

def payment_detail_mobile_money_gh(request):
    mm_payment_detail = {
        "PBFPubKey": settings.FLUTTERWAVE_PUBLIC_KEY,
        "payment_type": request.data.get('payment_type'),
        "country": request.data.get('country'),
        "amount": request.data.get('amount'),
        "network": request.data.get('network'),
        "currency": request.data.get('currency'),
        "voucher": request.data.get('voucher'),
        "email": request.user.email,
        "phonenumber": request.data.get('phonenumber'),
        "firstname": request.data.get('firstname'),
        "lastname": request.data.get('lastname'),
        "activity_type": request.data.get('activity_type'),
        "IP": get_client_ip(request),
        "txRef":  "PAYL-momo_Txref" + str(uuid.uuid4()),
        "orderRef":  "PAYL-momo_Oref" + str(uuid.uuid4()),
        "is_mobile_money_gh": request.data.get('is_mobile_money_gh'),
        "is_mobile_money_ug" : request.data.get('is_mobile_money_ug'),
        "device_fingerprint": "69e6b7f0b72037aa8428b70fbr03986c"
        }
    return mm_payment_detail

def bank_account_details(request):
    bank_account_data = {
        'amount': request.data.get('amount'),
        'account_bank': request.data.get('account_bank'),
        'account_number': request.data.get('account_number'),
        'seckey': settings.FLUTTERWAVE_SECRET_KEY,
        'narration': request.data.get('narration'),
        'currency': request.data.get('currency'),
        'reference':  "PAYL-bankAcRef" + str(uuid.uuid4()),
        'first_name':request.data.get('firstname'),
        'last_name': request.data.get('lastname'),
        'IP': get_client_ip(request),
        }
    return bank_account_data



def local_account_withdraw_details(request):
    local_account_account_details = { 
        'country': request.data.get('country'),
        'account_bank': request.data.get('account_bank'),
        'bank_name': request.data.get('bank_name'), # bank name should be gotten from the list of banks api
        'account_number': request.data.get('account_number'),
        'currency': request.data.get('currency'),
        'amount': request.data.get('amount'),
        
        'destination_branch_code': request.data.get('destination_branch_code'),
        
        'narration': request.data.get('narration'),
        'beneficiary_name': request.data.get('beneficiary_name'),

        'reference': "PAYL-transW" + str(uuid.uuid4()), 
        'published_date': timezone.now(),
        'created_date': timezone.now(),
        'seckey': settings.FLUTTERWAVE_SECRET_KEY
        }
    return local_account_account_details

# for manual withdrawal
def momo_withdraw_details(request):
    withdraw_detail = {
        "seckey": settings.FLUTTERWAVE_SECRET_KEY,
        'account_type': 'BankTransfer',
        'account_bank': request.data.get('account_bank'),
        'account_number': request.data.get('account_number'),
        'amount': request.data.get('amount'),
        'narration': request.data.get('narration'), 
        'currency': request.data.get('currency'),
        'reference': "PAYL-transW" + str(uuid.uuid4()),
    }
    return withdraw_detail

def foreign_account_withdraw_details(request):
    foreign_account_account_details = {
        "amount": request.data.get('amount'),
        "seckey": settings.FLUTTERWAVE_SECRET_KEY,
        "narration": request.data.get('narration'),
        "currency": request.data.get('currency'),
        "reference": "PAYL-transW" + str(uuid.uuid4()) + "_PMCKDU_1",
        "beneficiary_name": request.data.get('beneficiary_name'),
        "AccountNumber": request.data.get('account_number'),
        "RoutingNumber": request.data.get('routing_number'),
        "SwiftCode": request.data.get('swift_code'),
        "BankName": request.data.get('bank_name'),
        "BeneficiaryName": request.data.get('beneficiary_name'),
        "BeneficiaryAddress": request.data.get('bank_address'),
        "BeneficiaryCountry": request.data.get('country'),
        "debit_currency": "USD"
    }
    return foreign_account_account_details


