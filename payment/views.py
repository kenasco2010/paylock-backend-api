from django.shortcuts import render
from django.contrib.auth import authenticate, login, logout, get_user_model
from django.utils import timezone
from django.conf import settings
from django.db.models import Sum, Q, Count
from django.utils.crypto import get_random_string
from django.dispatch import receiver
from django.views.decorators.csrf import csrf_exempt
from rest_framework.status import (HTTP_201_CREATED,
                                   HTTP_200_OK,
                                   HTTP_400_BAD_REQUEST, HTTP_401_UNAUTHORIZED,
                                   HTTP_404_NOT_FOUND, HTTP_409_CONFLICT,
                                   HTTP_501_NOT_IMPLEMENTED)
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework import viewsets
from rest_framework.views import APIView
from rest_framework import status
from rest_framework.decorators import permission_classes, action
from rest_framework.authtoken.models import Token
from decimal import Decimal
from user_account.models import User
from paylock.permissions import IsSuperUserPermission, IsOwner
# from paylock_emails.signals import send_withdrawal_email_to_user
from .models import Payment, SavedCardToken
from api_integration.models import ApiTransaction
from transaction.models import Transaction
from .card_tokens import save_cards_token
from wallet.models import UserWallet, EscrowWallet
from withdraw.models import Withdraw
from paylock_emails.tasks import notify_user_successful_transfer
from paylock_emails.signals import load_wallet_with_card_success_reponse, \
    load_wallet_failed_reponse, load_wallet_with_momo_success_reponse, \
    load_wallet_with_qvo_payment_success_response
from .serializers import PaymentSerializer, SavedCardsTokenSerializer
from .json_payload import *
from paylock.send_text import send_funds_request_text_message_link_to_seller, \
		send_release_funds_text_message_link_to_buyer
from paylock_emails.signals import send_email_to_seller_about_payment, \
		send_email_to_buyer_about_payment, \
		send_email_to_seller_money_has_been_released
from paylock.currency_conversion import convert_currency
# Make request to payment api
import base64
from Crypto.Cipher import DES3
from Crypto.Cipher import AES
import hashlib
import requests
import json
from django.http import HttpResponse
from datetime import datetime
import time
import uuid

# Create your views here.
user_wallet = UserWallet()
# Function to call when updating any payment history. IT'S USED IN OTHER PLACES.
def update_payment_history(payment_type, description, amount, payment_status, 
    user, published_date, created_date):
    Payment.objects.create(
        payment_type = payment_type,
        description=description,
        amount=amount,
        payment_status=payment_status,
        user=user,
        published_date=published_date,
        created_date=created_date
    )

def update_payment_instance_after_webhook_response(txRef, response_message, order_ref, txref):
    payment = Payment.objects.get(
            txref=txRef
        )
    payment.charge_response_message = response_message
    payment.orderRef = order_ref
    payment.txref = txref
    payment.save()


def increase_wallet_balance_after_payment(payment_user, actual_amount, timestamp):
    try:
        my_wallet = UserWallet.objects.get(user=payment_user)
    except UserWallet.DoesNotExist:
        return Response({
            'status_code': status.HTTP_404_NOT_FOUND,
            'message': 'Your wallet cannot be found'
            }, status=status.HTTP_404_NOT_FOUND)
    my_wallet.balance += actual_amount
    my_wallet.last_used = timestamp
    my_wallet.save()

def decrease_wallet_balance_after_payment(payment_user, actual_amount, timestamp):
    try:
        my_wallet = UserWallet.objects.get(user=payment_user)
    except UserWallet.DoesNotExist:
        return Response({
            'status_code': status.HTTP_404_NOT_FOUND,
            'message': 'Your wallet cannot be found'
            }, status=status.HTTP_404_NOT_FOUND )
    my_wallet.balance -= actual_amount
    my_wallet.last_used = timestamp
    my_wallet.save()

def verify_payment_with_payment_processor_flutterwave(txref, *args, **kwargs):
    """ Verify payment processing by flutterwave after webhook
        If payment = business deduct amount from wallet instantly,
        Create a transaction and escrow wallet transaction
        credit business account & paylock account.
    
    """
    try:
        payment = Payment.objects.get(txref=txref)
    except Payment.DoesNotExist:
        return Response({
        'message': 'The payment does not exist.'
        })
    
    data = {
    "txref": payment.txref,
    "normalize":"1",
    "SECKEY":settings.FLUTTERWAVE_SECRET_KEY
    }
    # requests.post('http://aab12add.ngrok.io', json=data)
    response = requests.post(settings.FLUTTERWAVE_VERIFY_PAYMENT, data=data)
    response_json = response.json()
    # requests.post('http://aab12add.ngrok.io', json=response_json)
    try:
        response_json['data']     
        if response_json['data']['status'] == 'successful':
            payment.verification_status = response_json['data']['status']
            if 'card' in response_json['data'] and payment.save_card == True:
                # requests.post('http://aab12add.ngrok.io', json={
                #     "verify endpoint last 4 digits":response_json['data']['card']['last4digits']})
                save_cards_token(
                    payment.user, 
                    response_json['data']['card']['type'],
                    response_json['data']['card']['last4digits'], 
                    payment.user.email,
                    response_json['data']['card']['card_tokens'][0]['embedtoken']
                    )
            # Call the increase wallet amount function
            payment.payment_status = "completed"
            payment.save()
            increase_wallet_balance_after_payment(payment.user, payment.actual_amount, timezone.now())
            if payment.activity_type == "business":
                api_transaction = ApiTransaction.objects.get(transaction_id=payment.api_transaction_id)
                # get the transaction_id
                # create main transaction with details
                user_wallet = UserWallet.objects.get(user=payment.user)
                business_wallet = UserWallet.objects.get(user=api_transaction.collaborator)
                if user_wallet.balance < api_transaction.usd_amount:
                    return Response({
                        'status_code': status.HTTP_401_UNAUTHORIZED,
                        'message': 'Insufficient balance in your wallet for this transaction.'
                        }, status=status.HTTP_401_UNAUTHORIZED)
                if api_transaction.transaction_type == 'e-commerce':
                    main_transaction = Transaction.objects.create(
                        name=api_transaction.transaction_name,
                        api_transaction= api_transaction,
                        description =api_transaction.description,
                        transaction_role = 'owner',
                        escrow_fee_payment= 'user',
                        initial_amount = api_transaction.usd_amount,
                        amount = api_transaction.usd_amount,
                        markup = api_transaction.app_fee,
                        user = api_transaction.user,
                        contractor = api_transaction.collaborator,
                        project_owner_agreement= True,
                        contractor_agreement = True,
                        stage = 'ongoing',
                        activity_type = 'business',
                        has_paid = True,
                        created_date = timezone.now(),
                        published_date = timezone.now()
                    )
                    buyer_full_name = api_transaction.first_name + ' ' + api_transaction.last_name, 
                    send_funds_request_text_message_link_to_seller(buyer_full_name, api_transaction.collaborator.phone_number, api_transaction.transaction_id)

                elif api_transaction.transaction_type == 'marketplace':
                    main_transaction = Transaction.objects.create(
                        name=api_transaction.transaction_name,
                        api_transaction= api_transaction,
                        description =api_transaction.description,
                        transaction_role = 'owner',
                        escrow_fee_payment= 'user',
                        initial_amount = api_transaction.usd_amount,
                        amount = api_transaction.usd_amount,
                        markup = api_transaction.app_fee,
                        user = api_transaction.user,
                        contractor = api_transaction.merchant,
                        project_owner_agreement= True,
                        contractor_agreement = True,
                        stage = 'ongoing',
                        activity_type = 'business',
                        has_paid = True,
                        created_date = timezone.now(),
                        published_date = timezone.now()
                    )
                    buyer_full_name = api_transaction.first_name + ' ' + api_transaction.last_name, 
                    send_funds_request_text_message_link_to_seller(buyer_full_name, api_transaction.merchant.phone_number, api_transaction.transaction_id)
                # create an escrow wallet. 
                escrow_wallet = EscrowWallet.objects.create(
                    transaction=main_transaction,
                    sender=main_transaction.user,
                    recipient=main_transaction.contractor,
                    amount=main_transaction.amount,
                    balance=main_transaction.markup,
                    date_created=timezone.now(),
                    can_cancel = False,
                    is_active= True,
                    last_used = timezone.now()
                )
                # Deduct respective amounts from user/buyers wallet.
                user_wallet.balance -= api_transaction.usd_amount
                user_wallet.balance -= api_transaction.usd_business_fee
                user_wallet.balance -= api_transaction.usd_app_fee
                user_wallet.save()
                # update the business wallet
                business_wallet.balance += api_transaction.usd_business_fee
                business_wallet.save()
                # Update api transaction.
                api_transaction.stage = 'created'
                api_transaction.stage_description = 'Transaction has been paid for successfully and ongoing.'
                api_transaction.save()

                
                # Send funds request message link to seller's email (seller_email, transaction)
                send_email_to_seller_about_payment(api_transaction)
                send_email_to_buyer_about_payment(api_transaction)
        else:
            payment.status = response_json['data']['status']
            payment.charge_response_message = response_json['data']['chargemessage']
            payment.save()
            return Response({
            'status_code': status.HTTP_404_NOT_FOUND,
            'message': 'Your payment could not be verified.',
            'result': {
                'data': response_json['data']
            }
        }, status=status.HTTP_404_NOT_FOUND)
    except KeyError:
        return Response({
            'status_code': status.HTTP_404_NOT_FOUND,
            'message': 'Your payment could not be verified. Please initiate the payment again.',
            'result': {
                'data': response_json
            }
        }, status=status.HTTP_404_NOT_FOUND)


def getKey():
    """ This is the getKey function that generates an encryption Key for you by passing your Secret Key as a parameter.
    
    """
    seckey = settings.FLUTTERWAVE_SECRET_KEY
    hashedseckey = hashlib.md5(seckey.encode("utf-8")).hexdigest()
    hashedseckeylast12 = hashedseckey[-12:]
    seckeyadjusted = seckey.replace('FLWSECK-', '')
    seckeyadjustedfirst12 = seckeyadjusted[:12]
    return seckeyadjustedfirst12 + hashedseckeylast12

def encryptData(key, plainText):
    """ This is the encryption function that encrypts your payload by passing the text and your encryption Key.
    
    """
    blockSize = 8
    padDiff = blockSize - (len(plainText) % blockSize)
    cipher = DES3.new(key, DES3.MODE_ECB)
    plainText = "{}{}".format(plainText, "".join(chr(padDiff) * padDiff))
    encrypted = base64.b64encode(cipher.encrypt(plainText))
    return encrypted



class PaymentViewSet(viewsets.ReadOnlyModelViewSet):
    permission_classes = (IsAuthenticated, )
    serializer_class = PaymentSerializer
    queryset = Payment.objects.all()
    # renderer_classes = (StaticHTMLRenderer,)
    
    @action(methods=('POST',), detail=False, url_path='load-wallet', 
        permission_classes=(IsAuthenticated,))
    def make_payment_with_card(self, request, *args, **kwargs):
        """ Initial api call with card details
        
        """
        
        headers = {
            'content-type': settings.CONTENT_TYPE
        }
        card_details = payment_detail(request) # card details collection payload
        str_card_details = json.dumps(card_details) # converts dict to string with json.dumps
        payment = Payment.objects.create(
            payment_type=card_details['paymentType'],
            amount=card_details['amount'],
            amount_usd=request.data.get('amount_usd'),
            user=request.user,
            description="Load wallet",
            txref=card_details['txRef'],
            activity_type=card_details['activity_type'],
            save_card=request.data.get('save_card_option'),
            published_date=timezone.now()
            )
        
        payload = initial_payload(request) #Initial payload to send to flutterwave
        encode_data = json.dumps(payload) # Serialize object into Json formatted str
        encrypted_data = encryptData(getKey(), encode_data) # Pass data into encrypted Algorithm
        data = {
            "PBFPubKey": settings.FLUTTERWAVE_PUBLIC_KEY, # The public key from flutterwave
            "client": encrypted_data,
            "alg": "3DES-24"
        }
        
        response = requests.post(settings.FLUTTERWAVE_URL, data=data)
        response_json = response.json()
        # CODE IS ONLY USED WHEN WE ALLOW USER TO SPECIFY THEIR CURRENCY AND COUNTRY
        try:
            if response_json['data']:
                    if 'suggested_auth' in response_json['data']:
                        payment.payment_status = "in_progress"
                        payment.charge_response_message = 'Validate with {}'.format(response_json['data']['suggested_auth'])
                        payment.save()
                        payment = self.serializer_class(payment)
                        return Response({"status_code": status.HTTP_200_OK,
                                "message": response_json['message'],
                                "result": {
                                    "data": response_json,
                                    "payment_data": payment.data
                                }
                        }, status=status.HTTP_200_OK)
                    else:
                        payment.payment_status = "in_progress"
                        payment.charge_response_code = response_json['data']['chargeResponseCode']
                        payment.charge_response_message = response_json['data']['chargeResponseMessage']
                        payment.transaction_reference = response_json['data']['flwRef']
                        payment.txref = response_json['data']['txRef']
                        payment.appfee = response_json['data']['appfee']
                        payment.save()
                        payment = self.serializer_class(payment)
                        return Response({"status_code": status.HTTP_200_OK,
                                    "message": response_json['message'],
                                    "result": {
                                        "data": response_json,
                                        "payment_data": payment.data
                                    }
                            }, status=status.HTTP_200_OK)
            else:
                payment = self.serializer_class(payment)
                return Response({"status_code": status.HTTP_200_OK,
                            "message": response_json['message'],
                            "result": {
                                "data": response_json,
                                "payment_data": payment.data
                            }
                    },status=status.HTTP_200_OK)
        except  KeyError:
            payment.payment_status = "failed"
            payment.charge_response_code = response_json['data']['code']
            payment.charge_response_message = response_json['message']
            payment.save()
            payment = self.serializer_class(payment)
            return Response({"status_code": status.HTTP_200_OK,
                        "message": response_json['message'],
                        "result": {
                            "data": response_json,
                            "payment_data": payment.data
                        }
                }, status=status.HTTP_200_OK)
    
    # Validation with PIN
    @action(methods=('POST',), detail=False, url_path="validate-with-pin", 
    permission_classes=(IsAuthenticated,))
    def validate_with_pin(self, request, *args, **kwargs):
        """ Final api call with card details and extra details PIN Validation
        
        """
        payment_id = request.data.get("payment_id")
        payment = Payment.objects.get(pk=payment_id)
        payload = final_payment_payload_with_pin(request)
        encode_data = json.dumps(payload) 
        encrypted_data = encryptData(getKey(), encode_data)
        data = {
            "PBFPubKey": settings.FLUTTERWAVE_PUBLIC_KEY,
            "client": encrypted_data,
            "alg": "3DES-24"
        }
        response = requests.post(settings.FLUTTERWAVE_URL, data=data)
        response_json = response.json()
        # requests.post('http://83e01a4e.ngrok.io', json=response_json)
        try:
            response_json['data']['chargeResponseCode']
            payment.payment_status = "in_progress"
            payment.charge_response_code = response_json['data']['chargeResponseCode']
            payment.charge_response_message = response_json['data']['chargeResponseMessage']
            payment.transaction_reference = response_json['data']['flwRef']
            payment.txref = response_json['data']['txRef']
            payment.save()
            payment = self.serializer_class(payment)
            return Response({"status_code": status.HTTP_200_OK,
                        "result": {
                            "data": response_json,
                            "payment_data": payment.data
                        }
                }, status=status.HTTP_200_OK)
        except  KeyError:
            payment.payment_status = "failed"
            payment.charge_response_code = response_json['data']['code']
            payment.charge_response_message = response_json['message']
            payment.save()
            payment = self.serializer_class(payment)
            return Response({"status_code": status.HTTP_200_OK,
                        "message": response_json['message'],
                        "result": {
                            "data": response_json,
                            "payment_data": payment.data
                        }
                }, status=status.HTTP_200_OK)

    #  Final verification with payment with OTP
    @action(methods=('POST',), detail=False, url_path="verify-payment-with-otp", 
        permission_classes=(IsAuthenticated,))
    def validate_payment_with_otp(self, request, *args, **kwargs):
        """ Final payment confirmation with OTP
            Args:
                otp: the otp validation sent to your phone.
                payment_id: The payment id of the transaction.

            Returns:
                Returns payload from flutterwave if the transaction is verified.
        """
        otp = request.data.get('otp')
        payment_id = request.data.get('payment_id')
        current_payment = Payment.objects.get(pk=payment_id, user=request.user)
        validate_data = {
            "PBFPubKey": settings.FLUTTERWAVE_PUBLIC_KEY,
            "transaction_reference": current_payment.transaction_reference,
            "otp": otp
        }
        if not validate_data['otp']:
            return Response({'status_code': status.HTTP_404_NOT_FOUND,
            'message': 'Please type in the code sent to your phone.'
            }, status=status.HTTP_404_NOT_FOUND)
        else:
            url = settings.FLUTTERWAVE_VALIDATE_PAYMENT_URL
            request = requests.post(url, data=validate_data)
            validation_response = request.json()
            # requests.post('http://83e01a4e.ngrok.io', json=validation_response)
            if validation_response['status'] == "success"  and validation_response['data']['data']['responsecode'] == "00":              
                current_payment.payment_status = "completed"
                current_payment.charge_response_code = "00"
                current_payment.charge_response_message = ""
                current_payment.orderRef = validation_response['data']['tx']['orderRef']
                current_payment.txref = validation_response['data']['tx']['txRef']
                current_payment.save()
                current_payment = self.serializer_class(current_payment)
                return Response({'status_code': status.HTTP_200_OK,
                        'message': 'Your payment process is completed',
                        'result': {
                            'data': current_payment.data
                        }
                    }, status=status.HTTP_200_OK)
                
            else:
                return Response({'status_code': status.HTTP_400_BAD_REQUEST,
                        'message': validation_response['message'],
                    }, status=status.HTTP_400_BAD_REQUEST)
    
    # VBV Validation
    @action(methods=('POST',), detail=False, url_path="validate-with-vbv", 
        permission_classes=(IsAuthenticated,))
    def validate_with_vbv(self, request, *args, **kwargs):
        """ Final api call with card details and extra details for VBV validation
        
        """
        # return Response({"message": "Hitting this endpoint"})
        payment_id = request.data.get('payment_id')
        if not payment_id:
            return Response({'status_code': status.HTTP_404_NOT_FOUND,
            'message': 'Payment Id not found'
            }, status=status.HTTP_404_NOT_FOUND)
        payment = Payment.objects.get(pk=payment_id, user=request.user)
        payload = final_payment_payload_with_billing_address(request) # remove this.
        encode_data = json.dumps(payload) # Serialize object into Json formatted str
        encrypted_data = encryptData(getKey(), encode_data) # Pass data into encrypted Algorithm
        data = {
            "PBFPubKey": settings.FLUTTERWAVE_PUBLIC_KEY,
            "client": encrypted_data,
            "alg": "3DES-24"
        }
        response = requests.post(settings.FLUTTERWAVE_URL, data=data)
        response_json = response.json()
        # requests.post('http://83e01a4e.ngrok.io', json=response_json)
        try:
            response_json['data']['chargeResponseCode']
            payment.payment_status = "in_progress"
            payment.charge_response_code = response_json['data']['chargeResponseCode']
            payment.charge_response_message = response_json['data']['chargeResponseMessage']
            payment.transaction_reference = response_json['data']['flwRef']
            payment.txref = response_json['data']['txRef']
            payment.save()
            payment = self.serializer_class(payment)
            return Response({"status_code": status.HTTP_200_OK,
                        "result": {
                            "data": response_json,
                            "payment_data": payment.data
                        }
                }, status=status.HTTP_200_OK)
        except  KeyError:
            payment.payment_status = "failed"
            payment.charge_response_code = response_json['data']['code']
            payment.charge_response_message = response_json['message']
            payment.save()
            payment = self.serializer_class(payment)
            return Response({"status_code": status.HTTP_400_BAD_REQUEST,
                        "message": response_json['message'],
                        "result": {
                            "data": response_json,
                            "payment_data": payment.data
                        }
                }, status=status.HTTP_400_BAD_REQUEST)
    
    # Initialize the mobile money payment.
    @action(methods=('POST',), detail=False, url_path='load-wallet-gh-mobile-money', 
        permission_classes=(IsAuthenticated,))
    def load_wallet_with_mobile_money_gh(self, request, *args, **kwargs):
        """ Initialization of mobile money payment.
        
        """
        momo_countries = ['GH', 'RWF', 'UG', 'ZM']
        if request.user.user_profile.country not in momo_countries:
            return Response({
                'status_code': status.HTTP_400_BAD_REQUEST,
                'message': 'Mobile money payment is not accepted in your country.'
                }, status=status.HTTP_400_BAD_REQUEST)
        mm_payment_detail = payment_detail_mobile_money_gh(request) # card details collection payload
        payment = Payment.objects.create(
            payment_type=mm_payment_detail['payment_type'],
            amount=mm_payment_detail['amount'],
            amount_usd=request.data.get('amount_usd'),
            user=request.user,
            description="Wallet loaded with Mobile Money & converted to USD",
            txref=mm_payment_detail['txRef'],
            activity_type=mm_payment_detail['activity_type'],
            published_date=timezone.now()
            )
        encode_data = json.dumps(mm_payment_detail) # Serialize object into Json formatted str
        encrypted_data = encryptData(getKey(), encode_data) # Pass data into encrypted Algorithm
        
        data = {
            "PBFPubKey": settings.FLUTTERWAVE_PUBLIC_KEY, # The public key from flutterwave
            "client": encrypted_data,
            "alg": "3DES-24"
        }
        response = requests.post(settings.FLUTTERWAVE_GH_MM_URL, data=data)
        response_json = response.json()
        try:
            if response_json['data']:
                if response_json['status'] == 'success':
                    payment.payment_status = "in_progress"
                    payment.charge_response_code = response_json['data']['chargeResponseCode']
                    payment.charge_response_message = response_json['data']['chargeResponseMessage']
                    payment.transaction_reference = response_json['data']['flwRef']
                    # payment.txref = response_json['data']['txRef']
                    payment.appfee = response_json['data']['appfee']
                    # made changes here
                    payment.mobile_money_amount = response_json['data']['amount']
                    payment.save()
                    payment = self.serializer_class(payment)
                    return Response({"status_code": status.HTTP_201_CREATED,
                                'message': 'Mobile Money initialized',
                                "result": {
                                    "data": response_json,
                                    "payment_data": payment.data
                                }
                        }, status=status.HTTP_201_CREATED)
                else:
                    return Response({"status_code": status.HTTP_400_BAD_REQUEST,
                                'message': response_json['message']
                        }, status=status.HTTP_400_BAD_REQUEST)
            else:
                return Response({"status_code": status.HTTP_400_BAD_REQUEST,
                                'message': response_json
                        }, status=status.HTTP_400_BAD_REQUEST)
        except  KeyError:
            payment.payment_status = "failed"
            payment.charge_response_code = response_json['data']['code']
            payment.charge_response_message = response_json['message']
            payment.save()
            payment = self.serializer_class(payment)
            return Response({"status_code": status.HTTP_400_BAD_REQUEST,
                        'message': response_json['message'],
                        "result": {
                            "data": response_json,
                            "payment_data": payment.data
                        }
                },status=status.HTTP_400_BAD_REQUEST)

    @action(methods=('GET',), detail=False, url_path='show-saved-cards', 
        permission_classes=(IsAuthenticated,))
    def show_saved_card(self, request, *args, **kwargs):
        """ Show saved card to user

        """
        user = request.user
        saved_cards = SavedCardToken.objects.filter(user=user)
        if saved_cards.exists():
            saved_cards = SavedCardsTokenSerializer(saved_cards, many=True)
            return Response({"status_code": status.HTTP_200_OK,
                        'message': 'Your saved cards',
                        "result": {
                            "data": saved_cards.data
                        }
                },status=status.HTTP_200_OK)
        else:
            return Response({"status_code": status.HTTP_200_OK,
                        'message': 'No saved cards',
                },status=status.HTTP_200_OK)

    @action(methods=('POST',), detail=False, url_path='load-wallet-with-saved-card', 
        permission_classes=(IsAuthenticated,))
    def load_wallet_with_saved_card(self, request, *args, **kwargs):
        """ Load wallet or make payment with saved card which is
            by using the token returned in the verify payment response.
        
        """
        user = request.user
        data = {
        'token': request.data.get('token'),
        'SECKEY': settings.FLUTTERWAVE_SECRET_KEY,
        'email': user.email,
        "country":"NG",
        "amount":request.data.get('amount'),
        "firstname": user.user_profile.first_name,
        "lastname":user.user_profile.last_name,
        "currency": "USD",
        "txRef":  "PAYL-card_Tref" + str(uuid.uuid4()),
        }
        payment = Payment.objects.create(
            payment_type='Card token',
            amount=data['amount'],
            amount_usd=data['amount'],
            user=user,
            description="Load wallet with saved card / token",
            txref=data['txRef'],
            save_card=True,
            activity_type=request.data.get('activity_type'),
            published_date=timezone.now()
            )
        response = requests.post(settings.FLUTTERWAVE_CHARGE_WITH_TOKEN, data=data)
        response_json = response.json()
        try:
            if response_json['data']['status'] == "successful":
                payment.appfee = response_json['data']['appfee']
                payment.payment_status = "verifying"
                payment.charge_response_code ="02"
                # payment.actual_amount = Decimal(response_json['data']['amount']) - Decimal(response_json['data']['appfee']) 
            else:
                payment.payment_status = "in_progress"
            payment.save()
            return Response({"status_code": status.HTTP_200_OK,
                        'message': response_json['message'],
                        "result": {
                            "data": response_json
                        }
                },status=status.HTTP_200_OK)
        except KeyError:
            return Response({"status_code": status.HTTP_400_BAD_REQUEST,
                        'message': response_json['message'],
                        "result": {
                            "data": response_json
                        }
                },status=status.HTTP_400_BAD_REQUEST)

        
        
        

   

    # Webhook to check if transaction is successful
    @csrf_exempt
    @action(methods=('POST',), detail=False, url_path='webhook',
        permission_classes=(AllowAny,))
    def payment_webhook_url(self, request, *args, **kwargs):
        """ Webhook that updates our object if transaction is successful.
            If transaction is Card Transaction:
                Deduct app fee(flutterwave transaction fee) from amount.
                Credit wallet with Amount after deduction.
            If Transaction is Mobile Money Ghana:

        """
        request_body = request.body
        request_json = json.loads(request_body)
        # requests.post('http://aab12add.ngrok.io', json=request_json)
        try:
            txRef = request_json['txRef']
        except KeyError:
            txRef = request_json['transfer']['reference']
        payment = Payment.objects.get(
            txref=txRef
        )
        my_wallet = UserWallet.objects.get(user=payment.user)
        webhook_transaction_type = ['MOBILEMONEYGH_TRANSACTION', 'MOBILEMONEYZM_TRANSACTION', 'MOBILEMONEYUG_TRANSACTION', 'MOBILEMONEYRW_TRANSACTION']
        payment.charge_response_code ="00"
        payment.webhook_status = "Webhook response"
        # payment.payment_status = "completed"
        payment.actual_amount = payment.amount - payment.appfee
        payment.save()
        if payment.charge_response_code =="00":
            if request_json['event.type'] == "CARD_TRANSACTION":
               
                update_payment_instance_after_webhook_response(txRef, request_json['status'], request_json['orderRef'], txRef)
                verify_payment_with_payment_processor_flutterwave(payment.txref)
                load_wallet_with_card_success_reponse(payment, my_wallet)
            elif request_json['event.type'] in webhook_transaction_type:
                # make some clarification here.
                amount_to_be_converted = Decimal(payment.amount) - Decimal(payment.appfee)
                response = requests.get(settings.CURRENCY_LAYER_API, '&from=' + request_json['currency'] +'&to=USD&amount=' + str(amount_to_be_converted))
                rate = response.json()
                rate_amount = rate['result']
                # usd_amount = Decimal(payment.actual_amount) / Decimal(rate_amount)
                # made changes here.
                currency = request_json['currency'] # This is to show in email the currency used in laoding the momo
                payment.actual_amount = rate_amount
                payment.charge_response_message = request_json['status']
                payment.orderRef = request_json['orderRef']
                payment.txref = txRef
                payment.save()
                # increase_wallet_balance_after_payment(payment.user, Decimal(payment.actual_amount), timezone.now())
                verify_payment_with_payment_processor_flutterwave(payment.txref)
                
                load_wallet_with_momo_success_reponse(payment, my_wallet, currency)
            # For funds withdraw
            elif request_json['event.type'] == "Transfer" and request_json['transfer']['status']=="SUCCESSFUL":
                if request_json['transfer']['currency'] != "USD":
                    convert_fee_only = requests.get(settings.CURRENCY_LAYER_API, '&from=' + request_json['transfer']['currency'] +'&to=USD&amount=' + str(request_json['transfer']['fee']))
                    rate = convert_fee_only.json()
                    rate_fee = rate['result']
                    total_amount = payment.amount_usd + Decimal(rate_fee)
                    decrease_wallet_balance_after_payment(payment.user, Decimal(total_amount), timezone.now())
                    payment.charge_response_message = request_json['transfer']['status']
                    payment.txref = request_json['transfer']['reference']
                    payment.appfee = rate_fee
                    payment.actual_amount = total_amount
                    payment.save()
                    withdraw_obj = Withdraw.objects.get(
                    txref=txRef
                    )
                    notify_user_successful_transfer(withdraw_obj)
                else:
                    # deduct money from wallet balance
                    total_amount = payment.amount + payment.appfee
                    decrease_wallet_balance_after_payment(payment.user, total_amount, timezone.now())
                    payment.actual_amount = total_amount
                    payment.save()
                    withdraw_obj = Withdraw.objects.get(
                        txref=txRef
                    )
                    notify_user_successful_transfer(withdraw_obj)
                # send_withdrawal_email_to_user()

        else:
            payment.charge_response_code ="00"
            payment.webhook_status = "Webhook response"
            payment.payment_status = "failed"
            payment.charge_response_message = request_json['status']
            payment.orderRef = request_json['orderRef']
            payment.save()
            load_wallet_failed_reponse(payment, my_wallet)
            return Response({'status_code': status.HTTP_400_BAD_REQUEST,
                        'message': "Your payment failed",
                    }, status=status.HTTP_400_BAD_REQUEST)
        return HttpResponse(request_json)

    # Verify payment processing by flutterwave
    # @action(methods=('POST',), detail=False, url_path='verify-payment', 
    #     permission_classes=(IsAuthenticated,))
    # def verify_payment_with_payment_processor_flutterwave(self, request, *args, **kwargs):
    #     """ Verify payment processing by flutterwave after webhook
        
    #     """
    #     try:
    #         payment = Payment.objects.get(txref=request.data.get('payment_reference'))
    #     except Payment.DoesNotExist:
    #         return Response({
    #         'message': 'The payment does not exist.'
    #         })
        
    #     data = {
    #     "txref": payment.txref,
    #     "normalize":"1",
    #     "SECKEY":settings.FLUTTERWAVE_SECRET_KEY
    #     }
    #     response = requests.post(settings.FLUTTERWAVE_VERIFY_PAYMENT,data=data)
    #     response_json = response.json()

    #     try:
    #         response_json['data']
    #         payment.verification_status = response_json['data']['status']
    #         payment.embedtoken = response_json['data']['card']['card_tokens'][0]['embedtoken']
    #         payment.card_type = response_json['data']['card']['type']
    #         payment.last_four_digit = response_json['data']['card']['last4digits']
    #         payment.save()
    #         # if response_json['data']['status'] == 'successful':
    #             # call
    #             # increase_wallet_balance_after_payment(payment.user, Decimal(response_json['data']))
    #         return Response({
    #             'message': 'Your payment verification status is {}'.format(response_json['status']),
    #             'result': {
    #                 'data': response_json
    #             }
    #         })
    #     except KeyError:
    #         return Response({
    #             'message': 'Your payment could not be verified. Please initiate the payment again.'
                
    #         })


    @action(methods=('POST',), detail=False, url_path='load-wallet-chile-qvo', 
        permission_classes=(IsAuthenticated,))
    def load_wallet_with_chile_processor(self, request, *args, **kwargs):
        """ Initialization of payment with chilean processor QVO.
        
        """
        headers = {
            'Content-Type': 'application/json',
            'Accept':  'application/json',
        #prod    # 'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJjb21tZXJjZV9pZCI6ImNvbV85WVdrUzE0ckVia0ZxVHhZM3g5dnlnIiwiYXBpX3Rva2VuIjp0cnVlfQ.tROQjD_103ZilDnBL2TLzCqpeRaunT7MJi7TIiA2Lw0 '
            'Authorization': settings.QVO_TOKEN

        }
        # url = 'https://api.qvo.cl/webpay_plus/charge'
        url = settings.QVO_URL
        #PUT YOUR PAYLOAD HERE
        payload = {
            "amount":request.data.get('amount'),
            "description": request.data.get('description'),
            "return_url": request.data.get('return_url')

        }
        response = requests.post(url, json=payload, headers=headers)
        json_response = response.json()
        payment = Payment.objects.create(
            payment_type='card',
            amount=payload['amount'],
            user=request.user,
            description="Wallet loaded with CLP & converted to USD",
            txref=json_response['transaction_id'],
            published_date=timezone.now()
            )
        return Response({
            'status_code': status.HTTP_200_OK,
            'message': 'Payment initiated with QVO payment',
            'result': {
                'data': response.json()
            }
        }, status=status.HTTP_200_OK)
    

    @csrf_exempt
    @action(methods=('POST',), detail=False, url_path='qvo-chile-payment-webhook',
        permission_classes=(AllowAny,))
    def qvo_chile_payment_webhook_url(self, request, *args, **kwargs):
        """ Webhook that updates our object if transaction is successful.
            If transaction is successful it will check if the activity_type == 'personal' or 'business':
               if personal, amount will be converted and loaded in wallet.
               If business, amount will be converted and paid into business escrow account.

        """
        headers = {
            'Content-Type': 'application/json',
            'Accept':  'application/json',
        }
        event_json = json.loads(request.body)
        status = event_json['data']['transaction']['status']
        currency = event_json['data']['transaction']['currency']
        appfee = event_json['data']['transaction']['payment']['fee']
        if status == 'successful' and event_json['type'] == "transaction.payment_succeeded" and currency == 'CLP':
            txRef = event_json['data']['transaction']['id']
            extra_fee = event_json['data']['transaction']['payment']['extra_fee']
            application_fee = event_json['data']['transaction']['payment']['application_fee']
            currency = event_json['data']['transaction']['currency']
            payment = Payment.objects.get(
                txref=txRef
            )
            payment.appfee = event_json['data']['transaction']['payment']['fee']
            payment.payment_status = 'completed'
            payment.webhook_status = 'Webhook response'
            payment.charge_response_message = 'successful'
            payment.charge_response_code = '00'
            payment.transaction_reference = event_json['id']
            payment.save()

            amount_to_be_converted = Decimal(payment.amount) - Decimal(payment.appfee) - Decimal(extra_fee) - Decimal(application_fee)
            # response = requests.get(settings.CURRENCY_LAYER_API, '&from=' + currency +'&to=USD&amount=' + str(amount_to_be_converted))
            actual_amount_usd = convert_currency(currency, str(amount_to_be_converted))
            # printing the rate from the amount conversion
            # Converting the appfee (**ACTIVATE THIS WHEN YOU LEAVE THE FREE PLAN.)
            appfee_conversion = convert_currency(currency, str(appfee))

            # made changes here.
            currency = currency # This is to show in email the currency used in laoding the momo
            payment.actual_amount = actual_amount_usd
            payment.appfee = appfee_conversion # App fee for payment processing.
            # payment.appfee = appfee_converted # Saving the new app fee.
            payment.save()
            user_wallet = UserWallet.objects.get(user=payment.user)
            if user_wallet:
                user_wallet.balance += Decimal(payment.actual_amount)
                user_wallet.last_used = timezone.now()
                user_wallet.save()
                if payment.activity_type == 'business':
                    api_transaction = ApiTransaction.objects.get(transaction_id=payment.api_transaction_id)
                    business_wallet = UserWallet.objects.get(user=api_transaction.collaborator)
                    if user_wallet.balance < api_transaction.usd_amount:
                        return Response({
                        # 'status_code': status.HTTP_401_UNAUTHORIZED,
                        'message': 'Insufficient balance in your wallet for this transaction.'
                        })
                    if api_transaction.transaction_type == 'e-commerce':
                        main_transaction = Transaction.objects.create(
                        name=api_transaction.transaction_name,
                        api_transaction= api_transaction,
                        description =api_transaction.description,
                        transaction_role = 'owner',
                        escrow_fee_payment= 'user',
                        initial_amount = api_transaction.usd_amount,
                        amount = api_transaction.usd_amount,
                        markup = api_transaction.usd_app_fee,
                        user = api_transaction.user,
                        contractor = api_transaction.collaborator,
                        project_owner_agreement= True,
                        contractor_agreement = True,
                        stage = 'ongoing',
                        activity_type = 'business',
                        has_paid = True,
                        created_date = timezone.now(),
                        published_date = timezone.now()
                        )
                        buyer_full_name = api_transaction.first_name + ' ' + api_transaction.last_name, 
                        send_funds_request_text_message_link_to_seller(buyer_full_name, api_transaction.collaborator.phone_number, api_transaction.transaction_id)
                    elif api_transaction.transaction_type == 'marketplace':
                        main_transaction = Transaction.objects.create(
                            name=api_transaction.transaction_name,
                            api_transaction= api_transaction,
                            description =api_transaction.description,
                            transaction_role = 'owner',
                            escrow_fee_payment= 'user',
                            initial_amount = api_transaction.usd_amount,
                            amount = api_transaction.usd_amount,
                            markup = api_transaction.usd_app_fee,
                            user = api_transaction.user,
                            contractor = api_transaction.merchant,
                            project_owner_agreement= True,
                            contractor_agreement = True,
                            stage = 'ongoing',
                            activity_type = 'business',
                            has_paid = True,
                            created_date = timezone.now(),
                            published_date = timezone.now()
                        )
                        buyer_full_name = api_transaction.first_name + ' ' + api_transaction.last_name, 
                        send_funds_request_text_message_link_to_seller(buyer_full_name, api_transaction.merchant.phone_number, api_transaction.transaction_id)
                    # create an escrow wallet. 
                    escrow_wallet = EscrowWallet.objects.create(
                        transaction=main_transaction,
                        sender=main_transaction.user,
                        recipient=main_transaction.contractor,
                        amount=main_transaction.amount,
                        balance=main_transaction.markup,
                        date_created=timezone.now(),
                        can_cancel = False,
                        is_active= True,
                        last_used = timezone.now()
                    )
                    # Deduct respective amounts from user/buyers wallet.
                    user_wallet.balance -= api_transaction.usd_amount
                    user_wallet.balance -= api_transaction.usd_business_fee
                    user_wallet.balance -= api_transaction.usd_app_fee
                    user_wallet.save()
                    # update the business wallet
                    business_wallet.balance += api_transaction.usd_business_fee
                    business_wallet.save()
                    # Update api transaction.
                    api_transaction.stage = 'created'
                    api_transaction.stage_description = 'Transaction has been paid for successfully and ongoing.'
                    api_transaction.save()

                    buyer_full_name = api_transaction.first_name + ' ' + api_transaction.last_name, 
                    # Send funds request message link to seller's email (seller_email, transaction)
                    send_email_to_seller_about_payment(api_transaction)
                    send_email_to_buyer_about_payment(api_transaction)
                else:
                    load_wallet_with_qvo_payment_success_response(payment, user_wallet, currency)
        elif event_json['type'] == "transaction.payment_failed":
            txRef = event_json['data']['transaction']['id']
            payment = Payment.objects.get(
                txref=txRef
            )
            payment.appfee = event_json['data']['transaction']['payment']['fee']
            payment.payment_status = 'failed'
            payment.webhook_status = 'Webhook response'
            payment.charge_response_message = 'failed'
            payment.charge_response_code = '00'
            payment.transaction_reference = event_json['id']
            payment.save()
        return HttpResponse(status=200)
        

    @action(methods=('POST',), detail=False, url_path='load-wallet-square', 
        permission_classes=(IsAuthenticated,))
    def load_wallet_square(self, request, *args, **kwargs):
        """ Initialization of payment with square processor.
        
        """
        card_nonce = "fake-card-nonce-ok"
        idempotency_key = str(uuid.uuid1())
        reference_id ="Confirmation #12345"
        custom_note = "This is (not) a helpful note!"

        # Set the charge amount to 10 USD
        amount_money =  {
            "amount" : 10,
            "currency" : "USD",
            }
        buyer_email_address = "thebuyer@example.com"
        billing_address = {
            "address_line_1" : "1500 Electric Ave",
            "address_line_2" : "Suite 600",
            "locality" : "New York City",
            "administrative_district_level_1" : "NY",
            "postal_code" : "20003",
            "country" : "US",
            }
        shipping_address = {
            "address_line_1" : "123 Main St",
            "locality" : "San Francisco",
            "administrative_district_level_1" : "CA",
            "postal_code" : "94114",
            "country" : "US",
            }
        
        response = requests.post("https://connect.squareupstaging.com/v2/locations/CBASEEkp3dPPpMh6BzFPLC025UEgAQ/transactions",
        headers = {
            "Accept": "application/json",
            "Content-Type": "application/json",
            "Authorization": "Bearer EAAAEN5bcMgzjukH6x7Yg-Wx-QHaSu1t5usnXEOUwEYjfVhulCb9mumGBpGgFSnI ",
        },
        params = json.dumps({
            "card_nonce": card_nonce,
            "amount_money": amount_money,
            "idempotency_key": idempotency_key,
            "reference_id": reference_id,
            "note": custom_note,
            "billing_address": billing_address,
            "shipping_address": shipping_address,
            "buyer_email_address": buyer_email_address
        })
        )
        print(response.body)
        return Response({
                'status_code': status.HTTP_200_OK,
                'message': "some respnose here",
                'result': {
                    'data': "something"
                }
            },status=status.HTTP_200_OK)

    # Change the status of saved cards
    # @action(methods=('GET',), detail=False, url_path='change-save-card-status', 
    #     permission_classes=(IsAuthenticated,))
    # def change_saved_card_status(self, request, *args, **kwargs):
    #     """ endpoint to change the saved cards status to No

    #     """
    #     user=request.user
    #     payment  = Payment.objects.all().update(save_card=False)
    #     # for saved_card in payment:
    #     #     saved_card = False
    #     # payment.update()
    #     return Response({'message': 'Saved cards status changed'})