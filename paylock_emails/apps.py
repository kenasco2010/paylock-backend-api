from django.apps import AppConfig


class PaylockEmailsConfig(AppConfig):
    name = 'paylock_emails'
