from django.test import TestCase
from django.core import mail
from django.test import TestCase, RequestFactory, Client
from django.contrib.auth import get_user_model
from django.utils import timezone
from django.test.utils import override_settings
from rest_framework.authtoken.models import Token
from rest_framework.test import APIRequestFactory, APIClient, force_authenticate
import json

from payment_method.models import PaymentMethod
from transaction_type.models import TransactionType
from user_profile.models import UserProfile
from paylock_emails.signals import *
User = get_user_model()

@override_settings(EMAIL_BACKEND='django.core.mail.backends.console.EmailBackend')
class EmailBackendTest(TestCase):
    def test_backend_email_setting(self):
        mail_sent_success = mail.send_mail('Email backend settings Test', 
                       'This is to test if email backend setting is working',
                       '', ['kenmartey89@gmail.com'],
                       fail_silently=False)
        self.assertEquals(mail_sent_success, 1)



@override_settings(EMAIL_BACKEND='django.core.mail.backends.console.EmailBackend')
class FundsRequestInvitationTest(TestCase):
    def setUp(self):
        self.credentials = {
            'email': 'kenmartey89@gmail.com',
            'phone_number': '00233246424340',
            'password': 'ceremony'
        }
        self.user = User.objects.create_superuser(**self.credentials)
        self.payment_method = PaymentMethod.objects.create(
            payment_type='Mobile Money',
            created_date=timezone.now(), 
            user=self.user)
        self.user_profile = {
            'first_name': 'Kennedy',
            'last_name': 'Anyinatoe',
            'date_of_birth': '1989-07-09',
            'current_address': 'East Legon',
            'profile_image': ''

        }
        self.user_profile = UserProfile.objects.create(**self.user_profile, 
            user=self.user)

        self.transaction_type = TransactionType.objects.create(
            transaction_type= 'Product',
            created_date=timezone.now(), 
            user=self.user)
        self.transaction_info = {
            'name': 'Nike shoes',
            'transaction_type': self.transaction_type,
            'description': 'A pair of Nike shoes from Jumia',
            'payment_method': self.payment_method,
            'amount': '900',
            'markup': '0.00',
            'contractor': self.user,
            'contractor_agreement': False,
            'project_owner_agreement': True,
            'created_date': timezone.now()
        }
        self.transaction = Transaction.objects.create(**self.transaction_info, user=self.user)
        self.approve_transfer = {
            'transaction_id': self.transaction.id,
            'transaction': self.transaction,
            'published_date': timezone.now()
        }
        self.funds_request = RequestFundsTransfer.objects.create(**self.approve_transfer)

    def test_send_funds_request_invitation(self):
        client = APIClient()
        headers = {
            'HTTP_AUTHORIZATION': 'Token {}'.format(
                self.user.auth_token)
        }
        mail_sent_success = mail.send_mail('Funds request by >>>>> '+ '' + str(self.funds_request.transaction.contractor), 
                    self.funds_request.transaction.description,
                    '', [str(self.funds_request.transaction.user)], #The owner of the buyer or project owner
                    fail_silently=False)
        self.assertEquals(mail_sent_success, 1)

   