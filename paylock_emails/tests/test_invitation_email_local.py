from django.test import TestCase
from django.core import mail
from django.test import TestCase, RequestFactory, Client
from django.contrib.auth import get_user_model
from django.utils import timezone
from django.test.utils import override_settings
from rest_framework.authtoken.models import Token
from rest_framework.test import APIRequestFactory, APIClient, force_authenticate
import json

from payment_method.models import PaymentMethod
from transaction_type.models import TransactionType
from paylock_emails.signals import *
User = get_user_model()

@override_settings(EMAIL_BACKEND='django.core.mail.backends.console.EmailBackend')
class EmailBackendTest(TestCase):
    def test_backend_email_setting(self):
        mail_sent_success = mail.send_mail('Email backend settings Test', 
                       'This is to test if email backend setting is working',
                       '', ['kenmartey89@gmail.com'],
                       fail_silently=False)
        self.assertEquals(mail_sent_success, 1)


@override_settings(EMAIL_BACKEND='django.core.mail.backends.console.EmailBackend')
class InvitationEmailTest(TestCase):
    def setUp(self):
        self.credentials = {
            'email': 'kenmartey89@gmail.com',
            'phone_number': '00233246424340',
            'password': 'ceremony'
        }
        self.payment_method_input = {
            'payment_type':'Mobile Money',
            'created_date': timezone.now()
        }
        self.transaction_type_input = {
            'transaction_type': 'Product',
            'created_date': timezone.now()
        }
        self.user_profile = {
            'first_name': 'Kennedy',
            'last_name': 'Anyinatoe',
            'date_of_birth': '1989-07-09',
            'current_address': 'East Legon',
            'country': 'Ghana',
            'profile_image': ''

        }
        self.user_profile_api_data = {
            'first_name': 'Kennedy',
            'last_name': 'Anyinatoe',
            'date_of_birth': '1989-07-09',
            'current_address': 'East Legon',
            'country': 'Ghana',
            'profile_image': ''

        }
        self.user = User.objects.create_superuser(**self.credentials)
        payment_method = PaymentMethod.objects.create(**self.payment_method_input, 
        user=self.user)
        transaction_type = TransactionType.objects.create(**self.transaction_type_input, 
        user=self.user)
        self.user_profile = UserProfile.objects.create(**self.user_profile, 
            user=self.user)
        self.transaction_info = {
        'name': 'Nike shoes',
        'transaction_type': transaction_type.id,
        'description': 'A pair of Nike shoes from Jumia',
        'payment_method': payment_method.id,
        'amount': '900',
        'contractor': self.user.id,
        'contractor_agreement': False,
        'project_owner_agreement': True,
        'created_date': timezone.now()
        }


    def test_product_invitation_email(self):
        client = APIClient()
        headers = {
            'HTTP_AUTHORIZATION': 'Token {}'.format(
                self.user.auth_token)
        }
        request = self.client.post('/api/v1.0/transactions/create-transaction/', 
        self.transaction_info, format='json', **headers)
        self.contractor_id = request.data['result']['data']['contractor']
        self.transaction_id = request.data['result']['data']['transaction_type']
        self.transaction_type = TransactionType.objects.get(pk=self.transaction_id)
        self.contractor_email = get_user_model().objects.get(pk=self.contractor_id)
        mail_sent_success = mail.send_mail('Transaction Invitation. You have been invited to join '+ ''+ request.data['result']['data']['name'], 
                    request.data['result']['data']['description'],
                    '', [str(self.contractor_email)],
                    fail_silently=False)
        self.assertEquals(mail_sent_success, 1)
    
    
    def test_send_email_after_user_signup_and_fill_profile(self):
        client = APIClient()
        headers = {
            'HTTP_AUTHORIZATION': 'Token {}'.format(
                self.user.auth_token)
        }
        request = self.client.post('/api/v1.0/user-profile/create/', 
        self.user_profile_api_data, format='json', **headers)
        self.first_name = request.data['result']['data']['first_name']
        self.last_name = request.data['result']['data']['last_name']
        mail_sent_success = mail.send_mail('Welcome {} {} to PayLock'.format(self.first_name, self.last_name),
                        'We are glad you have joined us on PayLock. Kindly complete your identity verifications',
                        '', [str(self.user.email)],
                        fail_silently=False)
        self.assertEquals(mail_sent_success, 1)