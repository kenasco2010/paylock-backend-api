from django.shortcuts import render
from django.core.mail import EmailMultiAlternatives
from django.template.loader import get_template
from django.template import Context
from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver
from django.conf import settings
from celery.decorators import task
# Activate account packages
from user_account.tokens import account_activation_token, \
                    reset_password_token
from django.utils.http import urlsafe_base64_encode, \
    urlsafe_base64_decode
from django.utils.encoding import force_bytes, force_text
from django.contrib.sites.shortcuts import get_current_site
from django.conf import settings
import base64


def send_email(html_template, plaintext_template, email_subject, context, to):
    """ Email function for sending emails
    
    """
    htmly = get_template(html_template)
    plaintext = get_template(plaintext_template)
    d = context
    to=str(to)
    text_content = plaintext.render(d)
    html_content = htmly.render(d)
    msg = EmailMultiAlternatives(email_subject, text_content,
                               settings.DEFAULT_FROM_EMAIL, [to])
    msg.attach_alternative(html_content, "text/html")
    msg.send()



@task
def send_email_funds_has_been_approved_job(funds_details, **kwargs):
    """Send email to contractor or buyer when funds request is approved
    
    """
    send_email(
        html_template = 'funds_request/approved_funds_request_invitation.html',
        plaintext_template = 'funds_request/approved_funds_request_invitation.txt',
        email_subject = 'Funds has been Approved',
        to=funds_details.transaction.contractor,
        context = {
            'id': funds_details.id,
            'transaction': funds_details.transaction.name,
            'transaction_type': funds_details.transaction.transaction_type,
            'description': funds_details.transaction.description,
            'transaction_amount': funds_details.transaction.amount,
            'contractor': funds_details.transaction.contractor.user_profile.first_name,
            'project_owner': funds_details.transaction.user.user_profile.first_name,
            'domain_url': settings.DOMAIN_URL,
            'request_description': funds_details.request_description,
            'request_amount': funds_details.amount,
            'date_created': funds_details.created_date
            }
    )


@task  
def send_email_funds_has_been_rejected_job(funds_details, **kwargs):
    """ Send email to contractor or buyer when funds request is rejected
    
    """
    send_email(
        html_template = 'funds_request/reject_funds_request_invitation.html',
        plaintext_template = 'funds_request/reject_funds_request_invitation.txt',
        email_subject = 'Funds has been Rejected',
        to=funds_details.transaction.contractor,
        context = {
            'id': funds_details.id,
            'transaction': funds_details.transaction.name,
            'transaction_type': funds_details.transaction.transaction_type,
            'description': funds_details.transaction.description,
            'amount': funds_details.transaction.amount,
            'project_owner': funds_details.transaction.user.user_profile.first_name,
            'domain_url': settings.DOMAIN_URL,
            'request_description': funds_details.request_description,
            'request_amount': funds_details.amount,
            'date_created': funds_details.created_date
            }
    )


@task
def send_email_reverse_transaction_has_been_approved_job(reverse_obj, **kwargs):
    """ Send email to transaction owner when reverse transaction is approved by collaborator.
    
    """
    send_email(
            html_template = 'reverse_transaction/approved_reverse_transaction.html',
            plaintext_template = 'reverse_transaction/approved_reverse_transaction.txt',
            email_subject = 'Reverse transaction approved',
            to=reverse_obj.transaction.user,
            context = {
                'id': reverse_obj.id,
                'reverse_amount': reverse_obj.amount,
                'transaction': reverse_obj.transaction.name,
                'transaction_type': reverse_obj.transaction.transaction_type,
                'description': reverse_obj.transaction.description,
                'transaction_amount': reverse_obj.transaction.amount,
                # 'payment_method': reverse_obj.transaction.payment_method,
                'project_owner': reverse_obj.transaction.user.user_profile.first_name,
                'contractor': reverse_obj.transaction.contractor.user_profile.first_name,
                'domain_url': settings.DOMAIN_URL,
                'date_created': reverse_obj.created_date,
                }
        )


@task
def send_email_reverse_transaction_has_been_declined_job(reverse_obj, **kwargs):
    send_email(
            html_template = 'reverse_transaction/declined_reverse_transaction.html',
            plaintext_template = 'reverse_transaction/declined_reverse_transaction.txt',
            email_subject = 'Reverse transaction declined',
            to=reverse_obj.transaction.user,
            context = {
                'id': reverse_obj.id,
                'transaction': reverse_obj.transaction.name,
                'reverse_amount': reverse_obj.amount,
                'transaction_type': reverse_obj.transaction.transaction_type,
                'description': reverse_obj.transaction.description,
                'amount': reverse_obj.transaction.amount,
                # 'payment_method': reverse_obj.transaction.payment_method,
                'project_owner': reverse_obj.transaction.user.user_profile.first_name,
                'contractor': reverse_obj.transaction.contractor.user_profile.first_name,
                'date_created': reverse_obj.created_date,
                }
        )


def send_wallet_activation_email(request, user, create_wallet, **kwargs):
    """ Send activation email after account signup on paylock.
    
    """
    current_site = get_current_site(request)
    send_email(
            html_template = 'accounts/activate_wallet.html',
            plaintext_template = 'accounts/activate_wallet.txt',
            email_subject = 'Activate your Wallet on PayLock',
            to=user,
            context = {
                        'user': user,
                        'domain': current_site,
                        'userid':urlsafe_base64_encode(force_bytes(user.pk)).decode(),
                        # 'token':account_activation_token.make_token(user),
                        'wallet_activation_code': urlsafe_base64_encode(force_bytes(create_wallet.activation_code)).decode()
                        }
        )

@task
def reset_paylock_account_password_job(user, **kwargs):
    """ Reset your account password on payLock.
    
    """
    send_email(
            html_template = 'accounts/reset_password_email.html',
            plaintext_template = 'accounts/reset_password_email.txt',
            email_subject = 'Reset your account password on PayLock',
            to=user,
            context = {
                        'user': user,
                        'domain': settings.RESET_PASS_SITE_URL,
                        'user_pk':base64.b64encode(force_bytes(user.pk)),
                        'token':user.reset_pass_token,
                        'email': base64.b64encode(force_bytes(user))
                        
                        }
        )

def after_activation_email_template(request, **kwargs):
    """ Activation page afer activating user account
    
    """
    context ={
        'domain_url': settings.DOMAIN_URL
    }
    template = 'accounts/after_account_activation.html'
    return render(request, template, context)

# All emails about dispute on PayLock
def send_email_to_collaborator_about_dispute(dispute,**kwargs):
    """ Send email to collaborator when a dispute is created on PayLock 
        by transaction owner.

    
    """
    send_email(
            html_template = 'disputes/email_to_collaborator_about_dispute.html',
            plaintext_template = 'disputes/email_to_collaborator_about_dispute.txt',
            email_subject = 'A dispute has been raised on your transaction.',
            to=dispute.transaction.contractor,
        #     dispute information
            context = {
                'id': dispute.id,
			    'dispute_number': dispute.dispute_number,
                'dispute_reason': dispute.reason,
                'transaction': dispute.transaction.name,
                'transaction_type': dispute.transaction.transaction_type,
                'transaction_amount': dispute.transaction.amount,
                'transaction_owner': dispute.transaction.user.user_profile.first_name,
                'transaction_collaborator': dispute.transaction.contractor.user_profile.first_name,
                'dispute_created_date':dispute.date_created,
                'collaborator_name':dispute.transaction.contractor.user_profile.first_name,
                'domain_url': settings.DOMAIN_URL,
                }
        )


def copy_of_dispute_to_collaborator(dispute,**kwargs):
    """ Send a copy of dispute to  collaborator when he/she raises 
        a dispute on PayLock.
    
    """
    send_email(
            html_template = 'disputes/copy_dispute_info_to_collaborator.html',
            plaintext_template = 'disputes/copy_dispute_info_to_collaborator.txt',
            email_subject = 'You just raised a transaction dispute on PayLock.',
            to=dispute.transaction.contractor,
        #     dispute information
            context = {
                'id': dispute.id,
			    'dispute_number': dispute.dispute_number,
                'dispute_reason': dispute.reason,
                'transaction': dispute.transaction.name,
                'transaction_type': dispute.transaction.transaction_type,
                'transaction_amount': dispute.transaction.amount,
                'transaction_owner': dispute.transaction.user.user_profile.first_name,
                'transaction_collaborator': dispute.transaction.contractor.user_profile.first_name,
                'dispute_created_date':dispute.date_created,
                'collaborator_name':dispute.transaction.contractor.user_profile.first_name,
                'domain_url': settings.DOMAIN_URL,
            }
        )

def email_transaction_owner_about_dispute(dispute,**kwargs):
    """ Send email to transaction owner when a collaborator _
        creates the dispute on PayLock.
    
    """
    send_email(
            html_template = 'disputes/email_transaction_owner_about_dispute.html',
            plaintext_template = 'disputes/email_transaction_owner_about_dispute.txt',
            email_subject = 'A dispute has been raised on your transaction.',
            to=dispute.transaction.user,
        #     dispute information
            context = {
                'id': dispute.id,
                'dispute_number': dispute.dispute_number,
                'dispute_reason': dispute.reason,
                'transaction': dispute.transaction.name,
                'transaction_type': dispute.transaction.transaction_type,
                'transaction_amount': dispute.transaction.amount,
                'transaction_owner': dispute.transaction.user.user_profile.first_name,
                'transaction_collaborator': dispute.transaction.contractor.user_profile.first_name,
                'dispute_created_date':dispute.date_created,
                'transaction_owner_name':dispute.transaction.user.user_profile.first_name,
                'domain_url': settings.DOMAIN_URL,
            }
        )

def copy_of_dispute_to_transaction_owner(dispute,**kwargs):
    """ Send a copy of the dispute when transaction owner _
        creates the dispute on PayLock.
    
    """
    send_email(
            html_template = 'disputes/copy_dispute_info_to_onwer.html',
            plaintext_template = 'disputes/copy_dispute_info_to_onwer.txt',
            email_subject = 'You just raised a transaction dispute on PayLock.',
            to=dispute.transaction.user,
        #     dispute information
            context = {
                'id': dispute.id,
                'dispute_number': dispute.dispute_number,
                'dispute_reason': dispute.reason,
                'transaction': dispute.transaction.name,
                'transaction_type': dispute.transaction.transaction_type,
                'transaction_amount': dispute.transaction.amount,
                'transaction_owner': dispute.transaction.user,
                'transaction_collaborator': dispute.transaction.contractor.user_profile.first_name,
                'dispute_created_date':dispute.date_created,
                'transaction_owner_name':dispute.transaction.user.user_profile.first_name,
                'domain_url': settings.DOMAIN_URL,
            }
        )

@task
def dispute_response_email_job(dispute,**kwargs):
    """ Send email to whoever creates a dispute when a party 
        responds to the dispute.
    
    """
    send_email(
            html_template = 'disputes/dispute_response_email.html',
            plaintext_template = 'disputes/dispute_response_email.txt',
            email_subject = 'Your dispute has been agreed by your collaborator.',
            to=dispute.user,
        #     dispute information
            context = {
                'id': dispute.id,
                'dispute_number': dispute.dispute_number,
                'dispute_reason': dispute.reason,
                'transaction': dispute.transaction.name,
                'transaction_type': dispute.transaction.transaction_type,
                'transaction_amount': dispute.transaction.amount,
                'transaction_owner': dispute.transaction.user.user_profile.first_name,
                'transaction_collaborator': dispute.transaction.contractor.user_profile.first_name,
                'dispute_created_date':dispute.date_created,
                'transaction_owner_name':dispute.transaction.user.user_profile.first_name,
                'domain_url': settings.DOMAIN_URL
            }
        )

@task
def resolve_dispute_email_job(dispute,**kwargs):
    """ Send email to whoever creates a dispute when a party 
        responds to the dispute.
    
    """
    send_email(
            html_template = 'disputes/resolve_dispute_email.html',
            plaintext_template = 'disputes/resolve_dispute_email.txt',
            email_subject = 'Your dispute has been resolved by your transaction creator.',
            to=dispute.dispute_against,
        #     dispute information
            context = {
                'id': dispute.id,
                'dispute_number': dispute.dispute_number,
                'dispute_reason': dispute.reason,
                'transaction': dispute.transaction.name,
                'transaction_type': dispute.transaction.transaction_type,
                'transaction_amount': dispute.transaction.amount,
                'transaction_owner': dispute.transaction.user,
                'transaction_collaborator': dispute.transaction.contractor.user_profile.first_name,
                'dispute_created_date':dispute.date_created,
                'transaction_owner_name':dispute.transaction.user.user_profile.first_name,
                'domain_url': settings.DOMAIN_URL
            }
        )

@task
def send_email_to_legal_rep_about_dispute_job(instance, **kwargs):
    """ Send email to legal rep when a user invites him/her to join 
        a particular dispute.

    """
    send_email(
            html_template = 'legal/email_to_legal_rep_about_dispute.html',
            plaintext_template = 'legal/email_to_legal_rep_about_dispute.txt',
            email_subject = 'You have been added to resolve a dispute on PayLock',
            to=instance.legal_rep.user,
        #     dispute information
            context = {
                'id': instance.dispute.id,
                'dispute_number': instance.dispute.dispute_number,
                'dispute_created_date': instance.dispute.date_created,
                'dispute_reason': instance.dispute.reason,
                'transaction': instance.dispute.transaction.name,
                'transaction_type': instance.dispute.transaction.transaction_type,
                'transaction_amount': instance.dispute.transaction.amount,
                'transaction_owner': instance.dispute.transaction.user,
                'transaction_collaborator': instance.dispute.transaction.contractor,
                'legal_rep_name': instance.legal_rep.rep_name,
                'legal_rep_charge': instance.legal_rep.charge_rate,
                'domain_url': settings.DOMAIN_URL,
            }
        )

@task 
def payment_of_legal_fees_job(instance, **kwargs):
    """ Send email to invitor when money is deducted 
        from his / wallet after inviting an arbitrator.
    """
    send_email(
            html_template = 'legal/payment_of_legal_fees.html',
            plaintext_template = 'legal/payment_of_legal_fees.txt',
            email_subject = 'Payment of legal fees for dispute resolution',
            to=instance.invitor,
        #   dispute information
            context = {
                'id': instance.dispute.id,
                'dispute_number': instance.dispute.dispute_number,
                'dispute_created_date': instance.dispute.date_created,
                'dispute_reason': instance.dispute.reason,
                'transaction': instance.dispute.transaction.name,
                'transaction_type': instance.dispute.transaction.transaction_type,
                'transaction_amount': instance.dispute.transaction.amount,
                'transaction_owner': instance.dispute.transaction.user,
                'transaction_collaborator': instance.dispute.transaction.contractor,
                'legal_rep_name': instance.legal_rep.rep_name,
                'legal_rep_charge': instance.legal_rep.charge_rate,
                'domain_url': settings.DOMAIN_URL,
            }
        )

#  Dispute comment email notification
@task
def send_email_to_dispute_against_about_comment_job(dispute, comment, **kwargs):
    """ Send email to dispute against person when the creator leaves
    a comment.
    """
    send_email(
            html_template = 'dispute_comments/email_dispute_against_about_comment.html',
            plaintext_template = 'dispute_comments/email_dispute_against_about_comment.txt',
            email_subject = 'A reply has been left on the dispute. ',
            to=dispute.dispute_against,
        #   dispute information
            context = {
                'id': dispute.id,
                'dispute_number': dispute.dispute_number,
                'comment': comment.comment,
                'comment_date': comment.date_created,
                'domain_url': settings.DOMAIN_URL,
            }
        )
@task
def send_email_to_dispute_owner_about_comment_job(dispute, comment, **kwargs):
    """ Send email to dispute against person when the creator leaves
    a comment.
    """
    print("sending email ------>")
    send_email(
            html_template = 'dispute_comments/email_dispute_owner_about_comment.html',
            plaintext_template = 'dispute_comments/email_dispute_owner_about_comment.txt',
            email_subject = 'A reply has been left on the dispute. ',
            to=dispute.dispute_creator,
        #   dispute information
            context = {
                'id': dispute.id,
                'dispute_number': dispute.dispute_number,
                'comment': comment.comment,
                'comment_date': comment.date_created,
                'domain_url': settings.DOMAIN_URL,
            }
        )

@task
def send_email_to_dispute_creator_about_legal_advisor_commentjob(dispute, comment, **kwargs):
    """ Send email to dispute creator
        when a legal rep adds a comment to the dispute.
    """
    send_email(
            html_template = 'dispute_comments/email_dispute_creator_about_advisor_comment.html',
            plaintext_template = 'dispute_comments/email_dispute_creator_about_advisor_comment.txt',
            email_subject = 'Your legal advisor left a message. ',
            to=dispute.dispute_creator,
        #   dispute information
            context = {
                'id': dispute.id,
                'dispute_number': dispute.dispute_number,
                'comment': comment.comment,
                'comment_date': comment.date_created,
                'domain_url': settings.DOMAIN_URL,
            }
        )

@task
def send_email_to_dispute_against_about_legal_advisor_commentjob(dispute, comment, **kwargs):
    """ Send email to about legal against person
        when a legal rep adds a comment to the dispute.
    """
    send_email(
            html_template = 'dispute_comments/email_dispute_againt_about_advisor_comment.html',
            plaintext_template = 'dispute_comments/email_dispute_againt_about_advisor_comment.txt',
            email_subject = 'Your legal advisor left a message. ',
            to=dispute.dispute_against,
        #   dispute information
            context = {
                'id': dispute.id,
                'dispute_number': dispute.dispute_number,
                'comment': comment.comment,
                'comment_date': comment.date_created,
                'domain_url': settings.DOMAIN_URL,
            }
        )

@task 
def load_wallet_with_card_success_reponse_job(payment, my_wallet, **kwargs):
    """ Send email to user when load wallet is successful with card
        On PayLock.
    """
    send_email(
            html_template = 'load_wallet/card_load_wallet_success.html',
            plaintext_template = 'load_wallet/card_load_wallet_success.txt',
            email_subject = 'Wallet loaded successfully',
            to=payment.user,
        #   dispute information
            context = {
                'wallet_id': my_wallet.wallet_number,
                'amount': payment.amount,
                'charge_fee': payment.appfee,
                'actual_balance': payment.actual_amount,
                'remarks': "Wallet loaded successfully",
                'loaded_date': payment.created_date,
                'domain_url': settings.DOMAIN_URL,
            }
        )

@task 
def load_wallet_failed_reponse_job(payment, my_wallet, **kwargs):
    """ Send email to user when load wallet fails with card
        On PayLock.
    """
    send_email(
            html_template = 'load_wallet/card_load_wallet_fail.html',
            plaintext_template = 'load_wallet/card_load_wallet_fail.txt',
            email_subject = 'Wallet loading failed',
            to=payment.user,
        #   dispute information
            context = {
                'wallet_id': my_wallet.wallet_number,
                'amount': payment.amount,
                'charge_fee': payment.appfee,
                'actual_balance': payment.actual_amount,
                'remarks': "Wallet not loaded successfully",
                'loaded_date': payment.created_date,
                'domain_url': settings.DOMAIN_URL,
            }
        )


@task 
def load_wallet_with_momo_success_reponse_job(payment, my_wallet, currency, **kwargs):
    """ Send email to user when load wallet is successful with momo in GHS
        On PayLock.
    """
    send_email(
            html_template = 'load_wallet/momo_load_wallet_success.html',
            plaintext_template = 'load_wallet/momo_load_wallet_success.txt',
            email_subject = 'Wallet loaded successfully with mobile money.',
            to=payment.user,
        #   dispute information
            context = {
                'wallet_id': my_wallet.wallet_number,
                'wallet_load_currency': currency,
                'amount': payment.amount,
                'charge_fee': payment.appfee,
                'actual_balance': payment.actual_amount,
                'remarks': "Wallet loaded successfully",
                'loaded_date': payment.created_date,
                'domain_url': settings.DOMAIN_URL,
            }
        )

@task 
def load_wallet_chile_qvo_success_reponse_job(payment, my_wallet, currency, **kwargs):
    """ Send email to user when load wallet is successful with QVO in CLP
        On PayLock.
    """
    send_email(
            html_template = 'load_wallet/load_wallet_with_qvo_chile_success.html',
            plaintext_template = 'load_wallet/load_wallet_with_qvo_chile_success.txt',
            email_subject = 'Wallet loaded successfully.',
            to=payment.user,
            context = {
                'wallet_id': my_wallet.wallet_number,
                'wallet_load_currency': currency,
                'amount': payment.amount,
                'charge_fee': payment.appfee,
                'actual_balance': payment.actual_amount,
                'remarks': "Wallet loaded successfully",
                'loaded_date': payment.created_date,
                'domain_url': settings.DOMAIN_URL,
            }
        )

def reminder_to_respond_to_funds_request(funds_requests):
    """ Remind transaction owner to respond
        to the funds request by collaborator
    """
    print("The email function is called.", [funds_requests.transaction.user.email])
    send_email(
        html_template = 'funds_request/reminder_to_respond_to_funds_requests.html',
        plaintext_template = 'funds_request/reminder_to_respond_to_funds_requests.txt',
        email_subject = 'Reminder: You have to respond to your funds request.',
        to=funds_requests.transaction.user.email,
        context = {

            'id': funds_requests.id,
            'transaction': funds_requests.transaction.name,
            'transaction_type': funds_requests.transaction.transaction_type,
            'description': funds_requests.transaction.description,
            'transaction_amount': funds_requests.transaction.amount,
            'contractor': funds_requests.transaction.contractor.user_profile.first_name,
            'project_owner': funds_requests.transaction.user.user_profile.first_name,
            'domain_url': settings.DOMAIN_URL,
            'request_description': funds_requests.request_description,
            'request_amount': funds_requests.amount,
            'days_remaining': funds_requests.response_days_remaining,
            'date_created': funds_requests.created_date
            }
    )
    
def automatic_payment_success_email(funds_requests):
    """ An email to tell the collaborator, he/she has been 
        paid automatically after project owner not approving or 
        rejecting funds.
    """
    send_email(
        html_template = 'funds_request/automatic_payment_successful.html',
        plaintext_template = 'funds_request/automatic_payment_successful.txt',
        email_subject = '16 days is up and You have been paid.',
        to=funds_requests.transaction.contractor.email,
        context = {

            'id': funds_requests.id,
            'transaction': funds_requests.transaction.name,
            'transaction_type': funds_requests.transaction.transaction_type,
            'description': funds_requests.transaction.description,
            'transaction_amount': funds_requests.transaction.amount,
            'contractor': funds_requests.transaction.contractor.user_profile.first_name,
            'project_owner': funds_requests.transaction.user.user_profile.first_name,
            'domain_url': settings.DOMAIN_URL,
            'request_description': funds_requests.request_description,
            'request_amount': funds_requests.amount,
            'days_remaining': funds_requests.response_days_remaining,
            'date_created': funds_requests.created_date
            }
    )

def send_email_to_invite_collaborator(email, invitee_full_name):
    """ A customized email to invite a collaborator to join PayLock. 

    """
    send_email(
        html_template = 'invite-users/invite_collaborator_during_onboarding.html',
        plaintext_template = 'invite-users/invite_collaborator_during_onboarding.txt',
        email_subject = 'Transaction collaboration invitation.',
        to=email,
        context = {
            'email': email,
            'invitee_full_name': invitee_full_name,
            'domain_url': settings.DOMAIN_URL
            }
    )

def email_transaction_partner_about_account(request, user, invitee_full_name, temporal_password, wallet):
    """ Email to transaction partner to sign up on PayLock and collaborate

    """
    current_site = get_current_site(request)
    send_email(
        html_template = 'invite-users/invite_collaborator_after_automatic_signup.html',
        plaintext_template = 'invite-users/invite_collaborator_after_automatic_signup.txt',
        email_subject = 'Invitation to join a transaction.',
        to=user.email,
        context = {
            'email': user.email,
            'invitee_full_name': invitee_full_name,
            'temporal_password': temporal_password,
            'domain': current_site,
            'userid':urlsafe_base64_encode(force_bytes(user.pk)).decode(),
            'wallet_activation_code': urlsafe_base64_encode(force_bytes(wallet.activation_code)).decode(),
            'domain_url': settings.DOMAIN_URL
            }
    )

@task
def funds_sent_email_by_wallet_direct_job(wallet_direct, **kwargs):
    """
        Create an  email task to notify transaction owner when 
        money is sent using PayLock's wallet direct.
    
    """
    send_email(
        html_template = 'wallet_direct/funds_sent.html',
        plaintext_template = 'wallet_direct/funds_sent.txt',
        email_subject = 'Funds sent by wallet direct on PayLock',
        to=wallet_direct.user.email,
        context = {
            'id': wallet_direct.id,
            'name': wallet_direct.name,
            'description': wallet_direct.description,
            'amount': wallet_direct.amount,
            'markup': wallet_direct.markup,
            'receiver_first_name': wallet_direct.receiver.user_profile.first_name,
            'receiver_last_name': wallet_direct.receiver.user_profile.last_name,
            'receiver_email': wallet_direct.receiver,
            'domain_url': settings.DOMAIN_URL,
            'date_created': wallet_direct.created_date
            }
    )

@task
def funds_received_by_wallet_direct_by_email_job(wallet_direct, **kwargs):
    """
        Create an  email task to notify transaction owner when 
        money is sent using PayLock's wallet direct.
    
    """
    send_email(
        html_template = 'wallet_direct/funds_received.html',
        plaintext_template = 'wallet_direct/funds_received.txt',
        email_subject = 'Funds received by wallet direct on PayLock',
        to=wallet_direct.receiver.email,
        context = {
            'id': wallet_direct.id,
            'name': wallet_direct.name,
            'description': wallet_direct.description,
            'amount': wallet_direct.amount,
            'markup': wallet_direct.markup,
            'sender_first_name': wallet_direct.user.user_profile.first_name,
            'sender_last_name': wallet_direct.user.user_profile.last_name,
            'sender_email': wallet_direct.user.email,
            'domain_url': settings.DOMAIN_URL,
            'date_created': wallet_direct.created_date
            }
    )

@task
def send_email_to_seller_about_payment_job(transaction,**kwargs):
    """ Send email to seller informing him/her about the purchase payment
        to the escrow account.

        Inform seller to click request for funds when Item is delivered.
    """
    send_email(
            html_template = 'api_integration/email_seller_about_payment.html',
            plaintext_template = 'api_integration/email_seller_about_payment.txt',
            email_subject = 'Purchase notification on your site.',
            to=transaction.collaborator,
        #     tranasaction details
            context = {
                'id': transaction.id,
			    'transaction_id': transaction.transaction_id,
                'amount': transaction.amount,
                'charges': transaction.app_fee,
                'currency': transaction.currency,
                'transaction_name': transaction.transaction_name,
                'first_name': transaction.first_name,
                'last_name': transaction.last_name,
                'phone_number': transaction.phone_number,
                'created_date': transaction.created_date,
                'domain_url': settings.DOMAIN_URL,
                }
        )

@task
def send_email_to_buyer_about_payment_job(transaction,**kwargs):
    """ Email receipt to buyer informing him/her about the purchase payment
        to the escrow account.

        Inform buy to release money after Item is delivered.
    """
    send_email(
            html_template = 'api_integration/email_buyer_about_payment.html',
            plaintext_template = 'api_integration/email_buyer_about_payment.txt',
            email_subject = 'Receipt for your payment using PayLock payment.',
            to=transaction.user,
        #     tranasaction details
            context = {
                'id': transaction.id,
			    'transaction_id': transaction.transaction_id,
                'amount': transaction.amount,
                'charges': transaction.app_fee,
                'currency': transaction.currency,
                'transaction_name': transaction.transaction_name,
                'first_name': transaction.first_name,
                'last_name': transaction.last_name,
                'phone_number': transaction.phone_number,
                'created_date': transaction.created_date,
                'domain_url': settings.DOMAIN_URL,
                }
        )

@task
def send_email_to_seller_money_has_been_released_job(transaction,**kwargs):
    """ Email seller funds have been released to his PayLock account when buyer.
        releases the funds.
    """
    send_email(
            html_template = 'api_integration/email_seller_about_payment_release.html',
            plaintext_template = 'api_integration/email_seller_about_payment_release.txt',
            email_subject = 'Buyer {} {} has released your funds.'.format(transaction.first_name, transaction.last_name),
            to=transaction.user,
        #   tranasaction details
            context = {
                'id': transaction.id,
			    'transaction_id': transaction.transaction_id,
                'amount': transaction.amount,
                'charges': transaction.app_fee,
                'currency': transaction.currency,
                'transaction_name': transaction.transaction_name,
                'first_name': transaction.first_name,
                'last_name': transaction.last_name,
                'phone_number': transaction.phone_number,
                'created_date': transaction.created_date,
                'domain_url': settings.DOMAIN_URL,
                }
        )