from django.shortcuts import render
from django.core.mail import EmailMultiAlternatives
from django.template.loader import get_template
from django.template import Context
from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver
from django.conf import settings
from transaction.models import Transaction
from request_funds_transfer.models import RequestFundsTransfer
from user_profile.models import UserProfile
from dispute.models import Dispute

from wallet.models import EscrowWallet
from paylock_emails.views import *
from paylock_emails.tasks import *
from transaction_invitation.models import TransactionInvitation
from reverse_transaction.models import ReverseTransaction
from withdraw.models import Withdraw
from legal_firm.models import InviteLegalRep
# import boto3



@receiver(post_save, sender=TransactionInvitation)
def send_invitation_to_join_project( instance, created=False,  **kwargs):
    send_invitation_to_join_a_transaction_job.delay(instance, created=created)
    
@receiver(post_save, sender=EscrowWallet)
def escrow_wallet_details_to_contractor(instance, created=False, **kwargs):
	escrow_wallet_details_to_contractor_job.delay(instance, created=created)

@receiver(post_save, sender=TransactionInvitation)
def accept_and_decline_transaction_invitation_email(instance, **kwargs):
   """Email sent to transaction owner when collaborator accepts or
      rejects the transaction invitation. 
   """
   accept_transaction_invitation_email_job.delay(instance)
   decline_transaction_invitation_email_job.delay(instance)

@receiver(post_save, sender=ReverseTransaction)
def send_reverse_transaction_email_to_contractor_for_approval(instance, created=False, **kwargs):
	send_reverse_transaction_email_to_contractor_for_approval_job.delay(instance, created=created)

# imported on withdraw views.py
def send_withdrawal_email_to_user(withdraw, **kwargs):
   """ User receives an email when request to withdraw from 
      PayLock wallet.
   """
   send_withdrawal_email_to_user_job.delay(withdraw)

@receiver(post_save, sender=RequestFundsTransfer)
def send_funds_request_invitation(instance, created=False, **kwargs):
    """Send email to transaction owner about funds request on transaction"""
    funds_request_on_transaction_job.delay( instance, created=created)

# Send a welcome email to users after creating the profile
@receiver(post_save, sender=UserProfile)
def welcome_email_after_profile(instance, created=False, **kwargs):
    """ Send email to welcome users when they create profile"""
    welcome_email_after_profile_job.delay(instance, created=created)

@receiver(post_save, sender=Dispute)
def send_email_to_paylock_on_dispute(instance, created=False, **kwargs):
   """Send email to PayLock when a dispute is raised on a transaction."""
   send_email_to_paylock_on_dispute_job.delay(instance, created=created)

# from paylock views
def send_email_funds_has_been_approved(funds_details, **kwargs):
    send_email_funds_has_been_approved_job.delay(funds_details)


def send_email_funds_has_been_rejected(funds_details, **kwargs):
   send_email_funds_has_been_rejected_job.delay(funds_details)


def send_email_reverse_transaction_has_been_approved(reverse_obj, **kwargs):
    send_email_reverse_transaction_has_been_approved_job.delay(reverse_obj)


def send_email_reverse_transaction_has_been_declined(reverse_obj, **kwargs):
    send_email_reverse_transaction_has_been_declined_job.delay(reverse_obj)


def invite_new_users(email, invitee_full_name, invitation_link, **kwargs):
   users_to_join_paylock_bg_job.delay(email, invitee_full_name, invitation_link,)

def reset_paylock_account_password(user, **kwargs):
   reset_paylock_account_password_job.delay(user)

def respond_to_dispute_email(dispute, **kwargs):
   """ Send email when a dispute against person responds
   to dispute
   """
   dispute_response_email_job.delay(dispute)
   
def resolve_dispute_email(dispute, **kwargs):
   """ Send email when a dispute is resolved"""
   resolve_dispute_email_job.delay(dispute)

@receiver(post_save, sender=InviteLegalRep)
def send_email_to_legal_rep_about_dispute(instance, created=False, **kwargs):
   send_email_to_legal_rep_about_dispute_job.delay(instance, created=created)

@receiver(post_save, sender=InviteLegalRep)
def send_email_about_legal_fees_payment(instance, created=False, **kwargs):
   payment_of_legal_fees_job.delay(instance, created=created)


def send_email_to_dispute_against_about_comment(dispute, comment, **kwargs):
   """Send email to dispute against person when the owner leaves a comment
      on the particular dispute.
   """
   send_email_to_dispute_against_about_comment_job.delay(dispute, comment)

def send_email_to_dispute_owner_about_comment(dispute, comment, **kwargs):
   """Send email to dispute creator or owner  when the person against leaves a comment
      on the particular dispute.
   """
   send_email_to_dispute_owner_about_comment_job.delay(dispute, comment)

def send_email_to_both_dipuste_parties_about_legal_rep_comment(dispute, comment, **kwargs):
   """ Both parties in the dispute gets notified when a comment is left on a dispute 
   """
   send_email_to_dispute_creator_about_legal_advisor_commentjob.delay(dispute, comment)
   send_email_to_dispute_against_about_legal_advisor_commentjob.delay(dispute, comment)

   
def load_wallet_with_card_success_reponse(payment, my_wallet, **kwargs):
   """ Process load walle email after load is successful
   
   """
   load_wallet_with_card_success_reponse_job.delay(payment, my_wallet)



def load_wallet_failed_reponse(payment, my_wallet, **kwargs):
   """ Process load walle email after load fails
   
   """
   load_wallet_failed_reponse_job.delay(payment, my_wallet)

def load_wallet_with_momo_success_reponse(payment, my_wallet, currency, **kwargs):
   """ Process load wallet email after load fails
   
   """
   load_wallet_with_momo_success_reponse_job.delay(payment, my_wallet, currency)


def load_wallet_with_qvo_payment_success_response(payment, my_wallet, currency, **kwargs):
   """ Load wallet success email after processing by qvo.
   
   """
   load_wallet_chile_qvo_success_reponse_job(payment, my_wallet, currency)

def funds_sent_by_wallet_direct_by_email(wallet_direct, **kwargs):
   """
      Send email to transaction owner when money is sent using 
      PayLock's wallet direct.
   """
   funds_sent_email_by_wallet_direct_job.delay(wallet_direct)

def funds_received_by_wallet_direct_by_email(wallet_direct, **kwargs):
   """
      Send email to receiver  when money is received using 
      PayLock's wallet direct.
   """
   funds_received_by_wallet_direct_by_email_job.delay(wallet_direct)


def send_email_to_seller_about_payment(transaction,**kwargs):
   """
      Send email to seller about the payment using the escrow service
      on his site.
   """
   send_email_to_seller_about_payment_job.delay(transaction)

def send_email_to_buyer_about_payment(transaction, **kwargs):
   """
     Send email receipt to buyer about the payment using the escrow service
      on a website.
   """
   send_email_to_buyer_about_payment_job.delay(transaction)

def send_email_to_seller_money_has_been_released(transation, **kwargs):
   """
     Send email to inform seller or service provider, funds has been released to his 
     PayLock account.
   """
   send_email_to_seller_money_has_been_released_job.delay(transation)