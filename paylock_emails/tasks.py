from django.utils.crypto import get_random_string
from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.template.loader import get_template
from django.template import Context
from celery.decorators import task
import string
from slackclient import SlackClient
import urllib
import requests
from user_account.models import User
from paylock_emails.views import send_email
from transaction.views import markup_value


@task
def send_invitation_to_join_a_transaction_job(instance, created=False, **kwargs):
    """ Send invitation to collaborator to join a transaction 
    
	"""
    if created and instance.transaction.api_transaction is None:
        print("This code is suppposed to pass here")
        # if instance.transaction.escrow_fee_payment == 'both':
        #     escrow_fee = instance.transaction.markup
        # elif instance.transaction.escrow_fee_payment == 'owner':
        #     escrow_fee = 0
        # elif instance.transaction.escrow_fee_payment == 'collaborator':
        #     escrow_fee = markup_value(instance.transaction.amount, instance.transaction.user)
        send_email(
        html_template = 'transaction_invitation/transaction_invitations.html',
        plaintext_template = 'transaction_invitation/transaction_invitations.txt',
        email_subject = 'Transaction Invitation by {} on PayLock. '.format(instance.transaction.user.user_profile.first_name),
        to=instance.transaction.contractor.email,
        context = {
            'id': instance.id,
            'transaction': instance.transaction.name,
            'transaction_type': instance.transaction.transaction_type,
            'description': instance.transaction.description,
            'freelance_project_type': instance.transaction.freelance_project_type,
            'escrow_fee_payment': instance.transaction.escrow_fee_payment,
            'escrow_fee': instance.transaction.markup,
            'transaction_owner': instance.transaction.user.user_profile.first_name,
            'contractor': instance.transaction.contractor.user_profile.first_name,
            'amount': instance.transaction.amount,
            'date_created': instance.transaction.created_date,
            'domain_url': settings.DOMAIN_URL 
            }
    )
	
@task 
def escrow_wallet_details_to_contractor_job(instance, created=False, **kwargs):
	""" This sends Wallet information to contractor or collaborator
	
	"""
	if created and instance.transaction.transaction_role == "owner":
		send_email(
				html_template ='escrow_wallet/escrow_wallet_info.html',
				plaintext_template='escrow_wallet/escrow_wallet_info.txt',
				email_subject = 'Your money is safe on PayLock Escrow',
				to= instance.transaction.contractor,
				context = { 
					'transaction': instance.transaction.name,
					'unique_code': instance.unique_transaction_code,
					'amount': instance.transaction.amount,
					'transaction_owner': instance.transaction.user.user_profile.first_name,
					'collaborator': instance.transaction.contractor.user_profile.first_name,
					'domain_url': settings.DOMAIN_URL,
					'date_created': instance.transaction.created_date,
					}
			)

# Ger url for transaction funds request details page from Eric.
@task
def funds_request_on_transaction_job(instance, created=False, **kwargs):
    """ Send email to transaction owner about funds request.
	
	"""
    if created:
	    send_email(
            html_template ='funds_request/funds_request_email.html',
            plaintext_template = 'funds_request/funds_request_email.txt',
            email_subject = 'Funds request by {}'.format(instance.transaction.contractor.user_profile.first_name),
            to=instance.transaction.user,
            context = {
                'id': instance.id,
                'transaction': instance.transaction.name,
                'transaction_amount': instance.transaction.amount,
                'transaction_type': instance.transaction.transaction_type,
                'description': instance.transaction.description,
                'owner_name': instance.transaction.user.user_profile.first_name,
                'collaborator_name': instance.transaction.contractor.user_profile.first_name,
                'domain_url': settings.DOMAIN_URL,
				'request_description': instance.request_description,
            	'request_amount': instance.amount,
				'date_created': instance.created_date
                }
        )

@task
def welcome_email_after_profile_job(instance, created=False, **kwargs):
	""" Send email to welcome users when they create profile
	
	"""
	if created:
		send_email(
			html_template = 'welcome_users/welcome_user_email.html',
			plaintext_template = 'welcome_users/welcome_user_email.txt',
			email_subject = 'Welcome {} {} to PayLock'.format(instance.first_name, instance.last_name),
			to=instance.user.email,
			context = {
				'first_name': instance.first_name,
				'last_name': instance.last_name,
                'domain_url': settings.DOMAIN_URL
				}
		)

@task
def accept_transaction_invitation_email_job(instance, **kwargs):
	""" Email received by the transaction owner when the 
        transaction invitation is accepted.
	
	"""
	if instance.status == "approved" and instance.transaction.api_transaction is None:
		send_email(
			html_template = 'transaction_invitation/approve_transaction_invitation.html',
			plaintext_template = 'transaction_invitation/approve_transaction_invitation.txt',
			email_subject = 'Transaction invitation approved.',
			to=instance.transaction.user,
			context = {
                'id': instance.id,
				'transaction': instance.transaction.name,
				'transaction_type': instance.transaction.transaction_type,
				'description': instance.transaction.description,
				'amount': instance.transaction.amount,
				'project_owner': instance.transaction.user.user_profile.first_name,
				'contractor': instance.transaction.contractor.user_profile.first_name,
                'domain_url': settings.DOMAIN_URL,
				'date_created': instance.transaction.created_date,
				}
		)

@task
def decline_transaction_invitation_email_job(instance, **kwargs):
	""" Send email to transaction creator if the transaction is declined.
		
	"""
	if instance.status == "decline" and instance.transaction.api_transaction is None:
		
		send_email(
			html_template = 'transaction_invitation/decline_transaction_invitation.html',
			plaintext_template = 'transaction_invitation/decline_transaction_invitation.txt',
			email_subject = 'Transaction invitation declined.',
			to=instance.transaction.user,
			context = {
                'id': instance.id,
				'transaction': instance.transaction.name,
				'transaction_type': instance.transaction.transaction_type,
				'description': instance.transaction.description,
				'amount': instance.transaction.amount,
				# 'payment_method': instance.transaction.payment_method,
				'contractor': instance.transaction.contractor.user_profile.first_name,
                'domain_url': settings.DOMAIN_URL,
				'date_created': instance.transaction.created_date
				}
		)


@task
def send_reverse_transaction_email_to_contractor_for_approval_job(instance, created=False, **kwargs):
	""" Send email to collaborator when transaction owner is no long interested 
        in the transaction and decides to pull out.
	
	"""
	if created:
		send_email(
			html_template = 'reverse_transaction/reverse_transaction.html',
			plaintext_template = 'reverse_transaction/reverse_transaction.txt',
			email_subject = 'Reverse Transaction Action',
			to=instance.transaction.contractor,
			context = {
				'id': instance.id,
                'transaction': instance.transaction.name,
                'reverse_transaction_reason': instance.reverse_reason,
                'transaction_type': instance.transaction.transaction_type,
                'description': instance.transaction.description,
                'amount': instance.transaction.amount,
                'requested_amount': instance.amount,
                # 'payment_method': instance.transaction.payment_method,
                'project_owner': instance.transaction.user.user_profile.first_name,
                'contractor': instance.transaction.contractor.user_profile.first_name,
				'date_created': instance.created_date,
				'domain_url': settings.DOMAIN_URL
                }
		)

# This notifies user when a withdraw request is made on wallet.
@task
def send_withdrawal_email_to_user_job(instance, **kwargs):
	""" Send email to user when user requests for a withdraw of funds
	
	"""
	send_email(
		html_template = 'withdrawal_processing/withdraw.html',
		plaintext_template = 'withdrawal_processing/withdraw.txt',
		email_subject = 'Requested funds withdrawal',
		to=instance.user,
		context = {
			'owner_first_name': instance.user.user_profile.first_name,
			'currency': instance.currency,
			'bank_name': instance.bank_name,
			'fee': instance.fees,
			'status': instance.status,
			'account_type': instance.account_type,
			'amount': instance.amount,
			'requested_date': instance.created_date,
			'phone_number': instance.phone_number
			}
		)

# This function is called when the webhook returns success about withdrawal
def notify_user_successful_transfer(withdraw_obj, *args, **kwargs):
	send_email(
		html_template = 'withdrawal_processing/approved_withdrawal.html',
		plaintext_template = 'withdrawal_processing/approved_withdrawal.txt',
		email_subject = 'Withdraw Request approved.',
		to=withdraw_obj.user,
		context = {
			'owner_first_name': withdraw_obj.user.user_profile.first_name,
			'currency': withdraw_obj.currency,
            'bank_name': withdraw_obj.bank_name,
            'account_type': withdraw_obj.account_type,
            'amount': withdraw_obj.amount,
            'requested_date': withdraw_obj.created_date,
            'phone_number': withdraw_obj.phone_number
			}
	)


# Test url in the main function page to make sure it's correct.
@task
def users_to_join_paylock_bg_job(email, invitee_full_name, invitation_link, **kwargs):
    """ Invite new users to join paylock. 
        Send an email to incoming email address

    """
    send_email(
        html_template = 'invite-users/invite-new-users.html',
        plaintext_template = 'invite-users/invite-new-users.txt',
            email_subject = '{} invited to join paylock'.format(invitee_full_name),
            to=email,
            context = {
                        'invitation_link': invitation_link,
                        'invite_email': email,
                        'invitee_name': invitee_full_name,
                        'domain_url': settings.DOMAIN_URL
                        }
    )

@task
def send_email_to_paylock_on_dispute_job(instance, created=False, **kwargs):
    """ Send email to PayLock when someone creates a dispute on the platform.
        
    """
    if created:
	    send_email(
				html_template = 'disputes/email_paylock_on_dispute.html',
				plaintext_template = 'disputes/email_paylock_on_dispute.txt',
				email_subject = 'Dispute has been raised',
				to='support@paylock.io',
				context = {
                            'id': instance.id,
							'dispute_number': instance.dispute_number,
							'dispute_reason': instance.reason,
							'transaction': instance.transaction.name,
							'transaction_type': instance.transaction.transaction_type,
							'transaction_amount': instance.transaction.amount,
							'transaction_owner': instance.transaction.user.user_profile.first_name,
							'transaction_collaborator': instance.transaction.contractor.user_profile.first_name,
							'dispute_created_date':instance.date_created,
							'domain_ulr': settings.DOMAIN_URL
							}
			)