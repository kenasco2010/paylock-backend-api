from rest_framework import serializers
from .models import RequestFundsTransfer
from transaction.serializers import TransactionSerializer
from transaction.models import Transaction
from wallet.models import EscrowWallet
from wallet.serializers import EscrowWalletSerializer

class RequestFundsTransferSerializer(serializers.ModelSerializer):
    transaction = TransactionSerializer()
    class Meta:
        model = RequestFundsTransfer
        fields = ('id', 'transaction', 'milestone', 'status', 'published_date', 'request_description',
                'amount', 'created_date', 'modified'
                )


class FundsRequestOnlySerializer(serializers.ModelSerializer):
    
    class Meta:
        model = RequestFundsTransfer
        fields = ('id', 'transaction', 'status', 'published_date', 'request_description',
                'amount', 'created_date', 'modified'
                )

class TransactionFundsRequestDetailSerializer(serializers.ModelSerializer):
    request_funds_transfer = FundsRequestOnlySerializer(many=True)
    class Meta:
        model = Transaction
        fields = ('id', 'name', 'amount', 'description', 'request_funds_transfer','stage'
                )

class FundsTransferListSerializer(serializers.ModelSerializer):
    transaction_id = serializers.IntegerField(source='transaction.id')
    transaction_name = serializers.CharField(source='transaction.name')
    owner_full_name = serializers.SerializerMethodField()
    collaborator_full_name = serializers.SerializerMethodField()
    owner_profile_id = serializers.SerializerMethodField()
    collaborator_profile_id = serializers.SerializerMethodField()
    request_amount = serializers.SerializerMethodField()
    class Meta:
        model = RequestFundsTransfer
        fields = ('id','transaction_id', 'transaction_name', 'owner_full_name', 
             'collaborator_full_name', 'owner_profile_id', 'collaborator_profile_id',
             'status', 'published_date', 'request_description',
                'request_amount', 'created_date', 'modified'
                )
                
    def get_request_amount(self, funds_request):
        return '{}'.format(
            funds_request.amount)

    def get_owner_full_name(self, funds_request):
        return '{} {}'.format(
            funds_request.transaction.user.user_profile.first_name,
            funds_request.transaction.user.user_profile.last_name)
        
    def get_collaborator_full_name(self, funds_request):
        return '{} {}'.format(
            funds_request.transaction.contractor.user_profile.first_name,
            funds_request.transaction.contractor.user_profile.last_name)
    
    def get_owner_profile_id(self, funds_request):
        return '{0}'.format(
            funds_request.transaction.user.user_profile.id)
    
    def get_collaborator_profile_id(self, funds_request):
        return '{0}'.format(
            funds_request.transaction.contractor.user_profile.id)


class TransactionFundsSerializer(serializers.ModelSerializer):
    request_funds_transfer = FundsRequestOnlySerializer(many=True)
    transaction_id = serializers.SerializerMethodField()
    collaborator_profile_id = serializers.SerializerMethodField()
    collaborator_fullname = serializers.SerializerMethodField()
    collaborator_profile_image = serializers.SerializerMethodField()
    user_profile_id = serializers.SerializerMethodField()
    user_fullname = serializers.SerializerMethodField()
    transaction_amount = serializers.SerializerMethodField()
    transaction_date = serializers.SerializerMethodField()
    class Meta:
        model = Transaction
        fields = ('transaction_id', 'name', 'transaction_type_name', 'description', 
        'payment_method_type','initial_amount', 'transaction_amount', 'markup', 'stage', 'is_archived', 'collaborator_profile_id', 'collaborator_fullname', 
        'collaborator_profile_image', 'project_owner_agreement', 'contractor_agreement', 
        'contract_document', 'user_profile_id', 'user_fullname', 'published_date','transaction_date',
        'request_funds_transfer', 
        )
    def get_transaction_id(self, transaction):
        return '{}'.format(transaction.id)
    
    def get_collaborator_profile_id(self, transaction):
        return '{}'.format(transaction.contractor.user_profile.id)

    def get_collaborator_fullname(self, transaction):
        return '{} {}'.format(
            transaction.contractor.user_profile.first_name,
            transaction.contractor.user_profile.last_name,
            )

    def get_collaborator_profile_image(self, transaction):
        return '{}'.format(
            transaction.contractor.user_profile.profile_image,)

    def get_user_profile_id(self, transaction):
        return '{}'.format(
            transaction.user.user_profile.id
            )

    def get_user_fullname(self, transaction):
        return '{} {}'.format(
            transaction.user.user_profile.first_name,
             transaction.user.user_profile.last_name,)

    def get_transaction_amount(self, transaction):
        return '{}'.format(
            transaction.amount)

    def get_transaction_date(self, transaction):
        return '{}'.format(
            transaction.created_date)
    
    def get_transaction_date(self, transaction):
        return '{}'.format(
            transaction.created_date)