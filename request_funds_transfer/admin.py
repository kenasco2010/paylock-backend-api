from django.contrib import admin
from .models import RequestFundsTransfer

class RequestFundsTransferAdmin(admin.ModelAdmin):
    list_display = ('transaction', 'approver', 'status', 'created_date')


admin.site.register(RequestFundsTransfer, RequestFundsTransferAdmin)
# Register your models here.
