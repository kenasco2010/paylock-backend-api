from django.db import models
from django.conf import settings
from django.utils import timezone
from datetime import datetime, timedelta
from django.contrib.contenttypes.fields import GenericRelation
from paylock.choices import APPROVAL_STATUS
from dashboard_notification.models import Notification
from transaction.models import Transaction
from milestone.models import Milestone
from api_integration.models import ApiTransaction
# Create your models here.

AUTH_USER_MODEL = getattr(settings, 'AUTH_USER_MODEL', 'auth.User')

class RequestFundsTransfer(models.Model):
    transaction = models.ForeignKey(Transaction, related_name='request_funds_transfer',
        on_delete=models.SET_NULL,
        null=True
        )
    
    milestone = models.ForeignKey(Milestone,
        related_name='transfers',
        on_delete=models.SET_NULL,
        null=True,
        blank=True
        )
    approver = models.ForeignKey(
        AUTH_USER_MODEL, related_name='funds_transfer', on_delete=models.CASCADE,
        blank=True, null=True
        )
    request_description = models.TextField(
        default='Work done'
        )
    amount = models.DecimalField(
        max_digits=7, 
        decimal_places=2,
        null=True,
        blank=True,
        default=0.00
    )
    status = models.CharField(choices=APPROVAL_STATUS,
                                    max_length=20, blank=True,
                                    default="pending")
    requested_date = models.DateTimeField(default=timezone.now)
    response_date = models.DateTimeField(blank=True, null=True)
    response_days_remaining = models.CharField(
        max_length=20,
        blank=True, 
        null=True,
        default=14)
    future_date = models.DateTimeField(null=True, blank=True)
    is_approved_by_automation = models.CharField(max_length=50, default='No')
    notification = GenericRelation(Notification, related_query_name="funds_request")
    published_date = models.DateTimeField(blank=True, null=True)
    created_date = models.DateTimeField(default=timezone.now)
    modified = models.DateTimeField(auto_now=True)

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __unicode__(self):
        return '%s' % (self.transaction)

    def __str__(self):
        return '%s' % (self.transaction)
