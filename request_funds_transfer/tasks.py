from celery.task.schedules import crontab
from celery.decorators import periodic_task, task
from .models import RequestFundsTransfer
from datetime import timedelta, datetime, date
from django.utils import timezone
from paylock_emails.views import reminder_to_respond_to_funds_request, \
    automatic_payment_success_email
from wallet.models import EscrowWallet, UserWallet
from transaction.models import Transaction
from payment.views import update_payment_history


@task
def automate_response_when_not_responded(**kwargs):
    """ Automate response when funds are requested 
        and the transaction owner doesn't not respond.
    """
    # logger.info("The periodic task must happen here --------->...>>>>>>>>")
    funds_requests = RequestFundsTransfer.objects.filter(
        response_days_remaining__gte=0, status='pending').exclude(future_date__isnull=True)

    print("Number of queries in this is ", funds_requests)

    for days_remaining in funds_requests:
            num_of_days_remaining = days_remaining.future_date.date() - date.today()
            days_remaining.response_days_remaining = num_of_days_remaining.days
            days_remaining.save()
            # reminder_to_respond_to_funds_request(days_remaining)
            check_data_for_payment = (days_remaining.response_days_remaining == 0 and days_remaining.status=='pending' and days_remaining.is_approved_by_automation=='No')
            print("Step into this function to see if someone needs to be paid(days_remaining)")
            if check_data_for_payment:
               
                escrow_wallet = EscrowWallet.objects.filter(transaction=days_remaining.transaction)
                transactions = Transaction.objects.filter(id=days_remaining.transaction.id)
                collaborators_wallet = UserWallet.objects.filter(user=days_remaining.transaction.contractor)
                for escrow_transaction in escrow_wallet:
                    deduction = escrow_transaction.amount - days_remaining.amount
                    escrow_transaction.amount = deduction
                    escrow_transaction.save()
                    print("deductions .... ", escrow_transaction.amount)
                    # Upate the escrow wallet for the traan
                    if escrow_transaction.amount == 0.00:
                        escrow_transaction.payout_agreed = True
                        escrow_transaction.is_active = False
                        escrow_transaction.withdraw_date = timezone.now()
                        escrow_transaction.withdraw_status = "Funds withdrawn"
                        escrow_transaction.save()

                        # update the transaction
                        for transaction in transactions:
                            amount = transaction.amount - days_remaining.amount   
                            transaction.amount = amount  
                            transaction.stage = 'completed'  
                            transaction.save()
                        
                        # update user wallet with amount
                        for wallet in collaborators_wallet:
                            amount = wallet.balance + days_remaining.amount
                            wallet.balance = amount
                            wallet.save()

                    elif escrow_transaction.amount >= 0.00:
                #         # update the escrow transaction
                        escrow_transaction.payout_agreed = False
                        escrow_transaction.is_active = True
                        escrow_transaction.withdraw_date = timezone.now()
                        escrow_transaction.withdraw_status = "Part Funds withdrawn"
                        # # update the transaction
                        for transaction in transactions:
                            transaction.amount - days_remaining.amount     
                            transaction.stage = 'ongoing'  
                            transaction.save()
                        
                        for wallet in collaborators_wallet:
                            amount = wallet.balance + days_remaining.amount
                            wallet.balance = amount
                            wallet.save()

                            update_payment_history(
                                'Escrow funds moved to your wallet.',
                                'Funds have been automated and amount moved to your wallet.',
                                days_remaining.amount,
                                'completed',
                                days_remaining.transaction.user,
                                timezone.now(),
                                timezone.now()
                            )
                    automatic_payment_success_email(days_remaining)
                    # send email to project owner about payment.
                days_remaining.is_approved_by_automation = 'Yes'
                days_remaining.response_days_remaining = 0
                days_remaining.status = 'approved'
                days_remaining.approver = days_remaining.transaction.user
                days_remaining.save()

                # print("Escrow_wallet objects", escrow_wallet)
                # print("Trigger an action to pay")
                # set status to Approved
            
 