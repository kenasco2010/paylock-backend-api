from django.shortcuts import render
from django.contrib.auth import authenticate, login, logout, get_user_model
from django.utils import timezone
from django.db.models.signals import pre_save, post_save
from django.db.models import Sum, Q
from django.dispatch import receiver
from django.conf import settings
import datetime
from rest_framework.authentication import TokenAuthentication
from rest_framework.status import (HTTP_201_CREATED,
                                   HTTP_200_OK,
                                   HTTP_400_BAD_REQUEST, HTTP_401_UNAUTHORIZED,
                                   HTTP_404_NOT_FOUND, HTTP_409_CONFLICT)
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework import viewsets
from rest_framework import permissions, status, generics, authentication
from rest_framework.decorators import action
from rest_framework.decorators import detail_route, \
                    list_route, permission_classes, api_view
from rest_framework.authtoken.models import Token
from decimal import Decimal
from user_account.models import User, UserManager
from paylock.permissions import IsSuperUserPermission
from user_account.models import User, UserManager
from transaction.models import Transaction
from wallet.models import UserWallet, EscrowWallet
from .serializers import RequestFundsTransferSerializer, \
        TransactionFundsRequestDetailSerializer, \
        FundsTransferListSerializer, TransactionFundsSerializer
        
from transaction.serializers import TransactionSerializer
from request_funds_transfer.models import RequestFundsTransfer
from milestone.models import Milestone
from paylock_emails.signals import *
from .signals import *
from paylock_emails.views import *
from payment.views import update_payment_history
from datetime import datetime, timedelta
# Create your views here.


class RequestFundsTransferViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    serializer_class = RequestFundsTransferSerializer
    queryset = RequestFundsTransfer.objects.all().order_by('-id')

    @action(methods=('POST', ), detail=False, url_path='request-funds-transfer', 
                    permission_classes=(IsAuthenticated,))
    def request_funds_transfer_invitation(self, request, *args, **kwargs):
        """ Request funds transfer invitation to transaction owner for approval
        
        """
        user = request.user
        request_description = request.data.get('request_description', None)
        amount = request.data.get('amount', None)
        transaction_id = request.data.get('transaction_id', None)
        

        # check if amount requested for is more than the transaction amount

        try:
            transaction = Transaction.objects.get(pk=transaction_id)
        except Transaction.DoesNotExist:
            return Response({'status_code': status.HTTP_404_NOT_FOUND,
                    'message': 'Transaction does not exist',
                    })
        try:
            escrow = EscrowWallet.objects.get(transaction__id=transaction.id, transaction__contractor=request.user)
        except EscrowWallet.DoesNotExist:
            return Response({'status_code': status.HTTP_404_NOT_FOUND,
                    'message': 'This transaction is invalid. It does not have any amount attached to it.',
                    })
        if Decimal(escrow.amount) < Decimal(amount):
            return Response({'status_code': status.HTTP_400_BAD_REQUEST,
                'message': 'The amount you have entered is more than the amount deposited for you for this transaction',
                })
        if transaction.contractor == user:
            transfer = RequestFundsTransfer.objects.create(
                    transaction = transaction,
                    amount = amount,
                    request_description = request_description,
                    published_date = timezone.now(),
                    requested_date = timezone.now()
                )
            future_date = transfer.requested_date + timedelta(days=(transfer.response_days_remaining))
            transfer.future_date = future_date
            transfer.save()
            transfer = self.serializer_class(transfer)
            return Response({'status_code': status.HTTP_201_CREATED,
                        'message': 'Transfer request has been created and sent',
                        'result': {
                            'data': transfer.data
                        }
                }, status=status.HTTP_201_CREATED)

    @action(methods=('POST',), detail=False, url_path='approve-funds-transfer', 
                    permission_classes=(IsAuthenticated,))
    def accept_transfer_approval_invitation(self, request, *args, **kwargs):
        """ Approve transfer of funds to be paid and email is sent to seller or contractor
        
        """
        user = request.user
        transfer_invite_id = request.data.get('transfer_invite_id')
        transfer_invite = RequestFundsTransfer.objects.get(id=transfer_invite_id)
        # Check if transfer invite has a milestone object
        if transfer_invite.milestone is not None:
            try:
                milestone = Milestone.objects.get(id=transfer_invite.milestone.id)
            except Milestone.DoesNotExist:
                pass
            # set the milestone object stage to completed.
            if milestone:
                milestone.stage = 'completed'
                milestone.save()
        if transfer_invite.transaction.user == user:
            transfer_invite.status = 'approved'
            transfer_invite.approver = user
            transfer_invite.response_date = timezone.now()
            transfer_invite.response_days_remaining = 0
            transfer_invite.future_date = None
            transfer_invite.save()
            try: # Try getting collaborators wallet
                collaborator_wallet = UserWallet.objects.get(
                    user=transfer_invite.transaction.contractor,
                    is_active=True,
                    is_authorized=True
                    )           
            except UserWallet.DoesNotExist:
                return Response({'status_code': status.HTTP_404_NOT_FOUND,
                            'message': 'You have no wallet. Contact admin to create your wallet'
            }, status=status.HTTP_404_NOT_FOUND)
            try: # Try getting the escrow transaction created by the transaction owner
                escrow_wallet = EscrowWallet.objects.get(
                    transaction=transfer_invite.transaction.id,
                    recipient=transfer_invite.transaction.contractor,
                    is_active=True,
                    can_cancel=False,
                    payout_agreed=False
                    )
            except EscrowWallet.DoesNotExist:
                return Response({'status_code': status.HTTP_404_NOT_FOUND,
                            'message': 'No money was deposited in the escrow '
                            'account for you. Kindly contact the transaction '
                            'owner'
            }, status=status.HTTP_404_NOT_FOUND)
            collaborator_wallet.balance += Decimal(transfer_invite.amount) # Credit collaborators wallet with specified amount
            collaborator_wallet.last_used = timezone.now()
            collaborator_wallet.save()
            escrow_wallet.amount -= Decimal(transfer_invite.amount)
            escrow_wallet.last_used = timezone.now()
            escrow_wallet.save()
            if escrow_wallet.amount <= 0:
                escrow_wallet.payout_agreed = True
                escrow_wallet.is_active = False
                escrow_wallet.withdraw_date = timezone.now()
                escrow_wallet.withdraw_status = "Funds withdrawn"
                escrow_wallet.save() # check all fields when authorized to withdraw
                send_email_funds_has_been_approved(transfer_invite) # send email function

                # Mark transaction as completed.
                transaction = Transaction.objects.get(pk=transfer_invite.transaction.id)
                transaction.stage = 'completed'
                transaction.amount -= Decimal(transfer_invite.amount) # Deduct the amount from the transaction amount
                transaction.save()
                approve_transfer = self.serializer_class(transfer_invite)
                return Response({'status_code': status.HTTP_200_OK,
                                'message': 'Transfer request has been approved and funds transferred to collaborators account.',
                                'result': {
                                    'data': approve_transfer.data
                                }
                }, status=status.HTTP_200_OK)
            else:
                escrow_wallet.payout_agreed = False
                escrow_wallet.is_active = True
                escrow_wallet.withdraw_date = timezone.now()
                escrow_wallet.withdraw_status = "Part Funds withdrawn"
                escrow_wallet.last_used = timezone.now()
                escrow_wallet.save() # check all fields when authorized to withdraw
                send_email_funds_has_been_approved(transfer_invite) # send email function
                # Mark transaction as completed.
                transaction = Transaction.objects.get(pk=transfer_invite.transaction.id)
                transaction.stage = 'ongoing'
                transaction.amount -=  Decimal(transfer_invite.amount)
                transaction.save()
                update_payment_history(
                    'Escrow funds moved to your wallet.',
                    'Funds approved and amount moved to your wallet.',
                    transfer_invite.amount,
                    'completed',
                    transfer_invite.transaction.user,
                    timezone.now(),
                    timezone.now()
                )
                approve_transfer = self.serializer_class(transfer_invite)
                return Response({'status_code': status.HTTP_200_OK,
                                'message': 'Transfer request has been approved and funds transferred to collaborators account.',
                                'result': {
                                    'data': approve_transfer.data
                                }
                }, status=status.HTTP_200_OK)
        else:
            return Response({'status_code': status.HTTP_400_BAD_REQUEST,
                                'message': 'You are not authorized to approve this transfer',        
                }, status=status.HTTP_400_BAD_REQUEST)


    @action(methods=('POST',), detail=False, url_path='reject-funds-transfer', 
                    permission_classes=(IsAuthenticated,))
    def reject_transer_approval_invitation(self, request, *args, **kwargs):
        """ Reject transfer of funds to be paid
        
        """
        user = request.user
        transfer_invite_id = request.data.get('transfer_invite_id')
        transfer_invite = RequestFundsTransfer.objects.get(id=transfer_invite_id)
        # Check if transfer invite has a milestone object
        if transfer_invite.milestone is not None:
            try:
                milestone = Milestone.objects.get(id=transfer_invite.milestone.id)
            except Milestone.DoesNotExist:
                pass
            # set the milestone object stage to completed.
            if milestone:
                milestone.stage = 'ongoing'
                milestone.save()
        if transfer_invite.transaction.user == user:
            transfer_invite.status = 'declined'
            transfer_invite.response_date = timezone.now()
            transfer_invite.response_days_remaining = 0
            transfer_invite.future_date = None
            transfer_invite.save()
            # Change the status of the transaction to dispute
            transaction = Transaction.objects.get(pk=transfer_invite.transaction.id)
            transaction.stage = 'dispute'
            transaction.save()
            send_email_funds_has_been_rejected(transfer_invite) # send email function
            approve_transfer = self.serializer_class(transfer_invite)
            return Response({'status_code': status.HTTP_200_OK,
                            'message': 'Funds transfer request has been rejected',
                            'result': {
                                'data': approve_transfer.data
                            }
            }, status=status.HTTP_200_OK)
        else:
            return Response({'status_code': status.HTTP_400_BAD_REQUEST,
                                'message': 'You are not authorized to reject this transfer',        
                }, status=status.HTTP_400_BAD_REQUEST)


# FOR TRANSACTION OWNERS.
    @action(methods=('GET',), detail=False, url_path='pending-request-list', 
        permission_classes=(IsAuthenticated,))
    def pending_funds_release(self, request, *args, **kwargs):
        """List of pending transaction funds to be released"""
        user = request.user
        pending_request = RequestFundsTransfer.objects.filter(transaction__user=user, 
            transaction__is_archived=False, status='pending').order_by('-id')
        pending_request = FundsTransferListSerializer(pending_request, many=True)
        return Response({'status_code': HTTP_200_OK,
                        'message': 'Transaction funds pending approval by transaction owner',
                        'result': {
                            'data': pending_request.data
                        }
            })
    # List of funds requests by collaborator and the transaction owner actions on them
    @action(methods=('GET',), detail=False, url_path='funds-requested-with-your-response', 
        permission_classes=(IsAuthenticated,))
    def funds_requested_by_collaborator(self, request, *args, **kwargs):
        """ List of funds requested by the collaborator with their status.
            Whether pending, approved or declined by you the transaction owner
        
        """
        user = request.user
        pending_request = RequestFundsTransfer.objects.filter(transaction__user=user, 
            transaction__is_archived=False).order_by('-id')
        pending_request = FundsTransferListSerializer(pending_request, many=True)
        return Response({'status_code': HTTP_200_OK,
                        'message': 'Transaction funds resquested by your collaborator with their status',
                        'result': {
                            'data': pending_request.data
                        }
            }, status=status.HTTP_200_OK)


# FOR Collaborators


    # List of funds requests with their status
    @action(methods=('GET',), detail=False, url_path='request-transaction-funds-list', 
        permission_classes=(IsAuthenticated,))
    def request_transaction_funds_list(self, request, *args, **kwargs):
        """List of transaction funds to be requested"""
        user = request.user
        # funds_request_list = RequestFundsTransfer.objects.values_list('transaction', flat=True)
        escrow_transaction = EscrowWallet.objects.filter(
            is_active=True, recipient=user, can_cancel=False
            ).values_list('transaction', flat=True)
        transactions = Transaction.objects.filter( 
            Q(contractor=user, is_archived=False, stage='ongoing', 
                id__in=escrow_transaction) |
            Q(contractor=user, is_archived=False, stage='dispute_resolved', 
                id__in=escrow_transaction)
            ).prefetch_related('request_funds_transfer')
        pending_request = TransactionFundsSerializer(transactions, many=True)
        # pending_request = ActiveTransactionsInEscrow(transactions, many=True)
        return Response({'status_code': HTTP_200_OK,
                        'message': 'List of transactions you can request for it\'s funds',
                        'result': {
                            'data': pending_request.data
                        }
            }, status=status.HTTP_200_OK)

 
    @action(methods=('GET',), detail=False, url_path='funds-requests-with-status', 
        permission_classes=(IsAuthenticated,))
    def funds_request_status(self, request, *args, **kwargs):
        """ List of funds requests with their status, whether
            pending, approved or declined.
        """
        user = request.user
        approved_request = RequestFundsTransfer.objects.filter(
            transaction__contractor=user, 
            transaction__is_archived=False).order_by('-id')
        approved_request = FundsTransferListSerializer(approved_request, many=True)
        return Response({'status_code': HTTP_200_OK,
                        'message': 'List of funds requests with their status.',
                        'result': {
                            'data': approved_request.data
                        }
            }, status=status.HTTP_200_OK)

    @action(methods=('GET',), detail=False, url_path='details', 
        permission_classes=(IsAuthenticated,))
    def funds_request_details(self, request, *args, **kwargs):
        """ Details page for Funds request
            Get ID of funds request.
            Show transaction details
            Show funds request detail with the request ID

        """
        user = request.user
        transaction_id = request.query_params.get('transaction_id')
        funds_request = Transaction.objects.prefetch_related(
            'request_funds_transfer').get(id=transaction_id)
        withdrawn = RequestFundsTransfer.objects.filter(
            transaction__id=transaction_id, 
            status='approved'
        ).aggregate(Sum('amount'))
        serialize = TransactionFundsRequestDetailSerializer(funds_request)
        return Response({'status_code': HTTP_200_OK,
                        'message': 'Funds request detail page',
                        'result': {
                            'data': serialize.data,
                            'withdrawn': withdrawn
                        }
            }, status=status.HTTP_200_OK)

    
    @action(methods=('POST',), detail=False, url_path='milestone-funds-request', 
        permission_classes=(IsAuthenticated,))
    def milestone_funds_request(self, request, *args, **kwargs):
        """" This function allows a user to request for milestone funds after each milestone

        """
        user = request.user
        request_description = request.data.get('request_description', None)
        amount = request.data.get('amount', None)
        milestone_id = request.data.get('milestone_id', None)
        

        # check if amount requested for is more than the transaction amount

        try:
            milestone = Milestone.objects.get(pk=milestone_id)
        except Transaction.DoesNotExist:
            return Response({'status_code': status.HTTP_404_NOT_FOUND,
                    'message': 'Milestone does not exist',
                    })

        milestone.stage = 'funds requested' # Change the stage of the milestone.
        milestone.save()
        
        try:
            escrow = EscrowWallet.objects.get(
                transaction__id=milestone.transaction.id, 
                transaction__contractor=request.user,
                can_cancel=False, is_active=True
                )
        except EscrowWallet.DoesNotExist:
            return Response({'status_code': status.HTTP_404_NOT_FOUND,
                    'message': 'This transaction is either invalid or transaction invitation has'
                    'not been accepted'
                    }, status=status.HTTP_404_NOT_FOUND)

        if Decimal(escrow.amount) < Decimal(amount):
            return Response({'status_code': status.HTTP_400_BAD_REQUEST,
                'message': 'The amount you have entered is more than the amount deposited for you for this transaction',
                })

        if milestone.transaction.contractor == user:
            transfer = RequestFundsTransfer.objects.create(
                    transaction = milestone.transaction,
                    milestone=milestone,
                    amount = milestone.amount,
                    request_description = request_description,
                    published_date = timezone.now(),
                    requested_date = timezone.now()
                )
            future_date = transfer.requested_date + timedelta(days=(transfer.response_days_remaining))
            transfer.future_date = future_date
            transfer.save()
            transfer = self.serializer_class(transfer)
            return Response({'status_code': status.HTTP_201_CREATED,
                        'message': 'Transfer request has been created and sent to transaction owner for approval.',
                        'result': {
                            'data': transfer.data
                        }
                }, status=status.HTTP_201_CREATED)

    