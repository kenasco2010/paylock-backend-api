from django.apps import AppConfig


class RequestFundsTransferConfig(AppConfig):
    name = 'request_funds_transfer'
