from django.test import TestCase, RequestFactory, Client
from django.contrib.auth import get_user_model
from django.utils import timezone

from rest_framework.authtoken.models import Token
from rest_framework.test import APIRequestFactory
from rest_framework.test import APIClient
from rest_framework.test import force_authenticate
import json

from transaction_type.models import TransactionType
from payment_method.models import PaymentMethod
from transaction.models import Transaction
from user_profile.models import UserProfile
from transaction.serializers import TransactionSerializer
from request_funds_transfer.models import RequestFundsTransfer

User = get_user_model()

# Create your tests here.

class RequestFundsTransferTest(TestCase):
    
    def setUp(self):
        self.factory = RequestFactory()
        self.credentials = {
            'email': 'kenmartey89@gmail.com',
            'phone_number': '00233246424340',
            'password': 'ceremony',
            }
        self.user_profile = {
            'first_name': 'Kennedy',
            'last_name': 'Anyinatoe',
            'date_of_birth': '1989-07-09',
            'current_address': 'East Legon',
            'profile_image': ''

        }
        self.user = User.objects.create_superuser(**self.credentials)
        self.user_profile = UserProfile.objects.create(**self.user_profile, 
            user=self.user)

        self.payment_method = PaymentMethod.objects.create(
            payment_type='Mobile Money',
            created_date=timezone.now(), 
            user=self.user)

        self.transaction_type = TransactionType.objects.create(
            transaction_type= 'Product',
            created_date=timezone.now(), 
            user=self.user)

        self.transaction_info = {
        'name': 'Nike shoes',
        'transaction_type': self.transaction_type,
        'description': 'A pair of Nike shoes from Jumia',
        'payment_method': self.payment_method,
        'contractor': self.user,
        'currency': 'GHS',
        'amount': '900',
        'markup': '0',
        'contractor_agreement': False,
        'project_owner_agreement': True,
        'created_date': timezone.now()
        }
        
        self.transaction = Transaction.objects.create(**self.transaction_info, user=self.user)
        self.approve_transfer = {
            'transaction_id': self.transaction.id,
            'transaction': self.transaction,
            'published_date': timezone.now()
        }
        self.transfer_invite = RequestFundsTransfer.objects.create(**self.approve_transfer)
        # self.transfer_invite_id = RequestFundsTransfer.objects.get(id=self.transfer_invite)
        print ('>>> >>> >>>>>>>>>>>>>>>', self.transfer_invite.id)
        # self.invite = RequestFundsTransfer.objects.get(id=self.transfer_invite)
        # self.transfer_invite_id = self.invite.id
       
        # print ('>>> >>> >>>>>>>>>>>>>>>', self.transfer_invite.id)

    def test_send_transfer_approval_invitation(self, **kwargs):
        client = APIClient()
        headers = {
            'HTTP_AUTHORIZATION': 'Token {}'.format(
                self.user.auth_token)
        }
        request = self.client.post('/api/v1.0/transfers/request-funds-transfer/', 
        self.approve_transfer, format='json', **headers)
        print ('===================CREATE APPROVE FUNDS TRANSFER=========================')
        print ('>>> >>> >>> /CREATE APPROVE TRANSACTION/ content', request.content)
        self.assertEqual(request.status_code, 201)
        print ('===================END OF CREATE APPROVE TRANSACTION TEST=========================')
        print ('      ')


    # def test_approve_transfer_approval_invitation(self, **kwargs):
    #     client = APIClient()
    #     headers = {
    #         'HTTP_AUTHORIZATION': 'Token {}'.format(
    #             self.user.auth_token)
    #     }
    #     # transfer_invite_id = self.transfer_invite.id
        

    #     request = self.client.post('/api/v1.0/transfers/approve-funds-transfer/',  
    #     format='json', **headers)
    #     print ('===================APPROVE TRANSACTION=========================')
    #     self.assertEqual(request.status_code, 200)
    #     print ('===================END OF  APPROVE TRANSACTION TEST=========================')
    #     print ('      ')


    # def test_reject_transfer_approval_invitation(self, **kwargs):
    #     client = APIClient()
    #     headers = {
    #         'HTTP_AUTHORIZATION': 'Token {}'.format(
    #             self.user.auth_token)
    #     }
    #     # self.transfer_invite_id = RequestFundsTransfer.objects.get(id=self.transfer_invite.id)

    #     request = self.client.post('/api/v1.0/transfers/reject-funds-transfer/',  
    #     str(**self.invite.id), format='json', **headers)
    #     print ('===================REJECT TRANSACTION=========================')
    #     self.assertEqual(request.status_code, 200)
    #     print ('===================END OF REJECT TRANSACTION TEST=========================')
    #     print ('      ')