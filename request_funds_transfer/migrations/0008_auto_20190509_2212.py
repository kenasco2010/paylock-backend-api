# Generated by Django 2.0.6 on 2019-05-09 22:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('request_funds_transfer', '0007_requestfundstransfer_future_date'),
    ]

    operations = [
        migrations.AlterField(
            model_name='requestfundstransfer',
            name='response_days_remaining',
            field=models.CharField(blank=True, default=16, max_length=20, null=True),
        ),
    ]
