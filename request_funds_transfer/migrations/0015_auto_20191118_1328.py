# Generated by Django 2.0.6 on 2019-11-18 13:28

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('request_funds_transfer', '0014_remove_requestfundstransfer_api_transaction'),
    ]

    operations = [
        migrations.AlterField(
            model_name='requestfundstransfer',
            name='milestone',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='transfers', to='milestone.Milestone'),
        ),
        migrations.AlterField(
            model_name='requestfundstransfer',
            name='status',
            field=models.CharField(blank=True, choices=[('pending', 'Pending'), ('approved', 'Approved'), ('declined', 'Declined')], default='pending', max_length=20),
        ),
    ]
