# Generated by Django 2.0.6 on 2018-07-30 11:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user_profile', '0004_auto_20180730_1021'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='country',
            field=models.CharField(choices=[('GH', 'Ghana')], default=1, max_length=20),
            preserve_default=False,
        ),
    ]
