# Generated by Django 2.0.6 on 2018-07-30 09:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user_profile', '0002_auto_20180725_1457'),
    ]

    operations = [
        migrations.RenameField(
            model_name='userprofile',
            old_name='full_name',
            new_name='first_name',
        ),
        migrations.AddField(
            model_name='userprofile',
            name='last_last',
            field=models.CharField(default=1, max_length=100),
            preserve_default=False,
        ),
    ]
