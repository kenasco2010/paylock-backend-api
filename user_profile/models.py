from __future__ import unicode_literals
from django.contrib.auth.models import User
from django.utils import timezone
from django.conf import settings
from django.db import models
from imagekit.models import ProcessedImageField

from paylock.choices import COUNTRY
# Create your models here.

AUTH_USER_MODEL = getattr(settings, 'AUTH_USER_MODEL', 'auth.User')

class UserProfile(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    date_of_birth = models.DateField()
    current_address = models.CharField(max_length=200, blank=True, null=True)
    country = models.CharField(
        max_length=50, 
        blank=True, default="GH")
    profile_image = ProcessedImageField(upload_to='user_profile_image',
                                        format='JPEG',
                                        options={'quality': 30},
                                        null=True, blank=True)
    user = models.OneToOneField(AUTH_USER_MODEL,  on_delete=models.CASCADE,
    related_name='user_profile')
    profile_completed = models.BooleanField(default=False)
    published_date = models.DateTimeField(blank=True, null=True)
    created_date = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def publish(self):
        self.created_date = timezone.now()
        self.save()

    def __unicode__(self):
        return '{} {}'.format(self.first_name, self.last_name)

    def __str__(self):
        return '{} {}'.format(self.first_name, self.last_name)

    class Meta:
        verbose_name_plural = 'User Profiles'
