from django.shortcuts import render
from django.contrib.auth import authenticate, login, logout, get_user_model
from django.utils import timezone
from datetime import date
from rest_framework.authentication import TokenAuthentication
from rest_framework.status import (HTTP_201_CREATED,
                                   HTTP_200_OK,
                                   HTTP_400_BAD_REQUEST, HTTP_401_UNAUTHORIZED,
                                   HTTP_404_NOT_FOUND)
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework import viewsets
from rest_framework import permissions, status, generics, authentication
from rest_framework.decorators import detail_route, action, \
                    list_route, permission_classes, api_view
from rest_framework.authtoken.models import Token
from paylock.permissions import IsOwner
from user_account.models import User, UserManager
from .models import UserProfile
from transaction.models import Transaction
from wallet.models import EscrowWallet
from paylock_emails import signals
from .serializers import UserProfileSerializer, MyProfileSerializer, \
            EmailProfileSearchResult, ExternalProfileSerializer
import re

# Create your views here.
def create_collaborator_profile(firstname, lastname, user):
    UserProfile.objects.create(
        first_name = firstname,
        last_name = lastname,
        date_of_birth = date.today(),
        user = user,
        published_date = timezone.now()
    )


class UserProfileViewSet(viewsets.ModelViewSet):
    """Show list of user profiles when authenticated"""
    # permission_classes = (IsAuthenticated, )
    serializer_class = UserProfileSerializer
    queryset = UserProfile.objects.all()

    @list_route(methods=('POST',), url_path='create', 
        permission_classes=(IsAuthenticated, ))
    def create_user_profile(self, request, *args, **kwargs):
        """Create User profile when authenticated"""
        user = request.user
        user_profile_data=({
            'first_name': request.data.get('first_name'),
            'last_name': request.data.get('last_name'),
            'date_of_birth': request.data.get('date_of_birth'),
            'current_address': request.data.get('current_address'),
            'country': request.data.get('country'),
            'profile_image': request.FILES.get('profile_image'),
            'published_date': timezone.now(),
            'profile_completed': True,
            'user': user
            })

        try:
            user_profile = UserProfile.objects.get(user=user)
            if user_profile:
                user_profile.first_name = request.data.get('first_name')
                user_profile.last_name = request.data.get('last_name')
                user_profile.date_of_birth = request.data.get('date_of_birth')
                user_profile.current_address = request.data.get('current_address')
                user_profile.country = request.data.get('country')
                user_profile.profile_image = request.FILES.get('profile_image')
                user_profile.save()
                user_profile = self.serializer_class(user_profile)
                return Response({'status_code': status.HTTP_200_OK,
                    'message': 'Your profile has been updated',
                    'result': {
                        'data': user_profile.data
                        }
                    }, status=status.HTTP_200_OK)
        except UserProfile.DoesNotExist:
            if not user_profile_data['first_name']:
                return Response({'status_code': status.HTTP_404_NOT_FOUND,
                    'message': 'Your first name is required',
                    }, status=status.HTTP_404_NOT_FOUND)
            elif not user_profile_data['last_name']:
                return Response({'status_code': status.HTTP_404_NOT_FOUND,
                    'message': 'Your last name is required',
                    }, status=status.HTTP_404_NOT_FOUND)

            elif not user_profile_data['date_of_birth']:
                 return Response({'status_code': status.HTTP_404_NOT_FOUND,
                    'message': 'Your date of birth is required',
                    }, status=status.HTTP_404_NOT_FOUND)
            elif not user_profile_data['country']:
                 return Response({'status_code': status.HTTP_404_NOT_FOUND,
                    'message': 'Please select your country',
                    }, status=status.HTTP_404_NOT_FOUND)
            else:
                user_profile = UserProfile.objects.create(
                    **user_profile_data
                    )
                user_profile = self.serializer_class(user_profile)
                return Response({'status_code': status.HTTP_201_CREATED,
                        'message': 'Your have successfully created your profile',
                        'result': {
                            'data': user_profile.data
                            }
                        },status=status.HTTP_201_CREATED)

            
    @action(methods=('POST',), detail=False, url_path='my-profile', 
        permission_classes=(IsOwner,))
    def get_profile(self, request, *args, **kwargs):
        """ View my profile. 
            Parameters: profile id
        """
        user = request.user
        profile_id = request.data.get('profile_id')
        profile = UserProfile.objects.get(pk=profile_id)
        if profile.user == user:
            profile = MyProfileSerializer(profile)
            return Response({
                'status': status.HTTP_200_OK,
                'message':'My profile profile details',
                'result': {
                    'data': profile.data
                }
                }, status=status.HTTP_200_OK)
        else:
            return Response({
                'status': status.HTTP_400_BAD_REQUEST,
                'message':'You are not authorized to view another accounts profile.',
                }, status=status.HTTP_400_BAD_REQUEST)

    @action(methods=('POST',), detail=False, url_path='edit', 
        permission_classes=(IsOwner,))
    def edit_profile(self, request, *args, **kwargs):
        """ Edit Profile 
            Parameters: profile id
        """
        user = request.user
        profile_id = request.data.get('profile_id')
        try:
            profile = UserProfile.objects.get(pk=profile_id)
        except UserProfile.DoesNotExist:
            return Response({
                'status_code': HTTP_404_NOT_FOUND,
                'message': 'You do not have a profile on PayLock'
            },status=status.HTTP_404_NOT_FOUND)
        fields = ['first_name', 'last_name', 'date_of_birth', 'current_address', 'country']
        for key in fields:
            setattr(profile, key, request.data.get(key))
            profile.save()
            return Response({
                    'status_code': HTTP_200_OK,
                    'message': 'Your profile has been updated successfully.'
                },status=status.HTTP_200_OK)
        

    @action(methods=('POST',), detail=False, url_path='edit-profile-image', 
        permission_classes=(IsOwner,))
    def edit_profile(self, request, *args, **kwargs):
        """ Edit Profile image only 
            Parameters: profile id
        """
        user = request.user
        profile_id = request.data.get('profile_id')
        try:
            profile = UserProfile.objects.get(pk=profile_id)
        except UserProfile.DoesNotExist:
            return Response({
                'status_code': HTTP_404_NOT_FOUND,
                'message': 'You do not have a profile on PayLock'
            },status=status.HTTP_404_NOT_FOUND)
        profile.profile_image = request.FILES.get('profile_image')
        profile.save()
        return Response({
                    'status_code': HTTP_200_OK,
                    'message': 'Your profile image has been updated successfully.'
                },status=status.HTTP_200_OK)

    # Search for collaborators profile by email when creating a transaction.
    @action(methods=('POST',), detail=False, url_path='search-collaborator-profile-by-email', 
        permission_classes=(IsAuthenticated,))
    def search_profile_by_email(self, request, *args, **kwargs):
        """ Search for collaborators profile be email
            email : get email
        """
        email = request.data.get('email')
        try:
            profile = UserProfile.objects.filter(user__email__icontains=email)
        except UserProfile.DoesNotExist:
            return Response({
                'status': status.HTTP_404_NOT_FOUND,
                'message':'Profile details not found on PayLock',
                })
        profile = EmailProfileSearchResult(profile, many=True)
        return Response({
            'status': status.HTTP_200_OK,
            'result': {
                'data': profile.data
            }})

    @action(methods=('POST',), detail=False, url_path='external', 
        permission_classes=(IsAuthenticated,))
    def external_user_profiel(self, request, *args, **kwargs):
        """ External user profile from transaction
            Parameters: Profile id
        """
        profile_id = request.data.get('profile_id')
        try:
            profile = UserProfile.objects.get(pk=profile_id)
        except UserProfile.DoesNotExist:
            return Response({
                'status': status.HTTP_404_NOT_FOUND,
                'message':'Your collaborator has no profile attached to his account.',
                })
        
        user = User.objects.get(email=profile.user.email)
        # complated_transactions = EscrowWallet.objects.get(transaction)
        transaction = EscrowWallet.objects.all()
        profile = ExternalProfileSerializer(profile)
        return Response({
            'status': status.HTTP_200_OK,
            'result': {
                'data': profile.data,
                'transaction_data':{
                    # transaction owner
                'total_transaction_created': transaction.filter(
                    transaction__user=user).count(),
                'transactions_completed': transaction.filter(
                    transaction__user=user, can_cancel=False, is_active=False, 
                    payout_agreed=True).count(),
                # collaborator
                'total_collaborations': transaction.filter(
                    transaction__contractor=user).count(),
                'collaborations_completed': transaction.filter(
                    transaction__contractor=user, can_cancel=False, is_active=False, 
                    payout_agreed=True).count()

                # 'completed_transactions': 

                }
                
            }
            }, status=status.HTTP_200_OK)