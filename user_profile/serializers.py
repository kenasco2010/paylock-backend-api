from rest_framework import serializers
from user_account.serializers import UserSerializer

from .models import UserProfile

class UserProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserProfile
        fields = ('id', 'first_name', 'last_name', 'date_of_birth', 'current_address', 'country',
                'profile_image', 'user','profile_completed',
                'created_date')

        def check_date_of_birth(self):
            if date_of_birth >= timezone.now():
                raise ValidationError('Enter a valid Date Of Birth')

        def check_empty_first_name(self):
            if first_name is None:
                raise ValidationError('Full Name cannot be blank')


class MyProfileSerializer(serializers.ModelSerializer):
    user = UserSerializer()
    class Meta:
        model = UserProfile
        fields = ('user',
                'created_date')

class EmailProfileSearchResult(serializers.ModelSerializer):
    email = serializers.ReadOnlyField(source='user.email')
    class Meta:
        model = UserProfile
        fields = ('first_name', 'last_name', 'email', 'profile_image', 'created_date')  


class ExternalProfileSerializer(serializers.ModelSerializer):
    
    email = serializers.ReadOnlyField(source='user.email')
    phone_number = serializers.ReadOnlyField(source='user.phone_number')
   
    class Meta:
        model = UserProfile
        fields = ('email', 'phone_number', 'first_name', 'last_name',
            'profile_image', 'country', 'created_date')