from django.test import TestCase, RequestFactory, Client
from rest_framework.test import APIRequestFactory
from rest_framework.test import APIClient
from rest_framework.test import force_authenticate
from django.contrib.auth import get_user_model
from django.utils import timezone
from rest_framework.authtoken.models import Token
import json
from .models import UserProfile
from .serializers import UserProfileSerializer

# Create your tests here.
User = get_user_model()
class UserProfileTest(TestCase):

    def setUp(self):
        self.factory = RequestFactory()
        self.credentials = {
            'email': 'kenmartey89@gmail.com',
            'phone_number': '00233246424340',
            'password': 'ceremony',
            'is_active': True
            }
        self.user = User.objects.create_user(**self.credentials)
        self.data_input = {
        'first_name': 'Kennedy',
        'last_name': 'Anyinatoe',
        'date_of_birth': '1989-06-06',
        'current_address': '19 Banana ave',
        'country': 'Ghana',
        'profile_completed': False,
        'created_date': timezone.now(),
        'profile_image': '',
        }

    # Create User profile
    def test_create_user_profile(self, **kwargs):
        client = APIClient()
        headers = {
        'HTTP_AUTHORIZATION': 'Token {}'.format(self.user.auth_token),
        }
        request = self.client.post('/api/v1.0/user-profile/create/', 
            self.data_input, format='json', **headers
            )
        print ('===================CREATE PROFILE TEST=========================')
        print ('>>> >>> >>> /CREATE PROFILE TEST/ content', request.content)
        self.assertEqual(request.status_code, 201)
        print ('===================END OF CREATE PROFILE TEST=========================')
        print ('      ')

    # Get profile after creating
    def test_get_user_profile(self):
        client = APIClient()
        headers = {
        'HTTP_AUTHORIZATION': 'Token {}'.format(self.user.auth_token),
        }
        request = self.client.get('/api/v1.0/user-profile/', self.data_input,
            format='json', **headers
            )
        print ('===================GET PROFILE TEST=========================')
        print ('>>> >>> >>> /GET PROFILE TEST/ content', request.data)
        self.assertEqual(request.status_code, 200)
        print ('===================END OF GET PROFILE TEST=========================')
        print ('      ')


   