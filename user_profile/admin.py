from django.contrib import admin
import csv
from django.http import HttpResponse
from .models import UserProfile
# Register your models here.

class UserProfileAdmin(admin.ModelAdmin):
    actions = ['export_as_csv']
    search_fields = ['first_name', 'last_name']
    list_display  = ('first_name', 'last_name', 'user', 'user_phone_number', 'country', 'profile_completed', 
         'created_date')

    def user_phone_number(self, user_profile):
        return user_profile.user.phone_number

    def export_as_csv(self, request, queryset):
        meta = self.model._meta
        field_names = ['first_name','last_name', 'user', 'country', 'profile_completed']
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename={}.csv'.format(meta)
        writer = csv.writer(response)
        print(writer)
        writer.writerow(field_names)
        for obj in queryset:
            row = writer.writerow([getattr(obj, field) for field in field_names])
        return response
    export_as_csv.short_description = "Export Selected {}".format(UserProfile._meta.verbose_name_plural)
admin.site.register(UserProfile, UserProfileAdmin)