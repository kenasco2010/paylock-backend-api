from django.apps import AppConfig


class TransactionTypeConfig(AppConfig):
    name = 'transaction_type'
