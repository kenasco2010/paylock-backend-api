from django.test import TestCase
from django.test import TestCase, RequestFactory, Client
from django.contrib.auth import get_user_model
from django.utils import timezone
from rest_framework.test import APIRequestFactory, APIClient, force_authenticate
from rest_framework.authtoken.models import Token
import json
from .models import TransactionType
from .serializers import TransactionTypeSerializer

# Create your tests here.
User = get_user_model()
class TransactionTypeTest(TestCase):

    def setUp(self):
        self.credentials = {
            'email': 'kenmartey89@gmail.com',
            'phone_number': '00233246424340',
            'password': 'ceremony'
        }
        self.user = User.objects.create_superuser(**self.credentials)
        self.transaction_type_input = {
            'transaction_type': 'Product',
            'created_date': timezone.now()
        }
        self.transaction_type_input_update = {
            'transaction_type': 'Project',
            'created_date': timezone.now()
        }
        self.user = get_user_model().objects.get(email='kenmartey89@gmail.com')

    def test_add_transaction_type(self, **kwargs):
        client = APIClient()
        headers = {
            'HTTP_AUTHORIZATION': 'Token {}'.format(self.user.auth_token),
        }
        request = self.client.post('/api/v1.0/transaction-type/add-transaction-type/',
            self.transaction_type_input, format='json', **headers
        )
        print ('===================ADD TRANSACTION TYPE TEST=========================')
        print ('>>> >>> >>> /ADD TRANSACTION TYPE TEST/ content', request.content)
        self.assertEqual(request.status_code, 201)
        print ('===================ADD TRANSACTION TYPE TEST=========================')
        print ('      ')

    def test_update_transaction_type(self):
        client = APIClient()
        headers = {
            'HTTP_AUTHORIZATION': 'Token {}'.format(self.user.auth_token),
        }
        transaction_type = TransactionType.objects.create(**self.transaction_type_input,
        user=self.user)
        request = self.client.post('/api/v1.0/transaction-type/' + str(transaction_type.id) 
        +'/update-transaction-type/', self.transaction_type_input_update, 
        format='json', **headers
        )
        print ('===================UPDATE TRANSACTION TYPE TEST=========================')
        print ('>>> >>> >>> /UPDATE TRANSACTION TYPE METHOD TEST/ content', request.content)
        self.assertEqual(request.status_code, 201)
        print ('===================UPDATE TRANSACTION TYPE TEST=========================')
        print ('      ')

    def test_delete_transaction_type(self):
        client = APIClient()
        headers = {
            'HTTP_AUTHORIZATION': 'Token {}'.format(self.user.auth_token),
        }
        transaction_type = TransactionType.objects.create(**self.transaction_type_input,
        user=self.user)
        request = self.client.delete('/api/v1.0/transaction-type/' + str(transaction_type.id) 
        +'/delete-transaction-type/', self.transaction_type_input_update, 
        format='json', **headers
        )
        print ('===================DELETE TRANSACTION TYPE TEST=========================')
        print ('>>> >>> >>> /DELETE TRANSACTION TYPE METHOD TEST/ content', request.content)
        self.assertEqual(request.status_code, 200)
        print ('===================DELETE TRANSACTION TYPE TEST=========================')
        print ('      ')

    def test_empty_transaction_type_field(self):
        client = APIClient()
        headers = {
            'HTTP_AUTHORIZATION': 'Token {}'.format(self.user.auth_token),
        }
        transaction_type = ({'transaction_type':'', 'user':self.user})
        request = self.client.post('/api/v1.0/transaction-type/add-transaction-type/',
            transaction_type, format='json', **headers
        )
        print ('===================EMPTY TRANSACTION TYPE FIELD TEST=========================')
        print ('>>> >>> >>> /EMPTY TRANSACTION TYPE FIELD TEST/ content', request.content)
        self.assertEqual(request.status_code, 404)
        print ('===================EMPTY TRANSACTION TYPE FIELD TEST=========================')
        print ('      ')
