from django.db import models
from django.conf import settings
from django.utils import timezone
from payment_method.models import PaymentMethod

AUTH_USER_MODEL = getattr(settings, 'AUTH_USER_MODEL', 'auth.User')

# Create your models here.
class TransactionType(models.Model):
    transaction_type = models.CharField(max_length=200)
    user = models.ForeignKey(
        AUTH_USER_MODEL, related_name='transaction_type',
        on_delete=models.CASCADE)
    published_date = models.DateTimeField(blank=True, null=True)
    created_date = models.DateTimeField(default=timezone.now)
    modified = models.DateTimeField(auto_now=True)

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __unicode__(self):
        return '%s' % (self.transaction_type)

    def __str__(self):
        return '%s' % (self.transaction_type)
