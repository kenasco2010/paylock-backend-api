from django.conf.urls import url
from django.conf.urls import include
from django.urls import path
from rest_framework.routers import DefaultRouter
from .views import TransactionTypeViewSet

router = DefaultRouter()
router.register(r'v1.0/transaction-type', TransactionTypeViewSet)

urlpatterns = [
    path(r'', include(router.urls))
]