from django.contrib import admin

# Register your models here.
from .models import TransactionType

admin.site.register(TransactionType)