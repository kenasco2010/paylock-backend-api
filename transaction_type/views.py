from django.shortcuts import render
from django.contrib.auth import authenticate, login, logout, get_user_model
from django.utils import timezone
from rest_framework.authentication import TokenAuthentication
from rest_framework.status import (HTTP_201_CREATED,
                                   HTTP_200_OK,
                                   HTTP_400_BAD_REQUEST, HTTP_401_UNAUTHORIZED,
                                   HTTP_404_NOT_FOUND, HTTP_409_CONFLICT)
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework import viewsets
from rest_framework import permissions, status, generics, authentication
from rest_framework.decorators import detail_route, \
                    list_route, permission_classes, api_view
from rest_framework.authtoken.models import Token
from user_account.models import User, UserManager
from paylock.permissions import IsSuperUserPermission
from .models import TransactionType
from .serializers import TransactionTypeSerializer
# Create your views here.

class TransactionTypeViewSet(viewsets.ModelViewSet):
    """Shows a list of all transaction types"""
    permission_classes = (IsAuthenticated, )
    serializer_class = TransactionTypeSerializer
    queryset = TransactionType.objects.all().order_by('-id')

    @list_route(methods=('POST',), url_path='add-transaction-type', 
    permission_classes=(IsSuperUserPermission,))
    def add_transaction_type(self, request, *args, **kwargs):
        """Add transaction type eg. Product, Project"""
        user = request.user
        transaction_type_data ={
            'transaction_type': request.data.get('transaction_type'),
            'user': user,
            'created_date': timezone.now(),
            'published_date': timezone.now()
        }
        if not transaction_type_data['transaction_type']:
            return Response({'status_code': status.HTTP_404_NOT_FOUND,
                            'message': 'Please enter a transaction type'
                }, status=status.HTTP_404_NOT_FOUND)
        elif TransactionType.objects.filter(
            transaction_type=transaction_type_data['transaction_type']
            ).exists():
            return Response({'status_code': status.HTTP_409_CONFLICT,
                            'message': 'Transaction type already exist'
                }, status=status.HTTP_409_CONFLICT)
        else:
            transaction_type = TransactionType.objects.create(
                **transaction_type_data
                )
            transaction_type = self.serializer_class(transaction_type)
            return Response({'status_code': status.HTTP_201_CREATED,
                            'message': 'You have added a transaction type',
                            'result': {
                                'data': transaction_type.data
                            }
                }, status=status.HTTP_201_CREATED)
    
    @detail_route(methods=('POST',), url_path='update-transaction-type',
        permission_classes=(IsSuperUserPermission,))
    def update_transaction_type(self, request, pk=None, *args, **kwargs):
        """Edit transaction type"""
        user = request.user
        transaction_type = self.get_object()
        transaction_type.transaction_type = request.data.get('transaction_type')
        transaction_type.user = user
        transaction_type.save()
        transaction_type = self.serializer_class(transaction_type)
        return Response({'status_code': status.HTTP_201_CREATED,
                        'message': 'Transaction type updated',
                        'result': {
                            'data': transaction_type.data
                        }
            }, status=status.HTTP_201_CREATED)

    @detail_route(methods=('DELETE',), url_path='delete-transaction-type',
        permission_classes=(IsSuperUserPermission, ))
    def delete_transaction_type(self, request, pk=None):
        transaction_type = self.get_object()
        transaction_type.delete()
        return Response({'status_code': status.HTTP_200_OK,
                        'message': 'You have successfully deleted a transaction type'
            })
            

