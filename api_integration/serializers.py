from rest_framework import serializers
from .models import BusinessProfile, ApiIntegrationSetting, ApiTransaction
from transaction.serializers import TransactionSerializer
from request_funds_transfer.models import RequestFundsTransfer

# from .validators import validate_email

class ApiIntegrationSettingsSerializer(serializers.ModelSerializer):

	class Meta:
		model = ApiIntegrationSetting
		fields = '__all__'

class BusinessProfileSerializer(serializers.ModelSerializer):

	class Meta:
		model = BusinessProfile
		fields = '__all__'

class ApiTransactionsSerializer(serializers.ModelSerializer):

	class Meta:
		model = ApiTransaction
		fields = '__all__'


class APITransactionRequestFundsSerializer(serializers.ModelSerializer):
	"""
		Custom Funds Request Serializer for API Transaction.
	"""
	name = serializers.SerializerMethodField()
	amount = serializers.SerializerMethodField()
	status = serializers.SerializerMethodField()
	request_status = serializers.SerializerMethodField()
	class Meta:
		model = RequestFundsTransfer
		fields = ('id', 'name', 'amount', 'status', 'request_status', 'published_date', 'request_description',
                'amount', 'created_date', 'modified'
                )
	
	def get_name(self, funds_request):
		return '{}'.format(funds_request.transaction.name)

	def get_amount(self, funds_request):
		return '{}'.format(funds_request.transaction.amount)

	def get_status(self, funds_request):
		return '{}'.format(funds_request.transaction.stage)
	
	def get_request_status(self, funds_request):
		return '{}'.format(funds_request.status)