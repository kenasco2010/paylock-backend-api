from django.conf.urls import url
from django.conf.urls import include
from django.urls import path
from rest_framework.routers import DefaultRouter
from .views import ApiIntegrationSettingsViewSet
from .business_profile_views import BusinessProfileViewSet
from .webview_transaction_views import ApiTransactionsViewSet
from .funds_request_views import RespondToTransactionViewSet
from api_integration.payment_integration_views import ApiIntegrationPaymentViewSet
from .business_dashboard_data_views import BusinessDashboardDataViewSet

router = DefaultRouter()
router.register(r'v1.0/business-profile', BusinessProfileViewSet) # you don't need to specify base_name cos the models handle that for you.
router.register(r'v1.0/api-integration-setting', ApiIntegrationSettingsViewSet)
router.register(r'v1.0/api-integration', ApiTransactionsViewSet)
router.register(r'v1.0/api-integration/payment', ApiIntegrationPaymentViewSet)
router.register(r'v1.0/api-integration/funds', RespondToTransactionViewSet)
router.register(r'v1.0/api-integration/business-dashboard', BusinessDashboardDataViewSet)

urlpatterns = [
    path(r'', include(router.urls)),
]