from django.shortcuts import render
from rest_framework import viewsets, status
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.decorators import action
from django.utils import timezone
from django.conf import settings
from payment.models import Payment, SavedCardToken
from payment.views import encryptData, getKey
from .payment_json_payload import *
from payment.serializers import ApiIntegrationPaymentSerializer, SavedCardsTokenSerializer
from api_integration.models import *
from api_integration.serializers import ApiTransactionsSerializer
from paylock.currency_conversion import convert_currency
import requests
from decimal import Decimal
import uuid
import json


class ApiIntegrationPaymentViewSet(viewsets.ModelViewSet):
    """ ApiIntegrationPaymentViewSet handles all payments with
        3rd party integration.
    
    """
    queryset = ApiTransaction.objects.all()
    serializer_class = ApiTransactionsSerializer
    permission_classes = (AllowAny,)

    @action(methods=('POST',), detail=False, url_path='charge', permission_classes=(AllowAny,))
    def charge(self, request, *args, **kwargs):
        """ Function to charge card, mobile money or token(saved cards)
        """
        # user = request.user
        payment_type = request.data.get('payment_type')
        transaction_id = request.data.get('transaction_id')
        api_key = request.data.get('api_key')

        if payment_type == "" or not payment_type:
            return Response({'message': 'Payment type cannot be empty'})

        if transaction_id == "" or not transaction_id:
            return Response({'message': 'Transaction ID cannot be empty'})

        if api_key == "" or not api_key:
            return Response({'message': 'Please add your api key.'})

        try:
            api_settings = ApiIntegrationSetting.objects.get(token=api_key)
        except ApiIntegrationSetting.DoesNotExist:
            return Response({
                'status_code': status.HTTP_404_NOT_FOUND,
                'message': 'Your api key is wrong. It\'s not registered on PayLock'
            }, status=status.HTTP_404_NOT_FOUND)

        try:
            transaction = ApiTransaction.objects.get(transaction_id=transaction_id)
        except ApiTransaction.DoesNotExist:
            return Response({
                'status_code': status.HTTP_404_NOT_FOUND,
                'message': 'Transaction not found. Check the transaction id to make sure it\'s correct'
            }, status=status.HTTP_404_NOT_FOUND)
        try:
            business_profile = BusinessProfile.objects.get(user=transaction.collaborator)
        except BusinessProfile.DoesNotExist:
            return Response({
                'status_code': status.HTTP_404_NOT_FOUND,
                'message': 'Business profile not registered on PayLock.'
            }, status=status.HTTP_404_NOT_FOUND)
        
        if payment_type =="card":
            card_details = marketplace_card_payment_detail(request, api_settings.return_url) # card details collection payload
            # if business_profile.sales_currency != "USD":
            #     amount = convert_currency(business_profile.sales_currency, card_details['amount'])
            #     card_details['amount'] = round(amount,2)
            str_card_details = json.dumps(card_details) # converts dict to string with json.dumps
            payment = Payment.objects.create(
                api_transaction_id = transaction.transaction_id,
                payment_type=card_details['paymentType'],
                amount=card_details['amount'],
                amount_usd=request.data.get('amount_usd'),
                user=transaction.user,
                description="Api integration Payment",
                txref=card_details['txRef'],
                payment_status='initialized',
                activity_type='business',
                save_card=request.data.get('save_card_option'),
                published_date=timezone.now()
                )
            
            # payload = marketplace_card_payment_detail(request, api_settings.return_url) #Initial payload to send to flutterwave
            # print("Second payload",payload)
            encode_data = json.dumps(card_details) # Serialize object into Json formatted str
            encrypted_data = encryptData(getKey(), encode_data) # Pass data into encrypted Algorithm
            data = {
                "PBFPubKey": settings.FLUTTERWAVE_PUBLIC_KEY, # The public key from flutterwave
                "client": encrypted_data,
                "alg": "3DES-24"
            }
            response = requests.post(settings.FLUTTERWAVE_URL, data=data)
            response_json = response.json()
            try:
                if response_json['data']:
                    if 'suggested_auth' in response_json['data']:
                        payment.payment_status = "in_progress"
                        payment.charge_response_message = 'Validate with {}'.format(response_json['data']['suggested_auth'])
                        payment.save()
                        payment = ApiIntegrationPaymentSerializer(payment)
                        return Response({"status_code": status.HTTP_200_OK,
                                "message": response_json['message'],
                                "data": response_json,
                                "payment_data": payment.data
                        }, status=status.HTTP_200_OK)
                    else:
                        payment.payment_status = "in_progress"
                        payment.charge_response_code = response_json['data']['chargeResponseCode']
                        payment.charge_response_message = response_json['data']['chargeResponseMessage']
                        payment.transaction_reference = response_json['data']['flwRef']
                        payment.txref = response_json['data']['txRef']
                        payment.appfee = response_json['data']['appfee']
                        payment.save()
                        payment = ApiIntegrationPaymentSerializer(payment)
                        return Response({"status_code": status.HTTP_200_OK,
                                    "message": response_json['message'],
                                    "data": response_json,
                                    "payment_data": payment.data
                                    
                            },status=status.HTTP_200_OK)
                else:
                    payment = ApiIntegrationPaymentSerializer(payment)
                    return Response({"status_code": status.HTTP_200_OK,
                                "message": response_json['message'],
                                "data": response_json,
                                "payment_data": payment.data
                                
                        }, status=status.HTTP_200_OK)
            except  KeyError:
                payment.payment_status = "failed"
                payment.charge_response_code = response_json['data']['code']
                payment.charge_response_message = response_json['message']
                payment.save()
                payment = ApiIntegrationPaymentSerializer(payment)
                return Response({"status_code": status.HTTP_400_BAD_REQUEST,
                            # "message": response_json['message'],
                            "result": {
                                # "data": response_json,
                                # "payment_data": payment.data
                            }
                    }, status=status.HTTP_400_BAD_REQUEST)
        elif payment_type == "mobilemoney":
            """ Charge with mobile money.
            
            """
            payment_type = request.data.get('payment_type')
            api_key = request.data.get('api_key')

            if payment_type == "" or not payment_type:
                return Response({'message': 'Payment type cannot be empty'})

            if api_key == "" or not api_key:
                return Response({'message': 'Please add your api key.'})    

            try:
                api_settings = ApiIntegrationSetting.objects.get(token=api_key)
            except ApiIntegrationSetting.DoesNotExist:
                return Response({
                    'status_code': status.HTTP_404_NOT_FOUND,
                    'message': 'Your api key is wrong. It\'s not registered on PayLock'
                }, status=status.HTTP_404_NOT_FOUND)

            mm_payment_detail = marketplace_payment_detail_mobile_money(request, transaction.user.email) # momo details collection payload
            networks = ['RWF', 'MTN', 'TIGO', 'VODAFONE']
            if mm_payment_detail['network'] in networks:
                mm_payment_detail['is_mobile_money_gh'] = 1
                mm_payment_detail['voucher'] = ""
            elif mm_payment_detail['network'] == "VODAFONE":
                mm_payment_detail['voucher'] = request.data.get('voucher')
            usd_amount = convert_currency(mm_payment_detail['currency'], mm_payment_detail['amount'])
            payment = Payment.objects.create(
                api_transaction_id = transaction.transaction_id,
                payment_type=mm_payment_detail['payment_type'],
                amount=mm_payment_detail['amount'],
                amount_usd=usd_amount,
                user=transaction.user,
                description="Paid with Mobile Money & converted to USD",
                txref=mm_payment_detail['txRef'],
                orderRef=mm_payment_detail['orderRef'],
                activity_type="business",
                published_date=timezone.now()
                )
            encode_data = json.dumps(mm_payment_detail) # Serialize object into Json formatted str
            encrypted_data = encryptData(getKey(), encode_data) # Pass data into encrypted Algorithm
            data = {
                "PBFPubKey": settings.FLUTTERWAVE_PUBLIC_KEY, # The public key from flutterwave
                "client": encrypted_data,
                "alg": "3DES-24"
            }
            response = requests.post(settings.FLUTTERWAVE_GH_MM_URL, data=data)
            response_json = response.json()
            try:
                if response_json['data']:
                    if response_json['status'] == 'success':
                        payment.payment_status = "in_progress"
                        payment.charge_response_code = response_json['data']['chargeResponseCode']
                        payment.charge_response_message = response_json['data']['chargeResponseMessage']
                        payment.transaction_reference = response_json['data']['flwRef']
                        # payment.txref = response_json['data']['txRef']
                        payment.appfee = response_json['data']['appfee']
                        # made changes here
                        payment.mobile_money_amount = response_json['data']['amount']
                        payment.save()
                        payment = ApiIntegrationPaymentSerializer(payment)
                        return Response({"status_code": status.HTTP_201_CREATED,
                                    'message': 'Mobile Money initialized',
                                    "result": {
                                        "data": response_json,
                                        "payment_data": payment.data
                                    }
                            }, status=status.HTTP_201_CREATED)
                    else:
                        return Response({"status_code": status.HTTP_400_BAD_REQUEST,
                                    'message': response_json['message']
                            }, status=status.HTTP_400_BAD_REQUEST)
                else:
                    return Response({"status_code": status.HTTP_400_BAD_REQUEST,
                                    'message': response_json
                            }, status=status.HTTP_400_BAD_REQUEST)
            except  KeyError:
                payment.payment_status = "failed"
                payment.charge_response_code = response_json['data']['code']
                payment.charge_response_message = response_json['message']
                payment.save()
                payment = ApiIntegrationPaymentSerializer(payment)
                return Response({"status_code": HTTP_400_BAD_REQUEST,
                            'message': response_json['message'],
                            "result": {
                                "data": response_json,
                                "payment_data": payment.data
                            }
                    },status=status.HTTP_400_BAD_REQUEST)
        elif payment_type == "token":
            """ Make payment with saved card which is
                by using the token returned in the verify payment response.
        
            """
            payment_type = request.data.get('payment_type')
            api_key = request.data.get('api_key')

            if payment_type == "" or not payment_type:
                return Response({'message': 'Payment type cannot be empty'})

            if api_key == "" or not api_key:
                return Response({'message': 'Please add your api key.'})    

            try:
                api_settings = ApiIntegrationSetting.objects.get(token=api_key)
            except ApiIntegrationSetting.DoesNotExist:
                return Response({
                    'status_code': status.HTTP_404_NOT_FOUND,
                    'message': 'Your api key is wrong. It\'s not registered on PayLock'
                }, status=status.HTTP_404_NOT_FOUND)
            
            data = {
                "token": request.data.get('card_token'),
                "SECKEY": settings.FLUTTERWAVE_SECRET_KEY,
                "email": request.data.get('email'),
                "country":"NG",
                "amount":request.data.get('amount'),
                "firstname": request.data.get('first_name'),
                "lastname":request.data.get('last_name'),
                "currency": "USD",
                "txRef":  "PAYL-card_Tref" + str(uuid.uuid4()),
            }
            payment = Payment.objects.create(
                api_transaction_id = transaction.transaction_id,
                payment_type='Card token',
                amount=data['amount'],
                amount_usd=data['amount'],
                user=transaction.user,
                description="makepayment with saved card / token",
                txref=data['txRef'],
                save_card=False,
                activity_type="business",
                published_date=timezone.now()
                )
            response = requests.post(settings.FLUTTERWAVE_CHARGE_WITH_TOKEN, data=data)
            response_json = response.json()
            try:
                if response_json['data']['status'] == "successful":
                    payment.appfee = response_json['data']['appfee']
                    payment.payment_status = "verifying"
                    payment.charge_response_code ="02"
                    # payment.actual_amount = Decimal(response_json['data']['amount']) - Decimal(response_json['data']['appfee']) 
                else:
                    payment.payment_status = "in_progress"
                payment.save()
                return Response({"status_code": status.HTTP_200_OK,
                            'message': response_json['message'],
                            "result": {
                                "data": response_json
                            }
                    },status=status.HTTP_200_OK)
            except KeyError:
                return Response({"status_code": status.HTTP_400_BAD_REQUEST,
                            'message': response_json['message'],
                            "result": {
                                "data": response_json
                            }
                    },status=status.HTTP_400_BAD_REQUEST)
        return Response({"status_code": status.HTTP_200_OK,
                            'message': response_json['message'],
                            "result": {
                                "data": response_json
                            }
                    },status=status.HTTP_200_OK)

        
    # Validation with PIN
    @action(methods=('POST',), detail=False, url_path="validate-card-with-pin", 
    permission_classes=(AllowAny,))
    def validate_local_card_with_pin(self, request, *args, **kwargs):
        """ Validate your card with PIN and suggested auth
        
        """
        payment_type = request.data.get('payment_type')
        # transaction_id = request.data.get('transaction_id')
        api_key = request.data.get('api_key')
        payment_reference = request.data.get("txref")

        if payment_type == "" or not payment_type:
            return Response({'message': 'Payment type cannot be empty'})

        # if transaction_id == "" or not transaction_id:
        #     return Response({'message': 'Transaction ID cannot be empty'})

        if api_key == "" or not api_key:
            return Response({'message': 'Please add your api key.'})

        if payment_reference == "" or not payment_reference:
            return Response({'message': 'Please add your payment reference you got from your initial card endpoint call.'})

        try:
            api_settings = ApiIntegrationSetting.objects.get(token=api_key)
        except ApiIntegrationSetting.DoesNotExist:
            return Response({
                'status_code': status.HTTP_404_NOT_FOUND,
                'message': 'Your api key is wrong. It\'s not registered on PayLock'
            }, status=status.HTTP_404_NOT_FOUND)

        # try:
        #     transaction = ApiTransaction.objects.get(transaction_id=transaction_id)
        # except ApiTransaction.DoesNotExist:
        #     return Response({
        #         'status_code': status.HTTP_404_NOT_FOUND,
        #         'message': 'Transaction not found.'
        #     }, status=status.HTTP_404_NOT_FOUND)
        try:
            payment = Payment.objects.get(txref=payment_reference)
        except Payment.DoesNotExist:
            return Response({
                'status_code': status.HTTP_404_NOT_FOUND,
                'message': 'Payment with payment reference cannot be found.'
                }, status=status.HTTP_404_NOT_FOUND)

        payload = final_payment_payload_with_pin(request, api_settings.return_url)
        encode_data = json.dumps(payload) 
        encrypted_data = encryptData(getKey(), encode_data)
        data = {
            "PBFPubKey": settings.FLUTTERWAVE_PUBLIC_KEY,
            "client": encrypted_data,
            "alg": "3DES-24"
        }
        response = requests.post(settings.FLUTTERWAVE_URL, data=data)
        response_json = response.json()
        # requests.post('http://83e01a4e.ngrok.io', json=response_json)
        try:
            response_json['data']['chargeResponseCode']
            payment.payment_status = "in_progress"
            payment.charge_response_code = response_json['data']['chargeResponseCode']
            payment.charge_response_message = response_json['data']['chargeResponseMessage']
            payment.transaction_reference = response_json['data']['flwRef']
            payment.txref = response_json['data']['txRef']
            payment.save()
            payment = ApiIntegrationPaymentSerializer(payment)
            return Response({"status_code": status.HTTP_200_OK,
                            "data": response_json,
                            "payment_data": payment.data
                }, status=status.HTTP_200_OK)
        except  KeyError:
            payment.payment_status = "failed"
            payment.charge_response_code = response_json['data']['code']
            payment.charge_response_message = response_json['message']
            payment.save()
            payment = ApiIntegrationPaymentSerializer(payment)
            return Response({"status_code": status.HTTP_400_BAD_REQUEST,
                        "message": response_json['message'],
                            "data": response_json,
                            "payment_data": payment.data
                }, status=status.HTTP_400_BAD_REQUEST)
    
    #  Final verification with payment with OTP
    @action(methods=('POST',), detail=False, url_path="verify-payment-with-otp", 
        permission_classes=(AllowAny,))
    def validate_payment_with_otp(self, request, *args, **kwargs):
        """ Final payment confirmation with OTP
            Args:
                otp: the otp validation sent to your phone.
                payment_reference or txref: The payment id of the transaction.

            Returns:
                Returns payload from flutterwave if the transaction is verified.
        """
        otp = request.data.get('otp')
        payment_reference = request.data.get('txref')
        
        if not otp or otp == "":
            return Response({'status_code': status.HTTP_404_NOT_FOUND,
            'message': 'Please type in the code sent to your phone.'
            }, status=status.HTTP_404_NOT_FOUND)

        try:
            current_payment = Payment.objects.get(txref=payment_reference)
        except Payment.DoesNotExist:
            return Response({
                'status_code': status.HTTP_404_NOT_FOUND,
                'message': 'Payment with payment reference cannot be found.'
                }, status=status.HTTP_404_NOT_FOUND)

        validate_data = {
            "PBFPubKey": settings.FLUTTERWAVE_PUBLIC_KEY,
            "transaction_reference": current_payment.transaction_reference,
            "otp": otp
        }
        url = settings.FLUTTERWAVE_VALIDATE_PAYMENT_URL
        request = requests.post(url, data=validate_data)
        validation_response = request.json()
        # requests.post('http://83e01a4e.ngrok.io', json=validation_response)
        if validation_response['status'] == "success"  and validation_response['data']['data']['responsecode'] == "00":              
            current_payment.payment_status = "verifying"
            current_payment.charge_response_code = "02"
            current_payment.charge_response_message = ""
            current_payment.orderRef = validation_response['data']['tx']['orderRef']
            current_payment.txref = validation_response['data']['tx']['txRef']
            current_payment.save()
            current_payment = ApiIntegrationPaymentSerializer(current_payment)
            return Response({'status_code': status.HTTP_202_ACCEPTED,
                    'message': 'Your payment is being verified.',
                    'result': {
                        'data': current_payment.data
                    }
                }, status=status.HTTP_202_ACCEPTED)
        else:
            return Response({'status_code': status.HTTP_400_BAD_REQUEST,
                    'message': validation_response['message'],
                }, status=status.HTTP_400_BAD_REQUEST)
    
    # VBV Validation
    @action(methods=('POST',), detail=False, url_path="validate-with-vbv", 
        permission_classes=(AllowAny,))
    def validate_with_vbv(self, request, *args, **kwargs):
        """ Validate card by VBV or billing address
        
        """
        payment_type = request.data.get('payment_type')
        # transaction_id = request.data.get('transaction_id')
        api_key = request.data.get('api_key')
        payment_reference = request.data.get("txref")
        if payment_type == "" or not payment_type:
            return Response({'message': 'Payment type cannot be empty'})

        if api_key == "" or not api_key:
            return Response({'message': 'Please add your api key.'})

        if payment_reference == "" or not payment_reference:
            return Response({'message': 'Please add your payment reference you got from your initial card endpoint call.'})

        try:
            payment = Payment.objects.get(txref=payment_reference)
        except Payment.DoesNotExist:
            return Response({
                'status_code': status.HTTP_404_NOT_FOUND,
                'message': 'Payment with payment reference cannot be found. '
                'Either transaction is completed or is canceled. Initiate transaction again.'
                }, status=status.HTTP_404_NOT_FOUND)
        try:
            api_settings = ApiIntegrationSetting.objects.get(token=api_key)
        except ApiIntegrationSetting.DoesNotExist:
            return Response({
                'status_code': status.HTTP_404_NOT_FOUND,
                'message': 'Your api key is wrong. It\'s not registered on PayLock'
            }, status=status.HTTP_404_NOT_FOUND)

        payload = final_payment_payload_with_billing_address(request, api_settings.return_url) # remove this.
        encode_data = json.dumps(payload) # Serialize object into Json formatted str
        encrypted_data = encryptData(getKey(), encode_data) # Pass data into encrypted Algorithm
        data = {
            "PBFPubKey": settings.FLUTTERWAVE_PUBLIC_KEY,
            "client": encrypted_data,
            "alg": "3DES-24"
        }
        response = requests.post(settings.FLUTTERWAVE_URL, data=data)
        response_json = response.json()
        # requests.post('http://a1b148cb.ngrok.io', json=response_json)
        try:
            response_json['data']['chargeResponseCode']
            payment.payment_status = "in_progress"
            payment.charge_response_code = response_json['data']['chargeResponseCode']
            payment.charge_response_message = response_json['data']['chargeResponseMessage']
            payment.transaction_reference = response_json['data']['flwRef']
            payment.txref = response_json['data']['txRef']
            payment.save()
            payment = ApiIntegrationPaymentSerializer(payment)
            return Response({"status_code": status.HTTP_200_OK,
                        "result": {
                            "data": response_json,
                            "payment_data": payment.data
                        }
                })
        except  KeyError:
            payment.payment_status = "failed"
            payment.txref = ""
            # payment.charge_response_message = response_json['message']
            payment.save()
            payment = ApiIntegrationPaymentSerializer(payment)
            return Response({"status_code": status.HTTP_400_BAD_REQUEST,
                        "message": response_json['message'],
                        "result": {
                            "data": response_json,
                            "payment_data": payment.data
                        }
                }, status=status.HTTP_400_BAD_REQUEST)
        
    @action(methods=('POST',), detail=False, url_path="saved-cards", 
        permission_classes=(AllowAny,))
    def show_saved_cards(self, request, *args, **kwargs):
        """ Show saved cards
        
        """
        email = request.data.get('email')
        api_key = request.data.get('api_key')
        
        if email == "" or not email:
            return Response({'message': 'Please include user\'s email.'})

        if api_key == "" or not api_key:
            return Response({'message': 'Please add your api key.'})
        try:
            api_settings = ApiIntegrationSetting.objects.get(token=api_key)
        except ApiIntegrationSetting.DoesNotExist:
            return Response({
                'status_code': status.HTTP_404_NOT_FOUND,
                'message': 'Your api key is wrong. It\'s not registered on PayLock'
            }, status=status.HTTP_404_NOT_FOUND)
        
        saved_card = SavedCardToken.objects.filter(user__email=email)
        if saved_card.exists():
            saved_cards = SavedCardsTokenSerializer(saved_card, many=True)
            return Response({"status_code": status.HTTP_200_OK,
                        'message': 'Your saved cards',
                        "result": {
                            "data": saved_cards.data
                        }
                },status=status.HTTP_200_OK)
        else:
            return Response({
                'status_code': status.HTTP_404_NOT_FOUND,
                    'message': 'User has no saved cards'
                    })

    @action(methods=('POST',), detail=False, url_path="qvo-charge", 
        permission_classes=(AllowAny,))
    def chile_payment_qvo(self, request, *args, **kwargs):
        """ Accept payment in Chile with QVO.
        
        """
        headers = {
            'Content-Type': 'application/json',
            'Accept':  'application/json',
            'Authorization': settings.QVO_TOKEN

        }
        api_key = request.data.get('api_key')
        initial_transaction_id = request.data.get('transaction_id')

        if api_key == "" or not api_key:
            return Response({'message': 'Please add your api key.'})
        
        if initial_transaction_id == "" or not initial_transaction_id:
            return Response({'message': 'Transaction ID cannot be empty'})

        try:
            api_settings = ApiIntegrationSetting.objects.get(token=api_key)
        except ApiIntegrationSetting.DoesNotExist:
            return Response({
                'status_code': status.HTTP_404_NOT_FOUND,
                'message': 'Your api key is wrong. It\'s not registered on PayLock'
            }, status=status.HTTP_404_NOT_FOUND)

        try:
            api_transaction = ApiTransaction.objects.get(transaction_id=initial_transaction_id)
        except ApiTransaction.DoesNotExist:
            return Response({
                'status_code': status.HTTP_404_NOT_FOUND,
                'message': 'Transaction not found. Check the transaction id to make sure it\'s correct'
            }, status=status.HTTP_404_NOT_FOUND)

        # url = 'https://api.qvo.cl/webpay_plus/charge'
        url = settings.QVO_URL
        #PUT YOUR PAYLOAD HERE
        payload = {
            "amount":request.data.get('amount'),
            "description": request.data.get('description'),
            "return_url": api_settings.return_url

        }
        if payload['amount'] == "" or not payload['amount']:
            return Response({'message': 'Amount field is compulsory.'})

        if payload['description'] == "" or not payload['description']:
            return Response({'message': 'Please add a description to the request.'})
        
        usd_amount = convert_currency(api_transaction.currency, payload['amount'])
        
        response = requests.post(url, json=payload, headers=headers)
        json_response = response.json()
        # requests.post('https://09c9c2fd.ngrok.io', json=json_response)
        if 'error' in json_response:
            return Response({
            'status_code': status.HTTP_400_BAD_REQUEST,
            'message': 'Payment could not be initiated',
            'result': {
                'data': response.json()
            }
        }, status=status.HTTP_400_BAD_REQUEST)
        payment = Payment.objects.create(
            payment_type='card',
            api_transaction_id = api_transaction.transaction_id,
            amount=payload['amount'],
            amount_usd = usd_amount,
            user=api_transaction.user,
            description="Api integration with CLP & converted to USD",
            activity_type = 'business',
            txref=json_response['transaction_id'],
            published_date=timezone.now()
            )
        return Response({
            'status_code': status.HTTP_200_OK,
            'message': 'Payment initiated with QVO payment',
            'result': {
                'data': response.json()
            }
        }, status=status.HTTP_200_OK)