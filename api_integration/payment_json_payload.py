from django.utils import timezone
from django.conf import settings
import uuid

def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip

def marketplace_card_payment_detail(request, redirect_url):
    payment_detail = {
        "PBFPubKey": settings.FLUTTERWAVE_PUBLIC_KEY,
        "cardno": request.data.get('card_no'),
        "cvv": request.data.get('cvv'),
        "expirymonth": request.data.get('expiry_month'),
        "expiryyear": request.data.get('expiry_year'),
        "currency": "USD",
        "country": request.data.get('country'),
        "amount": request.data.get('amount'),
        "paymentType": request.data.get('payment_type'),
        "email": request.data.get('email'),
        "phonenumber": request.data.get('phone_number'),
        "firstname": request.data.get('first_name'),
        "lastname": request.data.get('last_name'),
        "activity_type": request.data.get('activity_type'),
        "IP": get_client_ip(request),
        "txRef":  "PAYL-card_Tref" + str(uuid.uuid4()),
        "redirect_url": redirect_url
        }
    return payment_detail

def final_payment_payload_with_pin(request, redirect_url):
    second_request_payload = {
        "PBFPubKey": settings.FLUTTERWAVE_PUBLIC_KEY,
        "pin": request.data.get('pin'),
        "suggested_auth": request.data.get('suggested_auth'),
        "IP": "355426087298442",
        # "txRef": "MC-" + str(timezone.now()),
        "redirect_url": redirect_url
        }
    payload = second_request_payload
    card_input_payload = dict(marketplace_card_payment_detail(request, redirect_url))
    final_payload = {**card_input_payload, **payload}
    return final_payload

def final_payment_payload_with_billing_address(request, redirect_url):
    billing_address = {
        "currency": "USD",
        "billingzip": request.data.get('billing_zip'),
        "billingcity": request.data.get('billing_city'),
        "billingaddress": request.data.get('billing_address'),
        "billingstate": request.data.get('billing_state'),
        "billingcountry": request.data.get('billing_country'),
        "redirect_url": redirect_url,
        "suggested_auth": request.data.get('suggested_auth')
    }
    avs_vbvsecurecode_payload = {
        "PBFPubKey": settings.FLUTTERWAVE_PUBLIC_KEY
       }
    billing_address_payload = billing_address
    vbv_payload = avs_vbvsecurecode_payload
    final_vbv_auth_payload = {**billing_address_payload, 
    **vbv_payload, **marketplace_card_payment_detail(request, redirect_url)}
    return final_vbv_auth_payload

# CHARGE WITH MOBILE MONEY

def marketplace_payment_detail_mobile_money(request, user_email):
    mm_payment_detail = {
        "PBFPubKey": settings.FLUTTERWAVE_PUBLIC_KEY,
        "payment_type": request.data.get('payment_type'),
        "country": request.data.get('country'),
        "amount": request.data.get('amount'),
        "network": request.data.get('network'),
        "currency": request.data.get('currency'),
        "voucher": request.data.get('voucher'),
        "email": user_email,
        "phonenumber": request.data.get('phone_number'),
        "firstname": request.data.get('first_name'),
        "lastname": request.data.get('last_name'),
        "activity_type": request.data.get('activity_type'),
        "IP": get_client_ip(request),
        "txRef":  "PAYL-momo_Txref" + str(uuid.uuid4()),
        "orderRef":  "PAYL-momo_Oref" + str(uuid.uuid4()),
        "is_mobile_money_gh": request.data.get('is_mobile_money_gh'),
        "is_mobile_money_ug" : request.data.get('is_mobile_money_ug'),
        "device_fingerprint": "69e6b7f0b72037aa8428b70fbr03986c"
        }
    return mm_payment_detail