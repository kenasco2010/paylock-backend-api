from django.shortcuts import render
from django.db.models import Sum, Q
from rest_framework import viewsets, status
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.decorators import action
from django.utils import timezone
from paylock.paginations import StandardResultsSetPagination
from django.conf import settings
from payment.models import Payment, SavedCardToken
from user_profile.models import UserProfile
from payment.views import encryptData, getKey
from payment.serializers import PaymentSerializer
from .payment_json_payload import *
from payment.serializers import ApiIntegrationPaymentSerializer, SavedCardsTokenSerializer
from api_integration.models import *
from api_integration.business_dashboard_data_serializers import *
from paylock.currency_conversion import convert_currency
import requests
from decimal import Decimal
import uuid
import json


class BusinessDashboardDataViewSet(viewsets.ModelViewSet):
    """ Data to show on the business dashboard

    """
    queryset = ApiTransaction.objects.all()
    serializer_class = BusinessCustomersSerializer
    permission_classes = (IsAuthenticated,)

    @action(methods=('GET',), detail=False, url_path='customers', permission_classes=(IsAuthenticated,))
    def business_customers(self, request, *args, **kwargs):
        """ List of customers that belongs to a particular business.

        """
        loggedin_user = request.user
        api_transactions = ApiTransaction.objects.filter(collaborator=loggedin_user)
        serialize_profiles = BusinessCustomersSerializer(api_transactions, many=True)
        return Response({
				'status_code': status.HTTP_200_OK,
				'message': 'Your business customers',
				'data': serialize_profiles.data
			}, status=status.HTTP_200_OK)
    

    @action(methods=('GET',), detail=False, url_path='transactions', permission_classes=(IsAuthenticated,))
    def business_transactions(self, request, *args, **kwargs):
        """ List of transactions that belongs to a particular business.

        """
        loggedin_user = request.user
        api_transactions = ApiTransaction.objects.filter(collaborator=loggedin_user)
        serialize_profiles = ApiTransactionsSerializer(api_transactions, many=True)
        return Response({
				'status_code': status.HTTP_200_OK,
				'message': 'Your business transactions',
				'data': serialize_profiles.data
			}, status=status.HTTP_200_OK)
    
    @action(methods=('GET',), detail=False, url_path='recent-transactions', permission_classes=(IsAuthenticated,))
    def business_recent_transactions(self, request, *args, **kwargs):
        """ List of recent transactions that belongs to a particular business.

        """
        loggedin_user = request.user
        api_transactions = ApiTransaction.objects.filter(collaborator=loggedin_user).order_by('-created_date')[:5]
        serialize_profiles = ApiTransactionsSerializer(api_transactions, many=True)
        return Response({
				'status_code': status.HTTP_200_OK,
				'message': 'Your recent business transactions',
				'data': serialize_profiles.data
			}, status=status.HTTP_200_OK)
    
    @action(methods=('GET',), detail=False, url_path='transaction-count', permission_classes=(IsAuthenticated,))
    def business_number_of_transactions(self, request, *args, **kwargs):
        """ List of recent transactions that belongs to a particular business.

        """
        loggedin_user = request.user
        api_transactions = ApiTransaction.objects.filter(collaborator=loggedin_user).count()
        return Response({
				'status_code': status.HTTP_200_OK,
				'message': 'Total number of transactions',
				'data': api_transactions
			}, status=status.HTTP_200_OK)
    
    @action(methods=('GET',), detail=False, url_path='total-transaction-amount', permission_classes=(IsAuthenticated,))
    def business_total_transaction_amount(self, request, *args, **kwargs):
        """ Total transaction amount for a business.

        """
        loggedin_user = request.user
        transaction_amount = ApiTransaction.objects.filter(collaborator=loggedin_user, stage='created').aggregate(Sum('usd_amount'))
        return Response({
				'status_code': status.HTTP_200_OK,
				'message': 'Total amount for the number of transactions on the dashboard.',
				'data': transaction_amount
			}, status=status.HTTP_200_OK)

    @action(methods=('GET',), detail=False, url_path='wallet-balance&activities', permission_classes=(IsAuthenticated,))
    def business_wallet_balance(self, request, *args, **kwargs):
        """ Get the wallet balance and wallet activities for businesses.

        """
        user = request.user
        paginator = StandardResultsSetPagination()
        payment_status = ['completed', 'in_progress', 'initialized', 'failed', 'canceled', 'outstanding']
        activities = Payment.objects.filter(user=user, payment_status__in=payment_status, activity_type = 'business').order_by('-id')
        result_page = paginator.paginate_queryset(activities, request)
        activities = PaymentSerializer(result_page, many=True)
        activities = paginator.get_paginated_response(activities.data)
        return Response({
            'status_code': status.HTTP_200_OK,
            'message': 'Business wallet activities',
            'result': {
                'data': activities.data,
                'wallet_balance': user.user_wallets.balance
            }
        }, status=status.HTTP_200_OK)

    
    