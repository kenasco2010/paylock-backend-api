from django.shortcuts import render
from django.utils import timezone
from django.conf import settings
from django.dispatch import receiver
from rest_framework import viewsets, status
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.decorators import action
from decimal import Decimal
from api_integration.models import ApiTransaction
from transaction.models import Transaction
from wallet.models import UserWallet, EscrowWallet
from api_integration.models import ApiIntegrationSetting, ApiTransaction
from .serializers import APITransactionRequestFundsSerializer
from request_funds_transfer.models import RequestFundsTransfer
from datetime import datetime, timedelta
from paylock_emails.signals import send_email_funds_has_been_approved, \
        send_email_to_seller_money_has_been_released
from payment.views import update_payment_history


class RespondToTransactionViewSet(viewsets.ModelViewSet):
    """ This viewset handles all the response to an api transaction.
        Request for funds by completing a service or delivering an item.
        Release funds 
        Decline funds

    """
    serializer_class = APITransactionRequestFundsSerializer
    queryset = RequestFundsTransfer.objects.all().order_by('-id')
    permission_classes = (AllowAny,)

    @action(methods=('POST', ), detail=False, url_path='request', 
                    permission_classes=(AllowAny,))
    def request_for_funds(self, request, *args, **kwargs):
        """ Request for funds when a service is rendered or product / Item is delivered.
        
        """

        transaction_id = request.data.get('transaction_id')
        api_key = request.data.get('api_key')
        if transaction_id == "" or not transaction_id:
            return Response({
                'status_code': status.HTTP_404_NOT_FOUND,
                'message': 'Transaction Id cannot be empty. Add your transaction id.'
                }, status=status.HTTP_404_NOT_FOUND)

        if api_key == "" or not api_key:
            return Response({
                'status_code': status.HTTP_404_NOT_FOUND,
                'message': 'API key cannot be empty. Add your api key.'
                }, status=status.HTTP_404_NOT_FOUND)

        try:
            api_settings = ApiIntegrationSetting.objects.get(token=api_key)
        except ApiIntegrationSetting.DoesNotExist:
            return Response({
                'status_code': status.HTTP_404_NOT_FOUND,
                'message': 'Your api key is wrong. It\'s not registered on PayLock'
            }, status=status.HTTP_404_NOT_FOUND)

        # check if amount requested for is more than the transaction amount

        try:
            transaction = Transaction.objects.get(api_transaction__transaction_id=transaction_id, api_transaction__stage='created')
        except Transaction.DoesNotExist:
            return Response({'status_code': status.HTTP_404_NOT_FOUND,
                    'message': 'Transaction not found. Check the status of the transaction for more details. ',
                    }, status=status.HTTP_404_NOT_FOUND,)
        try:
            escrow = EscrowWallet.objects.get(transaction__api_transaction__transaction_id=transaction.api_transaction.transaction_id, 
                transaction__stage='ongoing', is_active=True )
            # print(escrow)
        except EscrowWallet.DoesNotExist:
            return Response({'status_code': status.HTTP_404_NOT_FOUND,
                    'message': 'This transaction is invalid. There is no transaction in the escrow account.',
                    }, status=status.HTTP_404_NOT_FOUND)
        
        transfer = RequestFundsTransfer.objects.create(
                transaction = transaction,
                amount = transaction.amount,
                request_description = 'Request after delivery of product or service.',
                published_date = timezone.now(),
                requested_date = timezone.now()
            )
        future_date = transfer.requested_date + timedelta(days=(transfer.response_days_remaining))
        transfer.future_date = future_date
        transfer.save()
        # Update api transaction model
        api_transaction = ApiTransaction.objects.get(transaction_id = transaction_id)
        api_transaction.stage = 'funds requested'
        api_transaction.stage_description = 'Funds has been requested & pending.'
        api_transaction.save()
        send_release_funds_text_message_link_to_buyer(api_transaction.transaction_name, api_transaction.user.phone_number, api_transaction.transaction_id) # send this text message to the buyer of the item
        # Serialize object
        transfer_request = self.serializer_class(transfer)
        return Response({'status_code': status.HTTP_201_CREATED,
                    'message': 'Transfer request has been created and sent for confirmation and funds release.',
                    'result': {
                        'data': transfer_request.data
                    }
            }, status=status.HTTP_201_CREATED)

    @action(methods=('POST', ), detail=False, url_path='request-response', permission_classes=(AllowAny,))
    def respond_to_funds_request(self, request, *args, **kwargs):
        """ Accept funds request status to determine if approved or declined.
            Status values : approve or decline
        """
        transaction_id = request.data.get('transaction_id')
        api_key = request.data.get('api_key')
        request_status = request.data.get('request_status')
        if transaction_id == "" or not transaction_id:
            return Response({
                'status_code': status.HTTP_404_NOT_FOUND,
                'message': 'Transaction Id cannot be empty. Add your transaction id.'
                }, status=status.HTTP_404_NOT_FOUND)

        if api_key == "" or not api_key:
            return Response({
                'status_code': status.HTTP_404_NOT_FOUND,
                'message': 'API key cannot be empty. Add your api key.'
                }, status=status.HTTP_404_NOT_FOUND)

        if request_status == "" or not request_status:
            return Response({
                'status_code': status.HTTP_404_NOT_FOUND,
                'message': 'Request status field cannot be empty or have different value. '
                'Field value should be approve or decline.'
                }, status=status.HTTP_404_NOT_FOUND)

        try:
            api_settings = ApiIntegrationSetting.objects.get(token=api_key)
        except ApiIntegrationSetting.DoesNotExist:
            return Response({
                'status_code': status.HTTP_404_NOT_FOUND,
                'message': 'Your api key is wrong. It\'s not registered on PayLock'
            }, status=status.HTTP_404_NOT_FOUND)
        try:
            escrow_wallet = EscrowWallet.objects.get(transaction__api_transaction__transaction_id=transaction_id, 
                transaction__stage='ongoing', is_active=True, can_cancel=False, payin_agreed=True )
        except EscrowWallet.DoesNotExist:
            return Response({'status_code': status.HTTP_404_NOT_FOUND,
                    'message': 'This transaction is invalid. There is no transaction in the escrow account.',
                    }, status=status.HTTP_404_NOT_FOUND)

        try:
            funds_request = RequestFundsTransfer.objects.get(transaction__api_transaction__transaction_id=transaction_id, status='pending')
        except RequestFundsTransfer.DoesNotExist:
            funds_request = RequestFundsTransfer.objects.create(
                transaction = escrow_wallet.transaction,
                amount = escrow_wallet.amount,
                status='pending',
                request_description = 'Request after delivery of product or service',
                published_date = timezone.now(),
                requested_date = timezone.now()
            )
        try: # Get collaborators wallet. 
            collaborator_wallet = UserWallet.objects.get(
                    user=escrow_wallet.transaction.contractor,
                    is_active=True,
                    is_authorized=True
                    )           
        except UserWallet.DoesNotExist:
            return Response({
                'status_code': status.HTTP_404_NOT_FOUND,
                'message': 'You have no wallet. Contact admin to create your wallet'
            }, status=status.HTTP_404_NOT_FOUND)
        transaction = Transaction.objects.get(pk=funds_request.transaction.id)
        api_transaction = ApiTransaction.objects.get(transaction_id = transaction_id)
        if request_status == 'approve':
            funds_request_status = 'approved'
            # Debit the escrow wallet.
            escrow_wallet.amount -= Decimal(funds_request.amount)
            escrow_wallet.last_used = timezone.now()
            escrow_wallet.save()

            # Credit the receipient wallet
            collaborator_wallet.balance += Decimal(funds_request.amount) # Credit collaborators wallet with specified amount
            collaborator_wallet.last_used = timezone.now()
            collaborator_wallet.save()

            # Check if the escrow wallet is empty then change the status.
            if escrow_wallet.amount <= 0:
                escrow_wallet.payout_agreed = True
                escrow_wallet.is_active = False
                escrow_wallet.withdraw_date = timezone.now()
                escrow_wallet.withdraw_status = "Funds withdrawn"
                escrow_wallet.save() # check all fields when authorized to withdraw
                send_email_funds_has_been_approved(funds_request) # send email to buyer funds has been approved

                # Mark transaction as completed.
                transaction.stage = 'completed'
                transaction.amount -= Decimal(funds_request.amount) # Deduct the amount from the transaction amount
                # transaction.save()

                # Change status of the api transaction.
                # api_transaction = ApiTransaction.objects.get(transaction_id = transaction_id)
                api_transaction.stage='completed'
                api_transaction.stage_description = 'Funds have been released and Transaction is marked as completed.'
                # api_transaction.save()
                update_payment_history(
                    'Escrow funds moved to your wallet.',
                    'Funds approved and amount moved to your wallet.',
                    funds_request.amount,
                    'completed',
                    funds_request.transaction.user,
                    timezone.now(),
                    timezone.now()
                )
            # Send email to seller when payment is released.
            send_email_to_seller_money_has_been_released(api_transaction)
        elif request_status == 'decline':
            funds_request_status = 'declined'
            transaction.stage = 'in_dispute'
            # Update api transaction
            api_transaction.stage='in_dispute'
            api_transaction.stage_description = 'Funds have been declined and Transaction is marked as in dispute.'
                
        transaction.save()
        api_transaction.save()
        funds_request.status=funds_request_status
        funds_request.approver = escrow_wallet.sender
        funds_request.response_date = timezone.now()
        funds_request.response_days_remaining = 0
        funds_request.future_date = None
        funds_request.save()
        funds_status = self.serializer_class(funds_request)
        return Response({'status_code': status.HTTP_200_OK,
                        'message': 'Funds request has been {}.'.format(funds_request_status),
                        'result': {
                            'data': funds_status.data
                        }
        }, status=status.HTTP_200_OK)