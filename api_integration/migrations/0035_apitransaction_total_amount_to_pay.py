# Generated by Django 2.0.6 on 2019-10-23 18:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api_integration', '0034_auto_20191023_0406'),
    ]

    operations = [
        migrations.AddField(
            model_name='apitransaction',
            name='total_amount_to_pay',
            field=models.DecimalField(decimal_places=2, default=0.0, help_text='Total amount to pay for the transaction.', max_digits=7),
        ),
    ]
