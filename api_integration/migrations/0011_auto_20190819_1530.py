# Generated by Django 2.0.6 on 2019-08-19 15:30

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('api_integration', '0010_auto_20190816_1914'),
    ]

    operations = [
        migrations.AlterField(
            model_name='apiintegrationsetting',
            name='token',
            field=models.CharField(default=uuid.uuid4, max_length=65, unique=True),
        ),
    ]
