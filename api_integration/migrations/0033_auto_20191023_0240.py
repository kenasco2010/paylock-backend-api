# Generated by Django 2.0.6 on 2019-10-23 02:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api_integration', '0032_auto_20191023_0239'),
    ]

    operations = [
        migrations.AlterField(
            model_name='businessprofile',
            name='escrow_fees_payment_option',
            field=models.CharField(default='user', max_length=15),
        ),
    ]
