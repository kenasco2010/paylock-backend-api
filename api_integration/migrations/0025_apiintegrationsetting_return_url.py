# Generated by Django 2.0.6 on 2019-10-04 18:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api_integration', '0024_auto_20190909_2110'),
    ]

    operations = [
        migrations.AddField(
            model_name='apiintegrationsetting',
            name='return_url',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
    ]
