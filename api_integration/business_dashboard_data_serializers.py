from rest_framework import serializers
from .models import ApiTransaction
from user_profile.models import UserProfile

class BusinessCustomersSerializer(serializers.ModelSerializer):
    full_name = serializers.SerializerMethodField()
    user_email = serializers.SerializerMethodField()
    class Meta:
        model = ApiTransaction
        fields = ('full_name', 'user_email', 'amount', 'currency', 'stage', 'created_date')
    
    def get_full_name(self, transaction):
        return '{} {}'.format(transaction.user.user_profile.first_name, transaction.user.user_profile.last_name)

    def get_user_email(self, transaction):
        return '{}'.format(transaction.user.email)

# class BusinessMerchantsSerializer(serializers.ModelSerializer):
#     full_name = serializers.SerializerMethodField()
#     user_email = serializers.SerializerMethodField()
#     class Meta:
#         model = ApiTransaction
#         fields = ('full_name', 'user_email', 'created_date')
    
#     def get_full_name(self, transaction):
#         return '{} {}'.format(transaction.merchant.user_profile.first_name, transaction.merchant.user_profile.last_name)

#     def get_user_email(self, transaction):
#         return '{}'.format(transaction.merchant.email)

class UserProfileSerializer(serializers.ModelSerializer):
    user_email= serializers.ReadOnlyField(source='user.email')
    class Meta:
        model = UserProfile
        fields = ('id', 'first_name', 'last_name', 'user_email',
                'created_date')


class ApiTransactionsSerializer(serializers.ModelSerializer):

	class Meta:
		model = ApiTransaction
		fields = '__all__'