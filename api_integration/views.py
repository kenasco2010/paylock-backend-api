from django.shortcuts import render
from rest_framework import viewsets, status
from django.utils.crypto import get_random_string
from .models import ApiIntegrationSetting
from .serializers import ApiIntegrationSettingsSerializer
from rest_framework.status import (HTTP_201_CREATED,
                                   HTTP_200_OK,
                                   HTTP_400_BAD_REQUEST, HTTP_401_UNAUTHORIZED,
                                   HTTP_404_NOT_FOUND, HTTP_409_CONFLICT)
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.decorators import action
import secrets

# Create your views here.
def regenerate_api_token():
    unique_id = secrets.token_hex(30)
    return unique_id.upper()

class ApiIntegrationSettingsViewSet(viewsets.ModelViewSet):
    queryset = ApiIntegrationSetting.objects.all()
    serializer_class = ApiIntegrationSettingsSerializer
    permission_class = (IsAuthenticated,)

    @action(methods=('POST',), detail=False, url_path='generate-new-token', permission_class = (IsAuthenticated,))
    def add_new_token_and_settings(self, request, *args, **kwargs):
        """ Add new token and settings to be ready to start integrating
            PayLock api. 
        """
        user = request.user
        if ApiIntegrationSetting.objects.filter(user=user).exists():
            return Response({'status_code': status.HTTP_400_BAD_REQUEST,
                                'message': 'You already have a token associated with your developer account.'
                                }, status=HTTP_400_BAD_REQUEST)

        settings = ApiIntegrationSetting.objects.create(
            user = user
        )
        serialize_settings = self.serializer_class(settings)
        return Response({
            'status_code': HTTP_201_CREATED,
            'message': 'You have successfully generated your developer token.',
            'result': {
                'data': serialize_settings.data
            }
        }, status = status.HTTP_201_CREATED)

    @action(methods=('POST',), detail=False, url_path='regenerate-token', permission_class=(IsAuthenticated,))
    def replace_token(self, request, *args, **kwargs):
        """ Replace existing token
        
        """
        user = request.user
        old_token = request.data.get('old_token')
        try:
            settings = ApiIntegrationSetting.objects.get(user=user, token=old_token)
        except ApiIntegrationSetting.DoesNotExist:
            return Response({
            'status_code': HTTP_404_NOT_FOUND,
            'message': 'You do not have a developer token. '
            'Generate one before you can replace or old token has not been provided in the request.', 
            }, status = status.HTTP_404_NOT_FOUND)
    
        settings.token = regenerate_api_token()
        settings.save()
        serialize_settings = self.serializer_class(settings)
        return Response({
        'status_code': HTTP_201_CREATED,
        'message': 'You have successfully changed your developer token.',
        'result': {
            'data': serialize_settings.data
        }
    }, status = status.HTTP_201_CREATED)
    
    @action(methods=('POST',), detail=False, url_path='edit-settings', permission_class=(IsAuthenticated,))
    def edit_setting(self, request, *args, **kwargs):
        """ Add other details to api settings 
            Eg. Return_Url

        """
        return_url = request.data.get('return_url')
        api_usage_type = request.data.get('api_usage_type')
        user = request.user
        setting = ApiIntegrationSetting.objects.get(user=user)
        setting.return_url = return_url
        setting.api_usage_type = api_usage_type
        setting.save()
        return Response({
            'status_code': status.HTTP_200_OK,
            'message': 'You have successfully updated your settings.'
        })
