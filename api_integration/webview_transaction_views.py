from django.shortcuts import render
from rest_framework import viewsets, status
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.decorators import action
from rest_framework.authtoken.models import Token
from user_account.models import User, UserManager
from .models import ApiTransaction, ApiIntegrationSetting, BusinessProfile
from wallet.models import UserWallet, EscrowWallet
from transaction_invitation.models import TransactionInvitation
from request_funds_transfer.models import RequestFundsTransfer
from user_profile.models import UserProfile
from transaction_type.models import TransactionType
from .serializers import ApiTransactionsSerializer
from transaction.serializers import TransactionSerializer
from .signals import *
from datetime import date
import requests
import base64
from django.utils.encoding import force_bytes, force_text
from decimal import Decimal
from paylock_emails.signals import send_email_to_seller_about_payment, \
		send_email_to_buyer_about_payment, \
		send_email_to_seller_money_has_been_released
from payment.models import Payment
from user_account_setting.models import UserAccountSetting
from paylock.send_text import send_funds_request_text_message_link_to_seller, \
		send_release_funds_text_message_link_to_buyer
from payment.serializers import PaymentSerializer
from paylock.currency_conversion import convert_currency

def approve_transaction_invitation_status(main_transaction, api_transaction):
	#Accepts the transaction.
	invitation = TransactionInvitation.objects.get(transaction = main_transaction)
	invitation.status = 'approved'
	invitation.acceptor = api_transaction
	invitation.save()
	return invitation

def mark_api_tranaction_as_completed(transaction_id):
	api_transaction = ApiTransaction.objects.get(transaction_id=transaction_id)
	api_transaction.stage='completed'
	api_transaction.stage_description = 'Funds have been released and Transaction is marked as completed.'
	api_transaction.save()
	return api_transaction

def mark_api_tranaction_as_in_dispute(transaction_id):
	api_transaction = ApiTransaction.objects.get(transaction_id=transaction_id)
	api_transaction.stage='dispute'
	api_transaction.stage_description = 'Funds have been declined and Transaction is marked as in dispute.'
	api_transaction.save()
	return api_transaction

def update_recipient_wallet(recipient, amount):
	user_wallet = UserWallet.objects.get(user=recipient)
	user_wallet.balance += amount
	user_wallet.save()
	return user_wallet

def create_user_account_setting(user):
	UserAccountSetting.objects.create(
		user=user,
		general_promotional_emails=True,
		newsletter=True

	)
# Create your views here.
class ApiTransactionsViewSet(viewsets.ModelViewSet):
	""" ApiTransactionsViewSet
    
    """
	queryset = ApiTransaction.objects.all()
	serializer_class = ApiTransactionsSerializer
	permission_classes = (AllowAny,)

	
	@action(methods=('POST',), detail=False, url_path='initiate-transaction', permission_classes = (AllowAny,))
	def initiate_transaction(self, request, *args, **kwargs):
		""" Accept some details from a user to initiate the transaction
            through the api.
        
		"""
		token = request.data.get('api_key')
		email =  request.data.get('email') # used to check if buyer is on paylock or not.
		phone_number = request.data.get('phone_number')
		first_name = request.data.get('first_name')
		last_name = request.data.get('last_name')

		if not email:
			return Response({'status_code': status.HTTP_404_NOT_FOUND,
			'message': 'Please add client\'s  email.'
			}, status=status.HTTP_404_NOT_FOUND)
		elif not phone_number:
			return Response({'status_code': status.HTTP_404_NOT_FOUND,
			'message': 'Please add client\'s phone number '
			}, status=status.HTTP_404_NOT_FOUND)
		elif not first_name:
			return Response({'status_code': status.HTTP_404_NOT_FOUND,
			'message': 'Please add client\'s  first name.'
			}, status=status.HTTP_404_NOT_FOUND)
		elif not last_name:
			return Response({'status_code': status.HTTP_404_NOT_FOUND,
			'message': 'Please add client\'s  last name.'
			}, status=status.HTTP_404_NOT_FOUND)

		try: # Try getting a user/buyer by email if not create an account
			user = User.objects.get(email=email, phone_number=phone_number)
		except User.DoesNotExist:
			if User.objects.filter(phone_number=phone_number).exists():
				return Response({
					'status_code': status.HTTP_409_CONFLICT,
					'message': 'Phone number already exists. Please use a different number'
					}, status=status.HTTP_409_CONFLICT)
			elif User.objects.filter(email=email).exists():
				return Response({
					'status_code': status.HTTP_409_CONFLICT,
					'message': 'User email already exists. Please use a different email'
					}, status=status.HTTP_409_CONFLICT)
			else:
				user = User.objects.create( # If this doesn't work change to user signup endpoing, or add user profile to it.
					email=email,
					phone_number=phone_number
				)
				user.set_password(phone_number)
				user.is_active = True
				user.save()
				create_user_account_setting(user)
		
		try: # Get the user wallet and activate it.
			user_wallet = UserWallet.objects.get(user=user)
		except UserWallet.DoesNotExist:
			user_wallet = UserWallet.objects.create(
				user = user,
				backup_user = user,
				is_authorized= True,
				is_active = True,
				last_used = timezone.now()
			)
		try: # Get the user profile or create one.
			user_profile = UserProfile.objects.get(user=user)
		except UserProfile.DoesNotExist:
			user_profile = UserProfile.objects.create(
				user = user,
				first_name = first_name,
				last_name= last_name,
				date_of_birth=date.today()
			)

		try:
			api_setting = ApiIntegrationSetting.objects.get(token=token)
			business_profile = BusinessProfile.objects.get(api_token=api_setting)
		except ApiIntegrationSetting.DoesNotExist:
			return Response({
				'status_code': status.HTTP_404_NOT_FOUND,
				'message': 'This business is not authorized to receive payments with PayLock. API token maybe incorrect.'
			}, status= status.HTTP_404_NOT_FOUND)
		try:
			business_profile = BusinessProfile.objects.get(api_token=api_setting)
		except BusinessProfile.DoesNotExist:
			return Response({
				'status_code': status.HTTP_404_NOT_FOUND,
				'message': 'This token does\'t match any business profile on PayLock. Make sure your token is correct.'
			}, status= status.HTTP_404_NOT_FOUND)

		transaction_details = {
			'email': email,
			'phone_number': phone_number,
			'first_name': first_name,
			'last_name': last_name,
			'transaction_name': request.data.get('transaction_name'),
			'currency': business_profile.sales_currency,
			'escrow_fees_payment_option': business_profile.escrow_fees_payment_option,
			'description': request.data.get('description'),
			'amount': request.data.get('amount'),
			'user': user,
			'collaborator': api_setting.user,
		}
		
		if not transaction_details['last_name']:
			return Response({'status_code': status.HTTP_404_NOT_FOUND,
			'message': 'Please add client\'s  last name.'
			}, status=status.HTTP_404_NOT_FOUND)
		elif not transaction_details['transaction_name']:
			return Response({'status_code': status.HTTP_404_NOT_FOUND,
			'message': 'Add a transaction name'
			}, status=status.HTTP_404_NOT_FOUND)
		elif not transaction_details['amount']:
			return Response({'status_code': status.HTTP_404_NOT_FOUND,
			'message': 'Enter the amount of the transaction'
			}, status=status.HTTP_404_NOT_FOUND)
		elif not transaction_details['collaborator']:
			return Response({'status_code': status.HTTP_404_NOT_FOUND,
			'message': 'Your token has not been added to the request'
			}, status=status.HTTP_404_NOT_FOUND)
		
		if business_profile.sales_currency != "USD":
			usd_amount = convert_currency(transaction_details['currency'], transaction_details['amount'])
			app_fee = business_profile.escrow_charge * Decimal(transaction_details['amount'])
			usd_app_fee = convert_currency(transaction_details['currency'], app_fee)
			business_fee = business_profile.business_fee * transaction_details['amount']
			usd_business_fee = convert_currency(transaction_details['currency'], business_fee)
			print(usd_business_fee)
		else:
			usd_amount = transaction_details['amount']
			usd_app_fee = business_profile.escrow_charge * Decimal(transaction_details['amount'])
			usd_business_fee = business_profile.business_fee * transaction_details['amount']

		#Check if the transaction is escrow
		if business_profile.business_type == "marketplace":
			merchant_data ={
				"merchant_email": request.data.get('merchant_email'),
				"merchant_phone_number": request.data.get('merchant_phone_number'),
				"merchant_first_name": request.data.get('merchant_first_name'),
				"merchant_last_name": request.data.get('merchant_last_name')
			}
			
			if not merchant_data['merchant_email'] or merchant_data['merchant_email'] == "":
				return Response({'message': 'Merchant email cannot be empty.'})
			elif not merchant_data['merchant_phone_number'] or merchant_data['merchant_phone_number'] == "":
				return Response({'message': 'Merchant phone number cannot be empty.'})
			elif not merchant_data['merchant_first_name'] or merchant_data['merchant_first_name'] == "":
				return Response({'message': 'Merchant first name cannot be empty.'})
			elif not merchant_data['merchant_last_name'] or merchant_data['merchant_last_name'] == "":
				return Response({'message': 'Merchant last name cannot be empty.'})
			
			try: # Try getting a Merchant by email if not create an account
				merchant = User.objects.get(email=merchant_data['merchant_email'], phone_number=merchant_data['merchant_phone_number'])
			except User.DoesNotExist:
				if User.objects.filter(phone_number=merchant_data['merchant_phone_number']).exists():
					return Response({
						'status_code': status.HTTP_409_CONFLICT,
						'message': 'Merchant phone number already exists. Please use a different number'
						}, status=status.HTTP_409_CONFLICT)
				elif User.objects.filter(email=merchant_data['merchant_email']).exists():
					return Response({
						'status_code': status.HTTP_409_CONFLICT,
						'message': 'Merchant email already exists. Please use a different email'
						}, status=status.HTTP_409_CONFLICT)
				else:
					user = User.objects.create( # If this doesn't work change to user signup endpoing, or add user profile to it.
						email=merchant_data['merchant_email'],
						phone_number=merchant_data['merchant_phone_number']
					)
					user.set_password(merchant_data['merchant_phone_number'])
					user.is_active = True
					user.save()
					create_user_account_setting(merchant)
			
			try: # Get the merchant wallet or create wallet and activate it.
				user_wallet = UserWallet.objects.get(user=merchant)
			except UserWallet.DoesNotExist:
				user_wallet = UserWallet.objects.create(
					user = merchant,
					backup_user = merchant,
					is_authorized= True,
					is_active = True,
					last_used = timezone.now()
				)
			try: # Get the merchant profile or create merchant profile.
				user_profile = UserProfile.objects.get(user=merchant)
			except UserProfile.DoesNotExist:
				user_profile = UserProfile.objects.create(
					user = merchant,
					first_name = merchant_data['merchant_first_name'],
					last_name= merchant_data['merchant_last_name'],
					date_of_birth=date.today()
				)

			transaction = ApiTransaction.objects.create(
				**transaction_details,
				app_fee = business_profile.escrow_charge * Decimal(transaction_details['amount']), # How much PayLock is taking
				business_fee = business_profile.business_fee * transaction_details['amount'], # How much the business is taking.
				merchant = merchant,
				transaction_type = business_profile.business_type,
				usd_amount = usd_amount,
				usd_app_fee = usd_app_fee,
				usd_business_fee = usd_business_fee
				)
		else:
			transaction = ApiTransaction.objects.create(
				**transaction_details,
				app_fee = business_profile.escrow_charge * Decimal(transaction_details['amount']),
				business_fee = business_profile.business_fee * transaction_details['amount'],
				usd_amount = usd_amount,
				transaction_type = business_profile.business_type,
				usd_app_fee = usd_app_fee,
				usd_business_fee = usd_business_fee
				)
		total_fees = (transaction.app_fee + transaction.business_fee + transaction.amount) 
		processing_charge = Decimal(0.035) * total_fees
		transaction.payment_processing_fee = processing_charge
		transaction.total_amount_to_pay = total_fees + processing_charge
		# USD calculations.
		usd_total_amount = usd_amount + usd_app_fee + usd_business_fee
		usd_processing_charge = Decimal(0.035) * Decimal(usd_total_amount)
		transaction.usd_payment_processing_fee = usd_processing_charge
		transaction.usd_total_amount_to_pay = usd_processing_charge + Decimal(usd_total_amount)
		transaction.save()
		# This encoding is for webview url. 
		transactionid = base64.b64encode(force_bytes(transaction.transaction_id)).decode("utf-8") 
		owner_email = base64.b64encode(force_bytes(transaction.user)).decode("utf-8")
		collaborator_email = base64.b64encode(force_bytes(transaction.collaborator)).decode("utf-8")
		wallet_id = base64.b64encode(force_bytes(user_wallet.wallet_number)).decode("utf-8")

		serialized_api_transaction = self.serializer_class(transaction)
		#Check api usage type 
		if api_setting.api_usage_type == "endpoints":
			webview_redirect_url = ""
		else:
			webview_redirect_url = settings.DOMAIN_URL + 'webview/{}/payment/{}_{}/_&message=complete-payment/{}'.format(transactionid, 
				owner_email, collaborator_email, wallet_id),
		return Response({
			'status_code': status.HTTP_201_CREATED,
			'status': 'success',
			'message': 'You have successfully initiated a transaction.',
			'results': {
				'data': serialized_api_transaction.data,
				'api_usage_type': api_setting.api_usage_type,
				'webview_redirect_url': webview_redirect_url,
				'user_email': user.email,
				'wallet_id': user_wallet.wallet_number,
				'return_url': api_setting.return_url
				}
			}, status = status.HTTP_201_CREATED)
	

	@action(methods=('GET',), detail=False, url_path='redirected-page', permission_classes = (AllowAny,))
	def show_information_after_redirect(self, request, *args, **kwargs):
		""" Show transaction data, user data, wallet data after redirect_url is loaded in the browser.
			Query params: transaction_id, owner_email, collaborator_email, wallet_id
		"""
		transactionid = base64.b64decode(request.query_params.get('transaction_id')).decode("utf-8")
		wallet_id = base64.b64decode(request.query_params.get('wallet_id')).decode("utf-8")
		if not transactionid or not wallet_id:
			return Response({
				'status_code': status.HTTP_400_BAD_REQUEST,
				'message': 'Transaction Id or wallet Id not found in the request.'
			}, status=status.HTTP_400_BAD_REQUEST)
		try:
			wallet_info = UserWallet.objects.get(wallet_number=wallet_id)
		except UserWallet.DoesNotExist:
			return Response({
				'status_code': status.HTTP_409_CONFLICT,
				'message': 'Your wallet does not exist on PayLock. Log on to the dashboard to create a wallet',
				
				}, status = status.HTTP_409_CONFLICT)
		try:
			api_transaction = ApiTransaction.objects.get(transaction_id=transactionid, stage='initiated')
		except ApiTransaction.DoesNotExist:
			return Response({
				'status_code': status.HTTP_409_CONFLICT,
				'message': 'Transaction does not exist',
				
				}, status = status.HTTP_409_CONFLICT)
		user_token = Token.objects.get(user=api_transaction.user)
		profile = BusinessProfile.objects.get(user=api_transaction.collaborator)
		if profile.logo !='':
			business_logo = profile.logo.url
		else:
			business_logo = 'https://paylock-assets.s3-us-west-2.amazonaws.com/business_logo_placeholder.png'

		api_transaction_serializer = self.serializer_class(api_transaction)
		return Response({
			'status_code': status.HTTP_200_OK,
			'message': 'Transaction information on redirect page.',
			'result':{
				'transaction_data': api_transaction_serializer.data,
				'wallet_balance': wallet_info.balance,
				'business_name':profile.name,
				'business_logo': business_logo,
				'user_token': user_token.key
				}
			}, status = status.HTTP_200_OK)


	@action(methods=('GET',), detail=False, url_path='wallet-balance', permission_classes = (IsAuthenticated,))
	def wallet_balance(self, request, *args, **kwargs):
		""" This get function fetches the Wallet balance

		"""
		user = request.user
		try:

			wallet = UserWallet.objects.get(
				user=user
			)
		except UserWallet.DoesNotExist:
			return Response({
				'status_code': status.HTTP_404_NOT_FOUND,
				'message': 'Wallet does not exist. Please check your email'
			}, status = status.HTTP_404_NOT_FOUND)
		# wallet = UserWalletSerializer(payment)
		return Response({
				'status_code': status.HTTP_200_OK,
				'message': 'Your wallet balance.',
				'result': {
					'wallet_balance': wallet.balance
				}
			}, status=status.HTTP_200_OK)

	@action(methods=('GET',), detail=False, url_path='payment-webhook', permission_classes = (AllowAny,))
	def api_payment_webhook(self, request, *args, **kwargs):
		""" Function to determin the status of the payment processing using the api integration.

		"""
		transaction_reference = request.query_params.get('reference_number')
		try:
			payment = Payment.objects.get(
				txref=transaction_reference
			)
		except Payment.DoesNotExist:
			return Response({
				'status_code': status.HTTP_404_NOT_FOUND,
				'message': 'Payment processing details not available.'
			}, status = status.HTTP_404_NOT_FOUND)
		payment = PaymentSerializer(payment)
		return Response({
				'status_code': status.HTTP_200_OK,
				'message': 'Payment processing status from webhook.',
				'result': {
					'data': payment.data
				}
			}, status = status.HTTP_200_OK)
		

	@action(methods=('POST',), detail=False, url_path='create-transaction', permission_classes = (AllowAny,))
	def create_api_transaction(self, request, *args, **kwargs):
		""" Accept some details from a user to create a transaction
            through the api.
        
		"""
		transaction_id = request.data.get('transaction_id')
		email =  request.data.get('email')
		api_key = request.data.get('api_key')
		try:
			api_settings = ApiIntegrationSetting.objects.get(token=api_key)
		except ApiIntegrationSetting.DoesNotExist:
			return Response({
				'status_code': status.HTTP_404_NOT_FOUND,
				'message': 'Your api key is wrong. It\'s not registered on PayLock'
			}, status=status.HTTP_404_NOT_FOUND)

		try:
			business_profile = BusinessProfile.objects.get(api_token=api_settings.token)
		except BusinessProfile.DoesNotExist:
			return Response({
				'status_code': status.HTTP_404_NOT_FOUND,
				'message': 'Business profile is not registered on PayLock.'
			}, status=status.HTTP_404_NOT_FOUND)

		try:
			api_transaction = ApiTransaction.objects.get(transaction_id = transaction_id, stage='initiated')
		except ApiTransaction.DoesNotExist:
			return Response({'status_code': status.HTTP_404_NOT_FOUND,
				'message': 'This transaction is invalid. Please initiate again.'
				}, status = status.HTTP_404_NOT_FOUND)

		
		data = {
				'name': api_transaction.transaction_name,
				'api_transaction': api_transaction,
				'description': api_transaction.description,
				'transaction_role': 'owner',
				'escrow_fee_payment': 'user',
				'project_owner_agreement': True,
				'contractor_agreement': True,
				'stage': 'ongoing',
				'activity_type': 'business',
				'has_paid': True,
				'created_date': timezone.now(),
				'published_date': timezone.now()
			}
		if api_transaction.currency != 'USD':
			# Convert Amounnt
			usd_amount = convert_currency(api_transaction.currency, api_transaction.amount)
			# Convert app fee
			usd_app_fee = convert_currency(api_transaction.currency, api_transaction.app_fee)
			# Store converted values in variables
			initial_amount= Decimal(usd_amount)
			amount=Decimal(usd_amount)
			markup = Decimal(usd_app_fee)
		else: # Else keep the usd values in same variables.
			initial_amount= Decimal(api_transaction.amount)
			amount =Decimal(api_transaction.amount)
			markup =Decimal(api_transaction.app_fee)

		# Calculate and Charge the buyer the escrow fee
		escrow_fee = markup  / 2
		total_amount = amount + escrow_fee
		user_wallet = UserWallet.objects.get(user=api_transaction.user)
		if user_wallet.balance < total_amount:
			return Response({
				'status_code': status.HTTP_400_BAD_REQUEST,
				'message': 'You do not have enough money to complete this transaction.'
				' Kindly login to PayLock and top up your wallet or pay with your card.'
			}, status = status.HTTP_400_BAD_REQUEST)

		if business_profile.business_type == "marketplace":
			main_transaction = Transaction.objects.create(
			**data, initial_amount=initial_amount, amount=amount, 
			user=api_transaction.user, contractor=api_transaction.merchant, 
			markup=markup) # Creates the transaction

		elif business_profile.business_type == "e-commerce":
			main_transaction = Transaction.objects.create(
			**data, initial_amount=initial_amount, amount=amount, 
			user=api_transaction.user, contractor=api_transaction.collaborator, 
			markup=markup) # Creates the transaction

		try:
			escrow = EscrowWallet.objects.get(transaction__id=main_transaction.id, 
			transaction__contractor=main_transaction.contractor, is_active=True)
		except EscrowWallet.DoesNotExist:
			return Response({'status_code': status.HTTP_404_NOT_FOUND,
					'message': 'This transaction is invalid. It does not have any amount attached to it.',
					}, status = status.HTTP_404_NOT_FOUND) 

		user_wallet.balance -= Decimal(total_amount)
		user_wallet.save()

		# Charge the seller the escrow fee
		main_transaction_amount = Decimal(main_transaction.amount) - Decimal(escrow_fee)
		main_transaction.amount = main_transaction_amount
		main_transaction.save()

		# Approve / Accepts the transaction invitation automatically once created. 
		approve_transaction_invitation_status(main_transaction, api_transaction.collaborator)
		
		# Update the escrow wallet
		escrow.can_cancel = False
		escrow.amount = main_transaction.amount
		escrow.last_used = timezone.now()
		escrow.save()
		buyer_full_name = api_transaction.first_name + ' ' + api_transaction.last_name, 
		# Send funds request message link to seller's email (seller_email, transaction)
		send_email_to_seller_about_payment(api_transaction)
		send_funds_request_text_message_link_to_seller(buyer_full_name, api_transaction.collaborator.phone_number, api_transaction.transaction_id)
		# Send url to seller to approve if Item has been delivered then the funds request can be created

		send_email_to_buyer_about_payment(api_transaction)
		# send url to release funds to email and text message.
		api_transaction.stage='created'
		api_transaction.save()
		main_transaction = TransactionSerializer(main_transaction)
		return Response({'status_code': status.HTTP_201_CREATED,
				'message': 'Your transaction is has been created. The item has been paid into the escrow account.'
				' Your seller will be notified about this transaction.',
				'result': {
					'data': main_transaction.data
				}
				}, status=status.HTTP_201_CREATED )
		
	@action(methods=('POST',), detail=False, url_path='request-funds', permission_classes = (AllowAny,))
	def request_for_funds(self, request, *args, **kwargs):
		""" Request for funds through api when the item is delivered For WEBVIEW
        
		"""
		transaction_id = request.data.get('transaction_id')
		try:
			api_transaction = ApiTransaction.objects.get(transaction_id=transaction_id, stage='created')
		except ApiTransaction.DoesNotExist:
			return Response({
				'status_code': status.HTTP_404_NOT_FOUND,
				'message': 'This transaction is not currently active to request for funds',
			}, status = status.HTTP_404_NOT_FOUND)
		main_transaction = Transaction.objects.get(api_transaction=api_transaction)
		try:
			escrow = EscrowWallet.objects.get(transaction=main_transaction, is_active=True,
				can_cancel=False)
		except EscrowWallet.DoesNotExist:
			return Response({
				'status_code': status.HTTP_404_NOT_FOUND,
				'message': 'This transaction is in an invalid state. Contact PayLock to rectify this.',
			}, status=status.HTTP_404_NOT_FOUND)
		funds = RequestFundsTransfer.objects.create(
				transaction=main_transaction,
				status = 'pending',
				amount = escrow.amount,
				request_description = 'Request for funds after sending item or providing service',
				published_date = timezone.now(),
				requested_date = timezone.now()
			)
		# Buyer's phone number is user.phone_number
		send_release_funds_text_message_link_to_buyer(api_transaction.transaction_name, api_transaction.user.phone_number, api_transaction.transaction_id) # send this text message to the buyer of the item
		#Update api transaction stage
		api_transaction.stage='funds_requested'
		api_transaction.save()

		return Response({
			'status_code': status.HTTP_201_CREATED,
			'message': 'Your request has been sent to the buyer to complete the transaction. '
			'You will be notified once the buyer responds to the transaction.'
		}, status=status.HTTP_201_CREATED)
	
	@action(methods=('POST',), detail=False, url_path='release-funds', permission_classes = (AllowAny,))
	def release_funds(self, request, *args, **kwargs):
		""" Releasing of funds by the buyer when the item is delivered.
        
		"""
		transaction_id = request.data.get('transaction_id')
		try:
			main_transaction = Transaction.objects.get(api_transaction__transaction_id=transaction_id, api_transaction__stage='funds_requested')
		except Transaction.DoesNotExist:
			Response({'status_code': status.HTTP_404_NOT_FOUND,
						'message': 'No transaction found.'
		}, status=status.HTTP_404_NOT_FOUND)

		try:
			escrow_wallet = EscrowWallet.objects.get(transaction=main_transaction, is_active=True,
				can_cancel=False)
		except EscrowWallet.DoesNotExist:
			return Response({'status_code': status.HTTP_404_NOT_FOUND,
						'message': 'The escrow funds on this transaction is either inactive or has been withdrawn. ' 
						'Contact PayLock admin for more information'
		}, status=status.HTTP_404_NOT_FOUND)

		try:
			funds = RequestFundsTransfer.objects.get(transaction__api_transaction__transaction_id=transaction_id, status='pending')
		except RequestFundsTransfer.DoesNotExist:
			funds = RequestFundsTransfer.objects.create(
					transaction=main_transaction,
					status = 'approved',
					amount = escrow_wallet.amount,
					request_description = 'Request for funds after sending item or providing service',
					published_date = timezone.now(),
					requested_date = timezone.now()
				)
		user = funds.transaction.user
		# Update status of the funds request.
		funds.status='approved'
		funds.approver = user
		funds.response_date = timezone.now()
		funds.response_days_remaining = 0
		funds.future_date = None
		funds.save()

		# Update the recipients wallet.
		update_recipient_wallet(escrow_wallet.recipient,  escrow_wallet.amount)

		# Update the escrow wallet for that transaction.
		escrow_wallet.amount -= Decimal(funds.amount)
		escrow_wallet.last_used = timezone.now()
		escrow_wallet.save()
		escrow_wallet.payout_agreed = True
		escrow_wallet.is_active = False
		escrow_wallet.withdraw_date = timezone.now()
		escrow_wallet.withdraw_status = "Funds withdrawn"
		escrow_wallet.save() # check all fields when authorized to withdraw
		
		

		# Mark transaction as completed.
		main_transaction.stage = 'completed'
		main_transaction.save()

		
		# Update api transaction. Mark it as completed.
		api_transaction = mark_api_tranaction_as_completed(transaction_id)

		# Send email to seller when payment is released.
		send_email_to_seller_money_has_been_released(api_transaction)
		return Response({'status_code': status.HTTP_200_OK,
						'message': 'You have released the funds and will be deposited into seller\'s account.'
		}, status=status.HTTP_200_OK)


	@action(methods=('POST',), detail=False, url_path='retain-funds', permission_classes = (AllowAny,))
	def disapprove_funds_release(self, request, *args, **kwargs):
		""" Function to hold funds when the item is not delivered or has an issue.
        
		"""
		funds_id = request.data.get('funds_id')
		transaction_id = request.data.get('transaction_id')
		try:
			funds = RequestFundsTransfer.objects.get(id=funds_id, status='pending')
			user = funds.transaction.user
		except RequestFundsTransfer.DoesNotExist:
			return Response({
				'status_code': status.HTTP_404_NOT_FOUND,
				'message': 'This funds request does not exist.',
			}, status=status.HTTP_404_NOT_FOUND)
		
		try: #Try getting the escrow transaction created by the transaction owner
			escrow_wallet = EscrowWallet.objects.get(
				transaction=funds.transaction.id,
				recipient=funds.transaction.contractor,
				is_active=True,
				can_cancel=False,
				payout_agreed=False
				)
		except EscrowWallet.DoesNotExist:
			return Response({'status_code': status.HTTP_404_NOT_FOUND,
						'message': 'No money was deposited in the escrow '
						'account for you. Kindly contact the transaction '
						'owner'
		}, status=status.HTTP_404_NOT_FOUND)
		# Update the escrow wallet for that transaction.

		funds.status='decline'
		funds.approver = user
		funds.response_date = timezone.now()
		funds.response_days_remaining = 0
		funds.future_date = None
		funds.save()

		# Mark transaction as declined and in dispute.
		transaction = Transaction.objects.get(id=funds.transaction.id)
		transaction.stage = 'dispute'
		transaction.save()

		# Update api transaction to reflect dispute.
		mark_api_tranaction_as_in_dispute(transaction)
		
		return Response({'status_code': status.HTTP_200_OK,
						'message': 'Your transaction has gone into dispute, try resolving with the buyer and '
						'authorize for funds to be approved. '
		}, status=status.HTTP_200_OK)

	
	@action(methods=('GET',), detail=False, url_path='get-transaction-details', permission_classes = (AllowAny,))
	def show_transaction_details(self, request, *args, **kwargs):
		""" Function to show tranasction details when redirected to release funds.
        
		"""
		transaction_id = request.query_params.get('transaction_id')
		try:
			transaction = ApiTransaction.objects.get(transaction_id=transaction_id)
		except ApiTransaction.DoesNotExist:
			return Response({'status_code': status.HTTP_404_NOT_FOUND,
						'message': 'Transaction not found.'
		}, status=status.HTTP_404_NOT_FOUND)
		business= BusinessProfile.objects.get(user=transaction.collaborator)
		if business.logo !='':
			business_logo = business.logo.url
		else:
			business_logo = 'https://paylock-assets.s3-us-west-2.amazonaws.com/business_logo_placeholder.png'

		serialize_transaction = self.serializer_class(transaction)
		return Response({
				'status_code': status.HTTP_200_OK,
				'message': 'Transaction details.',
				'result': {
					'data': serialize_transaction.data,
					'merchant_name': business.name,
					'merchant_email': business.user.email,
					'business_logo': business_logo
				}
		}, status=status.HTTP_200_OK)

	@action(methods=('GET',), detail=False, url_path='transaction-status', permission_classes = (AllowAny,))
	def show_transaction_status(self, request, *args, **kwargs):
		""" Function to show tranasction status.
        
		"""
		api_key = request.data.get('api_key')
		transaction_id = request.data.get('transaction_id')

		if api_key == "" or not api_key:
			return Response({'message': 'Please add your api key.'})

		if transaction_id == "" or not transaction_id:
			return Response({'message': 'Transaction ID cannot be empty'})

		try:
			api_settings = ApiIntegrationSetting.objects.get(token=api_key)
		except ApiIntegrationSetting.DoesNotExist:
			return Response({
				'status_code': status.HTTP_404_NOT_FOUND,
				'message': 'Your api key is wrong. It\'s not registered on PayLock'
			}, status=status.HTTP_404_NOT_FOUND)

		try:
			transaction = ApiTransaction.objects.get(transaction_id=transaction_id)
		except ApiTransaction.DoesNotExist:
			return Response({
				'status_code': status.HTTP_404_NOT_FOUND,
				'message': 'Transaction not found. Check the transaction id to make sure it\'s correct'
			}, status=status.HTTP_404_NOT_FOUND)
        
		serialize_transaction = self.serializer_class(transaction)
		return Response({
				"status_code": status.HTTP_200_OK,
               	"data": serialize_transaction.data      
                }, status=status.HTTP_200_OK)