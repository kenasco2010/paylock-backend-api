from django.db import models
from django.utils.crypto import get_random_string
from django.conf import settings
from django.utils import timezone
from imagekit.models import ProcessedImageField
from datetime import date
import uuid
import secrets
from paylock.choices import PLATFORM_CURRENCY 
# from transaction.models import Transaction
from transaction_type.models import TransactionType
AUTH_USER_MODEL = getattr(settings, 'AUTH_USER_MODEL', 'auth.User')
# Create your models here.

def generate_api_token():
    unique_id = secrets.token_hex(30)
    return unique_id

def generate_transaction_id():
    transaction_id = secrets.token_hex(10)
    return transaction_id

class ApiIntegrationSetting(models.Model):
    token = models.CharField(
        max_length=65,
        unique=True,
        default=generate_api_token
    )
    user = models.OneToOneField(
        AUTH_USER_MODEL, 
        related_name='api_settings_user', 
        null=True, 
        on_delete=models.SET_NULL
        )
    api_usage_type = models.CharField(
        max_length=20,
        default='endpoints',
        )
    return_url = models.CharField(max_length=200,
        blank=True,
        )
    created_date = models.DateTimeField(default=timezone.now)
    modified = models.DateTimeField(auto_now=True)
    def __str__(self):
        return '{}'.format(self.token) or u"API Settings"

    class Meta:
        verbose_name_plural = u"API settings" or " "



class BusinessProfile(models.Model):
    api_token = models.ForeignKey(
        ApiIntegrationSetting, 
        related_name='api_settings_token',
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        help_text='The api token generated in the settings.'
        )
    name = models.CharField(max_length=100)
    product_name = models.CharField(
    	max_length=100,
    	blank=True)
    business_type = models.CharField(
        max_length=50,
        default='e-commerce')
    brief_description = models.TextField(
    	help_text="Give us a brief description \
    	about your company"
    	)
    website_url = models.CharField(
    	max_length=100
    	)
    current_address = models.CharField(
    	max_length=200
    	)
    country = models.CharField(
    	max_length=50
    	)
    sales_currency = models.CharField(
        max_length=5,
        default='GHS',
        help_text='The currency you are sell your products or services.'
    )
    escrow_charge = models.DecimalField(
        max_digits=7, 
        decimal_places=2, 
        default=0.01,
        help_text='Percentage PayLock charges on this account for each transaction.')
    business_fee = models.DecimalField(
        max_digits=7, 
        decimal_places=2, 
        default=0.00,
        help_text='Percentage merchant charges for transaction by the platform owners.')
    escrow_fees_payment_option = models.CharField(
        max_length=15,
        default = 'user'
    )
    logo = ProcessedImageField(upload_to='business_profile_image',
                                        format='JPEG',
                                        options={'quality': 30},
                                        null=True, blank=True)
    user = models.OneToOneField(AUTH_USER_MODEL,  on_delete=models.CASCADE,
    	related_name='business_profile_user')
    profile_completed = models.BooleanField(default=False)
    created_date = models.DateTimeField(default=timezone.now)
    modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{}'.format(self.name)

    class Meta:
        verbose_name_plural = 'Business Profile'

class ApiTransaction(models.Model):
    transaction_id = models.CharField(
        max_length=50,
        default=generate_transaction_id
        )
    email = models.CharField(max_length=100)
    phone_number = models.CharField(max_length=100)
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    transaction_name = models.CharField(max_length=500)
    transaction_type = models.CharField(
        max_length=50,
        default='e-commerce',
        help_text='The type of transaction from the api. This is determined by the business.'
        )
    description =models.CharField(
        max_length=500,
        blank=True)
    amount =  models.DecimalField(max_digits=7, decimal_places=2, 
        default=0.00)
    currency = models.CharField(max_length=100)
    
    app_fee = models.DecimalField(max_digits=7, decimal_places=2, 
        default=0.00)
    business_fee = models.DecimalField(max_digits=7, decimal_places=2, 
        default=0.00)
    payment_processing_fee = models.DecimalField(
        max_digits=7, decimal_places=2, 
        default=0.00,
        help_text='Payment processing charges from our payment provider in local currency'
    )
    total_amount_to_pay = models.DecimalField(
        max_digits=7, decimal_places=2, 
        default=0.00,
        help_text='Total amount to pay for the transaction.'
    )
    escrow_fees_payment_option = models.CharField(
        max_length=15, # user, both, business
        help_text='Who is paying the escrow fee'
    )
    usd_amount = models.DecimalField(
        max_digits=7, decimal_places=2, 
        default=0.00,
        help_text='USD value of the transaction amount. '
    )
    usd_app_fee = models.DecimalField(max_digits=7, decimal_places=2, 
        default=0.00,
        help_text='USD value of the app fee'
        )
    usd_business_fee = models.DecimalField(
        max_digits=7, decimal_places=2, 
        default=0.00,
        help_text='USD value of the business fee'
        )
    
    usd_payment_processing_fee = models.DecimalField(
        max_digits=7, decimal_places=2, 
        default=0.00,
        help_text='Payment processing charges from our payment provider in USD'
    )
    usd_total_amount_to_pay = models.DecimalField(
        max_digits=7, decimal_places=2, 
        default=0.00,
        help_text='USD value of the total amount to pay'
    )
    stage = models.CharField(
        max_length=20,
        default='initiated',
        help_text='Shows the status of the transaction.'
        )
    stage_description = models.CharField(
        max_length=200,
        default='Transaction has been initiated and pending payment.',
        help_text='Shows a vivid stage or status description of the transaction.'
        )
    collaborator = models.ForeignKey(
        AUTH_USER_MODEL, related_name='collaborator',
        on_delete=models.CASCADE,
        help_text='The business providing the platform'
        )
    merchant = models.ForeignKey(
        AUTH_USER_MODEL, related_name='merchant',
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        help_text='The person listing the product / service if market place.'
        )
    user = models.ForeignKey(
        AUTH_USER_MODEL, related_name='user',
        on_delete=models.CASCADE
        )
    created_date = models.DateTimeField(default=timezone.now)
    modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{}'.format(self.transaction_name)

    class Meta:
        verbose_name_plural = 'API Transactions'
