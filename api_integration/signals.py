from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver
from django.utils import timezone
from django.conf import settings
from transaction.models import Transaction
from .models import ApiTransaction
from wallet.models import EscrowWallet

# @receiver(post_save, sender=ApiTransaction)
# def move_money_to_escrow_wallet_after_transaction(instance, created=False, **kwargs):
#     """ After the api integration transaction model is created, 
#         Create the transaction on the main transaction model.
#     """
#     if created:
#         transaction = Transaction.objects.create(
#             name=instance.transaction_name,
#             description=instance.description,
#             transaction_role='collaborator',
#             initial_amount=instance.amount,
#             amount=instance.amount,
#             escrow_fee_payment='both',
#             markup=instance.app_fee,
#             user=instance.user,
#             contractor=instance.collaborator,
#             project_owner_agreement = True,
#             contractor_agreement = True,
#             stage = 'ongoing',
#             activity_type = 'business',
#             has_paid = True,
#             created_date= timezone.now()
#         )
#         escrow = EscrowWallet.objects.get(
#             transaction = transaction.id
#         )
#         escrow.amount = transaction.amount
#         escrow.recipient = transaction.contractor
#         escrow.can_cancel = False
#         escrow.last_used =timezone.now()
#         escrow.save()