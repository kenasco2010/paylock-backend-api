from django.shortcuts import render
from rest_framework import viewsets, status
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.decorators import action
from .models import BusinessProfile, ApiIntegrationSetting
from .serializers import BusinessProfileSerializer
# Create your views here.

class BusinessProfileViewSet(viewsets.ModelViewSet):
	""" BusinessProfileViewSet
    
    """
	queryset = BusinessProfile.objects.all()
	serializer_class = BusinessProfileSerializer
	permission_class = (IsAuthenticated,)

	@action(methods=('POST',), detail=False, url_path='create', permission_class = (IsAuthenticated,))
	def create_business_profile(self, request, *args, **kwargs):
		""" Complete your business profile on PayLock.

		"""
		user = request.user
		try:
			api_token = ApiIntegrationSetting.objects.get(token=request.data.get('api_token'))
		except ApiIntegrationSetting.DoesNotExist:
			return Response({
                    'status_code': status.HTTP_404_NOT_FOUND,
                    'message': 'Api token not found. Generate api token before completing your profile.'
                }, status=status.HTTP_404_NOT_FOUND)
		profile_detail = {
			'api_token': api_token,
			'name': request.data.get('name'),
			'business_type': request.data.get('business_type'),
			'brief_description': request.data.get('brief_description'),
			'website_url': request.data.get('website_url'),
			'current_address': request.data.get('current_address'),
			'country': request.data.get('country'),
			'sales_currency':request.data.get('sales_currency'),
			'escrow_fees_payment_option': request.data.get('escrow_fees_payment_option'),
			'logo': request.FILES.get('logo'),
			'profile_completed': True,
			'user': user
		}
		
		if BusinessProfile.objects.filter(user=user).exists():
			return Response({
				'status_code': status.HTTP_400_BAD_REQUEST,
				'message': 'Your business profile already exists.',
			}, status=status.HTTP_400_BAD_REQUEST)
		else:
			profile = BusinessProfile.objects.create(
				**profile_detail
			)
			user.account_type = 'both'
			user.save()
			serialize_profile = self.serializer_class(profile)
			return Response({
					'status_code': status.HTTP_201_CREATED,
					'message': 'You have successfully added your business profile.',
					'result': {
						'data': serialize_profile.data
					}
				}, status=status.HTTP_201_CREATED)
		

	@action(methods=('POST',), detail=False, url_path='edit', permission_class = (IsAuthenticated,))
	def edit_business_profile(self, request, *args, **kwargs):
		""" Edit your business profile on PayLock.

		"""
		user = request.user
		try:
			profile = BusinessProfile.objects.get(user=user)
		except BusinessProfile.DoesNotExist:
			return Response({
                'status_code': status.HTTP_404_NOT_FOUND,
                'message': 'You do not have a Business profile on PayLock'
            },status=status.HTTP_404_NOT_FOUND)
		fields = ['name', 'business_type', 'brief_description', 'wesbite_url', 'current_address', 'country', 'sales_currency']
		for key in fields:
			setattr(profile, key, request.data.get(key))
			profile.save()
		return Response({
				'status_code': status.HTTP_200_OK,
				'message': 'Your business profile has been edited.'
			},status=status.HTTP_200_OK)
	

	@action(methods=('POST',), detail=False, url_path='change-logo', permission_class = (IsAuthenticated,))
	def change_profile_logo(self, request, *args, **kwargs):
		""" Edit your business profile logo.

		"""
		user = request.user
		try:
			profile = BusinessProfile.objects.get(user=user)
		except BusinessProfile.DoesNotExist:
			return Response({
                'status_code': status.HTTP_404_NOT_FOUND,
                'message': 'You do not have a Business profile on PayLock'
            },status=status.HTTP_404_NOT_FOUND)
		profile.logo = request.FILES.get('logo')
		profile.save()
		return Response({
                'status_code': status.HTTP_200_OK,
                'message': 'Business profile logo changed successfully.'
            },status=status.HTTP_200_OK)

	@action(methods=('GET',), detail=False, url_path='show', permission_class = (IsAuthenticated,))
	def get_business_profile(self, request, *args, **kwargs):
		""" Get your business profile.

		"""
		user = request.user
		try:
			profile = BusinessProfile.objects.get(user=user)
		except BusinessProfile.DoesNotExist:
			return Response({
                'status_code': status.HTTP_404_NOT_FOUND,
                'message': 'You do not have a Business profile on PayLock'
            },status=status.HTTP_404_NOT_FOUND)
		serialize_profile = self.serializer_class(profile)
		return Response({
				'status_code': status.HTTP_200_OK,
				'message': 'Your business profile information.',
				'result': {
					'data': serialize_profile.data
				}
			}, status=status.HTTP_200_OK)

