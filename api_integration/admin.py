from django.contrib import admin
from .models import ApiIntegrationSetting, BusinessProfile, ApiTransaction
# Register your models here.

class ApiIntegrationSettingsAdmin(admin.ModelAdmin):
    list_display =('token', 'created_date', 'user', 'created_date')

class BusinessProfileAdmin(admin.ModelAdmin):
    list_display = ('name', 'product_name', 'escrow_fees_payment_option', 'website_url', 'user', 'created_date',)

class ApiTransactionAdmin(admin.ModelAdmin):
    list_display = ('transaction_id', 'user', 'phone_number', 'transaction_name', 'stage', 'currency', 'collaborator', 'created_date')
    

admin.site.register(ApiIntegrationSetting, ApiIntegrationSettingsAdmin)
admin.site.register(BusinessProfile, BusinessProfileAdmin)
admin.site.register(ApiTransaction, ApiTransactionAdmin)