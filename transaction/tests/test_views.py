from django.test import TestCase, RequestFactory, Client
from django.contrib.auth import get_user_model
from django.utils import timezone

from rest_framework.authtoken.models import Token
from rest_framework.test import APIRequestFactory
from rest_framework.test import APIClient
from rest_framework.test import force_authenticate
import json

from transaction_type.models import TransactionType
from payment_method.models import PaymentMethod
from transaction.models import Transaction
from user_profile.models import UserProfile
from transaction.serializers import TransactionSerializer

User = get_user_model()
# Create your tests here.


class TransactionTest(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.credentials = {
            'email': 'kenmartey89@gmail.com',
            'phone_number': '00233246424340',
            'password': 'ceremony',
        }
        self.contractor = {
            'email': 'dacodemonk@gmail.com',
            'phone_number': '00233246424341',
            'password': 'ceremony',
        }
        self.payment_method_input = {
            'payment_type':'Mobile Money',
            'created_date': timezone.now()
        }
        self.transaction_type_input = {
            'transaction_type': 'Project',
            'created_date': timezone.now()
        }
        self.user_profile = {
            'first_name': 'Kennedy',
            'last_name': 'Anyinatoe',
            'date_of_birth': '1989-07-09',
            'current_address': 'East Legon',
            'country': 'Ghana',
            'profile_image': ''

        }
        self.contractor_profile = {
            'first_name': 'Dacode',
            'last_name': 'Monk',
            'date_of_birth': '1989-07-01',
            'current_address': 'East Legon',
            'country': 'Ghana',
            'profile_image': ''

        }
        self.user = User.objects.create_superuser(**self.credentials)
        self.contractor = User.objects.create_superuser(**self.contractor)
        self.user_profile = UserProfile.objects.create(**self.user_profile, 
            user=self.user)
        self.contractor_profile = UserProfile.objects.create(**self.contractor_profile, 
        user=self.contractor)
        self.payment_method = PaymentMethod.objects.create(**self.payment_method_input, 
        user=self.user)
        self.transaction_type = TransactionType.objects.create(**self.transaction_type_input, 
        user=self.user)
        self.transaction_info = {
        'name': 'Nike shoes',
        'transaction_type': self.transaction_type.id,
        'description': 'A pair of Nike shoes from Jumia',
        'payment_method': self.payment_method.id,
        'currency': 'GHS',
        'amount': '900',
        'contractor': self.contractor.email,
        'contractor_agreement': False,
        'project_owner_agreement': True,
        'created_date': timezone.now()
        }
    
        self.no_transaction_name = {
        'name': '',
        'transaction_type': self.transaction_type.id,
        'description': 'A pair of Nike shoes from Jumia',
        'payment_method': self.payment_method.id,
        'currency': 'GHS',
        'amount': '900',
        'contractor': self.contractor.email,
        'contractor_agreement': False,
        'project_owner_agreement': True,
        'created_date': timezone.now()
        }

        self.no_transaction_description = {
        'name': 'Nike shoes',
        'transaction_type': self.transaction_type.id,
        'description': '',
        'payment_method': self.payment_method.id,
        'currency': 'GHS',
        'amount': '900',
        'contractor': self.contractor.email,
        'contractor_agreement': False,
        'project_owner_agreement': True,
        'created_date': timezone.now()
        }
        self.user = get_user_model().objects.get(
            email='kenmartey89@gmail.com'
            )
    
        self.transaction_info_update = {
        'contractor_agreement': True,
        'created_date': timezone.now()
        }
        
    def test_create_transaction(self, **kwargs):
        client = APIClient()
        headers = {
            'HTTP_AUTHORIZATION': 'Token {}'.format(
                self.user.auth_token)
        }
        
        amount = 900
        markup = float(0.04) * float(amount)
        request = self.client.post('/api/v1.0/transactions/create-transaction/', 
        {'name':'New website project', 'transaction_type': self.transaction_type.id, 'description':'Create a new website for Jumia store',
        'payment_method':self.payment_method.id, 'currency': 'GHS', 'amount': amount, 'markup': markup, 'contractor':self.contractor.email,
        'contractor_agreement':False, 'project_owner_agreement':True, 'created_date':timezone.now() },
        format='json', **headers)
        print ('===================CREATE PROJECT TRANSACTION=========================')
        print ('>>> >>> >>> /CREATE PROJECT TRANSACTION/ content', request.content)
        self.assertEqual(request.status_code, 201)
        print ('===================END OF CREATE PROJECT TRANSACTION=========================')
        print ('      ')


    def test_empty_product_name_transaction(self, **kwargs):
        client = APIClient()
        headers = {
            'HTTP_AUTHORIZATION': 'Token {}'.format(
                self.user.auth_token)
        }
        request = self.client.post('/api/v1.0/transactions/create-transaction/',
        self.no_transaction_name, format='json', **headers)
        print ('===================TEST EMPTY PRODUCT NAME=========================')
        print ('>>> >>> >>> /EMPTY PRODUCT NAME/ content', request.content)
        self.assertEqual(request.status_code, 404)
        print ('===================END OF TEST EMPTY PRODUCT NAME=========================')
        print ('      ')

    def test_empty_product_description_transaction(self, **kwargs):
        client = APIClient()
        headers = {
            'HTTP_AUTHORIZATION': 'Token {}'.format(
                self.user.auth_token)
        }
        request = self.client.post('/api/v1.0/transactions/create-transaction/',
        self.no_transaction_description, format='json', **headers)
        print ('===================TEST EMPTY PRODUCT DESCRIPTION=========================')
        print ('>>> >>> >>> /EMPTY PRODUCT DESCRIPTION/ content', request.content)
        self.assertEqual(request.status_code, 404)
        print ('===================END OF TEST EMPTY PRODUCT DESCRIPTION=========================')
        print ('      ')

    def test_update_product_transaction(self, **kwargs):
        client = APIClient()
        headers = {
            'HTTP_AUTHORIZATION': 'Token {}'.format(self.user.auth_token)
        }

        request = self.client.post('/api/v1.0/transactions/create-transaction/',
        self.transaction_info, format='json', **headers)
        transaction_id = request.data['result']['data']['id']
        request = self.client.post('/api/v1.0/transactions/'+ str(transaction_id) 
            +'/update-transaction/', self.transaction_info_update, 
            format='json', **headers
            )
        print ('===================UPDATED PAYMENT METHOD=========================')
        print ('>>> >>> >>> /UPDATE PAYMENT METHOD TEST/ content', request.content)
        self.assertEqual(request.status_code, 201)
        print ('===================END UPDATED PAYMENT METHOD TEST=========================')
        print ('      ')

