from rest_framework import serializers
from .models import Transaction
from milestone.serializers import MilestoneSerializer
from import_export.serializers import ImportExportSerializer
class TransactionSerializer(serializers.ModelSerializer):
    milestones = MilestoneSerializer(many=True)
    import_exports = ImportExportSerializer()
    class Meta:
        model = Transaction
        fields = ('id', 'name', 'transaction_type_name', 'freelance_project_type', 'description', 
        'payment_method_type', 'transaction_role', 'escrow_fee_payment', 'initial_amount', 'amount', 
        'markup', 'stage', 'is_archived', 
        'collaborator_profile_id', 'collaborator_fullname', 'collaborator_profile_image', 
        'project_owner_agreement', 'contractor_agreement', 'contract_document', 'user_profile_id', 
        'user_fullname', 'published_date', 'created_date', 'milestones', 'import_exports')


class DashboardRecentTransactionSerializer(serializers.ModelSerializer):
    transaction_type = serializers.SerializerMethodField()
    class Meta:
        model = Transaction
        fields = ('id', 'name', 'transaction_type', 'amount', 'stage', 'published_date', 
        'created_date')

    def get_transaction_type(self, transaction):
        return '{}'.format(transaction.transaction_type)


# class TransactionNotificationSerializer(serializers.ModelSerializer):
#     invitation_id = serializers.SerializerMethodField()
#     class Meta:
#         model = Transaction
#         fields = ('id', 'name', 'transaction_type_name', 'description', 
#         'payment_method_type','initial_amount', 'amount', 'markup', 'stage', 'is_archived', 'collaborator_profile_id', 'collaborator_fullname', 
#         'collaborator_profile_image', 'project_owner_agreement', 'contractor_agreement', 
#         'contract_document', 'user_profile_id', 'user_fullname', 'published_date', 
#         'created_date')

#     def get_invitation_id(self, )