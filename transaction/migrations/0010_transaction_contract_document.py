# Generated by Django 2.0.6 on 2018-08-10 11:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('transaction', '0009_auto_20180724_1030'),
    ]

    operations = [
        migrations.AddField(
            model_name='transaction',
            name='contract_document',
            field=models.FileField(blank=True, null=True, upload_to='contract_documents'),
        ),
    ]
