# Generated by Django 2.0.6 on 2019-05-20 17:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('transaction', '0019_transaction_initial_amount'),
    ]

    operations = [
        migrations.AddField(
            model_name='transaction',
            name='escrow_fee_payment',
            field=models.CharField(choices=[('owner', 'Owner'), ('collaborator', 'Collaborator'), ('both', 'Both')], default='owner', max_length=20),
        ),
        migrations.AddField(
            model_name='transaction',
            name='freelance_project_type',
            field=models.CharField(choices=[('one_time', 'One time project'), ('milestone', 'Milestone project')], default='milestone', max_length=20),
        ),
        migrations.AddField(
            model_name='transaction',
            name='transaction_role',
            field=models.CharField(choices=[('owner', 'Owner'), ('collaborator', 'Collaborator')], default='owner', max_length=15),
        ),
    ]
