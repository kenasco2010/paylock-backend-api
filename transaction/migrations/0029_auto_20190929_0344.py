# Generated by Django 2.0.6 on 2019-09-29 03:44

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('transaction', '0028_transaction_api_transaction'),
    ]

    operations = [
        migrations.AlterField(
            model_name='transaction',
            name='api_transaction',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='api_transactions', to='api_integration.ApiTransaction'),
        ),
    ]
