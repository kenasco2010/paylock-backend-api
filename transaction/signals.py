from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver
from django.utils import timezone
from django.conf import settings
from paylock.send_text import send_text_message_about_transaction
from wallet.models import EscrowWallet
from .models import Transaction
# from transaction.views import markup_value
from payment.models import Payment
from wallet import signals
import urllib
import requests



# @receiver(pre_save, sender=Transaction)
# def calculate_escrow_fee_for_collaborator(sender, instance, **kwargs):
#     print(">>>>>>>>>>>>> The  code passed through the signal")
#     if instance.escrow_fee_payment == 'collaborator':
#         print(">>>>>>>>>>>>> The user chose collaborator")
#         balance = float(instance.user.user_wallets.balance) - float(instance.amount)
#         instance.user.user_wallets.balance = balance
#         instance.user.user_wallets.save()
#         # deduct the fee from the amount 
#         markup = markup_value(instance.amount, instance.user)
#         print(">>>>>>>>>>>>> Check how much was calculated as markup")

#         instance.markup = markup
#         balance = float(instance.amount) - markup
#         transaction.amount = balance

@receiver(post_save, sender=Transaction)
def move_money_to_escrow_wallet_after_transaction(instance, created=False, **kwargs):
    """ Move money to PayLock Escrow wallet when a transaction is created
        Payment Object is also created to record the transaction.
        Text message is then sent to collaborator about the transaction.
    """
    if created and instance.activity_type == "personal":
        if created and instance.transaction_role == "owner":
            EscrowWallet.objects.create(
                transaction=instance,
                sender=instance.user,
                recipient=instance.contractor,
                amount=instance.amount,
                balance=instance.markup,
                date_created=timezone.now(),
                transfer_date=timezone.now()
            )
            Payment.objects.create(
                payment_type="wallet to escrow transaction",
                description=instance.name,
                amount=instance.amount,
                payment_status='completed',
                user=instance.user,
                published_date=timezone.now()
            )
            # SEND TEXT MESSAGE TO COLLABORATOR AFTER TRANSACTION IS CREATED IN HIS NAME.
            send_text_message_about_transaction(instance)
        elif created and instance.transaction_role == "collaborator":
            EscrowWallet.objects.create(
                transaction=instance,
                sender=instance.user,
                recipient=instance.contractor,
                amount=0.00,
                balance=instance.markup,
                date_created=timezone.now(),
                transfer_date=timezone.now()
            )
            Payment.objects.create(
                payment_type="wallet to escrow transaction",
                description=instance.name,
                amount=instance.amount,
                payment_status='outstanding',
                user=instance.user,
                published_date=timezone.now()
            )
            # SEND TEXT MESSAGE TO COLLABORATOR AFTER TRANSACTION IS CREATED IN HIS NAME.
            send_text_message_about_transaction(instance)
    else:
        # requests.post('https://2ca7dc43.ngrok.io', {'The code passed here. first. Let me just monitor': 'value'})
        Payment.objects.create(
                payment_type="wallet to escrow transaction",
                description=instance.name,
                amount=instance.amount,
                actual_amount=instance.amount,
                payment_status='completed',
                user=instance.user,
                activity_type= 'business',
                published_date=timezone.now()
            )
        send_text_message_about_transaction(instance)