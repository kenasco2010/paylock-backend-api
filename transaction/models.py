from django.db import models
from django.conf import settings
from django.utils import timezone
from django.dispatch import receiver
from django.db.models.signals import pre_save
from payment_method.models import PaymentMethod
from django.contrib.contenttypes.fields import GenericRelation
from dashboard_notification.models import Notification
# from wallet.models import UserWallet
from transaction_type.models import TransactionType
from api_integration.models import ApiTransaction
from decimal import *
from paylock.choices import TRANSACTION_STAGE, FREELANCE_PROJECT_TYPE, \
    TRANSACTION_ROLE, ESCROW_FEE_PAYMENT
AUTH_USER_MODEL = getattr(settings, 'AUTH_USER_MODEL', 'auth.User')

# Create your models here.

class Transaction(models.Model):
    name = models.CharField(max_length=200)
    transaction_type = models.ForeignKey(TransactionType,
        related_name='transactions', 
        on_delete=models.CASCADE,
        null=True,
        blank=True)
    api_transaction = models.ForeignKey(
        ApiTransaction, 
        related_name='api_transactions',
        on_delete=models.SET_NULL,
        null=True,
        blank=True
        )
    freelance_project_type = models.CharField(
        max_length=20,
        choices=FREELANCE_PROJECT_TYPE,
        default='milestone'
    )
    description = models.TextField()
    payment_method = models.ForeignKey(
        PaymentMethod, related_name='transaction',
        on_delete=models.CASCADE,
        blank=True,
        null=True
        )
    transaction_role = models.CharField(
        help_text='The role you play. Whether transaction owner or collaborator',
        max_length=15,
        choices=TRANSACTION_ROLE,
        default='owner'
    )
    initial_amount = models.DecimalField(max_digits=7, decimal_places=2)
    amount = models.DecimalField(max_digits=7, decimal_places=2, 
        default=0)
    escrow_fee_payment = models.CharField(
        help_text ='Who pays for the escrow fee',
        max_length=20,
        choices=ESCROW_FEE_PAYMENT,
        default='owner'
    )
    markup = models.DecimalField(
        help_text = 'Percentage of the transaction as escrow fee',
        max_digits=7, 
        decimal_places=2,
        default=0
        )
    contractor = models.ForeignKey(
        AUTH_USER_MODEL, related_name='transaction',
        on_delete=models.SET_NULL,
        blank = True,
        null=True,
        help_text = 'The Person you are transacting the business with, who will be receiving the payment'
        )
    project_owner_agreement = models.BooleanField(
        default=False
        )
    contractor_agreement = models.BooleanField(
        default=False
    )
    contract_document = models.FileField(upload_to='contract_documents',
        blank=True, null=True
        )
    stage = models.CharField(
        max_length=20, 
        choices=TRANSACTION_STAGE, 
        default='initialized'
    )
    is_archived = models.BooleanField(
        default=False
    )
    activity_type = models.CharField(
        max_length=20,
        default = 'personal'
    )
    user = models.ForeignKey(
        AUTH_USER_MODEL, related_name='transactions',
        on_delete=models.CASCADE,
        help_text = 'The person always making the payment for the  item or service'
        )
    has_paid = models.BooleanField(
        default=False
    )
    notification = GenericRelation(Notification, related_query_name="transactions")
    published_date = models.DateTimeField(blank=True, null=True)
    created_date = models.DateTimeField(default=timezone.now)
    modified = models.DateTimeField(auto_now=True)

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __unicode__(self):
        return '%s' % (self.name)

    def __str__(self):
        return '%s' % (self.name)

    def get_contractor(self):
        return '{} {}'.format(
            self.contractor.user_profile.first_name,
            self.contractor.user_profile.last_name)

    @property
    def user_profile_id(self):
        return '{}'.format(
            self.user.user_profile.id
            )

    @property
    def user_fullname(self):
        return '{} {}'.format(
            self.user.user_profile.first_name,
            self.user.user_profile.last_name
            )

    @property
    def collaborator_profile_id(self):
        return '{}'.format(
            self.contractor.user_profile.id
            )

    @property
    def collaborator_fullname(self):
        return '{} {}'.format(
            self.contractor.user_profile.first_name,
            self.contractor.user_profile.last_name
            )

    @property
    def transaction_type_name(self):
        return '{}'.format(self.transaction_type.transaction_type)
    
    @property
    def payment_method_type(self):
        return '{}'.format(self.payment_method.payment_type)

    @property
    def collaborator_profile_image(self):
        return '{}'.format(self.contractor.user_profile.profile_image)


# @receiver(pre_save, sender=Transaction)
# def calculate_markup(sender, instance, created=False, *args, **kwargs):
#     if created:
#         """Calculate the markup amount before saving transaction."""
#         print(">>>>>>>>>>>>>>> Get pricing from instance.", instance.user.get_pricing)
#         markup_percentage = float(instance.user.get_pricing)
#         new_float = float(instance.amount)
#         instance.markup = new_float * markup_percentage

    
