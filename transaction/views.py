from django.shortcuts import render
from django.utils import timezone
from django.db.models import Sum
from django.db.models import Q
from django.dispatch import receiver
from django.core.exceptions import ObjectDoesNotExist
from rest_framework.status import (HTTP_201_CREATED, HTTP_200_OK, HTTP_400_BAD_REQUEST, 
                                    HTTP_401_UNAUTHORIZED, HTTP_403_FORBIDDEN,
                                    HTTP_404_NOT_FOUND, HTTP_409_CONFLICT)
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework import viewsets
from rest_framework import status
from rest_framework.decorators import detail_route, action, \
                    list_route, permission_classes, api_view
from rest_framework.pagination import PageNumberPagination
from user_account.models import User, UserManager
from paylock.permissions import IsSuperUserPermission, IsOwner
from transaction_type.models import TransactionType
from payment_method.models import PaymentMethod
from transaction_invitation.models import TransactionInvitation
from .models import Transaction
from wallet.models import UserWallet, EscrowWallet
from wallet.serializers import UserWalletSerializer
from user_profile.models import UserProfile
from identity_verification.models import IdentityVerification
from request_funds_transfer.models import RequestFundsTransfer
from .serializers import TransactionSerializer, DashboardRecentTransactionSerializer
from reverse_transaction.serializers import ReverseTransactionListSerializer
from paylock_emails import signals
from transaction import signals
from transaction_invitation import views
from dashboard_notification.views import *
from dashboard_notification.signals import *
from django.db.models import Count
from django.core.mail import send_mail
from django.db.models.functions import ExtractMonth
from decimal import *
from user_account_setting.views import UserAccountSettingViewSet

# Create your views here.
account_settings = UserAccountSettingViewSet()
def total_transaction_amount(amount, user):
    pricing = user.get_pricing
    if amount <= 10000:
        markup = float(amount) * pricing
        total_amount = markup + float(amount)
    elif amount > 10000  and amount <= 20000:
        markup = float(amount) * 0.025
        total_amount = markup + float(amount)
    return total_amount


def markup_value(amount, user):
    """ We get the user pricing 
        and multiply by the amount 
        to get the escrow / markup 
        value.
    """
    pricing = user.get_pricing
    if float(amount) <= 10000: 
        markup = float(amount) * pricing
    elif float(amount) > 10000  and float(amount) <= 20000:
        markup = float(amount) * 0.025
    return markup

def escrow_payment_option(escrow_fee_payment, amount, user):
    """ This function defines who pays the escrow
        fees and based on that we either make one
        person pay fully or divide it by two.
        If payment is by onwer, escrow fee 100%
        If payment is by both, escrow fees is 50%
    """
    initial_markup_value = markup_value(amount, user)
    if escrow_fee_payment == 'owner':
        markup = initial_markup_value
    elif escrow_fee_payment == 'both':
        markup = initial_markup_value / 2
        # markup = float(amount) - markup_fees
    elif escrow_fee_payment == 'collaborator':
        markup =  initial_markup_value
    return markup

# override pagination class
class StandardResultsSetPagination(PageNumberPagination):
    page_size = 5
    page_size_query_param = 'page_size'

class TransactionViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated, )
    serializer_class = TransactionSerializer
    queryset = Transaction.objects.all().order_by('-id')
    pagination_class = StandardResultsSetPagination

    @list_route(methods=('POST', ), url_path='create-transaction', 
    permission_classes =(IsAuthenticated, ))
    def create_transaction(self, request, *args, **kwargs):
        """Create Transaction"""

        user = request.user
        contractor_email = request.data.get('contractor')
        try:
            transaction_type = TransactionType.objects.get(pk=request.data.get("transaction_type"))
        except TransactionType.DoesNotExist:
            return Response({
                'status_code': HTTP_404_NOT_FOUND,
                'message': 'Transaction type does not exist. Contact PayLock Admin to add your transaction type'
            }, status=status.HTTP_404_NOT_FOUND)
       

        # If user(contractor is not on the platform)
        try:
            contractor = User.objects.get(email=contractor_email)
        except User.DoesNotExist:
            return Response({'status_code': status.HTTP_404_NOT_FOUND,
                'message': 'Your collaborator doesn\'t have an account on PayLock'
                },status=status.HTTP_404_NOT_FOUND)

        try:
            verified = IdentityVerification.objects.get(user=user, verification_status=True)
        except IdentityVerification.DoesNotExist:
            return Response({
                'status_code': status.HTTP_403_FORBIDDEN,
                'message': 'You haven\'t verified your account. Verify your Identity by going to manage account on the left to proceed with the transaction. ' 
            }, status=status.HTTP_403_FORBIDDEN)

        # Error while creating transaction with same owner email address
        contractor = User.objects.get(email=request.data.get('contractor'))
        if contractor == request.user:
            return Response({'status_code': status.HTTP_400_BAD_REQUEST,
                'message': 'You can not make a transaction with your own email.'
                ' Please select another contractor email address'
                }, status=status.HTTP_400_BAD_REQUEST)
        try:
            my_wallet = UserWallet.objects.get(user=user)
        except UserWallet.DoesNotExist:
            return Response({'status_code': status.HTTP_400_BAD_REQUEST,
                'message': 'You do not have a wallet, Kindly contact admin to create a wallet for you',
                }, status=status.HTTP_400_BAD_REQUEST)
        transaction_amount = request.data.get('amount')
        transaction_detail = {
            'name': request.data.get('name'),
            'transaction_type': transaction_type,
            'freelance_project_type': request.data.get('freelance_project_type'),
            'description': request.data.get('description'),
            'transaction_role': request.data.get('transaction_role'),
            'initial_amount': request.data.get('amount'),
            'escrow_fee_payment': request.data.get('escrow_fee_payment'),
            'contractor': contractor, #the person incharge of doing the job. This is to send an invite to the person
            'project_owner_agreement': request.data.get('project_owner_agreement'),
            'contractor_agreement': False,
            'stage': 'initialized',
            'is_archived': False,
            'contract_document': request.FILES.get('contract_document'),
            'user': user,
            'published_date': timezone.now()
        }
            # Custom validations ### Refactor this code!
        if not transaction_detail['name']:
            return Response({'status_code': status.HTTP_404_NOT_FOUND,
            'message': 'Please enter a name for the transaction'
            }, status=status.HTTP_404_NOT_FOUND)
        elif not transaction_detail['transaction_type']:
            return Response({'status_code': status.HTTP_404_NOT_FOUND,
            'message': 'Please select a transaction type'
            }, status=status.HTTP_404_NOT_FOUND)
        elif not transaction_detail['description']:
            return Response({'status_code': status.HTTP_404_NOT_FOUND,
            'message': 'Please enter a description for the transaction,'\
            'please make sure it\'s descriptive enough'
            }, status=status.HTTP_404_NOT_FOUND)
        elif not transaction_amount:
            return Response({'status_code': status.HTTP_404_NOT_FOUND,
            'message': 'Please add an amount'
            }, status=status.HTTP_404_NOT_FOUND)


        if transaction_detail['transaction_role'] == 'owner':  
            if my_wallet.balance <= total_transaction_amount(
                float(transaction_amount), user):          
                return Response({'status_code': status.HTTP_404_NOT_FOUND,
                'message': 'The amount in your wallet is very low for this transaction. '
                'Kindly top up your wallet with your card.',
                }, status=status.HTTP_404_NOT_FOUND)

            if transaction_detail['escrow_fee_payment'] == 'owner':
                transaction = Transaction.objects.create(
                    **transaction_detail, 
                        markup=escrow_payment_option(
                        transaction_detail['escrow_fee_payment'], transaction_amount, user),
                        amount=transaction_amount
                        )
                total_amount = float(transaction.amount) + float(transaction.markup)
                balance = float(my_wallet.balance) - total_amount
                my_wallet.balance = balance
                my_wallet.save()
            elif transaction_detail['escrow_fee_payment'] == 'both':
                transaction = Transaction.objects.create(
                    **transaction_detail, 
                        markup=escrow_payment_option(
                        transaction_detail['escrow_fee_payment'], transaction_amount, user),
                        amount = float(transaction_amount) - escrow_payment_option(
                        transaction_detail['escrow_fee_payment'], float(transaction_amount), user),
                        )
                    
                # deduct the initial transaction amount and then deduct the markup which is half of the markup.
                calc_transaction_amount = float(my_wallet.balance) - float(transaction.initial_amount)
                my_wallet.balance = calc_transaction_amount - float(transaction.markup)
                my_wallet.save()
            elif transaction_detail['escrow_fee_payment'] == 'collaborator':
                transaction = Transaction.objects.create(
                    **transaction_detail, 
                        markup=escrow_payment_option(
                        transaction_detail['escrow_fee_payment'], transaction_amount, user),
                        amount = float(transaction_amount)
                    )
                transaction_amount = float(my_wallet.balance) - float(transaction.initial_amount)
                my_wallet.balance = transaction_amount
                my_wallet.save()
            user.change_plan
            transaction = self.serializer_class(transaction)
            return Response({'status_code': status.HTTP_201_CREATED,
                    'message': 'Thank you for creating this transaction. '\
                    'Kindly wait for your collaborator to accept your transaction.',
                    'result': {
                        'data': transaction.data
                        }
                    }, status=status.HTTP_201_CREATED)
        elif transaction_detail['transaction_role'] == 'collaborator':
            if transaction_detail['escrow_fee_payment'] == 'owner':
                # Save the escrow fee and save the transaction.
                transaction = Transaction.objects.create(
                    **transaction_detail, 
                        markup=escrow_payment_option(
                        transaction_detail['escrow_fee_payment'], transaction_amount, user),
                        amount = float(transaction_amount) + escrow_payment_option(
                        transaction_detail['escrow_fee_payment'], float(transaction_amount), user)
                    )
            elif transaction_detail['escrow_fee_payment'] == 'both':
                # check if collaborator has escrow amount in wallet to deduct the escrow fee
                escrow_fee = escrow_payment_option(transaction_detail['escrow_fee_payment'], transaction_amount, user)
                if my_wallet.balance < escrow_fee:
                    return Response({
                        'status_code': HTTP_400_BAD_REQUEST,
                        'message': 'You don\'t have enough funds to pay the escrow fees, Top up your wallet to create this transaction.'
                    })
                else:
                    my_wallet.balance -= Decimal(escrow_fee)
                    my_wallet.last_used = timezone.now()
                    my_wallet.save()
                transaction = Transaction.objects.create(
                     **transaction_detail, 
                        markup=escrow_payment_option(
                        transaction_detail['escrow_fee_payment'], transaction_amount, user),
                        amount = float(transaction_amount) + escrow_payment_option(
                        transaction_detail['escrow_fee_payment'], float(transaction_amount), user)
                    )
                               
            elif transaction_detail['escrow_fee_payment'] == 'collaborator':
                transaction = Transaction.objects.create(
                    **transaction_detail, 
                        markup=escrow_payment_option(
                        transaction_detail['escrow_fee_payment'], transaction_amount, user),
                        amount = float(transaction_amount)
                        )
                balance = float(my_wallet.balance) -  float(transaction.markup)
                my_wallet.balance = balance
                my_wallet.save()
        user.change_plan
        transaction = self.serializer_class(transaction)
        return Response({'status_code': status.HTTP_201_CREATED,
            'message': 'Thank you for creating this transaction. '\
            'Kindly wait for your collaborator to accept and pay for the transaction.',
            'result': {
                'data': transaction.data
                }
            }, status=status.HTTP_201_CREATED)
    

    @detail_route(methods=('POST', ), url_path='update-transaction', 
        permission_classes =(IsAuthenticated, ))
    def update_transaction(self, request, pk=None, *args, **kwargs):
        """ Update Transaction
        
        """
        transaction = self.get_object()
        transaction.contractor_agreement = request.data.get('contractor_agreement')
        transaction.save()
        transaction = self.serializer_class(transaction)
        return Response({'status_code': status.HTTP_201_CREATED,
            'message': 'You have updated this transaction',
            'result': {
                'data': transaction.data
            }
        }, status=status.HTTP_201_CREATED)


    @action(methods=('GET',), detail=False, url_path='dropdown-transaction-list', 
        permission_classes=(IsAuthenticated,))
    def dropdown_transaction_list(self, request, *args, **kwargs):
        user = request.user 
        transactions = Transaction.objects.filter(user=user, is_archived=False).order_by('-id')
        transactions = self.serializer_class(transactions, many=True)
        return Response({'status_code': HTTP_200_OK,
            'message': 'List of transactions for reverse transaction dropdownlist',
                'result': {
                    'data': transactions.data
                    }
                })
    
    # Active transactions on paylock for transaction owner
    @action(methods=('GET',), detail=False, url_path='owner-active-transactions', 
        permission_classes=(IsAuthenticated,))
    def owner_active_transactions(self, request, *args, **kwargs):
        """ List of transactions for the transaction owner
        
        """
        user = request.user
        transaction = EscrowWallet.objects.filter(transaction__user=user, 
            is_active=True, can_cancel=False, transaction__is_archived=False, sender=user)
        transactions = ReverseTransactionListSerializer(transaction, many=True)
        return Response({'status_code': HTTP_200_OK,
            'message': 'List of active transactions on PayLock',
                'result': {
                    'data': transactions.data
                    }
                    }, status=status.HTTP_200_OK)
    
     # Active transactions on paylock for collaborator
    @action(methods=('GET',), detail=False, url_path='collaborator-active-transactions', 
        permission_classes=(IsAuthenticated,))
    def collaborator_active_transactions(self, request, *args, **kwargs):
        """ List of transactions for the collaborator
        
        """
        user = request.user
        transaction = EscrowWallet.objects.filter(transaction__contractor=user, 
            is_active=True, can_cancel=False, transaction__is_archived=False, sender=user)
        transactions = ReverseTransactionListSerializer(transaction, many=True)
        return Response({'status_code': HTTP_200_OK,
            'message': 'List of active transactions on PayLock',
                'result': {
                    'data': transactions.data
                    }
                    }, status=status.HTTP_200_OK)


class DashboardSummaryViewSet(viewsets.ViewSet):
    
     # DASHBORD SUMMARY
    @list_route(methods=('GET',), url_path='summary')
    def total_transactions_created(self,request, *args, **kwargs):
        """Get dashboard summary for project owner"""
        user = request.user
        transactions = Transaction.objects.all()
        transaction_invitation = TransactionInvitation.objects.all()
        # fundrequests = RequestFundsTransfer.objects.all()
        account_settings.mark_new_user_as_false(request) # this functions mark the user as returning user
        try:
            wallet_balance = UserWallet.objects.get(user=user)
            wallet_balance = UserWalletSerializer(wallet_balance)
        except ObjectDoesNotExist:
            return Response({'status_code': HTTP_404_NOT_FOUND,
                    'message': "You don't have a wallet associated with your account" 
                })
        try:
            fund_requests = RequestFundsTransfer.objects.all()
        except EscrowWallet.RequestFundsTransfer:
            return Response({'status_code': HTTP_404_NOT_FOUND,
                    'message': "No funds have been requested." 
                })

        try:
            ongoing_transaction = EscrowWallet.objects.all()
        except EscrowWallet.DoesNotExist:
            return Response({'status_code': HTTP_404_NOT_FOUND,
                    'message': "No transaction reserved in the escrow wallet." 
                })
        return Response({'status_code': status.HTTP_200_OK,
                        'message': 'Dashboard home summary',
                        'result': {
                            'data': {
                                'no_of_transactions': transactions.filter(user=user).count(),
                                'transactions_joined': transaction_invitation.filter(
                                    transaction__contractor=user, status='approved').count(),
                                'funds_to_approve': fund_requests.filter(transaction__user=user, 
                                    status='pending').count(),
                                'approved_funds_to_contractor': fund_requests.filter(transaction__contractor=user,
                                    status='approved').count(),
                                # total amount a transaction owner has spent on transactions
                                'total_amount_spent_by_transaction_owner': fund_requests.filter(
                                    transaction__user=user).aggregate(Sum('amount')),
                                # Total amount of money received as a contractor on PayLock from all transactions
                                # you have been added to.
                                'total_amount_received_by_contractor': fund_requests.filter(
                                    transaction__contractor=user).aggregate(Sum('amount')),
                                'wallet_balance': wallet_balance.data,

                                # # Transaction ownwer
                                # 'amount_spent_on_transaction': fund_requests.filter(
                                #     transaction__user=user,status='approved'
                                #     ).aggregate(Sum('amount')),

                                # How much transaction owner currently has in Escrow on PayLock
                                'owner_money_in_paylockEscrow': ongoing_transaction.filter(
                                    sender=user, can_cancel=False, is_active=True
                                    ).aggregate(Sum('amount')),

                                # Collaborator
                                # 'amount_received_on_transaction': ongoing_transaction.filter(
                                #     recipient=user, is_active=False).aggregate(Sum('amount')),

                                # How muhc a collaborator has in his/her name in PayLock Escrow
                                'collaborator_money_available_in_paylockEscrow':ongoing_transaction.filter(
                                    recipient=user, is_active=True, 
                                    can_cancel=False).aggregate(Sum('amount'))
                            }
                        }
            })
    
    @action(methods=('GET',), detail=False, url_path='graph-data', 
        permission_classes=(IsAuthenticated,))
    def get_graph_data(self, request, *args, **kwargs):
        user = request.user
        # transaction = Transaction.objects.filter().extra(
        #     {'month':"Extract(month from created)"}
        #     ).values_list('created_date').annotate(Count('id'))
        user_transaction = Transaction.objects.annotate(
            month=ExtractMonth('created_date')
            ).values('month').annotate(
                count=Count('id')
                ).values('month', 'count').filter(user=user)

        collaborator_transaction = Transaction.objects.annotate(
            month=ExtractMonth('created_date')
            ).values('month').annotate(
                count=Count('id')
                ).values('month', 'count').filter(contractor=user, contractor_agreement=True )

        return Response({'status_code': HTTP_200_OK,
                    'message': "Transaction summary data for graph",
                    'result': {
                        'user_data': user_transaction,
                        'collaborator_data': collaborator_transaction

                    }
                        # 'month': transactions.
                    })
                    # transactions.data,

    @action(methods=('GET',), detail=False, url_path='recent-transactions', 
        permission_classes=(IsAuthenticated,))
    def recent_transactions(self, request, *args, **kwargs):
        """ These are the recent transactions on PayLock 
            on the dashboard home beneath the graph.
        
        """
        user = request.user
        transactions = Transaction.objects.filter(
            Q(user=user, is_archived=False) |
            Q(contractor=user, is_archived=False)
            ).order_by('-id')[:5]
        serialize = DashboardRecentTransactionSerializer(transactions, many=True)
        return Response({'status_code': HTTP_200_OK,
                    'message': "Your recent transactions on PayLock",
                    'result': {
                        'transactions': serialize.data
                        }
                    }, status=status.HTTP_200_OK)

