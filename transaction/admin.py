from django.contrib import admin
from .models import Transaction
# Register your models here.

class TransactionAdmin(admin.ModelAdmin):
    list_display = ('name', 'amount','markup',
        'project_owner_agreement', 'contractor_agreement', 'stage',
        'has_paid', 'created_date'
        )

admin.site.register(Transaction, TransactionAdmin)