from django.conf.urls import url
from django.conf.urls import include
from django.urls import path
from rest_framework.routers import DefaultRouter
from .views import TransactionViewSet, DashboardSummaryViewSet

router = DefaultRouter()
router.register(r'v1.0/transactions', TransactionViewSet)
router.register(r'v1.0/dashboard', DashboardSummaryViewSet, base_name='dashboard')


urlpatterns = [
    path(r'', include(router.urls))
]